// package: fcp.dictionary.v1.dictionary_private
// file: v1/dictionary_private/dictionaries_md.proto

import * as v1_dictionary_private_dictionaries_md_pb from "../../v1/dictionary_private/dictionaries_md_pb";
import * as v1_dictionary_private_database_pb from "../../v1/dictionary_private/database_pb";
import * as v1_dictionary_md_active_substance_pb from "../../v1/dictionary_md/active_substance_pb";
import * as v1_dictionary_md_active_substance_unit_pb from "../../v1/dictionary_md/active_substance_unit_pb";
import * as v1_dictionary_md_application_method_pb from "../../v1/dictionary_md/application_method_pb";
import * as v1_dictionary_md_application_rate_unit_chemical_pb from "../../v1/dictionary_md/application_rate_unit_chemical_pb";
import * as v1_dictionary_md_brand_pb from "../../v1/dictionary_md/brand_pb";
import * as v1_dictionary_md_category_pb from "../../v1/dictionary_md/category_pb";
import * as v1_dictionary_md_category_group_pb from "../../v1/dictionary_md/category_group_pb";
import * as v1_dictionary_md_chemical_class_group_pb from "../../v1/dictionary_md/chemical_class_group_pb";
import * as v1_dictionary_md_color_pb from "../../v1/dictionary_md/color_pb";
import * as v1_dictionary_md_distribution_type_chemical_pb from "../../v1/dictionary_md/distribution_type_chemical_pb";
import * as v1_dictionary_md_file_pb from "../../v1/dictionary_md/file_pb";
import * as v1_dictionary_md_preparative_form_pb from "../../v1/dictionary_md/preparative_form_pb";
import * as v1_dictionary_md_product_pb from "../../v1/dictionary_md/product_pb";
import * as v1_dictionary_md_product_package_pb from "../../v1/dictionary_md/product_package_pb";
import * as v1_dictionary_md_product_active_substance_pb from "../../v1/dictionary_md/product_active_substance_pb";
import * as v1_dictionary_md_product_category_pb from "../../v1/dictionary_md/product_category_pb";
import * as v1_dictionary_md_product_category_application_pb from "../../v1/dictionary_md/product_category_application_pb";
import * as v1_dictionary_md_product_group_pb from "../../v1/dictionary_md/product_group_pb";
import * as v1_dictionary_md_product_sub_group_pb from "../../v1/dictionary_md/product_sub_group_pb";
import * as v1_dictionary_md_product_preparative_form_pb from "../../v1/dictionary_md/product_preparative_form_pb";
import * as v1_dictionary_md_product_quantity_type_pb from "../../v1/dictionary_md/product_quantity_type_pb";
import * as v1_dictionary_md_spectrum_action_chemical_pb from "../../v1/dictionary_md/spectrum_action_chemical_pb";
import * as v1_dictionary_md_toxicity_class_pb from "../../v1/dictionary_md/toxicity_class_pb";
import * as v1_dictionary_md_unit_pb from "../../v1/dictionary_md/unit_pb";
import * as v1_dictionary_md_quantity_type_pb from "../../v1/dictionary_md/quantity_type_pb";
import * as v1_dictionary_md_seed_sort_type_pb from "../../v1/dictionary_md_seed/sort_type_pb";
import * as v1_dictionary_md_seed_adapt_type_pb from "../../v1/dictionary_md_seed/adapt_type_pb";
import * as v1_dictionary_md_seed_fruit_average_weight_pb from "../../v1/dictionary_md_seed/fruit_average_weight_pb";
import * as v1_dictionary_md_seed_fruit_form_pb from "../../v1/dictionary_md_seed/fruit_form_pb";
import * as v1_dictionary_md_seed_grow_type_pb from "../../v1/dictionary_md_seed/grow_type_pb";
import * as v1_dictionary_md_seed_grow_season_pb from "../../v1/dictionary_md_seed/grow_season_pb";
import * as v1_dictionary_md_seed_maturity_group_pb from "../../v1/dictionary_md_seed/maturity_group_pb";
import * as v1_dictionary_md_seed_origin_country_pb from "../../v1/dictionary_md_seed/origin_country_pb";
import * as v1_dictionary_md_seed_plant_type_pb from "../../v1/dictionary_md_seed/plant_type_pb";
import * as v1_dictionary_md_seed_pollination_type_pb from "../../v1/dictionary_md_seed/pollination_type_pb";
import * as v1_dictionary_md_seed_purpose_pb from "../../v1/dictionary_md_seed/purpose_pb";
import * as v1_dictionary_md_seed_seed_type_pb from "../../v1/dictionary_md_seed/seed_type_pb";
import * as v1_dictionary_md_seed_species_pb from "../../v1/dictionary_md_seed/species_pb";
import * as v1_dictionary_md_seed_technology_type_pb from "../../v1/dictionary_md_seed/technology_type_pb";
import * as v1_dictionary_md_seed_reproduction_pb from "../../v1/dictionary_md_seed/reproduction_pb";
import {grpc} from "@improbable-eng/grpc-web";

type DictionaryMdServiceCreateDbSnapshot = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_private_database_pb.CreateDbSnapshotRequest;
  readonly responseType: typeof v1_dictionary_private_database_pb.CreateDbSnapshotResponse;
};

type DictionaryMdServiceGetDbSnapshot = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_private_database_pb.GetDbSnapshotRequest;
  readonly responseType: typeof v1_dictionary_private_database_pb.GetDbSnapshotResponse;
};

type DictionaryMdServiceListDbSnapshot = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_private_database_pb.ListDbSnapshotRequest;
  readonly responseType: typeof v1_dictionary_private_database_pb.ListDbSnapshotResponse;
};

type DictionaryMdServiceGetActiveSubstanceUnit = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_active_substance_unit_pb.GetActiveSubstanceUnitRequest;
  readonly responseType: typeof v1_dictionary_md_active_substance_unit_pb.GetActiveSubstanceUnitResponse;
};

type DictionaryMdServiceListActiveSubstanceUnit = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_active_substance_unit_pb.ListActiveSubstanceUnitRequest;
  readonly responseType: typeof v1_dictionary_md_active_substance_unit_pb.ListActiveSubstanceUnitResponse;
};

type DictionaryMdServiceGetActiveSubstance = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_active_substance_pb.GetActiveSubstanceRequest;
  readonly responseType: typeof v1_dictionary_md_active_substance_pb.GetActiveSubstanceResponse;
};

type DictionaryMdServiceListActiveSubstance = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_active_substance_pb.ListActiveSubstanceRequest;
  readonly responseType: typeof v1_dictionary_md_active_substance_pb.ListActiveSubstanceResponse;
};

type DictionaryMdServiceSaveActiveSubstance = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_active_substance_pb.SaveActiveSubstanceRequest;
  readonly responseType: typeof v1_dictionary_md_active_substance_pb.SaveActiveSubstanceResponse;
};

type DictionaryMdServiceGetApplicationMethod = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_application_method_pb.GetApplicationMethodRequest;
  readonly responseType: typeof v1_dictionary_md_application_method_pb.GetApplicationMethodResponse;
};

type DictionaryMdServiceListApplicationMethod = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_application_method_pb.ListApplicationMethodRequest;
  readonly responseType: typeof v1_dictionary_md_application_method_pb.ListApplicationMethodResponse;
};

type DictionaryMdServiceSaveApplicationMethod = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_application_method_pb.SaveApplicationMethodRequest;
  readonly responseType: typeof v1_dictionary_md_application_method_pb.SaveApplicationMethodResponse;
};

type DictionaryMdServiceGetApplicationRateUnitChemical = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_application_rate_unit_chemical_pb.GetApplicationRateUnitChemicalRequest;
  readonly responseType: typeof v1_dictionary_md_application_rate_unit_chemical_pb.GetApplicationRateUnitChemicalResponse;
};

type DictionaryMdServiceListApplicationRateUnitChemical = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_application_rate_unit_chemical_pb.ListApplicationRateUnitChemicalRequest;
  readonly responseType: typeof v1_dictionary_md_application_rate_unit_chemical_pb.ListApplicationRateUnitChemicalResponse;
};

type DictionaryMdServiceSaveApplicationRateUnitChemical = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_application_rate_unit_chemical_pb.SaveApplicationRateUnitChemicalRequest;
  readonly responseType: typeof v1_dictionary_md_application_rate_unit_chemical_pb.SaveApplicationRateUnitChemicalResponse;
};

type DictionaryMdServiceGetBrand = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_brand_pb.GetBrandRequest;
  readonly responseType: typeof v1_dictionary_md_brand_pb.GetBrandResponse;
};

type DictionaryMdServiceListBrand = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_brand_pb.ListBrandRequest;
  readonly responseType: typeof v1_dictionary_md_brand_pb.ListBrandResponse;
};

type DictionaryMdServiceSaveBrand = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_brand_pb.SaveBrandRequest;
  readonly responseType: typeof v1_dictionary_md_brand_pb.SaveBrandResponse;
};

type DictionaryMdServiceGetCategory = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_category_pb.GetCategoryRequest;
  readonly responseType: typeof v1_dictionary_md_category_pb.GetCategoryResponse;
};

type DictionaryMdServiceListCategory = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_category_pb.ListCategoryRequest;
  readonly responseType: typeof v1_dictionary_md_category_pb.ListCategoryResponse;
};

type DictionaryMdServiceSaveCategory = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_category_pb.SaveCategoryRequest;
  readonly responseType: typeof v1_dictionary_md_category_pb.SaveCategoryResponse;
};

type DictionaryMdServiceGetCategoryGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_category_group_pb.GetCategoryGroupRequest;
  readonly responseType: typeof v1_dictionary_md_category_group_pb.GetCategoryGroupResponse;
};

type DictionaryMdServiceListCategoryGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_category_group_pb.ListCategoryGroupRequest;
  readonly responseType: typeof v1_dictionary_md_category_group_pb.ListCategoryGroupResponse;
};

type DictionaryMdServiceSaveCategoryGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_category_group_pb.SaveCategoryGroupRequest;
  readonly responseType: typeof v1_dictionary_md_category_group_pb.SaveCategoryGroupResponse;
};

type DictionaryMdServiceGetColor = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_color_pb.GetColorRequest;
  readonly responseType: typeof v1_dictionary_md_color_pb.GetColorResponse;
};

type DictionaryMdServiceListColor = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_color_pb.ListColorRequest;
  readonly responseType: typeof v1_dictionary_md_color_pb.ListColorResponse;
};

type DictionaryMdServiceSaveColor = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_color_pb.SaveColorRequest;
  readonly responseType: typeof v1_dictionary_md_color_pb.SaveColorResponse;
};

type DictionaryMdServiceGetChemicalClassGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_chemical_class_group_pb.GetChemicalClassGroupRequest;
  readonly responseType: typeof v1_dictionary_md_chemical_class_group_pb.GetChemicalClassGroupResponse;
};

type DictionaryMdServiceListChemicalClassGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_chemical_class_group_pb.ListChemicalClassGroupRequest;
  readonly responseType: typeof v1_dictionary_md_chemical_class_group_pb.ListChemicalClassGroupResponse;
};

type DictionaryMdServiceSaveChemicalClassGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_chemical_class_group_pb.SaveChemicalClassGroupRequest;
  readonly responseType: typeof v1_dictionary_md_chemical_class_group_pb.SaveChemicalClassGroupResponse;
};

type DictionaryMdServiceGetDistributionTypeChemical = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_distribution_type_chemical_pb.GetDistributionTypeChemicalRequest;
  readonly responseType: typeof v1_dictionary_md_distribution_type_chemical_pb.GetDistributionTypeChemicalResponse;
};

type DictionaryMdServiceListDistributionTypeChemical = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_distribution_type_chemical_pb.ListDistributionTypeChemicalRequest;
  readonly responseType: typeof v1_dictionary_md_distribution_type_chemical_pb.ListDistributionTypeChemicalResponse;
};

type DictionaryMdServiceSaveDistributionTypeChemical = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_distribution_type_chemical_pb.SaveDistributionTypeChemicalRequest;
  readonly responseType: typeof v1_dictionary_md_distribution_type_chemical_pb.SaveDistributionTypeChemicalResponse;
};

type DictionaryMdServiceGetProductFile = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_file_pb.GetProductFileRequest;
  readonly responseType: typeof v1_dictionary_md_file_pb.GetProductFileResponse;
};

type DictionaryMdServiceListProductFile = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_file_pb.ListProductFileRequest;
  readonly responseType: typeof v1_dictionary_md_file_pb.ListProductFileResponse;
};

type DictionaryMdServiceListGroupedPackageFile = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_file_pb.ListGroupedPackageFileRequest;
  readonly responseType: typeof v1_dictionary_md_file_pb.ListGroupedPackageFileResponse;
};

type DictionaryMdServiceSaveProductFile = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_file_pb.SaveProductFileRequest;
  readonly responseType: typeof v1_dictionary_md_file_pb.SaveProductFileResponse;
};

type DictionaryMdServiceDeleteProductFile = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_file_pb.DeleteProductFileRequest;
  readonly responseType: typeof v1_dictionary_md_file_pb.DeleteProductFileResponse;
};

type DictionaryMdServiceGetPreparativeForm = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_preparative_form_pb.GetPreparativeFormRequest;
  readonly responseType: typeof v1_dictionary_md_preparative_form_pb.GetPreparativeFormResponse;
};

type DictionaryMdServiceListPreparativeForm = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_preparative_form_pb.ListPreparativeFormRequest;
  readonly responseType: typeof v1_dictionary_md_preparative_form_pb.ListPreparativeFormResponse;
};

type DictionaryMdServiceSavePreparativeForm = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_preparative_form_pb.SavePreparativeFormRequest;
  readonly responseType: typeof v1_dictionary_md_preparative_form_pb.SavePreparativeFormResponse;
};

type DictionaryMdServiceGetProduct = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_pb.GetProductRequest;
  readonly responseType: typeof v1_dictionary_md_product_pb.GetProductResponse;
};

type DictionaryMdServiceListProduct = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_pb.ListProductRequest;
  readonly responseType: typeof v1_dictionary_md_product_pb.ListProductResponse;
};

type DictionaryMdServiceListProductIds = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_pb.ListProductIdsRequest;
  readonly responseType: typeof v1_dictionary_md_product_pb.ListProductIdsResponse;
};

type DictionaryMdServiceSaveProduct = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_pb.SaveProductRequest;
  readonly responseType: typeof v1_dictionary_md_product_pb.SaveProductResponse;
};

type DictionaryMdServiceGetProductActiveSubstance = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_active_substance_pb.GetProductActiveSubstanceRequest;
  readonly responseType: typeof v1_dictionary_md_product_active_substance_pb.GetProductActiveSubstanceResponse;
};

type DictionaryMdServiceListProductActiveSubstance = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_active_substance_pb.ListProductActiveSubstanceRequest;
  readonly responseType: typeof v1_dictionary_md_product_active_substance_pb.ListProductActiveSubstanceResponse;
};

type DictionaryMdServiceGetProductCategory = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_category_pb.GetProductCategoryRequest;
  readonly responseType: typeof v1_dictionary_md_product_category_pb.GetProductCategoryResponse;
};

type DictionaryMdServiceListProductCategory = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_category_pb.ListProductCategoryRequest;
  readonly responseType: typeof v1_dictionary_md_product_category_pb.ListProductCategoryResponse;
};

type DictionaryMdServiceGetProductCategoryApplication = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_category_application_pb.GetProductCategoryApplicationRequest;
  readonly responseType: typeof v1_dictionary_md_product_category_application_pb.GetProductCategoryApplicationResponse;
};

type DictionaryMdServiceListProductCategoryApplication = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_category_application_pb.ListProductCategoryApplicationRequest;
  readonly responseType: typeof v1_dictionary_md_product_category_application_pb.ListProductCategoryApplicationResponse;
};

type DictionaryMdServiceSaveProductCategoryApplication = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_category_application_pb.SaveProductCategoryApplicationRequest;
  readonly responseType: typeof v1_dictionary_md_product_category_application_pb.SaveProductCategoryApplicationResponse;
};

type DictionaryMdServiceGetProductGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_group_pb.GetProductGroupRequest;
  readonly responseType: typeof v1_dictionary_md_product_group_pb.GetProductGroupResponse;
};

type DictionaryMdServiceListProductGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_group_pb.ListProductGroupRequest;
  readonly responseType: typeof v1_dictionary_md_product_group_pb.ListProductGroupResponse;
};

type DictionaryMdServiceSaveProductGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_group_pb.SaveProductGroupRequest;
  readonly responseType: typeof v1_dictionary_md_product_group_pb.SaveProductGroupResponse;
};

type DictionaryMdServiceGetProductSubGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_sub_group_pb.GetProductSubGroupRequest;
  readonly responseType: typeof v1_dictionary_md_product_sub_group_pb.GetProductSubGroupResponse;
};

type DictionaryMdServiceListProductSubGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_sub_group_pb.ListProductSubGroupRequest;
  readonly responseType: typeof v1_dictionary_md_product_sub_group_pb.ListProductSubGroupResponse;
};

type DictionaryMdServiceSaveProductSubGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_sub_group_pb.SaveProductSubGroupRequest;
  readonly responseType: typeof v1_dictionary_md_product_sub_group_pb.SaveProductSubGroupResponse;
};

type DictionaryMdServiceGetProductPackage = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_package_pb.GetProductPackageRequest;
  readonly responseType: typeof v1_dictionary_md_product_package_pb.GetProductPackageResponse;
};

type DictionaryMdServiceListProductPackage = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_package_pb.ListProductPackageRequest;
  readonly responseType: typeof v1_dictionary_md_product_package_pb.ListProductPackageResponse;
};

type DictionaryMdServiceGetProductPreparativeForm = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_preparative_form_pb.GetProductPreparativeFormRequest;
  readonly responseType: typeof v1_dictionary_md_product_preparative_form_pb.GetProductPreparativeFormResponse;
};

type DictionaryMdServiceListProductPreparativeForm = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_preparative_form_pb.ListProductPreparativeFormRequest;
  readonly responseType: typeof v1_dictionary_md_product_preparative_form_pb.ListProductPreparativeFormResponse;
};

type DictionaryMdServiceGetProductQuantityType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_quantity_type_pb.GetProductQuantityTypeRequest;
  readonly responseType: typeof v1_dictionary_md_product_quantity_type_pb.GetProductQuantityTypeResponse;
};

type DictionaryMdServiceListProductQuantityType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_product_quantity_type_pb.ListProductQuantityTypeRequest;
  readonly responseType: typeof v1_dictionary_md_product_quantity_type_pb.ListProductQuantityTypeResponse;
};

type DictionaryMdServiceGetSpectrumActionChemical = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_spectrum_action_chemical_pb.GetSpectrumActionChemicalRequest;
  readonly responseType: typeof v1_dictionary_md_spectrum_action_chemical_pb.GetSpectrumActionChemicalResponse;
};

type DictionaryMdServiceListSpectrumActionChemical = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_spectrum_action_chemical_pb.ListSpectrumActionChemicalRequest;
  readonly responseType: typeof v1_dictionary_md_spectrum_action_chemical_pb.ListSpectrumActionChemicalResponse;
};

type DictionaryMdServiceSaveSpectrumActionChemical = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_spectrum_action_chemical_pb.SaveSpectrumActionChemicalRequest;
  readonly responseType: typeof v1_dictionary_md_spectrum_action_chemical_pb.SaveSpectrumActionChemicalResponse;
};

type DictionaryMdServiceGetToxicityClass = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_toxicity_class_pb.GetToxicityClassRequest;
  readonly responseType: typeof v1_dictionary_md_toxicity_class_pb.GetToxicityClassResponse;
};

type DictionaryMdServiceListToxicityClass = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_toxicity_class_pb.ListToxicityClassRequest;
  readonly responseType: typeof v1_dictionary_md_toxicity_class_pb.ListToxicityClassResponse;
};

type DictionaryMdServiceSaveToxicityClass = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_toxicity_class_pb.SaveToxicityClassRequest;
  readonly responseType: typeof v1_dictionary_md_toxicity_class_pb.SaveToxicityClassResponse;
};

type DictionaryMdServiceGetUnit = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_unit_pb.GetUnitRequest;
  readonly responseType: typeof v1_dictionary_md_unit_pb.GetUnitResponse;
};

type DictionaryMdServiceListUnit = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_unit_pb.ListUnitRequest;
  readonly responseType: typeof v1_dictionary_md_unit_pb.ListUnitResponse;
};

type DictionaryMdServiceSaveUnit = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_unit_pb.SaveUnitRequest;
  readonly responseType: typeof v1_dictionary_md_unit_pb.SaveUnitResponse;
};

type DictionaryMdServiceGetQuantityType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_quantity_type_pb.GetQuantityTypeRequest;
  readonly responseType: typeof v1_dictionary_md_quantity_type_pb.GetQuantityTypeResponse;
};

type DictionaryMdServiceListQuantityType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_quantity_type_pb.ListQuantityTypeRequest;
  readonly responseType: typeof v1_dictionary_md_quantity_type_pb.ListQuantityTypeResponse;
};

type DictionaryMdServiceSaveQuantityType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_quantity_type_pb.SaveQuantityTypeRequest;
  readonly responseType: typeof v1_dictionary_md_quantity_type_pb.SaveQuantityTypeResponse;
};

type DictionaryMdServiceGetSortType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_sort_type_pb.GetSortTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_sort_type_pb.GetSortTypeResponse;
};

type DictionaryMdServiceListSortType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_sort_type_pb.ListSortTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_sort_type_pb.ListSortTypeResponse;
};

type DictionaryMdServiceSaveSortType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_sort_type_pb.SaveSortTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_sort_type_pb.SaveSortTypeResponse;
};

type DictionaryMdServiceGetFruitForm = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_fruit_form_pb.GetFruitFormRequest;
  readonly responseType: typeof v1_dictionary_md_seed_fruit_form_pb.GetFruitFormResponse;
};

type DictionaryMdServiceListFruitForm = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_fruit_form_pb.ListFruitFormRequest;
  readonly responseType: typeof v1_dictionary_md_seed_fruit_form_pb.ListFruitFormResponse;
};

type DictionaryMdServiceSaveFruitForm = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_fruit_form_pb.SaveFruitFormRequest;
  readonly responseType: typeof v1_dictionary_md_seed_fruit_form_pb.SaveFruitFormResponse;
};

type DictionaryMdServiceGetReproduction = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_reproduction_pb.GetReproductionRequest;
  readonly responseType: typeof v1_dictionary_md_seed_reproduction_pb.GetReproductionResponse;
};

type DictionaryMdServiceListReproduction = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_reproduction_pb.ListReproductionRequest;
  readonly responseType: typeof v1_dictionary_md_seed_reproduction_pb.ListReproductionResponse;
};

type DictionaryMdServiceSaveReproduction = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_reproduction_pb.SaveReproductionRequest;
  readonly responseType: typeof v1_dictionary_md_seed_reproduction_pb.SaveReproductionResponse;
};

type DictionaryMdServiceGetSpecies = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_species_pb.GetSpeciesRequest;
  readonly responseType: typeof v1_dictionary_md_seed_species_pb.GetSpeciesResponse;
};

type DictionaryMdServiceListSpecies = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_species_pb.ListSpeciesRequest;
  readonly responseType: typeof v1_dictionary_md_seed_species_pb.ListSpeciesResponse;
};

type DictionaryMdServiceSaveSpecies = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_species_pb.SaveSpeciesRequest;
  readonly responseType: typeof v1_dictionary_md_seed_species_pb.SaveSpeciesResponse;
};

type DictionaryMdServiceGetTechnologyType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_technology_type_pb.GetTechnologyTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_technology_type_pb.GetTechnologyTypeResponse;
};

type DictionaryMdServiceListTechnologyType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_technology_type_pb.ListTechnologyTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_technology_type_pb.ListTechnologyTypeResponse;
};

type DictionaryMdServiceSaveTechnologyType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_technology_type_pb.SaveTechnologyTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_technology_type_pb.SaveTechnologyTypeResponse;
};

type DictionaryMdServiceGetMaturityGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_maturity_group_pb.GetMaturityGroupRequest;
  readonly responseType: typeof v1_dictionary_md_seed_maturity_group_pb.GetMaturityGroupResponse;
};

type DictionaryMdServiceListMaturityGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_maturity_group_pb.ListMaturityGroupRequest;
  readonly responseType: typeof v1_dictionary_md_seed_maturity_group_pb.ListMaturityGroupResponse;
};

type DictionaryMdServiceSaveMaturityGroup = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_maturity_group_pb.SaveMaturityGroupRequest;
  readonly responseType: typeof v1_dictionary_md_seed_maturity_group_pb.SaveMaturityGroupResponse;
};

type DictionaryMdServiceGetFruitAverageWeight = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_fruit_average_weight_pb.GetFruitAverageWeightRequest;
  readonly responseType: typeof v1_dictionary_md_seed_fruit_average_weight_pb.GetFruitAverageWeightResponse;
};

type DictionaryMdServiceListFruitAverageWeight = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_fruit_average_weight_pb.ListFruitAverageWeightRequest;
  readonly responseType: typeof v1_dictionary_md_seed_fruit_average_weight_pb.ListFruitAverageWeightResponse;
};

type DictionaryMdServiceSaveFruitAverageWeight = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_fruit_average_weight_pb.SaveFruitAverageWeightRequest;
  readonly responseType: typeof v1_dictionary_md_seed_fruit_average_weight_pb.SaveFruitAverageWeightResponse;
};

type DictionaryMdServiceGetSeedType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_seed_type_pb.GetSeedTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_seed_type_pb.GetSeedTypeResponse;
};

type DictionaryMdServiceListSeedType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_seed_type_pb.ListSeedTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_seed_type_pb.ListSeedTypeResponse;
};

type DictionaryMdServiceSaveSeedType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_seed_type_pb.SaveSeedTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_seed_type_pb.SaveSeedTypeResponse;
};

type DictionaryMdServiceGetPollinationType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_pollination_type_pb.GetPollinationTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_pollination_type_pb.GetPollinationTypeResponse;
};

type DictionaryMdServiceListPollinationType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_pollination_type_pb.ListPollinationTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_pollination_type_pb.ListPollinationTypeResponse;
};

type DictionaryMdServiceSavePollinationType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_pollination_type_pb.SavePollinationTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_pollination_type_pb.SavePollinationTypeResponse;
};

type DictionaryMdServiceGetAdaptType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_adapt_type_pb.GetAdaptTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_adapt_type_pb.GetAdaptTypeResponse;
};

type DictionaryMdServiceListAdaptType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_adapt_type_pb.ListAdaptTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_adapt_type_pb.ListAdaptTypeResponse;
};

type DictionaryMdServiceSaveAdaptType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_adapt_type_pb.SaveAdaptTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_adapt_type_pb.SaveAdaptTypeResponse;
};

type DictionaryMdServiceGetOriginCountry = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_origin_country_pb.GetOriginCountryRequest;
  readonly responseType: typeof v1_dictionary_md_seed_origin_country_pb.GetOriginCountryResponse;
};

type DictionaryMdServiceListOriginCountry = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_origin_country_pb.ListOriginCountryRequest;
  readonly responseType: typeof v1_dictionary_md_seed_origin_country_pb.ListOriginCountryResponse;
};

type DictionaryMdServiceSaveOriginCountry = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_origin_country_pb.SaveOriginCountryRequest;
  readonly responseType: typeof v1_dictionary_md_seed_origin_country_pb.SaveOriginCountryResponse;
};

type DictionaryMdServiceGetPlantType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_plant_type_pb.GetPlantTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_plant_type_pb.GetPlantTypeResponse;
};

type DictionaryMdServiceListPlantType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_plant_type_pb.ListPlantTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_plant_type_pb.ListPlantTypeResponse;
};

type DictionaryMdServiceSavePlantType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_plant_type_pb.SavePlantTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_plant_type_pb.SavePlantTypeResponse;
};

type DictionaryMdServiceGetPurpose = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_purpose_pb.GetPurposeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_purpose_pb.GetPurposeResponse;
};

type DictionaryMdServiceListPurpose = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_purpose_pb.ListPurposeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_purpose_pb.ListPurposeResponse;
};

type DictionaryMdServiceSavePurpose = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_purpose_pb.SavePurposeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_purpose_pb.SavePurposeResponse;
};

type DictionaryMdServiceGetGrowType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_grow_type_pb.GetGrowTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_grow_type_pb.GetGrowTypeResponse;
};

type DictionaryMdServiceListGrowType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_grow_type_pb.ListGrowTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_grow_type_pb.ListGrowTypeResponse;
};

type DictionaryMdServiceSaveGrowType = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_grow_type_pb.SaveGrowTypeRequest;
  readonly responseType: typeof v1_dictionary_md_seed_grow_type_pb.SaveGrowTypeResponse;
};

type DictionaryMdServiceGetGrowSeason = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_grow_season_pb.GetGrowSeasonRequest;
  readonly responseType: typeof v1_dictionary_md_seed_grow_season_pb.GetGrowSeasonResponse;
};

type DictionaryMdServiceListGrowSeason = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_grow_season_pb.ListGrowSeasonRequest;
  readonly responseType: typeof v1_dictionary_md_seed_grow_season_pb.ListGrowSeasonResponse;
};

type DictionaryMdServiceSaveGrowSeason = {
  readonly methodName: string;
  readonly service: typeof DictionaryMdService;
  readonly requestStream: false;
  readonly responseStream: false;
  readonly requestType: typeof v1_dictionary_md_seed_grow_season_pb.SaveGrowSeasonRequest;
  readonly responseType: typeof v1_dictionary_md_seed_grow_season_pb.SaveGrowSeasonResponse;
};

export class DictionaryMdService {
  static readonly serviceName: string;
  static readonly CreateDbSnapshot: DictionaryMdServiceCreateDbSnapshot;
  static readonly GetDbSnapshot: DictionaryMdServiceGetDbSnapshot;
  static readonly ListDbSnapshot: DictionaryMdServiceListDbSnapshot;
  static readonly GetActiveSubstanceUnit: DictionaryMdServiceGetActiveSubstanceUnit;
  static readonly ListActiveSubstanceUnit: DictionaryMdServiceListActiveSubstanceUnit;
  static readonly GetActiveSubstance: DictionaryMdServiceGetActiveSubstance;
  static readonly ListActiveSubstance: DictionaryMdServiceListActiveSubstance;
  static readonly SaveActiveSubstance: DictionaryMdServiceSaveActiveSubstance;
  static readonly GetApplicationMethod: DictionaryMdServiceGetApplicationMethod;
  static readonly ListApplicationMethod: DictionaryMdServiceListApplicationMethod;
  static readonly SaveApplicationMethod: DictionaryMdServiceSaveApplicationMethod;
  static readonly GetApplicationRateUnitChemical: DictionaryMdServiceGetApplicationRateUnitChemical;
  static readonly ListApplicationRateUnitChemical: DictionaryMdServiceListApplicationRateUnitChemical;
  static readonly SaveApplicationRateUnitChemical: DictionaryMdServiceSaveApplicationRateUnitChemical;
  static readonly GetBrand: DictionaryMdServiceGetBrand;
  static readonly ListBrand: DictionaryMdServiceListBrand;
  static readonly SaveBrand: DictionaryMdServiceSaveBrand;
  static readonly GetCategory: DictionaryMdServiceGetCategory;
  static readonly ListCategory: DictionaryMdServiceListCategory;
  static readonly SaveCategory: DictionaryMdServiceSaveCategory;
  static readonly GetCategoryGroup: DictionaryMdServiceGetCategoryGroup;
  static readonly ListCategoryGroup: DictionaryMdServiceListCategoryGroup;
  static readonly SaveCategoryGroup: DictionaryMdServiceSaveCategoryGroup;
  static readonly GetColor: DictionaryMdServiceGetColor;
  static readonly ListColor: DictionaryMdServiceListColor;
  static readonly SaveColor: DictionaryMdServiceSaveColor;
  static readonly GetChemicalClassGroup: DictionaryMdServiceGetChemicalClassGroup;
  static readonly ListChemicalClassGroup: DictionaryMdServiceListChemicalClassGroup;
  static readonly SaveChemicalClassGroup: DictionaryMdServiceSaveChemicalClassGroup;
  static readonly GetDistributionTypeChemical: DictionaryMdServiceGetDistributionTypeChemical;
  static readonly ListDistributionTypeChemical: DictionaryMdServiceListDistributionTypeChemical;
  static readonly SaveDistributionTypeChemical: DictionaryMdServiceSaveDistributionTypeChemical;
  static readonly GetProductFile: DictionaryMdServiceGetProductFile;
  static readonly ListProductFile: DictionaryMdServiceListProductFile;
  static readonly ListGroupedPackageFile: DictionaryMdServiceListGroupedPackageFile;
  static readonly SaveProductFile: DictionaryMdServiceSaveProductFile;
  static readonly DeleteProductFile: DictionaryMdServiceDeleteProductFile;
  static readonly GetPreparativeForm: DictionaryMdServiceGetPreparativeForm;
  static readonly ListPreparativeForm: DictionaryMdServiceListPreparativeForm;
  static readonly SavePreparativeForm: DictionaryMdServiceSavePreparativeForm;
  static readonly GetProduct: DictionaryMdServiceGetProduct;
  static readonly ListProduct: DictionaryMdServiceListProduct;
  static readonly ListProductIds: DictionaryMdServiceListProductIds;
  static readonly SaveProduct: DictionaryMdServiceSaveProduct;
  static readonly GetProductActiveSubstance: DictionaryMdServiceGetProductActiveSubstance;
  static readonly ListProductActiveSubstance: DictionaryMdServiceListProductActiveSubstance;
  static readonly GetProductCategory: DictionaryMdServiceGetProductCategory;
  static readonly ListProductCategory: DictionaryMdServiceListProductCategory;
  static readonly GetProductCategoryApplication: DictionaryMdServiceGetProductCategoryApplication;
  static readonly ListProductCategoryApplication: DictionaryMdServiceListProductCategoryApplication;
  static readonly SaveProductCategoryApplication: DictionaryMdServiceSaveProductCategoryApplication;
  static readonly GetProductGroup: DictionaryMdServiceGetProductGroup;
  static readonly ListProductGroup: DictionaryMdServiceListProductGroup;
  static readonly SaveProductGroup: DictionaryMdServiceSaveProductGroup;
  static readonly GetProductSubGroup: DictionaryMdServiceGetProductSubGroup;
  static readonly ListProductSubGroup: DictionaryMdServiceListProductSubGroup;
  static readonly SaveProductSubGroup: DictionaryMdServiceSaveProductSubGroup;
  static readonly GetProductPackage: DictionaryMdServiceGetProductPackage;
  static readonly ListProductPackage: DictionaryMdServiceListProductPackage;
  static readonly GetProductPreparativeForm: DictionaryMdServiceGetProductPreparativeForm;
  static readonly ListProductPreparativeForm: DictionaryMdServiceListProductPreparativeForm;
  static readonly GetProductQuantityType: DictionaryMdServiceGetProductQuantityType;
  static readonly ListProductQuantityType: DictionaryMdServiceListProductQuantityType;
  static readonly GetSpectrumActionChemical: DictionaryMdServiceGetSpectrumActionChemical;
  static readonly ListSpectrumActionChemical: DictionaryMdServiceListSpectrumActionChemical;
  static readonly SaveSpectrumActionChemical: DictionaryMdServiceSaveSpectrumActionChemical;
  static readonly GetToxicityClass: DictionaryMdServiceGetToxicityClass;
  static readonly ListToxicityClass: DictionaryMdServiceListToxicityClass;
  static readonly SaveToxicityClass: DictionaryMdServiceSaveToxicityClass;
  static readonly GetUnit: DictionaryMdServiceGetUnit;
  static readonly ListUnit: DictionaryMdServiceListUnit;
  static readonly SaveUnit: DictionaryMdServiceSaveUnit;
  static readonly GetQuantityType: DictionaryMdServiceGetQuantityType;
  static readonly ListQuantityType: DictionaryMdServiceListQuantityType;
  static readonly SaveQuantityType: DictionaryMdServiceSaveQuantityType;
  static readonly GetSortType: DictionaryMdServiceGetSortType;
  static readonly ListSortType: DictionaryMdServiceListSortType;
  static readonly SaveSortType: DictionaryMdServiceSaveSortType;
  static readonly GetFruitForm: DictionaryMdServiceGetFruitForm;
  static readonly ListFruitForm: DictionaryMdServiceListFruitForm;
  static readonly SaveFruitForm: DictionaryMdServiceSaveFruitForm;
  static readonly GetReproduction: DictionaryMdServiceGetReproduction;
  static readonly ListReproduction: DictionaryMdServiceListReproduction;
  static readonly SaveReproduction: DictionaryMdServiceSaveReproduction;
  static readonly GetSpecies: DictionaryMdServiceGetSpecies;
  static readonly ListSpecies: DictionaryMdServiceListSpecies;
  static readonly SaveSpecies: DictionaryMdServiceSaveSpecies;
  static readonly GetTechnologyType: DictionaryMdServiceGetTechnologyType;
  static readonly ListTechnologyType: DictionaryMdServiceListTechnologyType;
  static readonly SaveTechnologyType: DictionaryMdServiceSaveTechnologyType;
  static readonly GetMaturityGroup: DictionaryMdServiceGetMaturityGroup;
  static readonly ListMaturityGroup: DictionaryMdServiceListMaturityGroup;
  static readonly SaveMaturityGroup: DictionaryMdServiceSaveMaturityGroup;
  static readonly GetFruitAverageWeight: DictionaryMdServiceGetFruitAverageWeight;
  static readonly ListFruitAverageWeight: DictionaryMdServiceListFruitAverageWeight;
  static readonly SaveFruitAverageWeight: DictionaryMdServiceSaveFruitAverageWeight;
  static readonly GetSeedType: DictionaryMdServiceGetSeedType;
  static readonly ListSeedType: DictionaryMdServiceListSeedType;
  static readonly SaveSeedType: DictionaryMdServiceSaveSeedType;
  static readonly GetPollinationType: DictionaryMdServiceGetPollinationType;
  static readonly ListPollinationType: DictionaryMdServiceListPollinationType;
  static readonly SavePollinationType: DictionaryMdServiceSavePollinationType;
  static readonly GetAdaptType: DictionaryMdServiceGetAdaptType;
  static readonly ListAdaptType: DictionaryMdServiceListAdaptType;
  static readonly SaveAdaptType: DictionaryMdServiceSaveAdaptType;
  static readonly GetOriginCountry: DictionaryMdServiceGetOriginCountry;
  static readonly ListOriginCountry: DictionaryMdServiceListOriginCountry;
  static readonly SaveOriginCountry: DictionaryMdServiceSaveOriginCountry;
  static readonly GetPlantType: DictionaryMdServiceGetPlantType;
  static readonly ListPlantType: DictionaryMdServiceListPlantType;
  static readonly SavePlantType: DictionaryMdServiceSavePlantType;
  static readonly GetPurpose: DictionaryMdServiceGetPurpose;
  static readonly ListPurpose: DictionaryMdServiceListPurpose;
  static readonly SavePurpose: DictionaryMdServiceSavePurpose;
  static readonly GetGrowType: DictionaryMdServiceGetGrowType;
  static readonly ListGrowType: DictionaryMdServiceListGrowType;
  static readonly SaveGrowType: DictionaryMdServiceSaveGrowType;
  static readonly GetGrowSeason: DictionaryMdServiceGetGrowSeason;
  static readonly ListGrowSeason: DictionaryMdServiceListGrowSeason;
  static readonly SaveGrowSeason: DictionaryMdServiceSaveGrowSeason;
}

export type ServiceError = { message: string, code: number; metadata: grpc.Metadata }
export type Status = { details: string, code: number; metadata: grpc.Metadata }

interface UnaryResponse {
  cancel(): void;
}
interface ResponseStream<T> {
  cancel(): void;
  on(type: 'data', handler: (message: T) => void): ResponseStream<T>;
  on(type: 'end', handler: (status?: Status) => void): ResponseStream<T>;
  on(type: 'status', handler: (status: Status) => void): ResponseStream<T>;
}
interface RequestStream<T> {
  write(message: T): RequestStream<T>;
  end(): void;
  cancel(): void;
  on(type: 'end', handler: (status?: Status) => void): RequestStream<T>;
  on(type: 'status', handler: (status: Status) => void): RequestStream<T>;
}
interface BidirectionalStream<ReqT, ResT> {
  write(message: ReqT): BidirectionalStream<ReqT, ResT>;
  end(): void;
  cancel(): void;
  on(type: 'data', handler: (message: ResT) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'end', handler: (status?: Status) => void): BidirectionalStream<ReqT, ResT>;
  on(type: 'status', handler: (status: Status) => void): BidirectionalStream<ReqT, ResT>;
}

export class DictionaryMdServiceClient {
  readonly serviceHost: string;

  constructor(serviceHost: string, options?: grpc.RpcOptions);
  createDbSnapshot(
    requestMessage: v1_dictionary_private_database_pb.CreateDbSnapshotRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_private_database_pb.CreateDbSnapshotResponse|null) => void
  ): UnaryResponse;
  createDbSnapshot(
    requestMessage: v1_dictionary_private_database_pb.CreateDbSnapshotRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_private_database_pb.CreateDbSnapshotResponse|null) => void
  ): UnaryResponse;
  getDbSnapshot(
    requestMessage: v1_dictionary_private_database_pb.GetDbSnapshotRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_private_database_pb.GetDbSnapshotResponse|null) => void
  ): UnaryResponse;
  getDbSnapshot(
    requestMessage: v1_dictionary_private_database_pb.GetDbSnapshotRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_private_database_pb.GetDbSnapshotResponse|null) => void
  ): UnaryResponse;
  listDbSnapshot(
    requestMessage: v1_dictionary_private_database_pb.ListDbSnapshotRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_private_database_pb.ListDbSnapshotResponse|null) => void
  ): UnaryResponse;
  listDbSnapshot(
    requestMessage: v1_dictionary_private_database_pb.ListDbSnapshotRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_private_database_pb.ListDbSnapshotResponse|null) => void
  ): UnaryResponse;
  getActiveSubstanceUnit(
    requestMessage: v1_dictionary_md_active_substance_unit_pb.GetActiveSubstanceUnitRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_active_substance_unit_pb.GetActiveSubstanceUnitResponse|null) => void
  ): UnaryResponse;
  getActiveSubstanceUnit(
    requestMessage: v1_dictionary_md_active_substance_unit_pb.GetActiveSubstanceUnitRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_active_substance_unit_pb.GetActiveSubstanceUnitResponse|null) => void
  ): UnaryResponse;
  listActiveSubstanceUnit(
    requestMessage: v1_dictionary_md_active_substance_unit_pb.ListActiveSubstanceUnitRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_active_substance_unit_pb.ListActiveSubstanceUnitResponse|null) => void
  ): UnaryResponse;
  listActiveSubstanceUnit(
    requestMessage: v1_dictionary_md_active_substance_unit_pb.ListActiveSubstanceUnitRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_active_substance_unit_pb.ListActiveSubstanceUnitResponse|null) => void
  ): UnaryResponse;
  getActiveSubstance(
    requestMessage: v1_dictionary_md_active_substance_pb.GetActiveSubstanceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_active_substance_pb.GetActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  getActiveSubstance(
    requestMessage: v1_dictionary_md_active_substance_pb.GetActiveSubstanceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_active_substance_pb.GetActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  listActiveSubstance(
    requestMessage: v1_dictionary_md_active_substance_pb.ListActiveSubstanceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_active_substance_pb.ListActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  listActiveSubstance(
    requestMessage: v1_dictionary_md_active_substance_pb.ListActiveSubstanceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_active_substance_pb.ListActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  saveActiveSubstance(
    requestMessage: v1_dictionary_md_active_substance_pb.SaveActiveSubstanceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_active_substance_pb.SaveActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  saveActiveSubstance(
    requestMessage: v1_dictionary_md_active_substance_pb.SaveActiveSubstanceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_active_substance_pb.SaveActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  getApplicationMethod(
    requestMessage: v1_dictionary_md_application_method_pb.GetApplicationMethodRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_method_pb.GetApplicationMethodResponse|null) => void
  ): UnaryResponse;
  getApplicationMethod(
    requestMessage: v1_dictionary_md_application_method_pb.GetApplicationMethodRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_method_pb.GetApplicationMethodResponse|null) => void
  ): UnaryResponse;
  listApplicationMethod(
    requestMessage: v1_dictionary_md_application_method_pb.ListApplicationMethodRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_method_pb.ListApplicationMethodResponse|null) => void
  ): UnaryResponse;
  listApplicationMethod(
    requestMessage: v1_dictionary_md_application_method_pb.ListApplicationMethodRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_method_pb.ListApplicationMethodResponse|null) => void
  ): UnaryResponse;
  saveApplicationMethod(
    requestMessage: v1_dictionary_md_application_method_pb.SaveApplicationMethodRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_method_pb.SaveApplicationMethodResponse|null) => void
  ): UnaryResponse;
  saveApplicationMethod(
    requestMessage: v1_dictionary_md_application_method_pb.SaveApplicationMethodRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_method_pb.SaveApplicationMethodResponse|null) => void
  ): UnaryResponse;
  getApplicationRateUnitChemical(
    requestMessage: v1_dictionary_md_application_rate_unit_chemical_pb.GetApplicationRateUnitChemicalRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_rate_unit_chemical_pb.GetApplicationRateUnitChemicalResponse|null) => void
  ): UnaryResponse;
  getApplicationRateUnitChemical(
    requestMessage: v1_dictionary_md_application_rate_unit_chemical_pb.GetApplicationRateUnitChemicalRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_rate_unit_chemical_pb.GetApplicationRateUnitChemicalResponse|null) => void
  ): UnaryResponse;
  listApplicationRateUnitChemical(
    requestMessage: v1_dictionary_md_application_rate_unit_chemical_pb.ListApplicationRateUnitChemicalRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_rate_unit_chemical_pb.ListApplicationRateUnitChemicalResponse|null) => void
  ): UnaryResponse;
  listApplicationRateUnitChemical(
    requestMessage: v1_dictionary_md_application_rate_unit_chemical_pb.ListApplicationRateUnitChemicalRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_rate_unit_chemical_pb.ListApplicationRateUnitChemicalResponse|null) => void
  ): UnaryResponse;
  saveApplicationRateUnitChemical(
    requestMessage: v1_dictionary_md_application_rate_unit_chemical_pb.SaveApplicationRateUnitChemicalRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_rate_unit_chemical_pb.SaveApplicationRateUnitChemicalResponse|null) => void
  ): UnaryResponse;
  saveApplicationRateUnitChemical(
    requestMessage: v1_dictionary_md_application_rate_unit_chemical_pb.SaveApplicationRateUnitChemicalRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_application_rate_unit_chemical_pb.SaveApplicationRateUnitChemicalResponse|null) => void
  ): UnaryResponse;
  getBrand(
    requestMessage: v1_dictionary_md_brand_pb.GetBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_brand_pb.GetBrandResponse|null) => void
  ): UnaryResponse;
  getBrand(
    requestMessage: v1_dictionary_md_brand_pb.GetBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_brand_pb.GetBrandResponse|null) => void
  ): UnaryResponse;
  listBrand(
    requestMessage: v1_dictionary_md_brand_pb.ListBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_brand_pb.ListBrandResponse|null) => void
  ): UnaryResponse;
  listBrand(
    requestMessage: v1_dictionary_md_brand_pb.ListBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_brand_pb.ListBrandResponse|null) => void
  ): UnaryResponse;
  saveBrand(
    requestMessage: v1_dictionary_md_brand_pb.SaveBrandRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_brand_pb.SaveBrandResponse|null) => void
  ): UnaryResponse;
  saveBrand(
    requestMessage: v1_dictionary_md_brand_pb.SaveBrandRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_brand_pb.SaveBrandResponse|null) => void
  ): UnaryResponse;
  getCategory(
    requestMessage: v1_dictionary_md_category_pb.GetCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_pb.GetCategoryResponse|null) => void
  ): UnaryResponse;
  getCategory(
    requestMessage: v1_dictionary_md_category_pb.GetCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_pb.GetCategoryResponse|null) => void
  ): UnaryResponse;
  listCategory(
    requestMessage: v1_dictionary_md_category_pb.ListCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_pb.ListCategoryResponse|null) => void
  ): UnaryResponse;
  listCategory(
    requestMessage: v1_dictionary_md_category_pb.ListCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_pb.ListCategoryResponse|null) => void
  ): UnaryResponse;
  saveCategory(
    requestMessage: v1_dictionary_md_category_pb.SaveCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_pb.SaveCategoryResponse|null) => void
  ): UnaryResponse;
  saveCategory(
    requestMessage: v1_dictionary_md_category_pb.SaveCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_pb.SaveCategoryResponse|null) => void
  ): UnaryResponse;
  getCategoryGroup(
    requestMessage: v1_dictionary_md_category_group_pb.GetCategoryGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_group_pb.GetCategoryGroupResponse|null) => void
  ): UnaryResponse;
  getCategoryGroup(
    requestMessage: v1_dictionary_md_category_group_pb.GetCategoryGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_group_pb.GetCategoryGroupResponse|null) => void
  ): UnaryResponse;
  listCategoryGroup(
    requestMessage: v1_dictionary_md_category_group_pb.ListCategoryGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_group_pb.ListCategoryGroupResponse|null) => void
  ): UnaryResponse;
  listCategoryGroup(
    requestMessage: v1_dictionary_md_category_group_pb.ListCategoryGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_group_pb.ListCategoryGroupResponse|null) => void
  ): UnaryResponse;
  saveCategoryGroup(
    requestMessage: v1_dictionary_md_category_group_pb.SaveCategoryGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_group_pb.SaveCategoryGroupResponse|null) => void
  ): UnaryResponse;
  saveCategoryGroup(
    requestMessage: v1_dictionary_md_category_group_pb.SaveCategoryGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_category_group_pb.SaveCategoryGroupResponse|null) => void
  ): UnaryResponse;
  getColor(
    requestMessage: v1_dictionary_md_color_pb.GetColorRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_color_pb.GetColorResponse|null) => void
  ): UnaryResponse;
  getColor(
    requestMessage: v1_dictionary_md_color_pb.GetColorRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_color_pb.GetColorResponse|null) => void
  ): UnaryResponse;
  listColor(
    requestMessage: v1_dictionary_md_color_pb.ListColorRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_color_pb.ListColorResponse|null) => void
  ): UnaryResponse;
  listColor(
    requestMessage: v1_dictionary_md_color_pb.ListColorRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_color_pb.ListColorResponse|null) => void
  ): UnaryResponse;
  saveColor(
    requestMessage: v1_dictionary_md_color_pb.SaveColorRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_color_pb.SaveColorResponse|null) => void
  ): UnaryResponse;
  saveColor(
    requestMessage: v1_dictionary_md_color_pb.SaveColorRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_color_pb.SaveColorResponse|null) => void
  ): UnaryResponse;
  getChemicalClassGroup(
    requestMessage: v1_dictionary_md_chemical_class_group_pb.GetChemicalClassGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_chemical_class_group_pb.GetChemicalClassGroupResponse|null) => void
  ): UnaryResponse;
  getChemicalClassGroup(
    requestMessage: v1_dictionary_md_chemical_class_group_pb.GetChemicalClassGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_chemical_class_group_pb.GetChemicalClassGroupResponse|null) => void
  ): UnaryResponse;
  listChemicalClassGroup(
    requestMessage: v1_dictionary_md_chemical_class_group_pb.ListChemicalClassGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_chemical_class_group_pb.ListChemicalClassGroupResponse|null) => void
  ): UnaryResponse;
  listChemicalClassGroup(
    requestMessage: v1_dictionary_md_chemical_class_group_pb.ListChemicalClassGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_chemical_class_group_pb.ListChemicalClassGroupResponse|null) => void
  ): UnaryResponse;
  saveChemicalClassGroup(
    requestMessage: v1_dictionary_md_chemical_class_group_pb.SaveChemicalClassGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_chemical_class_group_pb.SaveChemicalClassGroupResponse|null) => void
  ): UnaryResponse;
  saveChemicalClassGroup(
    requestMessage: v1_dictionary_md_chemical_class_group_pb.SaveChemicalClassGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_chemical_class_group_pb.SaveChemicalClassGroupResponse|null) => void
  ): UnaryResponse;
  getDistributionTypeChemical(
    requestMessage: v1_dictionary_md_distribution_type_chemical_pb.GetDistributionTypeChemicalRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_distribution_type_chemical_pb.GetDistributionTypeChemicalResponse|null) => void
  ): UnaryResponse;
  getDistributionTypeChemical(
    requestMessage: v1_dictionary_md_distribution_type_chemical_pb.GetDistributionTypeChemicalRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_distribution_type_chemical_pb.GetDistributionTypeChemicalResponse|null) => void
  ): UnaryResponse;
  listDistributionTypeChemical(
    requestMessage: v1_dictionary_md_distribution_type_chemical_pb.ListDistributionTypeChemicalRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_distribution_type_chemical_pb.ListDistributionTypeChemicalResponse|null) => void
  ): UnaryResponse;
  listDistributionTypeChemical(
    requestMessage: v1_dictionary_md_distribution_type_chemical_pb.ListDistributionTypeChemicalRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_distribution_type_chemical_pb.ListDistributionTypeChemicalResponse|null) => void
  ): UnaryResponse;
  saveDistributionTypeChemical(
    requestMessage: v1_dictionary_md_distribution_type_chemical_pb.SaveDistributionTypeChemicalRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_distribution_type_chemical_pb.SaveDistributionTypeChemicalResponse|null) => void
  ): UnaryResponse;
  saveDistributionTypeChemical(
    requestMessage: v1_dictionary_md_distribution_type_chemical_pb.SaveDistributionTypeChemicalRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_distribution_type_chemical_pb.SaveDistributionTypeChemicalResponse|null) => void
  ): UnaryResponse;
  getProductFile(
    requestMessage: v1_dictionary_md_file_pb.GetProductFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_file_pb.GetProductFileResponse|null) => void
  ): UnaryResponse;
  getProductFile(
    requestMessage: v1_dictionary_md_file_pb.GetProductFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_file_pb.GetProductFileResponse|null) => void
  ): UnaryResponse;
  listProductFile(
    requestMessage: v1_dictionary_md_file_pb.ListProductFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_file_pb.ListProductFileResponse|null) => void
  ): UnaryResponse;
  listProductFile(
    requestMessage: v1_dictionary_md_file_pb.ListProductFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_file_pb.ListProductFileResponse|null) => void
  ): UnaryResponse;
  listGroupedPackageFile(
    requestMessage: v1_dictionary_md_file_pb.ListGroupedPackageFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_file_pb.ListGroupedPackageFileResponse|null) => void
  ): UnaryResponse;
  listGroupedPackageFile(
    requestMessage: v1_dictionary_md_file_pb.ListGroupedPackageFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_file_pb.ListGroupedPackageFileResponse|null) => void
  ): UnaryResponse;
  saveProductFile(
    requestMessage: v1_dictionary_md_file_pb.SaveProductFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_file_pb.SaveProductFileResponse|null) => void
  ): UnaryResponse;
  saveProductFile(
    requestMessage: v1_dictionary_md_file_pb.SaveProductFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_file_pb.SaveProductFileResponse|null) => void
  ): UnaryResponse;
  deleteProductFile(
    requestMessage: v1_dictionary_md_file_pb.DeleteProductFileRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_file_pb.DeleteProductFileResponse|null) => void
  ): UnaryResponse;
  deleteProductFile(
    requestMessage: v1_dictionary_md_file_pb.DeleteProductFileRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_file_pb.DeleteProductFileResponse|null) => void
  ): UnaryResponse;
  getPreparativeForm(
    requestMessage: v1_dictionary_md_preparative_form_pb.GetPreparativeFormRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_preparative_form_pb.GetPreparativeFormResponse|null) => void
  ): UnaryResponse;
  getPreparativeForm(
    requestMessage: v1_dictionary_md_preparative_form_pb.GetPreparativeFormRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_preparative_form_pb.GetPreparativeFormResponse|null) => void
  ): UnaryResponse;
  listPreparativeForm(
    requestMessage: v1_dictionary_md_preparative_form_pb.ListPreparativeFormRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_preparative_form_pb.ListPreparativeFormResponse|null) => void
  ): UnaryResponse;
  listPreparativeForm(
    requestMessage: v1_dictionary_md_preparative_form_pb.ListPreparativeFormRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_preparative_form_pb.ListPreparativeFormResponse|null) => void
  ): UnaryResponse;
  savePreparativeForm(
    requestMessage: v1_dictionary_md_preparative_form_pb.SavePreparativeFormRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_preparative_form_pb.SavePreparativeFormResponse|null) => void
  ): UnaryResponse;
  savePreparativeForm(
    requestMessage: v1_dictionary_md_preparative_form_pb.SavePreparativeFormRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_preparative_form_pb.SavePreparativeFormResponse|null) => void
  ): UnaryResponse;
  getProduct(
    requestMessage: v1_dictionary_md_product_pb.GetProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_pb.GetProductResponse|null) => void
  ): UnaryResponse;
  getProduct(
    requestMessage: v1_dictionary_md_product_pb.GetProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_pb.GetProductResponse|null) => void
  ): UnaryResponse;
  listProduct(
    requestMessage: v1_dictionary_md_product_pb.ListProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_pb.ListProductResponse|null) => void
  ): UnaryResponse;
  listProduct(
    requestMessage: v1_dictionary_md_product_pb.ListProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_pb.ListProductResponse|null) => void
  ): UnaryResponse;
  listProductIds(
    requestMessage: v1_dictionary_md_product_pb.ListProductIdsRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_pb.ListProductIdsResponse|null) => void
  ): UnaryResponse;
  listProductIds(
    requestMessage: v1_dictionary_md_product_pb.ListProductIdsRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_pb.ListProductIdsResponse|null) => void
  ): UnaryResponse;
  saveProduct(
    requestMessage: v1_dictionary_md_product_pb.SaveProductRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_pb.SaveProductResponse|null) => void
  ): UnaryResponse;
  saveProduct(
    requestMessage: v1_dictionary_md_product_pb.SaveProductRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_pb.SaveProductResponse|null) => void
  ): UnaryResponse;
  getProductActiveSubstance(
    requestMessage: v1_dictionary_md_product_active_substance_pb.GetProductActiveSubstanceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_active_substance_pb.GetProductActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  getProductActiveSubstance(
    requestMessage: v1_dictionary_md_product_active_substance_pb.GetProductActiveSubstanceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_active_substance_pb.GetProductActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  listProductActiveSubstance(
    requestMessage: v1_dictionary_md_product_active_substance_pb.ListProductActiveSubstanceRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_active_substance_pb.ListProductActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  listProductActiveSubstance(
    requestMessage: v1_dictionary_md_product_active_substance_pb.ListProductActiveSubstanceRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_active_substance_pb.ListProductActiveSubstanceResponse|null) => void
  ): UnaryResponse;
  getProductCategory(
    requestMessage: v1_dictionary_md_product_category_pb.GetProductCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_category_pb.GetProductCategoryResponse|null) => void
  ): UnaryResponse;
  getProductCategory(
    requestMessage: v1_dictionary_md_product_category_pb.GetProductCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_category_pb.GetProductCategoryResponse|null) => void
  ): UnaryResponse;
  listProductCategory(
    requestMessage: v1_dictionary_md_product_category_pb.ListProductCategoryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_category_pb.ListProductCategoryResponse|null) => void
  ): UnaryResponse;
  listProductCategory(
    requestMessage: v1_dictionary_md_product_category_pb.ListProductCategoryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_category_pb.ListProductCategoryResponse|null) => void
  ): UnaryResponse;
  getProductCategoryApplication(
    requestMessage: v1_dictionary_md_product_category_application_pb.GetProductCategoryApplicationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_category_application_pb.GetProductCategoryApplicationResponse|null) => void
  ): UnaryResponse;
  getProductCategoryApplication(
    requestMessage: v1_dictionary_md_product_category_application_pb.GetProductCategoryApplicationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_category_application_pb.GetProductCategoryApplicationResponse|null) => void
  ): UnaryResponse;
  listProductCategoryApplication(
    requestMessage: v1_dictionary_md_product_category_application_pb.ListProductCategoryApplicationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_category_application_pb.ListProductCategoryApplicationResponse|null) => void
  ): UnaryResponse;
  listProductCategoryApplication(
    requestMessage: v1_dictionary_md_product_category_application_pb.ListProductCategoryApplicationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_category_application_pb.ListProductCategoryApplicationResponse|null) => void
  ): UnaryResponse;
  saveProductCategoryApplication(
    requestMessage: v1_dictionary_md_product_category_application_pb.SaveProductCategoryApplicationRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_category_application_pb.SaveProductCategoryApplicationResponse|null) => void
  ): UnaryResponse;
  saveProductCategoryApplication(
    requestMessage: v1_dictionary_md_product_category_application_pb.SaveProductCategoryApplicationRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_category_application_pb.SaveProductCategoryApplicationResponse|null) => void
  ): UnaryResponse;
  getProductGroup(
    requestMessage: v1_dictionary_md_product_group_pb.GetProductGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_group_pb.GetProductGroupResponse|null) => void
  ): UnaryResponse;
  getProductGroup(
    requestMessage: v1_dictionary_md_product_group_pb.GetProductGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_group_pb.GetProductGroupResponse|null) => void
  ): UnaryResponse;
  listProductGroup(
    requestMessage: v1_dictionary_md_product_group_pb.ListProductGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_group_pb.ListProductGroupResponse|null) => void
  ): UnaryResponse;
  listProductGroup(
    requestMessage: v1_dictionary_md_product_group_pb.ListProductGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_group_pb.ListProductGroupResponse|null) => void
  ): UnaryResponse;
  saveProductGroup(
    requestMessage: v1_dictionary_md_product_group_pb.SaveProductGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_group_pb.SaveProductGroupResponse|null) => void
  ): UnaryResponse;
  saveProductGroup(
    requestMessage: v1_dictionary_md_product_group_pb.SaveProductGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_group_pb.SaveProductGroupResponse|null) => void
  ): UnaryResponse;
  getProductSubGroup(
    requestMessage: v1_dictionary_md_product_sub_group_pb.GetProductSubGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_sub_group_pb.GetProductSubGroupResponse|null) => void
  ): UnaryResponse;
  getProductSubGroup(
    requestMessage: v1_dictionary_md_product_sub_group_pb.GetProductSubGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_sub_group_pb.GetProductSubGroupResponse|null) => void
  ): UnaryResponse;
  listProductSubGroup(
    requestMessage: v1_dictionary_md_product_sub_group_pb.ListProductSubGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_sub_group_pb.ListProductSubGroupResponse|null) => void
  ): UnaryResponse;
  listProductSubGroup(
    requestMessage: v1_dictionary_md_product_sub_group_pb.ListProductSubGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_sub_group_pb.ListProductSubGroupResponse|null) => void
  ): UnaryResponse;
  saveProductSubGroup(
    requestMessage: v1_dictionary_md_product_sub_group_pb.SaveProductSubGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_sub_group_pb.SaveProductSubGroupResponse|null) => void
  ): UnaryResponse;
  saveProductSubGroup(
    requestMessage: v1_dictionary_md_product_sub_group_pb.SaveProductSubGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_sub_group_pb.SaveProductSubGroupResponse|null) => void
  ): UnaryResponse;
  getProductPackage(
    requestMessage: v1_dictionary_md_product_package_pb.GetProductPackageRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_package_pb.GetProductPackageResponse|null) => void
  ): UnaryResponse;
  getProductPackage(
    requestMessage: v1_dictionary_md_product_package_pb.GetProductPackageRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_package_pb.GetProductPackageResponse|null) => void
  ): UnaryResponse;
  listProductPackage(
    requestMessage: v1_dictionary_md_product_package_pb.ListProductPackageRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_package_pb.ListProductPackageResponse|null) => void
  ): UnaryResponse;
  listProductPackage(
    requestMessage: v1_dictionary_md_product_package_pb.ListProductPackageRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_package_pb.ListProductPackageResponse|null) => void
  ): UnaryResponse;
  getProductPreparativeForm(
    requestMessage: v1_dictionary_md_product_preparative_form_pb.GetProductPreparativeFormRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_preparative_form_pb.GetProductPreparativeFormResponse|null) => void
  ): UnaryResponse;
  getProductPreparativeForm(
    requestMessage: v1_dictionary_md_product_preparative_form_pb.GetProductPreparativeFormRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_preparative_form_pb.GetProductPreparativeFormResponse|null) => void
  ): UnaryResponse;
  listProductPreparativeForm(
    requestMessage: v1_dictionary_md_product_preparative_form_pb.ListProductPreparativeFormRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_preparative_form_pb.ListProductPreparativeFormResponse|null) => void
  ): UnaryResponse;
  listProductPreparativeForm(
    requestMessage: v1_dictionary_md_product_preparative_form_pb.ListProductPreparativeFormRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_preparative_form_pb.ListProductPreparativeFormResponse|null) => void
  ): UnaryResponse;
  getProductQuantityType(
    requestMessage: v1_dictionary_md_product_quantity_type_pb.GetProductQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_quantity_type_pb.GetProductQuantityTypeResponse|null) => void
  ): UnaryResponse;
  getProductQuantityType(
    requestMessage: v1_dictionary_md_product_quantity_type_pb.GetProductQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_quantity_type_pb.GetProductQuantityTypeResponse|null) => void
  ): UnaryResponse;
  listProductQuantityType(
    requestMessage: v1_dictionary_md_product_quantity_type_pb.ListProductQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_quantity_type_pb.ListProductQuantityTypeResponse|null) => void
  ): UnaryResponse;
  listProductQuantityType(
    requestMessage: v1_dictionary_md_product_quantity_type_pb.ListProductQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_product_quantity_type_pb.ListProductQuantityTypeResponse|null) => void
  ): UnaryResponse;
  getSpectrumActionChemical(
    requestMessage: v1_dictionary_md_spectrum_action_chemical_pb.GetSpectrumActionChemicalRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_spectrum_action_chemical_pb.GetSpectrumActionChemicalResponse|null) => void
  ): UnaryResponse;
  getSpectrumActionChemical(
    requestMessage: v1_dictionary_md_spectrum_action_chemical_pb.GetSpectrumActionChemicalRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_spectrum_action_chemical_pb.GetSpectrumActionChemicalResponse|null) => void
  ): UnaryResponse;
  listSpectrumActionChemical(
    requestMessage: v1_dictionary_md_spectrum_action_chemical_pb.ListSpectrumActionChemicalRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_spectrum_action_chemical_pb.ListSpectrumActionChemicalResponse|null) => void
  ): UnaryResponse;
  listSpectrumActionChemical(
    requestMessage: v1_dictionary_md_spectrum_action_chemical_pb.ListSpectrumActionChemicalRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_spectrum_action_chemical_pb.ListSpectrumActionChemicalResponse|null) => void
  ): UnaryResponse;
  saveSpectrumActionChemical(
    requestMessage: v1_dictionary_md_spectrum_action_chemical_pb.SaveSpectrumActionChemicalRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_spectrum_action_chemical_pb.SaveSpectrumActionChemicalResponse|null) => void
  ): UnaryResponse;
  saveSpectrumActionChemical(
    requestMessage: v1_dictionary_md_spectrum_action_chemical_pb.SaveSpectrumActionChemicalRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_spectrum_action_chemical_pb.SaveSpectrumActionChemicalResponse|null) => void
  ): UnaryResponse;
  getToxicityClass(
    requestMessage: v1_dictionary_md_toxicity_class_pb.GetToxicityClassRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_toxicity_class_pb.GetToxicityClassResponse|null) => void
  ): UnaryResponse;
  getToxicityClass(
    requestMessage: v1_dictionary_md_toxicity_class_pb.GetToxicityClassRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_toxicity_class_pb.GetToxicityClassResponse|null) => void
  ): UnaryResponse;
  listToxicityClass(
    requestMessage: v1_dictionary_md_toxicity_class_pb.ListToxicityClassRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_toxicity_class_pb.ListToxicityClassResponse|null) => void
  ): UnaryResponse;
  listToxicityClass(
    requestMessage: v1_dictionary_md_toxicity_class_pb.ListToxicityClassRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_toxicity_class_pb.ListToxicityClassResponse|null) => void
  ): UnaryResponse;
  saveToxicityClass(
    requestMessage: v1_dictionary_md_toxicity_class_pb.SaveToxicityClassRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_toxicity_class_pb.SaveToxicityClassResponse|null) => void
  ): UnaryResponse;
  saveToxicityClass(
    requestMessage: v1_dictionary_md_toxicity_class_pb.SaveToxicityClassRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_toxicity_class_pb.SaveToxicityClassResponse|null) => void
  ): UnaryResponse;
  getUnit(
    requestMessage: v1_dictionary_md_unit_pb.GetUnitRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_unit_pb.GetUnitResponse|null) => void
  ): UnaryResponse;
  getUnit(
    requestMessage: v1_dictionary_md_unit_pb.GetUnitRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_unit_pb.GetUnitResponse|null) => void
  ): UnaryResponse;
  listUnit(
    requestMessage: v1_dictionary_md_unit_pb.ListUnitRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_unit_pb.ListUnitResponse|null) => void
  ): UnaryResponse;
  listUnit(
    requestMessage: v1_dictionary_md_unit_pb.ListUnitRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_unit_pb.ListUnitResponse|null) => void
  ): UnaryResponse;
  saveUnit(
    requestMessage: v1_dictionary_md_unit_pb.SaveUnitRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_unit_pb.SaveUnitResponse|null) => void
  ): UnaryResponse;
  saveUnit(
    requestMessage: v1_dictionary_md_unit_pb.SaveUnitRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_unit_pb.SaveUnitResponse|null) => void
  ): UnaryResponse;
  getQuantityType(
    requestMessage: v1_dictionary_md_quantity_type_pb.GetQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_quantity_type_pb.GetQuantityTypeResponse|null) => void
  ): UnaryResponse;
  getQuantityType(
    requestMessage: v1_dictionary_md_quantity_type_pb.GetQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_quantity_type_pb.GetQuantityTypeResponse|null) => void
  ): UnaryResponse;
  listQuantityType(
    requestMessage: v1_dictionary_md_quantity_type_pb.ListQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_quantity_type_pb.ListQuantityTypeResponse|null) => void
  ): UnaryResponse;
  listQuantityType(
    requestMessage: v1_dictionary_md_quantity_type_pb.ListQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_quantity_type_pb.ListQuantityTypeResponse|null) => void
  ): UnaryResponse;
  saveQuantityType(
    requestMessage: v1_dictionary_md_quantity_type_pb.SaveQuantityTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_quantity_type_pb.SaveQuantityTypeResponse|null) => void
  ): UnaryResponse;
  saveQuantityType(
    requestMessage: v1_dictionary_md_quantity_type_pb.SaveQuantityTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_quantity_type_pb.SaveQuantityTypeResponse|null) => void
  ): UnaryResponse;
  getSortType(
    requestMessage: v1_dictionary_md_seed_sort_type_pb.GetSortTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_sort_type_pb.GetSortTypeResponse|null) => void
  ): UnaryResponse;
  getSortType(
    requestMessage: v1_dictionary_md_seed_sort_type_pb.GetSortTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_sort_type_pb.GetSortTypeResponse|null) => void
  ): UnaryResponse;
  listSortType(
    requestMessage: v1_dictionary_md_seed_sort_type_pb.ListSortTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_sort_type_pb.ListSortTypeResponse|null) => void
  ): UnaryResponse;
  listSortType(
    requestMessage: v1_dictionary_md_seed_sort_type_pb.ListSortTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_sort_type_pb.ListSortTypeResponse|null) => void
  ): UnaryResponse;
  saveSortType(
    requestMessage: v1_dictionary_md_seed_sort_type_pb.SaveSortTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_sort_type_pb.SaveSortTypeResponse|null) => void
  ): UnaryResponse;
  saveSortType(
    requestMessage: v1_dictionary_md_seed_sort_type_pb.SaveSortTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_sort_type_pb.SaveSortTypeResponse|null) => void
  ): UnaryResponse;
  getFruitForm(
    requestMessage: v1_dictionary_md_seed_fruit_form_pb.GetFruitFormRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_form_pb.GetFruitFormResponse|null) => void
  ): UnaryResponse;
  getFruitForm(
    requestMessage: v1_dictionary_md_seed_fruit_form_pb.GetFruitFormRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_form_pb.GetFruitFormResponse|null) => void
  ): UnaryResponse;
  listFruitForm(
    requestMessage: v1_dictionary_md_seed_fruit_form_pb.ListFruitFormRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_form_pb.ListFruitFormResponse|null) => void
  ): UnaryResponse;
  listFruitForm(
    requestMessage: v1_dictionary_md_seed_fruit_form_pb.ListFruitFormRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_form_pb.ListFruitFormResponse|null) => void
  ): UnaryResponse;
  saveFruitForm(
    requestMessage: v1_dictionary_md_seed_fruit_form_pb.SaveFruitFormRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_form_pb.SaveFruitFormResponse|null) => void
  ): UnaryResponse;
  saveFruitForm(
    requestMessage: v1_dictionary_md_seed_fruit_form_pb.SaveFruitFormRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_form_pb.SaveFruitFormResponse|null) => void
  ): UnaryResponse;
  getReproduction(
    requestMessage: v1_dictionary_md_seed_reproduction_pb.GetReproductionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_reproduction_pb.GetReproductionResponse|null) => void
  ): UnaryResponse;
  getReproduction(
    requestMessage: v1_dictionary_md_seed_reproduction_pb.GetReproductionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_reproduction_pb.GetReproductionResponse|null) => void
  ): UnaryResponse;
  listReproduction(
    requestMessage: v1_dictionary_md_seed_reproduction_pb.ListReproductionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_reproduction_pb.ListReproductionResponse|null) => void
  ): UnaryResponse;
  listReproduction(
    requestMessage: v1_dictionary_md_seed_reproduction_pb.ListReproductionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_reproduction_pb.ListReproductionResponse|null) => void
  ): UnaryResponse;
  saveReproduction(
    requestMessage: v1_dictionary_md_seed_reproduction_pb.SaveReproductionRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_reproduction_pb.SaveReproductionResponse|null) => void
  ): UnaryResponse;
  saveReproduction(
    requestMessage: v1_dictionary_md_seed_reproduction_pb.SaveReproductionRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_reproduction_pb.SaveReproductionResponse|null) => void
  ): UnaryResponse;
  getSpecies(
    requestMessage: v1_dictionary_md_seed_species_pb.GetSpeciesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_species_pb.GetSpeciesResponse|null) => void
  ): UnaryResponse;
  getSpecies(
    requestMessage: v1_dictionary_md_seed_species_pb.GetSpeciesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_species_pb.GetSpeciesResponse|null) => void
  ): UnaryResponse;
  listSpecies(
    requestMessage: v1_dictionary_md_seed_species_pb.ListSpeciesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_species_pb.ListSpeciesResponse|null) => void
  ): UnaryResponse;
  listSpecies(
    requestMessage: v1_dictionary_md_seed_species_pb.ListSpeciesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_species_pb.ListSpeciesResponse|null) => void
  ): UnaryResponse;
  saveSpecies(
    requestMessage: v1_dictionary_md_seed_species_pb.SaveSpeciesRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_species_pb.SaveSpeciesResponse|null) => void
  ): UnaryResponse;
  saveSpecies(
    requestMessage: v1_dictionary_md_seed_species_pb.SaveSpeciesRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_species_pb.SaveSpeciesResponse|null) => void
  ): UnaryResponse;
  getTechnologyType(
    requestMessage: v1_dictionary_md_seed_technology_type_pb.GetTechnologyTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_technology_type_pb.GetTechnologyTypeResponse|null) => void
  ): UnaryResponse;
  getTechnologyType(
    requestMessage: v1_dictionary_md_seed_technology_type_pb.GetTechnologyTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_technology_type_pb.GetTechnologyTypeResponse|null) => void
  ): UnaryResponse;
  listTechnologyType(
    requestMessage: v1_dictionary_md_seed_technology_type_pb.ListTechnologyTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_technology_type_pb.ListTechnologyTypeResponse|null) => void
  ): UnaryResponse;
  listTechnologyType(
    requestMessage: v1_dictionary_md_seed_technology_type_pb.ListTechnologyTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_technology_type_pb.ListTechnologyTypeResponse|null) => void
  ): UnaryResponse;
  saveTechnologyType(
    requestMessage: v1_dictionary_md_seed_technology_type_pb.SaveTechnologyTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_technology_type_pb.SaveTechnologyTypeResponse|null) => void
  ): UnaryResponse;
  saveTechnologyType(
    requestMessage: v1_dictionary_md_seed_technology_type_pb.SaveTechnologyTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_technology_type_pb.SaveTechnologyTypeResponse|null) => void
  ): UnaryResponse;
  getMaturityGroup(
    requestMessage: v1_dictionary_md_seed_maturity_group_pb.GetMaturityGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_maturity_group_pb.GetMaturityGroupResponse|null) => void
  ): UnaryResponse;
  getMaturityGroup(
    requestMessage: v1_dictionary_md_seed_maturity_group_pb.GetMaturityGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_maturity_group_pb.GetMaturityGroupResponse|null) => void
  ): UnaryResponse;
  listMaturityGroup(
    requestMessage: v1_dictionary_md_seed_maturity_group_pb.ListMaturityGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_maturity_group_pb.ListMaturityGroupResponse|null) => void
  ): UnaryResponse;
  listMaturityGroup(
    requestMessage: v1_dictionary_md_seed_maturity_group_pb.ListMaturityGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_maturity_group_pb.ListMaturityGroupResponse|null) => void
  ): UnaryResponse;
  saveMaturityGroup(
    requestMessage: v1_dictionary_md_seed_maturity_group_pb.SaveMaturityGroupRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_maturity_group_pb.SaveMaturityGroupResponse|null) => void
  ): UnaryResponse;
  saveMaturityGroup(
    requestMessage: v1_dictionary_md_seed_maturity_group_pb.SaveMaturityGroupRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_maturity_group_pb.SaveMaturityGroupResponse|null) => void
  ): UnaryResponse;
  getFruitAverageWeight(
    requestMessage: v1_dictionary_md_seed_fruit_average_weight_pb.GetFruitAverageWeightRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_average_weight_pb.GetFruitAverageWeightResponse|null) => void
  ): UnaryResponse;
  getFruitAverageWeight(
    requestMessage: v1_dictionary_md_seed_fruit_average_weight_pb.GetFruitAverageWeightRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_average_weight_pb.GetFruitAverageWeightResponse|null) => void
  ): UnaryResponse;
  listFruitAverageWeight(
    requestMessage: v1_dictionary_md_seed_fruit_average_weight_pb.ListFruitAverageWeightRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_average_weight_pb.ListFruitAverageWeightResponse|null) => void
  ): UnaryResponse;
  listFruitAverageWeight(
    requestMessage: v1_dictionary_md_seed_fruit_average_weight_pb.ListFruitAverageWeightRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_average_weight_pb.ListFruitAverageWeightResponse|null) => void
  ): UnaryResponse;
  saveFruitAverageWeight(
    requestMessage: v1_dictionary_md_seed_fruit_average_weight_pb.SaveFruitAverageWeightRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_average_weight_pb.SaveFruitAverageWeightResponse|null) => void
  ): UnaryResponse;
  saveFruitAverageWeight(
    requestMessage: v1_dictionary_md_seed_fruit_average_weight_pb.SaveFruitAverageWeightRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_fruit_average_weight_pb.SaveFruitAverageWeightResponse|null) => void
  ): UnaryResponse;
  getSeedType(
    requestMessage: v1_dictionary_md_seed_seed_type_pb.GetSeedTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_seed_type_pb.GetSeedTypeResponse|null) => void
  ): UnaryResponse;
  getSeedType(
    requestMessage: v1_dictionary_md_seed_seed_type_pb.GetSeedTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_seed_type_pb.GetSeedTypeResponse|null) => void
  ): UnaryResponse;
  listSeedType(
    requestMessage: v1_dictionary_md_seed_seed_type_pb.ListSeedTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_seed_type_pb.ListSeedTypeResponse|null) => void
  ): UnaryResponse;
  listSeedType(
    requestMessage: v1_dictionary_md_seed_seed_type_pb.ListSeedTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_seed_type_pb.ListSeedTypeResponse|null) => void
  ): UnaryResponse;
  saveSeedType(
    requestMessage: v1_dictionary_md_seed_seed_type_pb.SaveSeedTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_seed_type_pb.SaveSeedTypeResponse|null) => void
  ): UnaryResponse;
  saveSeedType(
    requestMessage: v1_dictionary_md_seed_seed_type_pb.SaveSeedTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_seed_type_pb.SaveSeedTypeResponse|null) => void
  ): UnaryResponse;
  getPollinationType(
    requestMessage: v1_dictionary_md_seed_pollination_type_pb.GetPollinationTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_pollination_type_pb.GetPollinationTypeResponse|null) => void
  ): UnaryResponse;
  getPollinationType(
    requestMessage: v1_dictionary_md_seed_pollination_type_pb.GetPollinationTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_pollination_type_pb.GetPollinationTypeResponse|null) => void
  ): UnaryResponse;
  listPollinationType(
    requestMessage: v1_dictionary_md_seed_pollination_type_pb.ListPollinationTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_pollination_type_pb.ListPollinationTypeResponse|null) => void
  ): UnaryResponse;
  listPollinationType(
    requestMessage: v1_dictionary_md_seed_pollination_type_pb.ListPollinationTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_pollination_type_pb.ListPollinationTypeResponse|null) => void
  ): UnaryResponse;
  savePollinationType(
    requestMessage: v1_dictionary_md_seed_pollination_type_pb.SavePollinationTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_pollination_type_pb.SavePollinationTypeResponse|null) => void
  ): UnaryResponse;
  savePollinationType(
    requestMessage: v1_dictionary_md_seed_pollination_type_pb.SavePollinationTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_pollination_type_pb.SavePollinationTypeResponse|null) => void
  ): UnaryResponse;
  getAdaptType(
    requestMessage: v1_dictionary_md_seed_adapt_type_pb.GetAdaptTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_adapt_type_pb.GetAdaptTypeResponse|null) => void
  ): UnaryResponse;
  getAdaptType(
    requestMessage: v1_dictionary_md_seed_adapt_type_pb.GetAdaptTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_adapt_type_pb.GetAdaptTypeResponse|null) => void
  ): UnaryResponse;
  listAdaptType(
    requestMessage: v1_dictionary_md_seed_adapt_type_pb.ListAdaptTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_adapt_type_pb.ListAdaptTypeResponse|null) => void
  ): UnaryResponse;
  listAdaptType(
    requestMessage: v1_dictionary_md_seed_adapt_type_pb.ListAdaptTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_adapt_type_pb.ListAdaptTypeResponse|null) => void
  ): UnaryResponse;
  saveAdaptType(
    requestMessage: v1_dictionary_md_seed_adapt_type_pb.SaveAdaptTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_adapt_type_pb.SaveAdaptTypeResponse|null) => void
  ): UnaryResponse;
  saveAdaptType(
    requestMessage: v1_dictionary_md_seed_adapt_type_pb.SaveAdaptTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_adapt_type_pb.SaveAdaptTypeResponse|null) => void
  ): UnaryResponse;
  getOriginCountry(
    requestMessage: v1_dictionary_md_seed_origin_country_pb.GetOriginCountryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_origin_country_pb.GetOriginCountryResponse|null) => void
  ): UnaryResponse;
  getOriginCountry(
    requestMessage: v1_dictionary_md_seed_origin_country_pb.GetOriginCountryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_origin_country_pb.GetOriginCountryResponse|null) => void
  ): UnaryResponse;
  listOriginCountry(
    requestMessage: v1_dictionary_md_seed_origin_country_pb.ListOriginCountryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_origin_country_pb.ListOriginCountryResponse|null) => void
  ): UnaryResponse;
  listOriginCountry(
    requestMessage: v1_dictionary_md_seed_origin_country_pb.ListOriginCountryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_origin_country_pb.ListOriginCountryResponse|null) => void
  ): UnaryResponse;
  saveOriginCountry(
    requestMessage: v1_dictionary_md_seed_origin_country_pb.SaveOriginCountryRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_origin_country_pb.SaveOriginCountryResponse|null) => void
  ): UnaryResponse;
  saveOriginCountry(
    requestMessage: v1_dictionary_md_seed_origin_country_pb.SaveOriginCountryRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_origin_country_pb.SaveOriginCountryResponse|null) => void
  ): UnaryResponse;
  getPlantType(
    requestMessage: v1_dictionary_md_seed_plant_type_pb.GetPlantTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_plant_type_pb.GetPlantTypeResponse|null) => void
  ): UnaryResponse;
  getPlantType(
    requestMessage: v1_dictionary_md_seed_plant_type_pb.GetPlantTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_plant_type_pb.GetPlantTypeResponse|null) => void
  ): UnaryResponse;
  listPlantType(
    requestMessage: v1_dictionary_md_seed_plant_type_pb.ListPlantTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_plant_type_pb.ListPlantTypeResponse|null) => void
  ): UnaryResponse;
  listPlantType(
    requestMessage: v1_dictionary_md_seed_plant_type_pb.ListPlantTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_plant_type_pb.ListPlantTypeResponse|null) => void
  ): UnaryResponse;
  savePlantType(
    requestMessage: v1_dictionary_md_seed_plant_type_pb.SavePlantTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_plant_type_pb.SavePlantTypeResponse|null) => void
  ): UnaryResponse;
  savePlantType(
    requestMessage: v1_dictionary_md_seed_plant_type_pb.SavePlantTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_plant_type_pb.SavePlantTypeResponse|null) => void
  ): UnaryResponse;
  getPurpose(
    requestMessage: v1_dictionary_md_seed_purpose_pb.GetPurposeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_purpose_pb.GetPurposeResponse|null) => void
  ): UnaryResponse;
  getPurpose(
    requestMessage: v1_dictionary_md_seed_purpose_pb.GetPurposeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_purpose_pb.GetPurposeResponse|null) => void
  ): UnaryResponse;
  listPurpose(
    requestMessage: v1_dictionary_md_seed_purpose_pb.ListPurposeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_purpose_pb.ListPurposeResponse|null) => void
  ): UnaryResponse;
  listPurpose(
    requestMessage: v1_dictionary_md_seed_purpose_pb.ListPurposeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_purpose_pb.ListPurposeResponse|null) => void
  ): UnaryResponse;
  savePurpose(
    requestMessage: v1_dictionary_md_seed_purpose_pb.SavePurposeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_purpose_pb.SavePurposeResponse|null) => void
  ): UnaryResponse;
  savePurpose(
    requestMessage: v1_dictionary_md_seed_purpose_pb.SavePurposeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_purpose_pb.SavePurposeResponse|null) => void
  ): UnaryResponse;
  getGrowType(
    requestMessage: v1_dictionary_md_seed_grow_type_pb.GetGrowTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_type_pb.GetGrowTypeResponse|null) => void
  ): UnaryResponse;
  getGrowType(
    requestMessage: v1_dictionary_md_seed_grow_type_pb.GetGrowTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_type_pb.GetGrowTypeResponse|null) => void
  ): UnaryResponse;
  listGrowType(
    requestMessage: v1_dictionary_md_seed_grow_type_pb.ListGrowTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_type_pb.ListGrowTypeResponse|null) => void
  ): UnaryResponse;
  listGrowType(
    requestMessage: v1_dictionary_md_seed_grow_type_pb.ListGrowTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_type_pb.ListGrowTypeResponse|null) => void
  ): UnaryResponse;
  saveGrowType(
    requestMessage: v1_dictionary_md_seed_grow_type_pb.SaveGrowTypeRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_type_pb.SaveGrowTypeResponse|null) => void
  ): UnaryResponse;
  saveGrowType(
    requestMessage: v1_dictionary_md_seed_grow_type_pb.SaveGrowTypeRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_type_pb.SaveGrowTypeResponse|null) => void
  ): UnaryResponse;
  getGrowSeason(
    requestMessage: v1_dictionary_md_seed_grow_season_pb.GetGrowSeasonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_season_pb.GetGrowSeasonResponse|null) => void
  ): UnaryResponse;
  getGrowSeason(
    requestMessage: v1_dictionary_md_seed_grow_season_pb.GetGrowSeasonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_season_pb.GetGrowSeasonResponse|null) => void
  ): UnaryResponse;
  listGrowSeason(
    requestMessage: v1_dictionary_md_seed_grow_season_pb.ListGrowSeasonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_season_pb.ListGrowSeasonResponse|null) => void
  ): UnaryResponse;
  listGrowSeason(
    requestMessage: v1_dictionary_md_seed_grow_season_pb.ListGrowSeasonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_season_pb.ListGrowSeasonResponse|null) => void
  ): UnaryResponse;
  saveGrowSeason(
    requestMessage: v1_dictionary_md_seed_grow_season_pb.SaveGrowSeasonRequest,
    metadata: grpc.Metadata,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_season_pb.SaveGrowSeasonResponse|null) => void
  ): UnaryResponse;
  saveGrowSeason(
    requestMessage: v1_dictionary_md_seed_grow_season_pb.SaveGrowSeasonRequest,
    callback: (error: ServiceError|null, responseMessage: v1_dictionary_md_seed_grow_season_pb.SaveGrowSeasonResponse|null) => void
  ): UnaryResponse;
}

