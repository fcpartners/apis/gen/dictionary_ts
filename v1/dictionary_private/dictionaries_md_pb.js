// source: v1/dictionary_private/dictionaries_md.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var v1_dictionary_private_database_pb = require('../../v1/dictionary_private/database_pb.js');
goog.object.extend(proto, v1_dictionary_private_database_pb);
var v1_dictionary_md_active_substance_pb = require('../../v1/dictionary_md/active_substance_pb.js');
goog.object.extend(proto, v1_dictionary_md_active_substance_pb);
var v1_dictionary_md_active_substance_unit_pb = require('../../v1/dictionary_md/active_substance_unit_pb.js');
goog.object.extend(proto, v1_dictionary_md_active_substance_unit_pb);
var v1_dictionary_md_application_method_pb = require('../../v1/dictionary_md/application_method_pb.js');
goog.object.extend(proto, v1_dictionary_md_application_method_pb);
var v1_dictionary_md_application_rate_unit_chemical_pb = require('../../v1/dictionary_md/application_rate_unit_chemical_pb.js');
goog.object.extend(proto, v1_dictionary_md_application_rate_unit_chemical_pb);
var v1_dictionary_md_brand_pb = require('../../v1/dictionary_md/brand_pb.js');
goog.object.extend(proto, v1_dictionary_md_brand_pb);
var v1_dictionary_md_category_pb = require('../../v1/dictionary_md/category_pb.js');
goog.object.extend(proto, v1_dictionary_md_category_pb);
var v1_dictionary_md_category_group_pb = require('../../v1/dictionary_md/category_group_pb.js');
goog.object.extend(proto, v1_dictionary_md_category_group_pb);
var v1_dictionary_md_chemical_class_group_pb = require('../../v1/dictionary_md/chemical_class_group_pb.js');
goog.object.extend(proto, v1_dictionary_md_chemical_class_group_pb);
var v1_dictionary_md_color_pb = require('../../v1/dictionary_md/color_pb.js');
goog.object.extend(proto, v1_dictionary_md_color_pb);
var v1_dictionary_md_distribution_type_chemical_pb = require('../../v1/dictionary_md/distribution_type_chemical_pb.js');
goog.object.extend(proto, v1_dictionary_md_distribution_type_chemical_pb);
var v1_dictionary_md_file_pb = require('../../v1/dictionary_md/file_pb.js');
goog.object.extend(proto, v1_dictionary_md_file_pb);
var v1_dictionary_md_preparative_form_pb = require('../../v1/dictionary_md/preparative_form_pb.js');
goog.object.extend(proto, v1_dictionary_md_preparative_form_pb);
var v1_dictionary_md_package_pb = require('../../v1/dictionary_md/package_pb.js');
goog.object.extend(proto, v1_dictionary_md_package_pb);
var v1_dictionary_md_product_pb = require('../../v1/dictionary_md/product_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_pb);
var v1_dictionary_md_product_package_pb = require('../../v1/dictionary_md/product_package_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_package_pb);
var v1_dictionary_md_product_active_substance_pb = require('../../v1/dictionary_md/product_active_substance_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_active_substance_pb);
var v1_dictionary_md_product_category_pb = require('../../v1/dictionary_md/product_category_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_category_pb);
var v1_dictionary_md_product_category_application_pb = require('../../v1/dictionary_md/product_category_application_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_category_application_pb);
var v1_dictionary_md_product_group_pb = require('../../v1/dictionary_md/product_group_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_group_pb);
var v1_dictionary_md_product_sub_group_pb = require('../../v1/dictionary_md/product_sub_group_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_sub_group_pb);
var v1_dictionary_md_product_preparative_form_pb = require('../../v1/dictionary_md/product_preparative_form_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_preparative_form_pb);
var v1_dictionary_md_product_quantity_type_pb = require('../../v1/dictionary_md/product_quantity_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_quantity_type_pb);
var v1_dictionary_md_spectrum_action_chemical_pb = require('../../v1/dictionary_md/spectrum_action_chemical_pb.js');
goog.object.extend(proto, v1_dictionary_md_spectrum_action_chemical_pb);
var v1_dictionary_md_toxicity_class_pb = require('../../v1/dictionary_md/toxicity_class_pb.js');
goog.object.extend(proto, v1_dictionary_md_toxicity_class_pb);
var v1_dictionary_md_unit_pb = require('../../v1/dictionary_md/unit_pb.js');
goog.object.extend(proto, v1_dictionary_md_unit_pb);
var v1_dictionary_md_quantity_type_pb = require('../../v1/dictionary_md/quantity_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_quantity_type_pb);
var v1_dictionary_md_seed_sort_type_pb = require('../../v1/dictionary_md_seed/sort_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_sort_type_pb);
var v1_dictionary_md_seed_adapt_type_pb = require('../../v1/dictionary_md_seed/adapt_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_adapt_type_pb);
var v1_dictionary_md_seed_fruit_average_weight_pb = require('../../v1/dictionary_md_seed/fruit_average_weight_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_fruit_average_weight_pb);
var v1_dictionary_md_seed_fruit_form_pb = require('../../v1/dictionary_md_seed/fruit_form_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_fruit_form_pb);
var v1_dictionary_md_seed_grow_type_pb = require('../../v1/dictionary_md_seed/grow_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_grow_type_pb);
var v1_dictionary_md_seed_grow_season_pb = require('../../v1/dictionary_md_seed/grow_season_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_grow_season_pb);
var v1_dictionary_md_seed_maturity_group_pb = require('../../v1/dictionary_md_seed/maturity_group_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_maturity_group_pb);
var v1_dictionary_md_seed_origin_country_pb = require('../../v1/dictionary_md_seed/origin_country_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_origin_country_pb);
var v1_dictionary_md_seed_plant_type_pb = require('../../v1/dictionary_md_seed/plant_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_plant_type_pb);
var v1_dictionary_md_seed_pollination_type_pb = require('../../v1/dictionary_md_seed/pollination_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_pollination_type_pb);
var v1_dictionary_md_seed_purpose_pb = require('../../v1/dictionary_md_seed/purpose_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_purpose_pb);
var v1_dictionary_md_seed_seed_type_pb = require('../../v1/dictionary_md_seed/seed_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_seed_type_pb);
var v1_dictionary_md_seed_species_pb = require('../../v1/dictionary_md_seed/species_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_species_pb);
var v1_dictionary_md_seed_technology_type_pb = require('../../v1/dictionary_md_seed/technology_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_technology_type_pb);
var v1_dictionary_md_seed_reproduction_pb = require('../../v1/dictionary_md_seed/reproduction_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_reproduction_pb);
