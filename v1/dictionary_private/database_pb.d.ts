// package: fcp.dictionary.v1.dictionary_private
// file: v1/dictionary_private/database.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_common_file_pb from "../../v1/dictionary_common/file_pb";

export class GetDbSnapshotRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDbSnapshotRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDbSnapshotRequest): GetDbSnapshotRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDbSnapshotRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDbSnapshotRequest;
  static deserializeBinaryFromReader(message: GetDbSnapshotRequest, reader: jspb.BinaryReader): GetDbSnapshotRequest;
}

export namespace GetDbSnapshotRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetDbSnapshotResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_dictionary_common_file_pb.DbSnapshot | undefined;
  setItem(value?: v1_dictionary_common_file_pb.DbSnapshot): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDbSnapshotResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetDbSnapshotResponse): GetDbSnapshotResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDbSnapshotResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDbSnapshotResponse;
  static deserializeBinaryFromReader(message: GetDbSnapshotResponse, reader: jspb.BinaryReader): GetDbSnapshotResponse;
}

export namespace GetDbSnapshotResponse {
  export type AsObject = {
    item?: v1_dictionary_common_file_pb.DbSnapshot.AsObject,
  }
}

export class ListDbSnapshotRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): v1_dictionary_common_file_pb.DbSnapshotFilter | undefined;
  setFilter(value?: v1_dictionary_common_file_pb.DbSnapshotFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDbSnapshotRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDbSnapshotRequest): ListDbSnapshotRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDbSnapshotRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDbSnapshotRequest;
  static deserializeBinaryFromReader(message: ListDbSnapshotRequest, reader: jspb.BinaryReader): ListDbSnapshotRequest;
}

export namespace ListDbSnapshotRequest {
  export type AsObject = {
    filter?: v1_dictionary_common_file_pb.DbSnapshotFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListDbSnapshotResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<v1_dictionary_common_file_pb.DbSnapshot>;
  setItemsList(value: Array<v1_dictionary_common_file_pb.DbSnapshot>): void;
  addItems(value?: v1_dictionary_common_file_pb.DbSnapshot, index?: number): v1_dictionary_common_file_pb.DbSnapshot;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDbSnapshotResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListDbSnapshotResponse): ListDbSnapshotResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDbSnapshotResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDbSnapshotResponse;
  static deserializeBinaryFromReader(message: ListDbSnapshotResponse, reader: jspb.BinaryReader): ListDbSnapshotResponse;
}

export namespace ListDbSnapshotResponse {
  export type AsObject = {
    itemsList: Array<v1_dictionary_common_file_pb.DbSnapshot.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class CreateDbSnapshotRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateDbSnapshotRequest.AsObject;
  static toObject(includeInstance: boolean, msg: CreateDbSnapshotRequest): CreateDbSnapshotRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateDbSnapshotRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateDbSnapshotRequest;
  static deserializeBinaryFromReader(message: CreateDbSnapshotRequest, reader: jspb.BinaryReader): CreateDbSnapshotRequest;
}

export namespace CreateDbSnapshotRequest {
  export type AsObject = {
  }
}

export class CreateDbSnapshotResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_dictionary_common_file_pb.DbSnapshot | undefined;
  setItem(value?: v1_dictionary_common_file_pb.DbSnapshot): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CreateDbSnapshotResponse.AsObject;
  static toObject(includeInstance: boolean, msg: CreateDbSnapshotResponse): CreateDbSnapshotResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CreateDbSnapshotResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CreateDbSnapshotResponse;
  static deserializeBinaryFromReader(message: CreateDbSnapshotResponse, reader: jspb.BinaryReader): CreateDbSnapshotResponse;
}

export namespace CreateDbSnapshotResponse {
  export type AsObject = {
    item?: v1_dictionary_common_file_pb.DbSnapshot.AsObject,
  }
}

