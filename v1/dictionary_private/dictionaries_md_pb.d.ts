// package: fcp.dictionary.v1.dictionary_private
// file: v1/dictionary_private/dictionaries_md.proto

import * as jspb from "google-protobuf";
import * as v1_dictionary_private_database_pb from "../../v1/dictionary_private/database_pb";
import * as v1_dictionary_md_active_substance_pb from "../../v1/dictionary_md/active_substance_pb";
import * as v1_dictionary_md_active_substance_unit_pb from "../../v1/dictionary_md/active_substance_unit_pb";
import * as v1_dictionary_md_application_method_pb from "../../v1/dictionary_md/application_method_pb";
import * as v1_dictionary_md_application_rate_unit_chemical_pb from "../../v1/dictionary_md/application_rate_unit_chemical_pb";
import * as v1_dictionary_md_brand_pb from "../../v1/dictionary_md/brand_pb";
import * as v1_dictionary_md_category_pb from "../../v1/dictionary_md/category_pb";
import * as v1_dictionary_md_category_group_pb from "../../v1/dictionary_md/category_group_pb";
import * as v1_dictionary_md_chemical_class_group_pb from "../../v1/dictionary_md/chemical_class_group_pb";
import * as v1_dictionary_md_color_pb from "../../v1/dictionary_md/color_pb";
import * as v1_dictionary_md_distribution_type_chemical_pb from "../../v1/dictionary_md/distribution_type_chemical_pb";
import * as v1_dictionary_md_file_pb from "../../v1/dictionary_md/file_pb";
import * as v1_dictionary_md_preparative_form_pb from "../../v1/dictionary_md/preparative_form_pb";
import * as v1_dictionary_md_package_pb from "../../v1/dictionary_md/package_pb";
import * as v1_dictionary_md_product_pb from "../../v1/dictionary_md/product_pb";
import * as v1_dictionary_md_product_package_pb from "../../v1/dictionary_md/product_package_pb";
import * as v1_dictionary_md_product_active_substance_pb from "../../v1/dictionary_md/product_active_substance_pb";
import * as v1_dictionary_md_product_category_pb from "../../v1/dictionary_md/product_category_pb";
import * as v1_dictionary_md_product_category_application_pb from "../../v1/dictionary_md/product_category_application_pb";
import * as v1_dictionary_md_product_group_pb from "../../v1/dictionary_md/product_group_pb";
import * as v1_dictionary_md_product_sub_group_pb from "../../v1/dictionary_md/product_sub_group_pb";
import * as v1_dictionary_md_product_preparative_form_pb from "../../v1/dictionary_md/product_preparative_form_pb";
import * as v1_dictionary_md_product_quantity_type_pb from "../../v1/dictionary_md/product_quantity_type_pb";
import * as v1_dictionary_md_spectrum_action_chemical_pb from "../../v1/dictionary_md/spectrum_action_chemical_pb";
import * as v1_dictionary_md_toxicity_class_pb from "../../v1/dictionary_md/toxicity_class_pb";
import * as v1_dictionary_md_unit_pb from "../../v1/dictionary_md/unit_pb";
import * as v1_dictionary_md_quantity_type_pb from "../../v1/dictionary_md/quantity_type_pb";
import * as v1_dictionary_md_seed_sort_type_pb from "../../v1/dictionary_md_seed/sort_type_pb";
import * as v1_dictionary_md_seed_adapt_type_pb from "../../v1/dictionary_md_seed/adapt_type_pb";
import * as v1_dictionary_md_seed_fruit_average_weight_pb from "../../v1/dictionary_md_seed/fruit_average_weight_pb";
import * as v1_dictionary_md_seed_fruit_form_pb from "../../v1/dictionary_md_seed/fruit_form_pb";
import * as v1_dictionary_md_seed_grow_type_pb from "../../v1/dictionary_md_seed/grow_type_pb";
import * as v1_dictionary_md_seed_grow_season_pb from "../../v1/dictionary_md_seed/grow_season_pb";
import * as v1_dictionary_md_seed_maturity_group_pb from "../../v1/dictionary_md_seed/maturity_group_pb";
import * as v1_dictionary_md_seed_origin_country_pb from "../../v1/dictionary_md_seed/origin_country_pb";
import * as v1_dictionary_md_seed_plant_type_pb from "../../v1/dictionary_md_seed/plant_type_pb";
import * as v1_dictionary_md_seed_pollination_type_pb from "../../v1/dictionary_md_seed/pollination_type_pb";
import * as v1_dictionary_md_seed_purpose_pb from "../../v1/dictionary_md_seed/purpose_pb";
import * as v1_dictionary_md_seed_seed_type_pb from "../../v1/dictionary_md_seed/seed_type_pb";
import * as v1_dictionary_md_seed_species_pb from "../../v1/dictionary_md_seed/species_pb";
import * as v1_dictionary_md_seed_technology_type_pb from "../../v1/dictionary_md_seed/technology_type_pb";
import * as v1_dictionary_md_seed_reproduction_pb from "../../v1/dictionary_md_seed/reproduction_pb";

