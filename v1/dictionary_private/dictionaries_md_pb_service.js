// package: fcp.dictionary.v1.dictionary_private
// file: v1/dictionary_private/dictionaries_md.proto

var v1_dictionary_private_dictionaries_md_pb = require("../../v1/dictionary_private/dictionaries_md_pb");
var v1_dictionary_private_database_pb = require("../../v1/dictionary_private/database_pb");
var v1_dictionary_md_active_substance_pb = require("../../v1/dictionary_md/active_substance_pb");
var v1_dictionary_md_active_substance_unit_pb = require("../../v1/dictionary_md/active_substance_unit_pb");
var v1_dictionary_md_application_method_pb = require("../../v1/dictionary_md/application_method_pb");
var v1_dictionary_md_application_rate_unit_chemical_pb = require("../../v1/dictionary_md/application_rate_unit_chemical_pb");
var v1_dictionary_md_brand_pb = require("../../v1/dictionary_md/brand_pb");
var v1_dictionary_md_category_pb = require("../../v1/dictionary_md/category_pb");
var v1_dictionary_md_category_group_pb = require("../../v1/dictionary_md/category_group_pb");
var v1_dictionary_md_chemical_class_group_pb = require("../../v1/dictionary_md/chemical_class_group_pb");
var v1_dictionary_md_color_pb = require("../../v1/dictionary_md/color_pb");
var v1_dictionary_md_distribution_type_chemical_pb = require("../../v1/dictionary_md/distribution_type_chemical_pb");
var v1_dictionary_md_file_pb = require("../../v1/dictionary_md/file_pb");
var v1_dictionary_md_preparative_form_pb = require("../../v1/dictionary_md/preparative_form_pb");
var v1_dictionary_md_product_pb = require("../../v1/dictionary_md/product_pb");
var v1_dictionary_md_product_package_pb = require("../../v1/dictionary_md/product_package_pb");
var v1_dictionary_md_product_active_substance_pb = require("../../v1/dictionary_md/product_active_substance_pb");
var v1_dictionary_md_product_category_pb = require("../../v1/dictionary_md/product_category_pb");
var v1_dictionary_md_product_category_application_pb = require("../../v1/dictionary_md/product_category_application_pb");
var v1_dictionary_md_product_group_pb = require("../../v1/dictionary_md/product_group_pb");
var v1_dictionary_md_product_sub_group_pb = require("../../v1/dictionary_md/product_sub_group_pb");
var v1_dictionary_md_product_preparative_form_pb = require("../../v1/dictionary_md/product_preparative_form_pb");
var v1_dictionary_md_product_quantity_type_pb = require("../../v1/dictionary_md/product_quantity_type_pb");
var v1_dictionary_md_spectrum_action_chemical_pb = require("../../v1/dictionary_md/spectrum_action_chemical_pb");
var v1_dictionary_md_toxicity_class_pb = require("../../v1/dictionary_md/toxicity_class_pb");
var v1_dictionary_md_unit_pb = require("../../v1/dictionary_md/unit_pb");
var v1_dictionary_md_quantity_type_pb = require("../../v1/dictionary_md/quantity_type_pb");
var v1_dictionary_md_seed_sort_type_pb = require("../../v1/dictionary_md_seed/sort_type_pb");
var v1_dictionary_md_seed_adapt_type_pb = require("../../v1/dictionary_md_seed/adapt_type_pb");
var v1_dictionary_md_seed_fruit_average_weight_pb = require("../../v1/dictionary_md_seed/fruit_average_weight_pb");
var v1_dictionary_md_seed_fruit_form_pb = require("../../v1/dictionary_md_seed/fruit_form_pb");
var v1_dictionary_md_seed_grow_type_pb = require("../../v1/dictionary_md_seed/grow_type_pb");
var v1_dictionary_md_seed_grow_season_pb = require("../../v1/dictionary_md_seed/grow_season_pb");
var v1_dictionary_md_seed_maturity_group_pb = require("../../v1/dictionary_md_seed/maturity_group_pb");
var v1_dictionary_md_seed_origin_country_pb = require("../../v1/dictionary_md_seed/origin_country_pb");
var v1_dictionary_md_seed_plant_type_pb = require("../../v1/dictionary_md_seed/plant_type_pb");
var v1_dictionary_md_seed_pollination_type_pb = require("../../v1/dictionary_md_seed/pollination_type_pb");
var v1_dictionary_md_seed_purpose_pb = require("../../v1/dictionary_md_seed/purpose_pb");
var v1_dictionary_md_seed_seed_type_pb = require("../../v1/dictionary_md_seed/seed_type_pb");
var v1_dictionary_md_seed_species_pb = require("../../v1/dictionary_md_seed/species_pb");
var v1_dictionary_md_seed_technology_type_pb = require("../../v1/dictionary_md_seed/technology_type_pb");
var v1_dictionary_md_seed_reproduction_pb = require("../../v1/dictionary_md_seed/reproduction_pb");
var grpc = require("@improbable-eng/grpc-web").grpc;

var DictionaryMdService = (function () {
  function DictionaryMdService() {}
  DictionaryMdService.serviceName = "fcp.dictionary.v1.dictionary_private.DictionaryMdService";
  return DictionaryMdService;
}());

DictionaryMdService.CreateDbSnapshot = {
  methodName: "CreateDbSnapshot",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_private_database_pb.CreateDbSnapshotRequest,
  responseType: v1_dictionary_private_database_pb.CreateDbSnapshotResponse
};

DictionaryMdService.GetDbSnapshot = {
  methodName: "GetDbSnapshot",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_private_database_pb.GetDbSnapshotRequest,
  responseType: v1_dictionary_private_database_pb.GetDbSnapshotResponse
};

DictionaryMdService.ListDbSnapshot = {
  methodName: "ListDbSnapshot",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_private_database_pb.ListDbSnapshotRequest,
  responseType: v1_dictionary_private_database_pb.ListDbSnapshotResponse
};

DictionaryMdService.GetActiveSubstanceUnit = {
  methodName: "GetActiveSubstanceUnit",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_active_substance_unit_pb.GetActiveSubstanceUnitRequest,
  responseType: v1_dictionary_md_active_substance_unit_pb.GetActiveSubstanceUnitResponse
};

DictionaryMdService.ListActiveSubstanceUnit = {
  methodName: "ListActiveSubstanceUnit",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_active_substance_unit_pb.ListActiveSubstanceUnitRequest,
  responseType: v1_dictionary_md_active_substance_unit_pb.ListActiveSubstanceUnitResponse
};

DictionaryMdService.GetActiveSubstance = {
  methodName: "GetActiveSubstance",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_active_substance_pb.GetActiveSubstanceRequest,
  responseType: v1_dictionary_md_active_substance_pb.GetActiveSubstanceResponse
};

DictionaryMdService.ListActiveSubstance = {
  methodName: "ListActiveSubstance",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_active_substance_pb.ListActiveSubstanceRequest,
  responseType: v1_dictionary_md_active_substance_pb.ListActiveSubstanceResponse
};

DictionaryMdService.SaveActiveSubstance = {
  methodName: "SaveActiveSubstance",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_active_substance_pb.SaveActiveSubstanceRequest,
  responseType: v1_dictionary_md_active_substance_pb.SaveActiveSubstanceResponse
};

DictionaryMdService.GetApplicationMethod = {
  methodName: "GetApplicationMethod",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_application_method_pb.GetApplicationMethodRequest,
  responseType: v1_dictionary_md_application_method_pb.GetApplicationMethodResponse
};

DictionaryMdService.ListApplicationMethod = {
  methodName: "ListApplicationMethod",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_application_method_pb.ListApplicationMethodRequest,
  responseType: v1_dictionary_md_application_method_pb.ListApplicationMethodResponse
};

DictionaryMdService.SaveApplicationMethod = {
  methodName: "SaveApplicationMethod",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_application_method_pb.SaveApplicationMethodRequest,
  responseType: v1_dictionary_md_application_method_pb.SaveApplicationMethodResponse
};

DictionaryMdService.GetApplicationRateUnitChemical = {
  methodName: "GetApplicationRateUnitChemical",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_application_rate_unit_chemical_pb.GetApplicationRateUnitChemicalRequest,
  responseType: v1_dictionary_md_application_rate_unit_chemical_pb.GetApplicationRateUnitChemicalResponse
};

DictionaryMdService.ListApplicationRateUnitChemical = {
  methodName: "ListApplicationRateUnitChemical",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_application_rate_unit_chemical_pb.ListApplicationRateUnitChemicalRequest,
  responseType: v1_dictionary_md_application_rate_unit_chemical_pb.ListApplicationRateUnitChemicalResponse
};

DictionaryMdService.SaveApplicationRateUnitChemical = {
  methodName: "SaveApplicationRateUnitChemical",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_application_rate_unit_chemical_pb.SaveApplicationRateUnitChemicalRequest,
  responseType: v1_dictionary_md_application_rate_unit_chemical_pb.SaveApplicationRateUnitChemicalResponse
};

DictionaryMdService.GetBrand = {
  methodName: "GetBrand",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_brand_pb.GetBrandRequest,
  responseType: v1_dictionary_md_brand_pb.GetBrandResponse
};

DictionaryMdService.ListBrand = {
  methodName: "ListBrand",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_brand_pb.ListBrandRequest,
  responseType: v1_dictionary_md_brand_pb.ListBrandResponse
};

DictionaryMdService.SaveBrand = {
  methodName: "SaveBrand",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_brand_pb.SaveBrandRequest,
  responseType: v1_dictionary_md_brand_pb.SaveBrandResponse
};

DictionaryMdService.GetCategory = {
  methodName: "GetCategory",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_category_pb.GetCategoryRequest,
  responseType: v1_dictionary_md_category_pb.GetCategoryResponse
};

DictionaryMdService.ListCategory = {
  methodName: "ListCategory",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_category_pb.ListCategoryRequest,
  responseType: v1_dictionary_md_category_pb.ListCategoryResponse
};

DictionaryMdService.SaveCategory = {
  methodName: "SaveCategory",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_category_pb.SaveCategoryRequest,
  responseType: v1_dictionary_md_category_pb.SaveCategoryResponse
};

DictionaryMdService.GetCategoryGroup = {
  methodName: "GetCategoryGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_category_group_pb.GetCategoryGroupRequest,
  responseType: v1_dictionary_md_category_group_pb.GetCategoryGroupResponse
};

DictionaryMdService.ListCategoryGroup = {
  methodName: "ListCategoryGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_category_group_pb.ListCategoryGroupRequest,
  responseType: v1_dictionary_md_category_group_pb.ListCategoryGroupResponse
};

DictionaryMdService.SaveCategoryGroup = {
  methodName: "SaveCategoryGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_category_group_pb.SaveCategoryGroupRequest,
  responseType: v1_dictionary_md_category_group_pb.SaveCategoryGroupResponse
};

DictionaryMdService.GetColor = {
  methodName: "GetColor",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_color_pb.GetColorRequest,
  responseType: v1_dictionary_md_color_pb.GetColorResponse
};

DictionaryMdService.ListColor = {
  methodName: "ListColor",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_color_pb.ListColorRequest,
  responseType: v1_dictionary_md_color_pb.ListColorResponse
};

DictionaryMdService.SaveColor = {
  methodName: "SaveColor",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_color_pb.SaveColorRequest,
  responseType: v1_dictionary_md_color_pb.SaveColorResponse
};

DictionaryMdService.GetChemicalClassGroup = {
  methodName: "GetChemicalClassGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_chemical_class_group_pb.GetChemicalClassGroupRequest,
  responseType: v1_dictionary_md_chemical_class_group_pb.GetChemicalClassGroupResponse
};

DictionaryMdService.ListChemicalClassGroup = {
  methodName: "ListChemicalClassGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_chemical_class_group_pb.ListChemicalClassGroupRequest,
  responseType: v1_dictionary_md_chemical_class_group_pb.ListChemicalClassGroupResponse
};

DictionaryMdService.SaveChemicalClassGroup = {
  methodName: "SaveChemicalClassGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_chemical_class_group_pb.SaveChemicalClassGroupRequest,
  responseType: v1_dictionary_md_chemical_class_group_pb.SaveChemicalClassGroupResponse
};

DictionaryMdService.GetDistributionTypeChemical = {
  methodName: "GetDistributionTypeChemical",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_distribution_type_chemical_pb.GetDistributionTypeChemicalRequest,
  responseType: v1_dictionary_md_distribution_type_chemical_pb.GetDistributionTypeChemicalResponse
};

DictionaryMdService.ListDistributionTypeChemical = {
  methodName: "ListDistributionTypeChemical",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_distribution_type_chemical_pb.ListDistributionTypeChemicalRequest,
  responseType: v1_dictionary_md_distribution_type_chemical_pb.ListDistributionTypeChemicalResponse
};

DictionaryMdService.SaveDistributionTypeChemical = {
  methodName: "SaveDistributionTypeChemical",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_distribution_type_chemical_pb.SaveDistributionTypeChemicalRequest,
  responseType: v1_dictionary_md_distribution_type_chemical_pb.SaveDistributionTypeChemicalResponse
};

DictionaryMdService.GetProductFile = {
  methodName: "GetProductFile",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_file_pb.GetProductFileRequest,
  responseType: v1_dictionary_md_file_pb.GetProductFileResponse
};

DictionaryMdService.ListProductFile = {
  methodName: "ListProductFile",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_file_pb.ListProductFileRequest,
  responseType: v1_dictionary_md_file_pb.ListProductFileResponse
};

DictionaryMdService.ListGroupedPackageFile = {
  methodName: "ListGroupedPackageFile",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_file_pb.ListGroupedPackageFileRequest,
  responseType: v1_dictionary_md_file_pb.ListGroupedPackageFileResponse
};

DictionaryMdService.SaveProductFile = {
  methodName: "SaveProductFile",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_file_pb.SaveProductFileRequest,
  responseType: v1_dictionary_md_file_pb.SaveProductFileResponse
};

DictionaryMdService.DeleteProductFile = {
  methodName: "DeleteProductFile",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_file_pb.DeleteProductFileRequest,
  responseType: v1_dictionary_md_file_pb.DeleteProductFileResponse
};

DictionaryMdService.GetPreparativeForm = {
  methodName: "GetPreparativeForm",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_preparative_form_pb.GetPreparativeFormRequest,
  responseType: v1_dictionary_md_preparative_form_pb.GetPreparativeFormResponse
};

DictionaryMdService.ListPreparativeForm = {
  methodName: "ListPreparativeForm",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_preparative_form_pb.ListPreparativeFormRequest,
  responseType: v1_dictionary_md_preparative_form_pb.ListPreparativeFormResponse
};

DictionaryMdService.SavePreparativeForm = {
  methodName: "SavePreparativeForm",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_preparative_form_pb.SavePreparativeFormRequest,
  responseType: v1_dictionary_md_preparative_form_pb.SavePreparativeFormResponse
};

DictionaryMdService.GetProduct = {
  methodName: "GetProduct",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_pb.GetProductRequest,
  responseType: v1_dictionary_md_product_pb.GetProductResponse
};

DictionaryMdService.ListProduct = {
  methodName: "ListProduct",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_pb.ListProductRequest,
  responseType: v1_dictionary_md_product_pb.ListProductResponse
};

DictionaryMdService.ListProductIds = {
  methodName: "ListProductIds",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_pb.ListProductIdsRequest,
  responseType: v1_dictionary_md_product_pb.ListProductIdsResponse
};

DictionaryMdService.SaveProduct = {
  methodName: "SaveProduct",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_pb.SaveProductRequest,
  responseType: v1_dictionary_md_product_pb.SaveProductResponse
};

DictionaryMdService.GetProductActiveSubstance = {
  methodName: "GetProductActiveSubstance",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_active_substance_pb.GetProductActiveSubstanceRequest,
  responseType: v1_dictionary_md_product_active_substance_pb.GetProductActiveSubstanceResponse
};

DictionaryMdService.ListProductActiveSubstance = {
  methodName: "ListProductActiveSubstance",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_active_substance_pb.ListProductActiveSubstanceRequest,
  responseType: v1_dictionary_md_product_active_substance_pb.ListProductActiveSubstanceResponse
};

DictionaryMdService.GetProductCategory = {
  methodName: "GetProductCategory",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_category_pb.GetProductCategoryRequest,
  responseType: v1_dictionary_md_product_category_pb.GetProductCategoryResponse
};

DictionaryMdService.ListProductCategory = {
  methodName: "ListProductCategory",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_category_pb.ListProductCategoryRequest,
  responseType: v1_dictionary_md_product_category_pb.ListProductCategoryResponse
};

DictionaryMdService.GetProductCategoryApplication = {
  methodName: "GetProductCategoryApplication",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_category_application_pb.GetProductCategoryApplicationRequest,
  responseType: v1_dictionary_md_product_category_application_pb.GetProductCategoryApplicationResponse
};

DictionaryMdService.ListProductCategoryApplication = {
  methodName: "ListProductCategoryApplication",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_category_application_pb.ListProductCategoryApplicationRequest,
  responseType: v1_dictionary_md_product_category_application_pb.ListProductCategoryApplicationResponse
};

DictionaryMdService.SaveProductCategoryApplication = {
  methodName: "SaveProductCategoryApplication",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_category_application_pb.SaveProductCategoryApplicationRequest,
  responseType: v1_dictionary_md_product_category_application_pb.SaveProductCategoryApplicationResponse
};

DictionaryMdService.GetProductGroup = {
  methodName: "GetProductGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_group_pb.GetProductGroupRequest,
  responseType: v1_dictionary_md_product_group_pb.GetProductGroupResponse
};

DictionaryMdService.ListProductGroup = {
  methodName: "ListProductGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_group_pb.ListProductGroupRequest,
  responseType: v1_dictionary_md_product_group_pb.ListProductGroupResponse
};

DictionaryMdService.SaveProductGroup = {
  methodName: "SaveProductGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_group_pb.SaveProductGroupRequest,
  responseType: v1_dictionary_md_product_group_pb.SaveProductGroupResponse
};

DictionaryMdService.GetProductSubGroup = {
  methodName: "GetProductSubGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_sub_group_pb.GetProductSubGroupRequest,
  responseType: v1_dictionary_md_product_sub_group_pb.GetProductSubGroupResponse
};

DictionaryMdService.ListProductSubGroup = {
  methodName: "ListProductSubGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_sub_group_pb.ListProductSubGroupRequest,
  responseType: v1_dictionary_md_product_sub_group_pb.ListProductSubGroupResponse
};

DictionaryMdService.SaveProductSubGroup = {
  methodName: "SaveProductSubGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_sub_group_pb.SaveProductSubGroupRequest,
  responseType: v1_dictionary_md_product_sub_group_pb.SaveProductSubGroupResponse
};

DictionaryMdService.GetProductPackage = {
  methodName: "GetProductPackage",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_package_pb.GetProductPackageRequest,
  responseType: v1_dictionary_md_product_package_pb.GetProductPackageResponse
};

DictionaryMdService.ListProductPackage = {
  methodName: "ListProductPackage",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_package_pb.ListProductPackageRequest,
  responseType: v1_dictionary_md_product_package_pb.ListProductPackageResponse
};

DictionaryMdService.GetProductPreparativeForm = {
  methodName: "GetProductPreparativeForm",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_preparative_form_pb.GetProductPreparativeFormRequest,
  responseType: v1_dictionary_md_product_preparative_form_pb.GetProductPreparativeFormResponse
};

DictionaryMdService.ListProductPreparativeForm = {
  methodName: "ListProductPreparativeForm",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_preparative_form_pb.ListProductPreparativeFormRequest,
  responseType: v1_dictionary_md_product_preparative_form_pb.ListProductPreparativeFormResponse
};

DictionaryMdService.GetProductQuantityType = {
  methodName: "GetProductQuantityType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_quantity_type_pb.GetProductQuantityTypeRequest,
  responseType: v1_dictionary_md_product_quantity_type_pb.GetProductQuantityTypeResponse
};

DictionaryMdService.ListProductQuantityType = {
  methodName: "ListProductQuantityType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_product_quantity_type_pb.ListProductQuantityTypeRequest,
  responseType: v1_dictionary_md_product_quantity_type_pb.ListProductQuantityTypeResponse
};

DictionaryMdService.GetSpectrumActionChemical = {
  methodName: "GetSpectrumActionChemical",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_spectrum_action_chemical_pb.GetSpectrumActionChemicalRequest,
  responseType: v1_dictionary_md_spectrum_action_chemical_pb.GetSpectrumActionChemicalResponse
};

DictionaryMdService.ListSpectrumActionChemical = {
  methodName: "ListSpectrumActionChemical",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_spectrum_action_chemical_pb.ListSpectrumActionChemicalRequest,
  responseType: v1_dictionary_md_spectrum_action_chemical_pb.ListSpectrumActionChemicalResponse
};

DictionaryMdService.SaveSpectrumActionChemical = {
  methodName: "SaveSpectrumActionChemical",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_spectrum_action_chemical_pb.SaveSpectrumActionChemicalRequest,
  responseType: v1_dictionary_md_spectrum_action_chemical_pb.SaveSpectrumActionChemicalResponse
};

DictionaryMdService.GetToxicityClass = {
  methodName: "GetToxicityClass",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_toxicity_class_pb.GetToxicityClassRequest,
  responseType: v1_dictionary_md_toxicity_class_pb.GetToxicityClassResponse
};

DictionaryMdService.ListToxicityClass = {
  methodName: "ListToxicityClass",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_toxicity_class_pb.ListToxicityClassRequest,
  responseType: v1_dictionary_md_toxicity_class_pb.ListToxicityClassResponse
};

DictionaryMdService.SaveToxicityClass = {
  methodName: "SaveToxicityClass",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_toxicity_class_pb.SaveToxicityClassRequest,
  responseType: v1_dictionary_md_toxicity_class_pb.SaveToxicityClassResponse
};

DictionaryMdService.GetUnit = {
  methodName: "GetUnit",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_unit_pb.GetUnitRequest,
  responseType: v1_dictionary_md_unit_pb.GetUnitResponse
};

DictionaryMdService.ListUnit = {
  methodName: "ListUnit",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_unit_pb.ListUnitRequest,
  responseType: v1_dictionary_md_unit_pb.ListUnitResponse
};

DictionaryMdService.SaveUnit = {
  methodName: "SaveUnit",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_unit_pb.SaveUnitRequest,
  responseType: v1_dictionary_md_unit_pb.SaveUnitResponse
};

DictionaryMdService.GetQuantityType = {
  methodName: "GetQuantityType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_quantity_type_pb.GetQuantityTypeRequest,
  responseType: v1_dictionary_md_quantity_type_pb.GetQuantityTypeResponse
};

DictionaryMdService.ListQuantityType = {
  methodName: "ListQuantityType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_quantity_type_pb.ListQuantityTypeRequest,
  responseType: v1_dictionary_md_quantity_type_pb.ListQuantityTypeResponse
};

DictionaryMdService.SaveQuantityType = {
  methodName: "SaveQuantityType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_quantity_type_pb.SaveQuantityTypeRequest,
  responseType: v1_dictionary_md_quantity_type_pb.SaveQuantityTypeResponse
};

DictionaryMdService.GetSortType = {
  methodName: "GetSortType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_sort_type_pb.GetSortTypeRequest,
  responseType: v1_dictionary_md_seed_sort_type_pb.GetSortTypeResponse
};

DictionaryMdService.ListSortType = {
  methodName: "ListSortType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_sort_type_pb.ListSortTypeRequest,
  responseType: v1_dictionary_md_seed_sort_type_pb.ListSortTypeResponse
};

DictionaryMdService.SaveSortType = {
  methodName: "SaveSortType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_sort_type_pb.SaveSortTypeRequest,
  responseType: v1_dictionary_md_seed_sort_type_pb.SaveSortTypeResponse
};

DictionaryMdService.GetFruitForm = {
  methodName: "GetFruitForm",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_fruit_form_pb.GetFruitFormRequest,
  responseType: v1_dictionary_md_seed_fruit_form_pb.GetFruitFormResponse
};

DictionaryMdService.ListFruitForm = {
  methodName: "ListFruitForm",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_fruit_form_pb.ListFruitFormRequest,
  responseType: v1_dictionary_md_seed_fruit_form_pb.ListFruitFormResponse
};

DictionaryMdService.SaveFruitForm = {
  methodName: "SaveFruitForm",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_fruit_form_pb.SaveFruitFormRequest,
  responseType: v1_dictionary_md_seed_fruit_form_pb.SaveFruitFormResponse
};

DictionaryMdService.GetReproduction = {
  methodName: "GetReproduction",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_reproduction_pb.GetReproductionRequest,
  responseType: v1_dictionary_md_seed_reproduction_pb.GetReproductionResponse
};

DictionaryMdService.ListReproduction = {
  methodName: "ListReproduction",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_reproduction_pb.ListReproductionRequest,
  responseType: v1_dictionary_md_seed_reproduction_pb.ListReproductionResponse
};

DictionaryMdService.SaveReproduction = {
  methodName: "SaveReproduction",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_reproduction_pb.SaveReproductionRequest,
  responseType: v1_dictionary_md_seed_reproduction_pb.SaveReproductionResponse
};

DictionaryMdService.GetSpecies = {
  methodName: "GetSpecies",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_species_pb.GetSpeciesRequest,
  responseType: v1_dictionary_md_seed_species_pb.GetSpeciesResponse
};

DictionaryMdService.ListSpecies = {
  methodName: "ListSpecies",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_species_pb.ListSpeciesRequest,
  responseType: v1_dictionary_md_seed_species_pb.ListSpeciesResponse
};

DictionaryMdService.SaveSpecies = {
  methodName: "SaveSpecies",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_species_pb.SaveSpeciesRequest,
  responseType: v1_dictionary_md_seed_species_pb.SaveSpeciesResponse
};

DictionaryMdService.GetTechnologyType = {
  methodName: "GetTechnologyType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_technology_type_pb.GetTechnologyTypeRequest,
  responseType: v1_dictionary_md_seed_technology_type_pb.GetTechnologyTypeResponse
};

DictionaryMdService.ListTechnologyType = {
  methodName: "ListTechnologyType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_technology_type_pb.ListTechnologyTypeRequest,
  responseType: v1_dictionary_md_seed_technology_type_pb.ListTechnologyTypeResponse
};

DictionaryMdService.SaveTechnologyType = {
  methodName: "SaveTechnologyType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_technology_type_pb.SaveTechnologyTypeRequest,
  responseType: v1_dictionary_md_seed_technology_type_pb.SaveTechnologyTypeResponse
};

DictionaryMdService.GetMaturityGroup = {
  methodName: "GetMaturityGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_maturity_group_pb.GetMaturityGroupRequest,
  responseType: v1_dictionary_md_seed_maturity_group_pb.GetMaturityGroupResponse
};

DictionaryMdService.ListMaturityGroup = {
  methodName: "ListMaturityGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_maturity_group_pb.ListMaturityGroupRequest,
  responseType: v1_dictionary_md_seed_maturity_group_pb.ListMaturityGroupResponse
};

DictionaryMdService.SaveMaturityGroup = {
  methodName: "SaveMaturityGroup",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_maturity_group_pb.SaveMaturityGroupRequest,
  responseType: v1_dictionary_md_seed_maturity_group_pb.SaveMaturityGroupResponse
};

DictionaryMdService.GetFruitAverageWeight = {
  methodName: "GetFruitAverageWeight",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_fruit_average_weight_pb.GetFruitAverageWeightRequest,
  responseType: v1_dictionary_md_seed_fruit_average_weight_pb.GetFruitAverageWeightResponse
};

DictionaryMdService.ListFruitAverageWeight = {
  methodName: "ListFruitAverageWeight",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_fruit_average_weight_pb.ListFruitAverageWeightRequest,
  responseType: v1_dictionary_md_seed_fruit_average_weight_pb.ListFruitAverageWeightResponse
};

DictionaryMdService.SaveFruitAverageWeight = {
  methodName: "SaveFruitAverageWeight",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_fruit_average_weight_pb.SaveFruitAverageWeightRequest,
  responseType: v1_dictionary_md_seed_fruit_average_weight_pb.SaveFruitAverageWeightResponse
};

DictionaryMdService.GetSeedType = {
  methodName: "GetSeedType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_seed_type_pb.GetSeedTypeRequest,
  responseType: v1_dictionary_md_seed_seed_type_pb.GetSeedTypeResponse
};

DictionaryMdService.ListSeedType = {
  methodName: "ListSeedType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_seed_type_pb.ListSeedTypeRequest,
  responseType: v1_dictionary_md_seed_seed_type_pb.ListSeedTypeResponse
};

DictionaryMdService.SaveSeedType = {
  methodName: "SaveSeedType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_seed_type_pb.SaveSeedTypeRequest,
  responseType: v1_dictionary_md_seed_seed_type_pb.SaveSeedTypeResponse
};

DictionaryMdService.GetPollinationType = {
  methodName: "GetPollinationType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_pollination_type_pb.GetPollinationTypeRequest,
  responseType: v1_dictionary_md_seed_pollination_type_pb.GetPollinationTypeResponse
};

DictionaryMdService.ListPollinationType = {
  methodName: "ListPollinationType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_pollination_type_pb.ListPollinationTypeRequest,
  responseType: v1_dictionary_md_seed_pollination_type_pb.ListPollinationTypeResponse
};

DictionaryMdService.SavePollinationType = {
  methodName: "SavePollinationType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_pollination_type_pb.SavePollinationTypeRequest,
  responseType: v1_dictionary_md_seed_pollination_type_pb.SavePollinationTypeResponse
};

DictionaryMdService.GetAdaptType = {
  methodName: "GetAdaptType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_adapt_type_pb.GetAdaptTypeRequest,
  responseType: v1_dictionary_md_seed_adapt_type_pb.GetAdaptTypeResponse
};

DictionaryMdService.ListAdaptType = {
  methodName: "ListAdaptType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_adapt_type_pb.ListAdaptTypeRequest,
  responseType: v1_dictionary_md_seed_adapt_type_pb.ListAdaptTypeResponse
};

DictionaryMdService.SaveAdaptType = {
  methodName: "SaveAdaptType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_adapt_type_pb.SaveAdaptTypeRequest,
  responseType: v1_dictionary_md_seed_adapt_type_pb.SaveAdaptTypeResponse
};

DictionaryMdService.GetOriginCountry = {
  methodName: "GetOriginCountry",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_origin_country_pb.GetOriginCountryRequest,
  responseType: v1_dictionary_md_seed_origin_country_pb.GetOriginCountryResponse
};

DictionaryMdService.ListOriginCountry = {
  methodName: "ListOriginCountry",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_origin_country_pb.ListOriginCountryRequest,
  responseType: v1_dictionary_md_seed_origin_country_pb.ListOriginCountryResponse
};

DictionaryMdService.SaveOriginCountry = {
  methodName: "SaveOriginCountry",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_origin_country_pb.SaveOriginCountryRequest,
  responseType: v1_dictionary_md_seed_origin_country_pb.SaveOriginCountryResponse
};

DictionaryMdService.GetPlantType = {
  methodName: "GetPlantType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_plant_type_pb.GetPlantTypeRequest,
  responseType: v1_dictionary_md_seed_plant_type_pb.GetPlantTypeResponse
};

DictionaryMdService.ListPlantType = {
  methodName: "ListPlantType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_plant_type_pb.ListPlantTypeRequest,
  responseType: v1_dictionary_md_seed_plant_type_pb.ListPlantTypeResponse
};

DictionaryMdService.SavePlantType = {
  methodName: "SavePlantType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_plant_type_pb.SavePlantTypeRequest,
  responseType: v1_dictionary_md_seed_plant_type_pb.SavePlantTypeResponse
};

DictionaryMdService.GetPurpose = {
  methodName: "GetPurpose",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_purpose_pb.GetPurposeRequest,
  responseType: v1_dictionary_md_seed_purpose_pb.GetPurposeResponse
};

DictionaryMdService.ListPurpose = {
  methodName: "ListPurpose",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_purpose_pb.ListPurposeRequest,
  responseType: v1_dictionary_md_seed_purpose_pb.ListPurposeResponse
};

DictionaryMdService.SavePurpose = {
  methodName: "SavePurpose",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_purpose_pb.SavePurposeRequest,
  responseType: v1_dictionary_md_seed_purpose_pb.SavePurposeResponse
};

DictionaryMdService.GetGrowType = {
  methodName: "GetGrowType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_grow_type_pb.GetGrowTypeRequest,
  responseType: v1_dictionary_md_seed_grow_type_pb.GetGrowTypeResponse
};

DictionaryMdService.ListGrowType = {
  methodName: "ListGrowType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_grow_type_pb.ListGrowTypeRequest,
  responseType: v1_dictionary_md_seed_grow_type_pb.ListGrowTypeResponse
};

DictionaryMdService.SaveGrowType = {
  methodName: "SaveGrowType",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_grow_type_pb.SaveGrowTypeRequest,
  responseType: v1_dictionary_md_seed_grow_type_pb.SaveGrowTypeResponse
};

DictionaryMdService.GetGrowSeason = {
  methodName: "GetGrowSeason",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_grow_season_pb.GetGrowSeasonRequest,
  responseType: v1_dictionary_md_seed_grow_season_pb.GetGrowSeasonResponse
};

DictionaryMdService.ListGrowSeason = {
  methodName: "ListGrowSeason",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_grow_season_pb.ListGrowSeasonRequest,
  responseType: v1_dictionary_md_seed_grow_season_pb.ListGrowSeasonResponse
};

DictionaryMdService.SaveGrowSeason = {
  methodName: "SaveGrowSeason",
  service: DictionaryMdService,
  requestStream: false,
  responseStream: false,
  requestType: v1_dictionary_md_seed_grow_season_pb.SaveGrowSeasonRequest,
  responseType: v1_dictionary_md_seed_grow_season_pb.SaveGrowSeasonResponse
};

exports.DictionaryMdService = DictionaryMdService;

function DictionaryMdServiceClient(serviceHost, options) {
  this.serviceHost = serviceHost;
  this.options = options || {};
}

DictionaryMdServiceClient.prototype.createDbSnapshot = function createDbSnapshot(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.CreateDbSnapshot, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getDbSnapshot = function getDbSnapshot(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetDbSnapshot, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listDbSnapshot = function listDbSnapshot(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListDbSnapshot, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getActiveSubstanceUnit = function getActiveSubstanceUnit(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetActiveSubstanceUnit, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listActiveSubstanceUnit = function listActiveSubstanceUnit(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListActiveSubstanceUnit, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getActiveSubstance = function getActiveSubstance(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetActiveSubstance, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listActiveSubstance = function listActiveSubstance(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListActiveSubstance, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveActiveSubstance = function saveActiveSubstance(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveActiveSubstance, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getApplicationMethod = function getApplicationMethod(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetApplicationMethod, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listApplicationMethod = function listApplicationMethod(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListApplicationMethod, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveApplicationMethod = function saveApplicationMethod(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveApplicationMethod, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getApplicationRateUnitChemical = function getApplicationRateUnitChemical(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetApplicationRateUnitChemical, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listApplicationRateUnitChemical = function listApplicationRateUnitChemical(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListApplicationRateUnitChemical, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveApplicationRateUnitChemical = function saveApplicationRateUnitChemical(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveApplicationRateUnitChemical, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getBrand = function getBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listBrand = function listBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveBrand = function saveBrand(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveBrand, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getCategory = function getCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listCategory = function listCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveCategory = function saveCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getCategoryGroup = function getCategoryGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetCategoryGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listCategoryGroup = function listCategoryGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListCategoryGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveCategoryGroup = function saveCategoryGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveCategoryGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getColor = function getColor(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetColor, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listColor = function listColor(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListColor, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveColor = function saveColor(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveColor, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getChemicalClassGroup = function getChemicalClassGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetChemicalClassGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listChemicalClassGroup = function listChemicalClassGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListChemicalClassGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveChemicalClassGroup = function saveChemicalClassGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveChemicalClassGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getDistributionTypeChemical = function getDistributionTypeChemical(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetDistributionTypeChemical, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listDistributionTypeChemical = function listDistributionTypeChemical(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListDistributionTypeChemical, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveDistributionTypeChemical = function saveDistributionTypeChemical(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveDistributionTypeChemical, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getProductFile = function getProductFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetProductFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listProductFile = function listProductFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListProductFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listGroupedPackageFile = function listGroupedPackageFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListGroupedPackageFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveProductFile = function saveProductFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveProductFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.deleteProductFile = function deleteProductFile(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.DeleteProductFile, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getPreparativeForm = function getPreparativeForm(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetPreparativeForm, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listPreparativeForm = function listPreparativeForm(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListPreparativeForm, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.savePreparativeForm = function savePreparativeForm(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SavePreparativeForm, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getProduct = function getProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listProduct = function listProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listProductIds = function listProductIds(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListProductIds, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveProduct = function saveProduct(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveProduct, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getProductActiveSubstance = function getProductActiveSubstance(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetProductActiveSubstance, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listProductActiveSubstance = function listProductActiveSubstance(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListProductActiveSubstance, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getProductCategory = function getProductCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetProductCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listProductCategory = function listProductCategory(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListProductCategory, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getProductCategoryApplication = function getProductCategoryApplication(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetProductCategoryApplication, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listProductCategoryApplication = function listProductCategoryApplication(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListProductCategoryApplication, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveProductCategoryApplication = function saveProductCategoryApplication(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveProductCategoryApplication, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getProductGroup = function getProductGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetProductGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listProductGroup = function listProductGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListProductGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveProductGroup = function saveProductGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveProductGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getProductSubGroup = function getProductSubGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetProductSubGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listProductSubGroup = function listProductSubGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListProductSubGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveProductSubGroup = function saveProductSubGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveProductSubGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getProductPackage = function getProductPackage(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetProductPackage, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listProductPackage = function listProductPackage(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListProductPackage, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getProductPreparativeForm = function getProductPreparativeForm(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetProductPreparativeForm, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listProductPreparativeForm = function listProductPreparativeForm(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListProductPreparativeForm, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getProductQuantityType = function getProductQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetProductQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listProductQuantityType = function listProductQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListProductQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getSpectrumActionChemical = function getSpectrumActionChemical(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetSpectrumActionChemical, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listSpectrumActionChemical = function listSpectrumActionChemical(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListSpectrumActionChemical, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveSpectrumActionChemical = function saveSpectrumActionChemical(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveSpectrumActionChemical, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getToxicityClass = function getToxicityClass(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetToxicityClass, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listToxicityClass = function listToxicityClass(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListToxicityClass, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveToxicityClass = function saveToxicityClass(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveToxicityClass, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getUnit = function getUnit(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetUnit, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listUnit = function listUnit(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListUnit, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveUnit = function saveUnit(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveUnit, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getQuantityType = function getQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listQuantityType = function listQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveQuantityType = function saveQuantityType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveQuantityType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getSortType = function getSortType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetSortType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listSortType = function listSortType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListSortType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveSortType = function saveSortType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveSortType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getFruitForm = function getFruitForm(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetFruitForm, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listFruitForm = function listFruitForm(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListFruitForm, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveFruitForm = function saveFruitForm(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveFruitForm, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getReproduction = function getReproduction(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetReproduction, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listReproduction = function listReproduction(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListReproduction, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveReproduction = function saveReproduction(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveReproduction, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getSpecies = function getSpecies(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetSpecies, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listSpecies = function listSpecies(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListSpecies, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveSpecies = function saveSpecies(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveSpecies, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getTechnologyType = function getTechnologyType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetTechnologyType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listTechnologyType = function listTechnologyType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListTechnologyType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveTechnologyType = function saveTechnologyType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveTechnologyType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getMaturityGroup = function getMaturityGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetMaturityGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listMaturityGroup = function listMaturityGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListMaturityGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveMaturityGroup = function saveMaturityGroup(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveMaturityGroup, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getFruitAverageWeight = function getFruitAverageWeight(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetFruitAverageWeight, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listFruitAverageWeight = function listFruitAverageWeight(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListFruitAverageWeight, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveFruitAverageWeight = function saveFruitAverageWeight(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveFruitAverageWeight, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getSeedType = function getSeedType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetSeedType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listSeedType = function listSeedType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListSeedType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveSeedType = function saveSeedType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveSeedType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getPollinationType = function getPollinationType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetPollinationType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listPollinationType = function listPollinationType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListPollinationType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.savePollinationType = function savePollinationType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SavePollinationType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getAdaptType = function getAdaptType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetAdaptType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listAdaptType = function listAdaptType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListAdaptType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveAdaptType = function saveAdaptType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveAdaptType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getOriginCountry = function getOriginCountry(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetOriginCountry, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listOriginCountry = function listOriginCountry(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListOriginCountry, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveOriginCountry = function saveOriginCountry(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveOriginCountry, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getPlantType = function getPlantType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetPlantType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listPlantType = function listPlantType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListPlantType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.savePlantType = function savePlantType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SavePlantType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getPurpose = function getPurpose(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetPurpose, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listPurpose = function listPurpose(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListPurpose, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.savePurpose = function savePurpose(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SavePurpose, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getGrowType = function getGrowType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetGrowType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listGrowType = function listGrowType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListGrowType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveGrowType = function saveGrowType(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveGrowType, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.getGrowSeason = function getGrowSeason(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.GetGrowSeason, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.listGrowSeason = function listGrowSeason(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.ListGrowSeason, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

DictionaryMdServiceClient.prototype.saveGrowSeason = function saveGrowSeason(requestMessage, metadata, callback) {
  if (arguments.length === 2) {
    callback = arguments[1];
  }
  var client = grpc.unary(DictionaryMdService.SaveGrowSeason, {
    request: requestMessage,
    host: this.serviceHost,
    metadata: metadata,
    transport: this.options.transport,
    debug: this.options.debug,
    onEnd: function (response) {
      if (callback) {
        if (response.status !== grpc.Code.OK) {
          var err = new Error(response.statusMessage);
          err.code = response.status;
          err.metadata = response.trailers;
          callback(err, null);
        } else {
          callback(null, response.message);
        }
      }
    }
  });
  return {
    cancel: function () {
      callback = null;
      client.close();
    }
  };
};

exports.DictionaryMdServiceClient = DictionaryMdServiceClient;

