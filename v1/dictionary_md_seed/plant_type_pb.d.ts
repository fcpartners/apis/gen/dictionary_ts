// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/plant_type.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class PlantType extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getType(): v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap];
  setType(value: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]): void;

  getProcess(): v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap];
  setProcess(value: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap]): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PlantType.AsObject;
  static toObject(includeInstance: boolean, msg: PlantType): PlantType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PlantType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PlantType;
  static deserializeBinaryFromReader(message: PlantType, reader: jspb.BinaryReader): PlantType;
}

export namespace PlantType {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    type: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap],
    process: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap],
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class PlantTypeFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasProcess(): boolean;
  clearProcess(): void;
  getProcess(): v1_dictionary_common_enum_product_pb.ProductProcessValue | undefined;
  setProcess(value?: v1_dictionary_common_enum_product_pb.ProductProcessValue): void;

  hasType(): boolean;
  clearType(): void;
  getType(): v1_dictionary_common_enum_product_pb.ProductTypeValue | undefined;
  setType(value?: v1_dictionary_common_enum_product_pb.ProductTypeValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PlantTypeFilter.AsObject;
  static toObject(includeInstance: boolean, msg: PlantTypeFilter): PlantTypeFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PlantTypeFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PlantTypeFilter;
  static deserializeBinaryFromReader(message: PlantTypeFilter, reader: jspb.BinaryReader): PlantTypeFilter;
}

export namespace PlantTypeFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    process?: v1_dictionary_common_enum_product_pb.ProductProcessValue.AsObject,
    type?: v1_dictionary_common_enum_product_pb.ProductTypeValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetPlantTypeRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPlantTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPlantTypeRequest): GetPlantTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPlantTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPlantTypeRequest;
  static deserializeBinaryFromReader(message: GetPlantTypeRequest, reader: jspb.BinaryReader): GetPlantTypeRequest;
}

export namespace GetPlantTypeRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetPlantTypeResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): PlantType | undefined;
  setItem(value?: PlantType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPlantTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetPlantTypeResponse): GetPlantTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPlantTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPlantTypeResponse;
  static deserializeBinaryFromReader(message: GetPlantTypeResponse, reader: jspb.BinaryReader): GetPlantTypeResponse;
}

export namespace GetPlantTypeResponse {
  export type AsObject = {
    item?: PlantType.AsObject,
  }
}

export class ListPlantTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PlantTypeFilter | undefined;
  setFilter(value?: PlantTypeFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPlantTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPlantTypeRequest): ListPlantTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPlantTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPlantTypeRequest;
  static deserializeBinaryFromReader(message: ListPlantTypeRequest, reader: jspb.BinaryReader): ListPlantTypeRequest;
}

export namespace ListPlantTypeRequest {
  export type AsObject = {
    filter?: PlantTypeFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListPlantTypeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<PlantType>;
  setItemsList(value: Array<PlantType>): void;
  addItems(value?: PlantType, index?: number): PlantType;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPlantTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPlantTypeResponse): ListPlantTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPlantTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPlantTypeResponse;
  static deserializeBinaryFromReader(message: ListPlantTypeResponse, reader: jspb.BinaryReader): ListPlantTypeResponse;
}

export namespace ListPlantTypeResponse {
  export type AsObject = {
    itemsList: Array<PlantType.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SavePlantTypeRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): PlantType | undefined;
  setItem(value?: PlantType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SavePlantTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SavePlantTypeRequest): SavePlantTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SavePlantTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SavePlantTypeRequest;
  static deserializeBinaryFromReader(message: SavePlantTypeRequest, reader: jspb.BinaryReader): SavePlantTypeRequest;
}

export namespace SavePlantTypeRequest {
  export type AsObject = {
    item?: PlantType.AsObject,
  }
}

export class SavePlantTypeResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SavePlantTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SavePlantTypeResponse): SavePlantTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SavePlantTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SavePlantTypeResponse;
  static deserializeBinaryFromReader(message: SavePlantTypeResponse, reader: jspb.BinaryReader): SavePlantTypeResponse;
}

export namespace SavePlantTypeResponse {
  export type AsObject = {
    id: number,
  }
}

