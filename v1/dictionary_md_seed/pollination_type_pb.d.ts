// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/pollination_type.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class PollinationType extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PollinationType.AsObject;
  static toObject(includeInstance: boolean, msg: PollinationType): PollinationType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PollinationType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PollinationType;
  static deserializeBinaryFromReader(message: PollinationType, reader: jspb.BinaryReader): PollinationType;
}

export namespace PollinationType {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class PollinationTypeFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PollinationTypeFilter.AsObject;
  static toObject(includeInstance: boolean, msg: PollinationTypeFilter): PollinationTypeFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PollinationTypeFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PollinationTypeFilter;
  static deserializeBinaryFromReader(message: PollinationTypeFilter, reader: jspb.BinaryReader): PollinationTypeFilter;
}

export namespace PollinationTypeFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetPollinationTypeRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPollinationTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPollinationTypeRequest): GetPollinationTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPollinationTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPollinationTypeRequest;
  static deserializeBinaryFromReader(message: GetPollinationTypeRequest, reader: jspb.BinaryReader): GetPollinationTypeRequest;
}

export namespace GetPollinationTypeRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetPollinationTypeResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): PollinationType | undefined;
  setItem(value?: PollinationType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPollinationTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetPollinationTypeResponse): GetPollinationTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPollinationTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPollinationTypeResponse;
  static deserializeBinaryFromReader(message: GetPollinationTypeResponse, reader: jspb.BinaryReader): GetPollinationTypeResponse;
}

export namespace GetPollinationTypeResponse {
  export type AsObject = {
    item?: PollinationType.AsObject,
  }
}

export class ListPollinationTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PollinationTypeFilter | undefined;
  setFilter(value?: PollinationTypeFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPollinationTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPollinationTypeRequest): ListPollinationTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPollinationTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPollinationTypeRequest;
  static deserializeBinaryFromReader(message: ListPollinationTypeRequest, reader: jspb.BinaryReader): ListPollinationTypeRequest;
}

export namespace ListPollinationTypeRequest {
  export type AsObject = {
    filter?: PollinationTypeFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListPollinationTypeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<PollinationType>;
  setItemsList(value: Array<PollinationType>): void;
  addItems(value?: PollinationType, index?: number): PollinationType;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPollinationTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPollinationTypeResponse): ListPollinationTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPollinationTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPollinationTypeResponse;
  static deserializeBinaryFromReader(message: ListPollinationTypeResponse, reader: jspb.BinaryReader): ListPollinationTypeResponse;
}

export namespace ListPollinationTypeResponse {
  export type AsObject = {
    itemsList: Array<PollinationType.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SavePollinationTypeRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): PollinationType | undefined;
  setItem(value?: PollinationType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SavePollinationTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SavePollinationTypeRequest): SavePollinationTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SavePollinationTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SavePollinationTypeRequest;
  static deserializeBinaryFromReader(message: SavePollinationTypeRequest, reader: jspb.BinaryReader): SavePollinationTypeRequest;
}

export namespace SavePollinationTypeRequest {
  export type AsObject = {
    item?: PollinationType.AsObject,
  }
}

export class SavePollinationTypeResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SavePollinationTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SavePollinationTypeResponse): SavePollinationTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SavePollinationTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SavePollinationTypeResponse;
  static deserializeBinaryFromReader(message: SavePollinationTypeResponse, reader: jspb.BinaryReader): SavePollinationTypeResponse;
}

export namespace SavePollinationTypeResponse {
  export type AsObject = {
    id: number,
  }
}

