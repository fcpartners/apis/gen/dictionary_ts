// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/seed_type.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class SeedType extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SeedType.AsObject;
  static toObject(includeInstance: boolean, msg: SeedType): SeedType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SeedType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SeedType;
  static deserializeBinaryFromReader(message: SeedType, reader: jspb.BinaryReader): SeedType;
}

export namespace SeedType {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class SeedTypeFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SeedTypeFilter.AsObject;
  static toObject(includeInstance: boolean, msg: SeedTypeFilter): SeedTypeFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SeedTypeFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SeedTypeFilter;
  static deserializeBinaryFromReader(message: SeedTypeFilter, reader: jspb.BinaryReader): SeedTypeFilter;
}

export namespace SeedTypeFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetSeedTypeRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSeedTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSeedTypeRequest): GetSeedTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSeedTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSeedTypeRequest;
  static deserializeBinaryFromReader(message: GetSeedTypeRequest, reader: jspb.BinaryReader): GetSeedTypeRequest;
}

export namespace GetSeedTypeRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetSeedTypeResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): SeedType | undefined;
  setItem(value?: SeedType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSeedTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetSeedTypeResponse): GetSeedTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSeedTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSeedTypeResponse;
  static deserializeBinaryFromReader(message: GetSeedTypeResponse, reader: jspb.BinaryReader): GetSeedTypeResponse;
}

export namespace GetSeedTypeResponse {
  export type AsObject = {
    item?: SeedType.AsObject,
  }
}

export class ListSeedTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): SeedTypeFilter | undefined;
  setFilter(value?: SeedTypeFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSeedTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListSeedTypeRequest): ListSeedTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSeedTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSeedTypeRequest;
  static deserializeBinaryFromReader(message: ListSeedTypeRequest, reader: jspb.BinaryReader): ListSeedTypeRequest;
}

export namespace ListSeedTypeRequest {
  export type AsObject = {
    filter?: SeedTypeFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListSeedTypeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<SeedType>;
  setItemsList(value: Array<SeedType>): void;
  addItems(value?: SeedType, index?: number): SeedType;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSeedTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListSeedTypeResponse): ListSeedTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSeedTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSeedTypeResponse;
  static deserializeBinaryFromReader(message: ListSeedTypeResponse, reader: jspb.BinaryReader): ListSeedTypeResponse;
}

export namespace ListSeedTypeResponse {
  export type AsObject = {
    itemsList: Array<SeedType.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveSeedTypeRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): SeedType | undefined;
  setItem(value?: SeedType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSeedTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSeedTypeRequest): SaveSeedTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSeedTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSeedTypeRequest;
  static deserializeBinaryFromReader(message: SaveSeedTypeRequest, reader: jspb.BinaryReader): SaveSeedTypeRequest;
}

export namespace SaveSeedTypeRequest {
  export type AsObject = {
    item?: SeedType.AsObject,
  }
}

export class SaveSeedTypeResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSeedTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSeedTypeResponse): SaveSeedTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSeedTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSeedTypeResponse;
  static deserializeBinaryFromReader(message: SaveSeedTypeResponse, reader: jspb.BinaryReader): SaveSeedTypeResponse;
}

export namespace SaveSeedTypeResponse {
  export type AsObject = {
    id: number,
  }
}

