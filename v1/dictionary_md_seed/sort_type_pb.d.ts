// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/sort_type.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class SortType extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getType(): v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap];
  setType(value: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]): void;

  getProcess(): v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap];
  setProcess(value: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap]): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SortType.AsObject;
  static toObject(includeInstance: boolean, msg: SortType): SortType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SortType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SortType;
  static deserializeBinaryFromReader(message: SortType, reader: jspb.BinaryReader): SortType;
}

export namespace SortType {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    type: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap],
    process: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap],
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class SortTypeFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasProcess(): boolean;
  clearProcess(): void;
  getProcess(): v1_dictionary_common_enum_product_pb.ProductProcessValue | undefined;
  setProcess(value?: v1_dictionary_common_enum_product_pb.ProductProcessValue): void;

  hasType(): boolean;
  clearType(): void;
  getType(): v1_dictionary_common_enum_product_pb.ProductTypeValue | undefined;
  setType(value?: v1_dictionary_common_enum_product_pb.ProductTypeValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SortTypeFilter.AsObject;
  static toObject(includeInstance: boolean, msg: SortTypeFilter): SortTypeFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SortTypeFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SortTypeFilter;
  static deserializeBinaryFromReader(message: SortTypeFilter, reader: jspb.BinaryReader): SortTypeFilter;
}

export namespace SortTypeFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    process?: v1_dictionary_common_enum_product_pb.ProductProcessValue.AsObject,
    type?: v1_dictionary_common_enum_product_pb.ProductTypeValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetSortTypeRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSortTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSortTypeRequest): GetSortTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSortTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSortTypeRequest;
  static deserializeBinaryFromReader(message: GetSortTypeRequest, reader: jspb.BinaryReader): GetSortTypeRequest;
}

export namespace GetSortTypeRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetSortTypeResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): SortType | undefined;
  setItem(value?: SortType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSortTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetSortTypeResponse): GetSortTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSortTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSortTypeResponse;
  static deserializeBinaryFromReader(message: GetSortTypeResponse, reader: jspb.BinaryReader): GetSortTypeResponse;
}

export namespace GetSortTypeResponse {
  export type AsObject = {
    item?: SortType.AsObject,
  }
}

export class ListSortTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): SortTypeFilter | undefined;
  setFilter(value?: SortTypeFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSortTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListSortTypeRequest): ListSortTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSortTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSortTypeRequest;
  static deserializeBinaryFromReader(message: ListSortTypeRequest, reader: jspb.BinaryReader): ListSortTypeRequest;
}

export namespace ListSortTypeRequest {
  export type AsObject = {
    filter?: SortTypeFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListSortTypeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<SortType>;
  setItemsList(value: Array<SortType>): void;
  addItems(value?: SortType, index?: number): SortType;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSortTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListSortTypeResponse): ListSortTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSortTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSortTypeResponse;
  static deserializeBinaryFromReader(message: ListSortTypeResponse, reader: jspb.BinaryReader): ListSortTypeResponse;
}

export namespace ListSortTypeResponse {
  export type AsObject = {
    itemsList: Array<SortType.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveSortTypeRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): SortType | undefined;
  setItem(value?: SortType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSortTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSortTypeRequest): SaveSortTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSortTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSortTypeRequest;
  static deserializeBinaryFromReader(message: SaveSortTypeRequest, reader: jspb.BinaryReader): SaveSortTypeRequest;
}

export namespace SaveSortTypeRequest {
  export type AsObject = {
    item?: SortType.AsObject,
  }
}

export class SaveSortTypeResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSortTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSortTypeResponse): SaveSortTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSortTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSortTypeResponse;
  static deserializeBinaryFromReader(message: SaveSortTypeResponse, reader: jspb.BinaryReader): SaveSortTypeResponse;
}

export namespace SaveSortTypeResponse {
  export type AsObject = {
    id: number,
  }
}

