// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/maturity_group.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class MaturityGroup extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MaturityGroup.AsObject;
  static toObject(includeInstance: boolean, msg: MaturityGroup): MaturityGroup.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MaturityGroup, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MaturityGroup;
  static deserializeBinaryFromReader(message: MaturityGroup, reader: jspb.BinaryReader): MaturityGroup;
}

export namespace MaturityGroup {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class MaturityGroupFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): MaturityGroupFilter.AsObject;
  static toObject(includeInstance: boolean, msg: MaturityGroupFilter): MaturityGroupFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: MaturityGroupFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): MaturityGroupFilter;
  static deserializeBinaryFromReader(message: MaturityGroupFilter, reader: jspb.BinaryReader): MaturityGroupFilter;
}

export namespace MaturityGroupFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetMaturityGroupRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMaturityGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetMaturityGroupRequest): GetMaturityGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMaturityGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMaturityGroupRequest;
  static deserializeBinaryFromReader(message: GetMaturityGroupRequest, reader: jspb.BinaryReader): GetMaturityGroupRequest;
}

export namespace GetMaturityGroupRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetMaturityGroupResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): MaturityGroup | undefined;
  setItem(value?: MaturityGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetMaturityGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetMaturityGroupResponse): GetMaturityGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetMaturityGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetMaturityGroupResponse;
  static deserializeBinaryFromReader(message: GetMaturityGroupResponse, reader: jspb.BinaryReader): GetMaturityGroupResponse;
}

export namespace GetMaturityGroupResponse {
  export type AsObject = {
    item?: MaturityGroup.AsObject,
  }
}

export class ListMaturityGroupRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): MaturityGroupFilter | undefined;
  setFilter(value?: MaturityGroupFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMaturityGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListMaturityGroupRequest): ListMaturityGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMaturityGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMaturityGroupRequest;
  static deserializeBinaryFromReader(message: ListMaturityGroupRequest, reader: jspb.BinaryReader): ListMaturityGroupRequest;
}

export namespace ListMaturityGroupRequest {
  export type AsObject = {
    filter?: MaturityGroupFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListMaturityGroupResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<MaturityGroup>;
  setItemsList(value: Array<MaturityGroup>): void;
  addItems(value?: MaturityGroup, index?: number): MaturityGroup;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListMaturityGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListMaturityGroupResponse): ListMaturityGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListMaturityGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListMaturityGroupResponse;
  static deserializeBinaryFromReader(message: ListMaturityGroupResponse, reader: jspb.BinaryReader): ListMaturityGroupResponse;
}

export namespace ListMaturityGroupResponse {
  export type AsObject = {
    itemsList: Array<MaturityGroup.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveMaturityGroupRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): MaturityGroup | undefined;
  setItem(value?: MaturityGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveMaturityGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveMaturityGroupRequest): SaveMaturityGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveMaturityGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveMaturityGroupRequest;
  static deserializeBinaryFromReader(message: SaveMaturityGroupRequest, reader: jspb.BinaryReader): SaveMaturityGroupRequest;
}

export namespace SaveMaturityGroupRequest {
  export type AsObject = {
    item?: MaturityGroup.AsObject,
  }
}

export class SaveMaturityGroupResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveMaturityGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveMaturityGroupResponse): SaveMaturityGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveMaturityGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveMaturityGroupResponse;
  static deserializeBinaryFromReader(message: SaveMaturityGroupResponse, reader: jspb.BinaryReader): SaveMaturityGroupResponse;
}

export namespace SaveMaturityGroupResponse {
  export type AsObject = {
    id: number,
  }
}

