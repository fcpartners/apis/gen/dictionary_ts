// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/sowing_unit.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class SowingUnit extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getShortName(): string;
  setShortName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SowingUnit.AsObject;
  static toObject(includeInstance: boolean, msg: SowingUnit): SowingUnit.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SowingUnit, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SowingUnit;
  static deserializeBinaryFromReader(message: SowingUnit, reader: jspb.BinaryReader): SowingUnit;
}

export namespace SowingUnit {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    shortName: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class SowingUnitFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasShortName(): boolean;
  clearShortName(): void;
  getShortName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setShortName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SowingUnitFilter.AsObject;
  static toObject(includeInstance: boolean, msg: SowingUnitFilter): SowingUnitFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SowingUnitFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SowingUnitFilter;
  static deserializeBinaryFromReader(message: SowingUnitFilter, reader: jspb.BinaryReader): SowingUnitFilter;
}

export namespace SowingUnitFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    shortName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetSowingUnitRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSowingUnitRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSowingUnitRequest): GetSowingUnitRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSowingUnitRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSowingUnitRequest;
  static deserializeBinaryFromReader(message: GetSowingUnitRequest, reader: jspb.BinaryReader): GetSowingUnitRequest;
}

export namespace GetSowingUnitRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetSowingUnitResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): SowingUnit | undefined;
  setItem(value?: SowingUnit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSowingUnitResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetSowingUnitResponse): GetSowingUnitResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSowingUnitResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSowingUnitResponse;
  static deserializeBinaryFromReader(message: GetSowingUnitResponse, reader: jspb.BinaryReader): GetSowingUnitResponse;
}

export namespace GetSowingUnitResponse {
  export type AsObject = {
    item?: SowingUnit.AsObject,
  }
}

export class ListSowingUnitRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): SowingUnitFilter | undefined;
  setFilter(value?: SowingUnitFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSowingUnitRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListSowingUnitRequest): ListSowingUnitRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSowingUnitRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSowingUnitRequest;
  static deserializeBinaryFromReader(message: ListSowingUnitRequest, reader: jspb.BinaryReader): ListSowingUnitRequest;
}

export namespace ListSowingUnitRequest {
  export type AsObject = {
    filter?: SowingUnitFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListSowingUnitResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<SowingUnit>;
  setItemsList(value: Array<SowingUnit>): void;
  addItems(value?: SowingUnit, index?: number): SowingUnit;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSowingUnitResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListSowingUnitResponse): ListSowingUnitResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSowingUnitResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSowingUnitResponse;
  static deserializeBinaryFromReader(message: ListSowingUnitResponse, reader: jspb.BinaryReader): ListSowingUnitResponse;
}

export namespace ListSowingUnitResponse {
  export type AsObject = {
    itemsList: Array<SowingUnit.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveSowingUnitRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): SowingUnit | undefined;
  setItem(value?: SowingUnit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSowingUnitRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSowingUnitRequest): SaveSowingUnitRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSowingUnitRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSowingUnitRequest;
  static deserializeBinaryFromReader(message: SaveSowingUnitRequest, reader: jspb.BinaryReader): SaveSowingUnitRequest;
}

export namespace SaveSowingUnitRequest {
  export type AsObject = {
    item?: SowingUnit.AsObject,
  }
}

export class SaveSowingUnitResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSowingUnitResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSowingUnitResponse): SaveSowingUnitResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSowingUnitResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSowingUnitResponse;
  static deserializeBinaryFromReader(message: SaveSowingUnitResponse, reader: jspb.BinaryReader): SaveSowingUnitResponse;
}

export namespace SaveSowingUnitResponse {
  export type AsObject = {
    id: number,
  }
}

