// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/origin_country.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class OriginCountry extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OriginCountry.AsObject;
  static toObject(includeInstance: boolean, msg: OriginCountry): OriginCountry.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OriginCountry, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OriginCountry;
  static deserializeBinaryFromReader(message: OriginCountry, reader: jspb.BinaryReader): OriginCountry;
}

export namespace OriginCountry {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class OriginCountryFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): OriginCountryFilter.AsObject;
  static toObject(includeInstance: boolean, msg: OriginCountryFilter): OriginCountryFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: OriginCountryFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): OriginCountryFilter;
  static deserializeBinaryFromReader(message: OriginCountryFilter, reader: jspb.BinaryReader): OriginCountryFilter;
}

export namespace OriginCountryFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetOriginCountryRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetOriginCountryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetOriginCountryRequest): GetOriginCountryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetOriginCountryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetOriginCountryRequest;
  static deserializeBinaryFromReader(message: GetOriginCountryRequest, reader: jspb.BinaryReader): GetOriginCountryRequest;
}

export namespace GetOriginCountryRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetOriginCountryResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): OriginCountry | undefined;
  setItem(value?: OriginCountry): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetOriginCountryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetOriginCountryResponse): GetOriginCountryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetOriginCountryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetOriginCountryResponse;
  static deserializeBinaryFromReader(message: GetOriginCountryResponse, reader: jspb.BinaryReader): GetOriginCountryResponse;
}

export namespace GetOriginCountryResponse {
  export type AsObject = {
    item?: OriginCountry.AsObject,
  }
}

export class ListOriginCountryRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): OriginCountryFilter | undefined;
  setFilter(value?: OriginCountryFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOriginCountryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListOriginCountryRequest): ListOriginCountryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOriginCountryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOriginCountryRequest;
  static deserializeBinaryFromReader(message: ListOriginCountryRequest, reader: jspb.BinaryReader): ListOriginCountryRequest;
}

export namespace ListOriginCountryRequest {
  export type AsObject = {
    filter?: OriginCountryFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListOriginCountryResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<OriginCountry>;
  setItemsList(value: Array<OriginCountry>): void;
  addItems(value?: OriginCountry, index?: number): OriginCountry;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListOriginCountryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListOriginCountryResponse): ListOriginCountryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListOriginCountryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListOriginCountryResponse;
  static deserializeBinaryFromReader(message: ListOriginCountryResponse, reader: jspb.BinaryReader): ListOriginCountryResponse;
}

export namespace ListOriginCountryResponse {
  export type AsObject = {
    itemsList: Array<OriginCountry.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveOriginCountryRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): OriginCountry | undefined;
  setItem(value?: OriginCountry): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveOriginCountryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveOriginCountryRequest): SaveOriginCountryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveOriginCountryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveOriginCountryRequest;
  static deserializeBinaryFromReader(message: SaveOriginCountryRequest, reader: jspb.BinaryReader): SaveOriginCountryRequest;
}

export namespace SaveOriginCountryRequest {
  export type AsObject = {
    item?: OriginCountry.AsObject,
  }
}

export class SaveOriginCountryResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveOriginCountryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveOriginCountryResponse): SaveOriginCountryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveOriginCountryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveOriginCountryResponse;
  static deserializeBinaryFromReader(message: SaveOriginCountryResponse, reader: jspb.BinaryReader): SaveOriginCountryResponse;
}

export namespace SaveOriginCountryResponse {
  export type AsObject = {
    id: number,
  }
}

