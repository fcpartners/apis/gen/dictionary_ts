// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/grow_type.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class GrowType extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GrowType.AsObject;
  static toObject(includeInstance: boolean, msg: GrowType): GrowType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GrowType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GrowType;
  static deserializeBinaryFromReader(message: GrowType, reader: jspb.BinaryReader): GrowType;
}

export namespace GrowType {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class GrowTypeFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GrowTypeFilter.AsObject;
  static toObject(includeInstance: boolean, msg: GrowTypeFilter): GrowTypeFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GrowTypeFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GrowTypeFilter;
  static deserializeBinaryFromReader(message: GrowTypeFilter, reader: jspb.BinaryReader): GrowTypeFilter;
}

export namespace GrowTypeFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetGrowTypeRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetGrowTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetGrowTypeRequest): GetGrowTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetGrowTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetGrowTypeRequest;
  static deserializeBinaryFromReader(message: GetGrowTypeRequest, reader: jspb.BinaryReader): GetGrowTypeRequest;
}

export namespace GetGrowTypeRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetGrowTypeResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): GrowType | undefined;
  setItem(value?: GrowType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetGrowTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetGrowTypeResponse): GetGrowTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetGrowTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetGrowTypeResponse;
  static deserializeBinaryFromReader(message: GetGrowTypeResponse, reader: jspb.BinaryReader): GetGrowTypeResponse;
}

export namespace GetGrowTypeResponse {
  export type AsObject = {
    item?: GrowType.AsObject,
  }
}

export class ListGrowTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): GrowTypeFilter | undefined;
  setFilter(value?: GrowTypeFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGrowTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListGrowTypeRequest): ListGrowTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGrowTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGrowTypeRequest;
  static deserializeBinaryFromReader(message: ListGrowTypeRequest, reader: jspb.BinaryReader): ListGrowTypeRequest;
}

export namespace ListGrowTypeRequest {
  export type AsObject = {
    filter?: GrowTypeFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListGrowTypeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<GrowType>;
  setItemsList(value: Array<GrowType>): void;
  addItems(value?: GrowType, index?: number): GrowType;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGrowTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListGrowTypeResponse): ListGrowTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGrowTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGrowTypeResponse;
  static deserializeBinaryFromReader(message: ListGrowTypeResponse, reader: jspb.BinaryReader): ListGrowTypeResponse;
}

export namespace ListGrowTypeResponse {
  export type AsObject = {
    itemsList: Array<GrowType.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveGrowTypeRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): GrowType | undefined;
  setItem(value?: GrowType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveGrowTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveGrowTypeRequest): SaveGrowTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveGrowTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveGrowTypeRequest;
  static deserializeBinaryFromReader(message: SaveGrowTypeRequest, reader: jspb.BinaryReader): SaveGrowTypeRequest;
}

export namespace SaveGrowTypeRequest {
  export type AsObject = {
    item?: GrowType.AsObject,
  }
}

export class SaveGrowTypeResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveGrowTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveGrowTypeResponse): SaveGrowTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveGrowTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveGrowTypeResponse;
  static deserializeBinaryFromReader(message: SaveGrowTypeResponse, reader: jspb.BinaryReader): SaveGrowTypeResponse;
}

export namespace SaveGrowTypeResponse {
  export type AsObject = {
    id: number,
  }
}

