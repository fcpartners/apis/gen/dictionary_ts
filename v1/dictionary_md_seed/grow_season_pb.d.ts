// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/grow_season.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class GrowSeason extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GrowSeason.AsObject;
  static toObject(includeInstance: boolean, msg: GrowSeason): GrowSeason.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GrowSeason, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GrowSeason;
  static deserializeBinaryFromReader(message: GrowSeason, reader: jspb.BinaryReader): GrowSeason;
}

export namespace GrowSeason {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class GrowSeasonFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GrowSeasonFilter.AsObject;
  static toObject(includeInstance: boolean, msg: GrowSeasonFilter): GrowSeasonFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GrowSeasonFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GrowSeasonFilter;
  static deserializeBinaryFromReader(message: GrowSeasonFilter, reader: jspb.BinaryReader): GrowSeasonFilter;
}

export namespace GrowSeasonFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetGrowSeasonRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetGrowSeasonRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetGrowSeasonRequest): GetGrowSeasonRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetGrowSeasonRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetGrowSeasonRequest;
  static deserializeBinaryFromReader(message: GetGrowSeasonRequest, reader: jspb.BinaryReader): GetGrowSeasonRequest;
}

export namespace GetGrowSeasonRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetGrowSeasonResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): GrowSeason | undefined;
  setItem(value?: GrowSeason): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetGrowSeasonResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetGrowSeasonResponse): GetGrowSeasonResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetGrowSeasonResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetGrowSeasonResponse;
  static deserializeBinaryFromReader(message: GetGrowSeasonResponse, reader: jspb.BinaryReader): GetGrowSeasonResponse;
}

export namespace GetGrowSeasonResponse {
  export type AsObject = {
    item?: GrowSeason.AsObject,
  }
}

export class ListGrowSeasonRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): GrowSeasonFilter | undefined;
  setFilter(value?: GrowSeasonFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGrowSeasonRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListGrowSeasonRequest): ListGrowSeasonRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGrowSeasonRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGrowSeasonRequest;
  static deserializeBinaryFromReader(message: ListGrowSeasonRequest, reader: jspb.BinaryReader): ListGrowSeasonRequest;
}

export namespace ListGrowSeasonRequest {
  export type AsObject = {
    filter?: GrowSeasonFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListGrowSeasonResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<GrowSeason>;
  setItemsList(value: Array<GrowSeason>): void;
  addItems(value?: GrowSeason, index?: number): GrowSeason;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGrowSeasonResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListGrowSeasonResponse): ListGrowSeasonResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGrowSeasonResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGrowSeasonResponse;
  static deserializeBinaryFromReader(message: ListGrowSeasonResponse, reader: jspb.BinaryReader): ListGrowSeasonResponse;
}

export namespace ListGrowSeasonResponse {
  export type AsObject = {
    itemsList: Array<GrowSeason.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveGrowSeasonRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): GrowSeason | undefined;
  setItem(value?: GrowSeason): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveGrowSeasonRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveGrowSeasonRequest): SaveGrowSeasonRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveGrowSeasonRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveGrowSeasonRequest;
  static deserializeBinaryFromReader(message: SaveGrowSeasonRequest, reader: jspb.BinaryReader): SaveGrowSeasonRequest;
}

export namespace SaveGrowSeasonRequest {
  export type AsObject = {
    item?: GrowSeason.AsObject,
  }
}

export class SaveGrowSeasonResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveGrowSeasonResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveGrowSeasonResponse): SaveGrowSeasonResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveGrowSeasonResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveGrowSeasonResponse;
  static deserializeBinaryFromReader(message: SaveGrowSeasonResponse, reader: jspb.BinaryReader): SaveGrowSeasonResponse;
}

export namespace SaveGrowSeasonResponse {
  export type AsObject = {
    id: number,
  }
}

