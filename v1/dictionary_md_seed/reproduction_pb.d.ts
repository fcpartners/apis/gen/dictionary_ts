// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/reproduction.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class Reproduction extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Reproduction.AsObject;
  static toObject(includeInstance: boolean, msg: Reproduction): Reproduction.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Reproduction, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Reproduction;
  static deserializeBinaryFromReader(message: Reproduction, reader: jspb.BinaryReader): Reproduction;
}

export namespace Reproduction {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ReproductionFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ReproductionFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ReproductionFilter): ReproductionFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ReproductionFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ReproductionFilter;
  static deserializeBinaryFromReader(message: ReproductionFilter, reader: jspb.BinaryReader): ReproductionFilter;
}

export namespace ReproductionFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetReproductionRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetReproductionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetReproductionRequest): GetReproductionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetReproductionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetReproductionRequest;
  static deserializeBinaryFromReader(message: GetReproductionRequest, reader: jspb.BinaryReader): GetReproductionRequest;
}

export namespace GetReproductionRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetReproductionResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Reproduction | undefined;
  setItem(value?: Reproduction): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetReproductionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetReproductionResponse): GetReproductionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetReproductionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetReproductionResponse;
  static deserializeBinaryFromReader(message: GetReproductionResponse, reader: jspb.BinaryReader): GetReproductionResponse;
}

export namespace GetReproductionResponse {
  export type AsObject = {
    item?: Reproduction.AsObject,
  }
}

export class ListReproductionRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ReproductionFilter | undefined;
  setFilter(value?: ReproductionFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListReproductionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListReproductionRequest): ListReproductionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListReproductionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListReproductionRequest;
  static deserializeBinaryFromReader(message: ListReproductionRequest, reader: jspb.BinaryReader): ListReproductionRequest;
}

export namespace ListReproductionRequest {
  export type AsObject = {
    filter?: ReproductionFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListReproductionResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Reproduction>;
  setItemsList(value: Array<Reproduction>): void;
  addItems(value?: Reproduction, index?: number): Reproduction;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListReproductionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListReproductionResponse): ListReproductionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListReproductionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListReproductionResponse;
  static deserializeBinaryFromReader(message: ListReproductionResponse, reader: jspb.BinaryReader): ListReproductionResponse;
}

export namespace ListReproductionResponse {
  export type AsObject = {
    itemsList: Array<Reproduction.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveReproductionRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Reproduction | undefined;
  setItem(value?: Reproduction): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveReproductionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveReproductionRequest): SaveReproductionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveReproductionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveReproductionRequest;
  static deserializeBinaryFromReader(message: SaveReproductionRequest, reader: jspb.BinaryReader): SaveReproductionRequest;
}

export namespace SaveReproductionRequest {
  export type AsObject = {
    item?: Reproduction.AsObject,
  }
}

export class SaveReproductionResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveReproductionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveReproductionResponse): SaveReproductionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveReproductionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveReproductionResponse;
  static deserializeBinaryFromReader(message: SaveReproductionResponse, reader: jspb.BinaryReader): SaveReproductionResponse;
}

export namespace SaveReproductionResponse {
  export type AsObject = {
    id: number,
  }
}

