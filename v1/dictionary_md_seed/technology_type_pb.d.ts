// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/technology_type.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class TechnologyType extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TechnologyType.AsObject;
  static toObject(includeInstance: boolean, msg: TechnologyType): TechnologyType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TechnologyType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TechnologyType;
  static deserializeBinaryFromReader(message: TechnologyType, reader: jspb.BinaryReader): TechnologyType;
}

export namespace TechnologyType {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class TechnologyTypeFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): TechnologyTypeFilter.AsObject;
  static toObject(includeInstance: boolean, msg: TechnologyTypeFilter): TechnologyTypeFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: TechnologyTypeFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): TechnologyTypeFilter;
  static deserializeBinaryFromReader(message: TechnologyTypeFilter, reader: jspb.BinaryReader): TechnologyTypeFilter;
}

export namespace TechnologyTypeFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetTechnologyTypeRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTechnologyTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetTechnologyTypeRequest): GetTechnologyTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTechnologyTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTechnologyTypeRequest;
  static deserializeBinaryFromReader(message: GetTechnologyTypeRequest, reader: jspb.BinaryReader): GetTechnologyTypeRequest;
}

export namespace GetTechnologyTypeRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetTechnologyTypeResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): TechnologyType | undefined;
  setItem(value?: TechnologyType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetTechnologyTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetTechnologyTypeResponse): GetTechnologyTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetTechnologyTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetTechnologyTypeResponse;
  static deserializeBinaryFromReader(message: GetTechnologyTypeResponse, reader: jspb.BinaryReader): GetTechnologyTypeResponse;
}

export namespace GetTechnologyTypeResponse {
  export type AsObject = {
    item?: TechnologyType.AsObject,
  }
}

export class ListTechnologyTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): TechnologyTypeFilter | undefined;
  setFilter(value?: TechnologyTypeFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListTechnologyTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListTechnologyTypeRequest): ListTechnologyTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListTechnologyTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListTechnologyTypeRequest;
  static deserializeBinaryFromReader(message: ListTechnologyTypeRequest, reader: jspb.BinaryReader): ListTechnologyTypeRequest;
}

export namespace ListTechnologyTypeRequest {
  export type AsObject = {
    filter?: TechnologyTypeFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListTechnologyTypeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<TechnologyType>;
  setItemsList(value: Array<TechnologyType>): void;
  addItems(value?: TechnologyType, index?: number): TechnologyType;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListTechnologyTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListTechnologyTypeResponse): ListTechnologyTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListTechnologyTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListTechnologyTypeResponse;
  static deserializeBinaryFromReader(message: ListTechnologyTypeResponse, reader: jspb.BinaryReader): ListTechnologyTypeResponse;
}

export namespace ListTechnologyTypeResponse {
  export type AsObject = {
    itemsList: Array<TechnologyType.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveTechnologyTypeRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): TechnologyType | undefined;
  setItem(value?: TechnologyType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveTechnologyTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveTechnologyTypeRequest): SaveTechnologyTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveTechnologyTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveTechnologyTypeRequest;
  static deserializeBinaryFromReader(message: SaveTechnologyTypeRequest, reader: jspb.BinaryReader): SaveTechnologyTypeRequest;
}

export namespace SaveTechnologyTypeRequest {
  export type AsObject = {
    item?: TechnologyType.AsObject,
  }
}

export class SaveTechnologyTypeResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveTechnologyTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveTechnologyTypeResponse): SaveTechnologyTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveTechnologyTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveTechnologyTypeResponse;
  static deserializeBinaryFromReader(message: SaveTechnologyTypeResponse, reader: jspb.BinaryReader): SaveTechnologyTypeResponse;
}

export namespace SaveTechnologyTypeResponse {
  export type AsObject = {
    id: number,
  }
}

