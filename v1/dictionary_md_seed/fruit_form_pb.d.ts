// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/fruit_form.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class FruitForm extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FruitForm.AsObject;
  static toObject(includeInstance: boolean, msg: FruitForm): FruitForm.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FruitForm, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FruitForm;
  static deserializeBinaryFromReader(message: FruitForm, reader: jspb.BinaryReader): FruitForm;
}

export namespace FruitForm {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class FruitFormFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FruitFormFilter.AsObject;
  static toObject(includeInstance: boolean, msg: FruitFormFilter): FruitFormFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FruitFormFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FruitFormFilter;
  static deserializeBinaryFromReader(message: FruitFormFilter, reader: jspb.BinaryReader): FruitFormFilter;
}

export namespace FruitFormFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetFruitFormRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFruitFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFruitFormRequest): GetFruitFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFruitFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFruitFormRequest;
  static deserializeBinaryFromReader(message: GetFruitFormRequest, reader: jspb.BinaryReader): GetFruitFormRequest;
}

export namespace GetFruitFormRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetFruitFormResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): FruitForm | undefined;
  setItem(value?: FruitForm): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFruitFormResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFruitFormResponse): GetFruitFormResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFruitFormResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFruitFormResponse;
  static deserializeBinaryFromReader(message: GetFruitFormResponse, reader: jspb.BinaryReader): GetFruitFormResponse;
}

export namespace GetFruitFormResponse {
  export type AsObject = {
    item?: FruitForm.AsObject,
  }
}

export class ListFruitFormRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FruitFormFilter | undefined;
  setFilter(value?: FruitFormFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFruitFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFruitFormRequest): ListFruitFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFruitFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFruitFormRequest;
  static deserializeBinaryFromReader(message: ListFruitFormRequest, reader: jspb.BinaryReader): ListFruitFormRequest;
}

export namespace ListFruitFormRequest {
  export type AsObject = {
    filter?: FruitFormFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListFruitFormResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<FruitForm>;
  setItemsList(value: Array<FruitForm>): void;
  addItems(value?: FruitForm, index?: number): FruitForm;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFruitFormResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListFruitFormResponse): ListFruitFormResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFruitFormResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFruitFormResponse;
  static deserializeBinaryFromReader(message: ListFruitFormResponse, reader: jspb.BinaryReader): ListFruitFormResponse;
}

export namespace ListFruitFormResponse {
  export type AsObject = {
    itemsList: Array<FruitForm.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveFruitFormRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): FruitForm | undefined;
  setItem(value?: FruitForm): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveFruitFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveFruitFormRequest): SaveFruitFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveFruitFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveFruitFormRequest;
  static deserializeBinaryFromReader(message: SaveFruitFormRequest, reader: jspb.BinaryReader): SaveFruitFormRequest;
}

export namespace SaveFruitFormRequest {
  export type AsObject = {
    item?: FruitForm.AsObject,
  }
}

export class SaveFruitFormResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveFruitFormResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveFruitFormResponse): SaveFruitFormResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveFruitFormResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveFruitFormResponse;
  static deserializeBinaryFromReader(message: SaveFruitFormResponse, reader: jspb.BinaryReader): SaveFruitFormResponse;
}

export namespace SaveFruitFormResponse {
  export type AsObject = {
    id: number,
  }
}

