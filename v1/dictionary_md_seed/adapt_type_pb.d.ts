// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/adapt_type.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class AdaptType extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AdaptType.AsObject;
  static toObject(includeInstance: boolean, msg: AdaptType): AdaptType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AdaptType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AdaptType;
  static deserializeBinaryFromReader(message: AdaptType, reader: jspb.BinaryReader): AdaptType;
}

export namespace AdaptType {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class AdaptTypeFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): AdaptTypeFilter.AsObject;
  static toObject(includeInstance: boolean, msg: AdaptTypeFilter): AdaptTypeFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: AdaptTypeFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): AdaptTypeFilter;
  static deserializeBinaryFromReader(message: AdaptTypeFilter, reader: jspb.BinaryReader): AdaptTypeFilter;
}

export namespace AdaptTypeFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetAdaptTypeRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAdaptTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetAdaptTypeRequest): GetAdaptTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetAdaptTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetAdaptTypeRequest;
  static deserializeBinaryFromReader(message: GetAdaptTypeRequest, reader: jspb.BinaryReader): GetAdaptTypeRequest;
}

export namespace GetAdaptTypeRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetAdaptTypeResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): AdaptType | undefined;
  setItem(value?: AdaptType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetAdaptTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetAdaptTypeResponse): GetAdaptTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetAdaptTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetAdaptTypeResponse;
  static deserializeBinaryFromReader(message: GetAdaptTypeResponse, reader: jspb.BinaryReader): GetAdaptTypeResponse;
}

export namespace GetAdaptTypeResponse {
  export type AsObject = {
    item?: AdaptType.AsObject,
  }
}

export class ListAdaptTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): AdaptTypeFilter | undefined;
  setFilter(value?: AdaptTypeFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListAdaptTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListAdaptTypeRequest): ListAdaptTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListAdaptTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListAdaptTypeRequest;
  static deserializeBinaryFromReader(message: ListAdaptTypeRequest, reader: jspb.BinaryReader): ListAdaptTypeRequest;
}

export namespace ListAdaptTypeRequest {
  export type AsObject = {
    filter?: AdaptTypeFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListAdaptTypeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<AdaptType>;
  setItemsList(value: Array<AdaptType>): void;
  addItems(value?: AdaptType, index?: number): AdaptType;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListAdaptTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListAdaptTypeResponse): ListAdaptTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListAdaptTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListAdaptTypeResponse;
  static deserializeBinaryFromReader(message: ListAdaptTypeResponse, reader: jspb.BinaryReader): ListAdaptTypeResponse;
}

export namespace ListAdaptTypeResponse {
  export type AsObject = {
    itemsList: Array<AdaptType.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveAdaptTypeRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): AdaptType | undefined;
  setItem(value?: AdaptType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveAdaptTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveAdaptTypeRequest): SaveAdaptTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveAdaptTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveAdaptTypeRequest;
  static deserializeBinaryFromReader(message: SaveAdaptTypeRequest, reader: jspb.BinaryReader): SaveAdaptTypeRequest;
}

export namespace SaveAdaptTypeRequest {
  export type AsObject = {
    item?: AdaptType.AsObject,
  }
}

export class SaveAdaptTypeResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveAdaptTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveAdaptTypeResponse): SaveAdaptTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveAdaptTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveAdaptTypeResponse;
  static deserializeBinaryFromReader(message: SaveAdaptTypeResponse, reader: jspb.BinaryReader): SaveAdaptTypeResponse;
}

export namespace SaveAdaptTypeResponse {
  export type AsObject = {
    id: number,
  }
}

