// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/species.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class Species extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Species.AsObject;
  static toObject(includeInstance: boolean, msg: Species): Species.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Species, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Species;
  static deserializeBinaryFromReader(message: Species, reader: jspb.BinaryReader): Species;
}

export namespace Species {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class SpeciesFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SpeciesFilter.AsObject;
  static toObject(includeInstance: boolean, msg: SpeciesFilter): SpeciesFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SpeciesFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SpeciesFilter;
  static deserializeBinaryFromReader(message: SpeciesFilter, reader: jspb.BinaryReader): SpeciesFilter;
}

export namespace SpeciesFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetSpeciesRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSpeciesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSpeciesRequest): GetSpeciesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSpeciesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSpeciesRequest;
  static deserializeBinaryFromReader(message: GetSpeciesRequest, reader: jspb.BinaryReader): GetSpeciesRequest;
}

export namespace GetSpeciesRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetSpeciesResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Species | undefined;
  setItem(value?: Species): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSpeciesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetSpeciesResponse): GetSpeciesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSpeciesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSpeciesResponse;
  static deserializeBinaryFromReader(message: GetSpeciesResponse, reader: jspb.BinaryReader): GetSpeciesResponse;
}

export namespace GetSpeciesResponse {
  export type AsObject = {
    item?: Species.AsObject,
  }
}

export class ListSpeciesRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): SpeciesFilter | undefined;
  setFilter(value?: SpeciesFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSpeciesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListSpeciesRequest): ListSpeciesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSpeciesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSpeciesRequest;
  static deserializeBinaryFromReader(message: ListSpeciesRequest, reader: jspb.BinaryReader): ListSpeciesRequest;
}

export namespace ListSpeciesRequest {
  export type AsObject = {
    filter?: SpeciesFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListSpeciesResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Species>;
  setItemsList(value: Array<Species>): void;
  addItems(value?: Species, index?: number): Species;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSpeciesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListSpeciesResponse): ListSpeciesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSpeciesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSpeciesResponse;
  static deserializeBinaryFromReader(message: ListSpeciesResponse, reader: jspb.BinaryReader): ListSpeciesResponse;
}

export namespace ListSpeciesResponse {
  export type AsObject = {
    itemsList: Array<Species.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveSpeciesRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Species | undefined;
  setItem(value?: Species): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSpeciesRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSpeciesRequest): SaveSpeciesRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSpeciesRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSpeciesRequest;
  static deserializeBinaryFromReader(message: SaveSpeciesRequest, reader: jspb.BinaryReader): SaveSpeciesRequest;
}

export namespace SaveSpeciesRequest {
  export type AsObject = {
    item?: Species.AsObject,
  }
}

export class SaveSpeciesResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSpeciesResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSpeciesResponse): SaveSpeciesResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSpeciesResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSpeciesResponse;
  static deserializeBinaryFromReader(message: SaveSpeciesResponse, reader: jspb.BinaryReader): SaveSpeciesResponse;
}

export namespace SaveSpeciesResponse {
  export type AsObject = {
    id: number,
  }
}

