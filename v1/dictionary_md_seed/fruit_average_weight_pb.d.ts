// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/fruit_average_weight.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class FruitAverageWeight extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getAverageWeight(): number;
  setAverageWeight(value: number): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FruitAverageWeight.AsObject;
  static toObject(includeInstance: boolean, msg: FruitAverageWeight): FruitAverageWeight.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FruitAverageWeight, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FruitAverageWeight;
  static deserializeBinaryFromReader(message: FruitAverageWeight, reader: jspb.BinaryReader): FruitAverageWeight;
}

export namespace FruitAverageWeight {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    averageWeight: number,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class FruitAverageWeightFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FruitAverageWeightFilter.AsObject;
  static toObject(includeInstance: boolean, msg: FruitAverageWeightFilter): FruitAverageWeightFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FruitAverageWeightFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FruitAverageWeightFilter;
  static deserializeBinaryFromReader(message: FruitAverageWeightFilter, reader: jspb.BinaryReader): FruitAverageWeightFilter;
}

export namespace FruitAverageWeightFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetFruitAverageWeightRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFruitAverageWeightRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetFruitAverageWeightRequest): GetFruitAverageWeightRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFruitAverageWeightRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFruitAverageWeightRequest;
  static deserializeBinaryFromReader(message: GetFruitAverageWeightRequest, reader: jspb.BinaryReader): GetFruitAverageWeightRequest;
}

export namespace GetFruitAverageWeightRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetFruitAverageWeightResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): FruitAverageWeight | undefined;
  setItem(value?: FruitAverageWeight): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetFruitAverageWeightResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetFruitAverageWeightResponse): GetFruitAverageWeightResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetFruitAverageWeightResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetFruitAverageWeightResponse;
  static deserializeBinaryFromReader(message: GetFruitAverageWeightResponse, reader: jspb.BinaryReader): GetFruitAverageWeightResponse;
}

export namespace GetFruitAverageWeightResponse {
  export type AsObject = {
    item?: FruitAverageWeight.AsObject,
  }
}

export class ListFruitAverageWeightRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): FruitAverageWeightFilter | undefined;
  setFilter(value?: FruitAverageWeightFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFruitAverageWeightRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListFruitAverageWeightRequest): ListFruitAverageWeightRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFruitAverageWeightRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFruitAverageWeightRequest;
  static deserializeBinaryFromReader(message: ListFruitAverageWeightRequest, reader: jspb.BinaryReader): ListFruitAverageWeightRequest;
}

export namespace ListFruitAverageWeightRequest {
  export type AsObject = {
    filter?: FruitAverageWeightFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListFruitAverageWeightResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<FruitAverageWeight>;
  setItemsList(value: Array<FruitAverageWeight>): void;
  addItems(value?: FruitAverageWeight, index?: number): FruitAverageWeight;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListFruitAverageWeightResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListFruitAverageWeightResponse): ListFruitAverageWeightResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListFruitAverageWeightResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListFruitAverageWeightResponse;
  static deserializeBinaryFromReader(message: ListFruitAverageWeightResponse, reader: jspb.BinaryReader): ListFruitAverageWeightResponse;
}

export namespace ListFruitAverageWeightResponse {
  export type AsObject = {
    itemsList: Array<FruitAverageWeight.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveFruitAverageWeightRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): FruitAverageWeight | undefined;
  setItem(value?: FruitAverageWeight): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveFruitAverageWeightRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveFruitAverageWeightRequest): SaveFruitAverageWeightRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveFruitAverageWeightRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveFruitAverageWeightRequest;
  static deserializeBinaryFromReader(message: SaveFruitAverageWeightRequest, reader: jspb.BinaryReader): SaveFruitAverageWeightRequest;
}

export namespace SaveFruitAverageWeightRequest {
  export type AsObject = {
    item?: FruitAverageWeight.AsObject,
  }
}

export class SaveFruitAverageWeightResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveFruitAverageWeightResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveFruitAverageWeightResponse): SaveFruitAverageWeightResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveFruitAverageWeightResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveFruitAverageWeightResponse;
  static deserializeBinaryFromReader(message: SaveFruitAverageWeightResponse, reader: jspb.BinaryReader): SaveFruitAverageWeightResponse;
}

export namespace SaveFruitAverageWeightResponse {
  export type AsObject = {
    id: number,
  }
}

