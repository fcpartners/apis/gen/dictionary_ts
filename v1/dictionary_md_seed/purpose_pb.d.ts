// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md_seed/purpose.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class Purpose extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Purpose.AsObject;
  static toObject(includeInstance: boolean, msg: Purpose): Purpose.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Purpose, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Purpose;
  static deserializeBinaryFromReader(message: Purpose, reader: jspb.BinaryReader): Purpose;
}

export namespace Purpose {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class PurposeFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PurposeFilter.AsObject;
  static toObject(includeInstance: boolean, msg: PurposeFilter): PurposeFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PurposeFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PurposeFilter;
  static deserializeBinaryFromReader(message: PurposeFilter, reader: jspb.BinaryReader): PurposeFilter;
}

export namespace PurposeFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetPurposeRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPurposeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPurposeRequest): GetPurposeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPurposeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPurposeRequest;
  static deserializeBinaryFromReader(message: GetPurposeRequest, reader: jspb.BinaryReader): GetPurposeRequest;
}

export namespace GetPurposeRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetPurposeResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Purpose | undefined;
  setItem(value?: Purpose): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPurposeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetPurposeResponse): GetPurposeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPurposeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPurposeResponse;
  static deserializeBinaryFromReader(message: GetPurposeResponse, reader: jspb.BinaryReader): GetPurposeResponse;
}

export namespace GetPurposeResponse {
  export type AsObject = {
    item?: Purpose.AsObject,
  }
}

export class ListPurposeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PurposeFilter | undefined;
  setFilter(value?: PurposeFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPurposeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPurposeRequest): ListPurposeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPurposeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPurposeRequest;
  static deserializeBinaryFromReader(message: ListPurposeRequest, reader: jspb.BinaryReader): ListPurposeRequest;
}

export namespace ListPurposeRequest {
  export type AsObject = {
    filter?: PurposeFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListPurposeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Purpose>;
  setItemsList(value: Array<Purpose>): void;
  addItems(value?: Purpose, index?: number): Purpose;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPurposeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPurposeResponse): ListPurposeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPurposeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPurposeResponse;
  static deserializeBinaryFromReader(message: ListPurposeResponse, reader: jspb.BinaryReader): ListPurposeResponse;
}

export namespace ListPurposeResponse {
  export type AsObject = {
    itemsList: Array<Purpose.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SavePurposeRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Purpose | undefined;
  setItem(value?: Purpose): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SavePurposeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SavePurposeRequest): SavePurposeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SavePurposeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SavePurposeRequest;
  static deserializeBinaryFromReader(message: SavePurposeRequest, reader: jspb.BinaryReader): SavePurposeRequest;
}

export namespace SavePurposeRequest {
  export type AsObject = {
    item?: Purpose.AsObject,
  }
}

export class SavePurposeResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SavePurposeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SavePurposeResponse): SavePurposeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SavePurposeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SavePurposeResponse;
  static deserializeBinaryFromReader(message: SavePurposeResponse, reader: jspb.BinaryReader): SavePurposeResponse;
}

export namespace SavePurposeResponse {
  export type AsObject = {
    id: number,
  }
}

