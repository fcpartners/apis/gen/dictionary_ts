// package: fcp.dictionary.v1.dictionary_common
// file: v1/dictionary_common/enum_product.proto

import * as jspb from "google-protobuf";

export class ProductProcessValue extends jspb.Message {
  getValue(): ProductProcessMap[keyof ProductProcessMap];
  setValue(value: ProductProcessMap[keyof ProductProcessMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductProcessValue.AsObject;
  static toObject(includeInstance: boolean, msg: ProductProcessValue): ProductProcessValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductProcessValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductProcessValue;
  static deserializeBinaryFromReader(message: ProductProcessValue, reader: jspb.BinaryReader): ProductProcessValue;
}

export namespace ProductProcessValue {
  export type AsObject = {
    value: ProductProcessMap[keyof ProductProcessMap],
  }
}

export class ProductTypeValue extends jspb.Message {
  getValue(): ProductTypeMap[keyof ProductTypeMap];
  setValue(value: ProductTypeMap[keyof ProductTypeMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductTypeValue.AsObject;
  static toObject(includeInstance: boolean, msg: ProductTypeValue): ProductTypeValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductTypeValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductTypeValue;
  static deserializeBinaryFromReader(message: ProductTypeValue, reader: jspb.BinaryReader): ProductTypeValue;
}

export namespace ProductTypeValue {
  export type AsObject = {
    value: ProductTypeMap[keyof ProductTypeMap],
  }
}

export class FileTypeValue extends jspb.Message {
  getValue(): FileTypeMap[keyof FileTypeMap];
  setValue(value: FileTypeMap[keyof FileTypeMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): FileTypeValue.AsObject;
  static toObject(includeInstance: boolean, msg: FileTypeValue): FileTypeValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: FileTypeValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): FileTypeValue;
  static deserializeBinaryFromReader(message: FileTypeValue, reader: jspb.BinaryReader): FileTypeValue;
}

export namespace FileTypeValue {
  export type AsObject = {
    value: FileTypeMap[keyof FileTypeMap],
  }
}

export class ImageQualityValue extends jspb.Message {
  getValue(): ImageQualityMap[keyof ImageQualityMap];
  setValue(value: ImageQualityMap[keyof ImageQualityMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ImageQualityValue.AsObject;
  static toObject(includeInstance: boolean, msg: ImageQualityValue): ImageQualityValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ImageQualityValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ImageQualityValue;
  static deserializeBinaryFromReader(message: ImageQualityValue, reader: jspb.BinaryReader): ImageQualityValue;
}

export namespace ImageQualityValue {
  export type AsObject = {
    value: ImageQualityMap[keyof ImageQualityMap],
  }
}

export class SpectrumActionTypeValue extends jspb.Message {
  getValue(): SpectrumActionTypeMap[keyof SpectrumActionTypeMap];
  setValue(value: SpectrumActionTypeMap[keyof SpectrumActionTypeMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SpectrumActionTypeValue.AsObject;
  static toObject(includeInstance: boolean, msg: SpectrumActionTypeValue): SpectrumActionTypeValue.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SpectrumActionTypeValue, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SpectrumActionTypeValue;
  static deserializeBinaryFromReader(message: SpectrumActionTypeValue, reader: jspb.BinaryReader): SpectrumActionTypeValue;
}

export namespace SpectrumActionTypeValue {
  export type AsObject = {
    value: SpectrumActionTypeMap[keyof SpectrumActionTypeMap],
  }
}

export interface ProductProcessMap {
  UNSPECIFIED: 0;
  RESOURCES: 1;
  SERVICES: 2;
  COMMODITIES: 3;
  FOOD: 4;
}

export const ProductProcess: ProductProcessMap;

export interface ProductTypeMap {
  PRODUCTTYPE_UNSPECIFIED: 0;
  RESOURCES_SEEDS: 100;
  RESOURCES_FERTILIZE: 101;
  RESOURCES_MICROFERTILIZE: 102;
  RESOURCES_CHEMICAL: 103;
  RESOURCES_FUEL: 104;
  SERVICES_BANKING: 200;
  SERVICES_INSURANCE: 201;
  SERVICES_LEGAL: 202;
  COMMODITIES_CROPS: 300;
  COMMODITIES_ANIMAL: 301;
  COMMODITIES_AQUA_CULTURE: 302;
  COMMODITIES_PROCESSED: 303;
  COMMODITIES_VEGETABLES_FRUIT_BERRY: 304;
  FOOD_CROPS: 400;
  FOOD_ANIMAL: 401;
  FOOD_AQUA_CULTURE: 402;
  FOOD_PROCESSED: 403;
}

export const ProductType: ProductTypeMap;

export interface FileTypeMap {
  FT_UNSPECIFIED: 0;
  FT_IMAGE: 1;
  FT_VIDEO: 2;
  FT_DOCUMENT: 3;
}

export const FileType: FileTypeMap;

export interface ImageQualityMap {
  IQ_UNSPECIFIED: 0;
  IQ_DETAILED: 1;
  IQ_COMPRESSED: 2;
}

export const ImageQuality: ImageQualityMap;

export interface SpectrumActionTypeMap {
  SPT_UNSPECIFIED: 0;
  SPT_DISEASE: 1;
  SPT_PEST: 2;
  SPT_WEED: 3;
}

export const SpectrumActionType: SpectrumActionTypeMap;

