// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/product_category.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ProductCategory extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getProductId(): number;
  setProductId(value: number): void;

  getProductName(): string;
  setProductName(value: string): void;

  getCategoryId(): number;
  setCategoryId(value: number): void;

  getCategoryName(): string;
  setCategoryName(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductCategory.AsObject;
  static toObject(includeInstance: boolean, msg: ProductCategory): ProductCategory.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductCategory, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductCategory;
  static deserializeBinaryFromReader(message: ProductCategory, reader: jspb.BinaryReader): ProductCategory;
}

export namespace ProductCategory {
  export type AsObject = {
    id: number,
    productId: number,
    productName: string,
    categoryId: number,
    categoryName: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ProductCategoryFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasCategoryId(): boolean;
  clearCategoryId(): void;
  getCategoryId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCategoryId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductCategoryFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductCategoryFilter): ProductCategoryFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductCategoryFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductCategoryFilter;
  static deserializeBinaryFromReader(message: ProductCategoryFilter, reader: jspb.BinaryReader): ProductCategoryFilter;
}

export namespace ProductCategoryFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    categoryId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetProductCategoryRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductCategoryRequest): GetProductCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductCategoryRequest;
  static deserializeBinaryFromReader(message: GetProductCategoryRequest, reader: jspb.BinaryReader): GetProductCategoryRequest;
}

export namespace GetProductCategoryRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductCategoryResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductCategory | undefined;
  setItem(value?: ProductCategory): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductCategoryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductCategoryResponse): GetProductCategoryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductCategoryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductCategoryResponse;
  static deserializeBinaryFromReader(message: GetProductCategoryResponse, reader: jspb.BinaryReader): GetProductCategoryResponse;
}

export namespace GetProductCategoryResponse {
  export type AsObject = {
    item?: ProductCategory.AsObject,
  }
}

export class ListProductCategoryRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductCategoryFilter | undefined;
  setFilter(value?: ProductCategoryFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductCategoryRequest): ListProductCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductCategoryRequest;
  static deserializeBinaryFromReader(message: ListProductCategoryRequest, reader: jspb.BinaryReader): ListProductCategoryRequest;
}

export namespace ListProductCategoryRequest {
  export type AsObject = {
    filter?: ProductCategoryFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductCategoryResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductCategory>;
  setItemsList(value: Array<ProductCategory>): void;
  addItems(value?: ProductCategory, index?: number): ProductCategory;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductCategoryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductCategoryResponse): ListProductCategoryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductCategoryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductCategoryResponse;
  static deserializeBinaryFromReader(message: ListProductCategoryResponse, reader: jspb.BinaryReader): ListProductCategoryResponse;
}

export namespace ListProductCategoryResponse {
  export type AsObject = {
    itemsList: Array<ProductCategory.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveProductCategoryRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductCategory | undefined;
  setItem(value?: ProductCategory): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductCategoryRequest): SaveProductCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductCategoryRequest;
  static deserializeBinaryFromReader(message: SaveProductCategoryRequest, reader: jspb.BinaryReader): SaveProductCategoryRequest;
}

export namespace SaveProductCategoryRequest {
  export type AsObject = {
    item?: ProductCategory.AsObject,
  }
}

export class SaveProductCategoryResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductCategoryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductCategoryResponse): SaveProductCategoryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductCategoryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductCategoryResponse;
  static deserializeBinaryFromReader(message: SaveProductCategoryResponse, reader: jspb.BinaryReader): SaveProductCategoryResponse;
}

export namespace SaveProductCategoryResponse {
  export type AsObject = {
    id: number,
  }
}

