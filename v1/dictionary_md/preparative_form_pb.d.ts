// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/preparative_form.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class PreparativeForm extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getFullName(): string;
  setFullName(value: string): void;

  getAbbreviation(): string;
  setAbbreviation(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PreparativeForm.AsObject;
  static toObject(includeInstance: boolean, msg: PreparativeForm): PreparativeForm.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PreparativeForm, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PreparativeForm;
  static deserializeBinaryFromReader(message: PreparativeForm, reader: jspb.BinaryReader): PreparativeForm;
}

export namespace PreparativeForm {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    fullName: string,
    abbreviation: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class PreparativeFormFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasFullName(): boolean;
  clearFullName(): void;
  getFullName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFullName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasAbbreviation(): boolean;
  clearAbbreviation(): void;
  getAbbreviation(): google_protobuf_wrappers_pb.StringValue | undefined;
  setAbbreviation(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): PreparativeFormFilter.AsObject;
  static toObject(includeInstance: boolean, msg: PreparativeFormFilter): PreparativeFormFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: PreparativeFormFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): PreparativeFormFilter;
  static deserializeBinaryFromReader(message: PreparativeFormFilter, reader: jspb.BinaryReader): PreparativeFormFilter;
}

export namespace PreparativeFormFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    fullName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    abbreviation?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetPreparativeFormRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPreparativeFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetPreparativeFormRequest): GetPreparativeFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPreparativeFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPreparativeFormRequest;
  static deserializeBinaryFromReader(message: GetPreparativeFormRequest, reader: jspb.BinaryReader): GetPreparativeFormRequest;
}

export namespace GetPreparativeFormRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetPreparativeFormResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): PreparativeForm | undefined;
  setItem(value?: PreparativeForm): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetPreparativeFormResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetPreparativeFormResponse): GetPreparativeFormResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetPreparativeFormResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetPreparativeFormResponse;
  static deserializeBinaryFromReader(message: GetPreparativeFormResponse, reader: jspb.BinaryReader): GetPreparativeFormResponse;
}

export namespace GetPreparativeFormResponse {
  export type AsObject = {
    item?: PreparativeForm.AsObject,
  }
}

export class ListPreparativeFormRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): PreparativeFormFilter | undefined;
  setFilter(value?: PreparativeFormFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPreparativeFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListPreparativeFormRequest): ListPreparativeFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPreparativeFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPreparativeFormRequest;
  static deserializeBinaryFromReader(message: ListPreparativeFormRequest, reader: jspb.BinaryReader): ListPreparativeFormRequest;
}

export namespace ListPreparativeFormRequest {
  export type AsObject = {
    filter?: PreparativeFormFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListPreparativeFormResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<PreparativeForm>;
  setItemsList(value: Array<PreparativeForm>): void;
  addItems(value?: PreparativeForm, index?: number): PreparativeForm;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListPreparativeFormResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListPreparativeFormResponse): ListPreparativeFormResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListPreparativeFormResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListPreparativeFormResponse;
  static deserializeBinaryFromReader(message: ListPreparativeFormResponse, reader: jspb.BinaryReader): ListPreparativeFormResponse;
}

export namespace ListPreparativeFormResponse {
  export type AsObject = {
    itemsList: Array<PreparativeForm.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SavePreparativeFormRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): PreparativeForm | undefined;
  setItem(value?: PreparativeForm): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SavePreparativeFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SavePreparativeFormRequest): SavePreparativeFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SavePreparativeFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SavePreparativeFormRequest;
  static deserializeBinaryFromReader(message: SavePreparativeFormRequest, reader: jspb.BinaryReader): SavePreparativeFormRequest;
}

export namespace SavePreparativeFormRequest {
  export type AsObject = {
    item?: PreparativeForm.AsObject,
  }
}

export class SavePreparativeFormResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SavePreparativeFormResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SavePreparativeFormResponse): SavePreparativeFormResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SavePreparativeFormResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SavePreparativeFormResponse;
  static deserializeBinaryFromReader(message: SavePreparativeFormResponse, reader: jspb.BinaryReader): SavePreparativeFormResponse;
}

export namespace SavePreparativeFormResponse {
  export type AsObject = {
    id: number,
  }
}

