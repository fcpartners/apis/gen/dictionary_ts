// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/product_category_application.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_md_application_method_pb from "../../v1/dictionary_md/application_method_pb";
import * as v1_dictionary_md_spectrum_action_chemical_pb from "../../v1/dictionary_md/spectrum_action_chemical_pb";

export class ProductCategoryApplication extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getProductId(): number;
  setProductId(value: number): void;

  getProductName(): string;
  setProductName(value: string): void;

  getCategoryId(): number;
  setCategoryId(value: number): void;

  getCategoryName(): string;
  setCategoryName(value: string): void;

  getApplicationRateUnitId(): number;
  setApplicationRateUnitId(value: number): void;

  getApplicationRateUnitName(): string;
  setApplicationRateUnitName(value: string): void;

  getWaitingPeriod(): string;
  setWaitingPeriod(value: string): void;

  getCompatibility(): string;
  setCompatibility(value: string): void;

  getWorkFluidConsumeRate(): string;
  setWorkFluidConsumeRate(value: string): void;

  getApplicationRate(): string;
  setApplicationRate(value: string): void;

  getApplicationPeriod(): string;
  setApplicationPeriod(value: string): void;

  getApplicationTemperature(): string;
  setApplicationTemperature(value: string): void;

  getApplicationMultiplicity(): string;
  setApplicationMultiplicity(value: string): void;

  getMaxApplicationMultiplicity(): string;
  setMaxApplicationMultiplicity(value: string): void;

  getApplicationRecommendation(): string;
  setApplicationRecommendation(value: string): void;

  clearSpectrumActionChemicalsList(): void;
  getSpectrumActionChemicalsList(): Array<v1_dictionary_md_spectrum_action_chemical_pb.SpectrumActionChemical>;
  setSpectrumActionChemicalsList(value: Array<v1_dictionary_md_spectrum_action_chemical_pb.SpectrumActionChemical>): void;
  addSpectrumActionChemicals(value?: v1_dictionary_md_spectrum_action_chemical_pb.SpectrumActionChemical, index?: number): v1_dictionary_md_spectrum_action_chemical_pb.SpectrumActionChemical;

  clearApplicationMethodsList(): void;
  getApplicationMethodsList(): Array<v1_dictionary_md_application_method_pb.ApplicationMethod>;
  setApplicationMethodsList(value: Array<v1_dictionary_md_application_method_pb.ApplicationMethod>): void;
  addApplicationMethods(value?: v1_dictionary_md_application_method_pb.ApplicationMethod, index?: number): v1_dictionary_md_application_method_pb.ApplicationMethod;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductCategoryApplication.AsObject;
  static toObject(includeInstance: boolean, msg: ProductCategoryApplication): ProductCategoryApplication.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductCategoryApplication, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductCategoryApplication;
  static deserializeBinaryFromReader(message: ProductCategoryApplication, reader: jspb.BinaryReader): ProductCategoryApplication;
}

export namespace ProductCategoryApplication {
  export type AsObject = {
    id: number,
    productId: number,
    productName: string,
    categoryId: number,
    categoryName: string,
    applicationRateUnitId: number,
    applicationRateUnitName: string,
    waitingPeriod: string,
    compatibility: string,
    workFluidConsumeRate: string,
    applicationRate: string,
    applicationPeriod: string,
    applicationTemperature: string,
    applicationMultiplicity: string,
    maxApplicationMultiplicity: string,
    applicationRecommendation: string,
    spectrumActionChemicalsList: Array<v1_dictionary_md_spectrum_action_chemical_pb.SpectrumActionChemical.AsObject>,
    applicationMethodsList: Array<v1_dictionary_md_application_method_pb.ApplicationMethod.AsObject>,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ProductCategoryApplicationFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductName(): boolean;
  clearProductName(): void;
  getProductName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setProductName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCategoryId(): boolean;
  clearCategoryId(): void;
  getCategoryId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCategoryId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasCategoryName(): boolean;
  clearCategoryName(): void;
  getCategoryName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCategoryName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductCategoryApplicationFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductCategoryApplicationFilter): ProductCategoryApplicationFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductCategoryApplicationFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductCategoryApplicationFilter;
  static deserializeBinaryFromReader(message: ProductCategoryApplicationFilter, reader: jspb.BinaryReader): ProductCategoryApplicationFilter;
}

export namespace ProductCategoryApplicationFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    categoryId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    categoryName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetProductCategoryApplicationRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductCategoryApplicationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductCategoryApplicationRequest): GetProductCategoryApplicationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductCategoryApplicationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductCategoryApplicationRequest;
  static deserializeBinaryFromReader(message: GetProductCategoryApplicationRequest, reader: jspb.BinaryReader): GetProductCategoryApplicationRequest;
}

export namespace GetProductCategoryApplicationRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductCategoryApplicationResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductCategoryApplication | undefined;
  setItem(value?: ProductCategoryApplication): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductCategoryApplicationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductCategoryApplicationResponse): GetProductCategoryApplicationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductCategoryApplicationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductCategoryApplicationResponse;
  static deserializeBinaryFromReader(message: GetProductCategoryApplicationResponse, reader: jspb.BinaryReader): GetProductCategoryApplicationResponse;
}

export namespace GetProductCategoryApplicationResponse {
  export type AsObject = {
    item?: ProductCategoryApplication.AsObject,
  }
}

export class ListProductCategoryApplicationRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductCategoryApplicationFilter | undefined;
  setFilter(value?: ProductCategoryApplicationFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductCategoryApplicationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductCategoryApplicationRequest): ListProductCategoryApplicationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductCategoryApplicationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductCategoryApplicationRequest;
  static deserializeBinaryFromReader(message: ListProductCategoryApplicationRequest, reader: jspb.BinaryReader): ListProductCategoryApplicationRequest;
}

export namespace ListProductCategoryApplicationRequest {
  export type AsObject = {
    filter?: ProductCategoryApplicationFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductCategoryApplicationResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductCategoryApplication>;
  setItemsList(value: Array<ProductCategoryApplication>): void;
  addItems(value?: ProductCategoryApplication, index?: number): ProductCategoryApplication;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductCategoryApplicationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductCategoryApplicationResponse): ListProductCategoryApplicationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductCategoryApplicationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductCategoryApplicationResponse;
  static deserializeBinaryFromReader(message: ListProductCategoryApplicationResponse, reader: jspb.BinaryReader): ListProductCategoryApplicationResponse;
}

export namespace ListProductCategoryApplicationResponse {
  export type AsObject = {
    itemsList: Array<ProductCategoryApplication.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveProductCategoryApplicationRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductCategoryApplication | undefined;
  setItem(value?: ProductCategoryApplication): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductCategoryApplicationRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductCategoryApplicationRequest): SaveProductCategoryApplicationRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductCategoryApplicationRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductCategoryApplicationRequest;
  static deserializeBinaryFromReader(message: SaveProductCategoryApplicationRequest, reader: jspb.BinaryReader): SaveProductCategoryApplicationRequest;
}

export namespace SaveProductCategoryApplicationRequest {
  export type AsObject = {
    item?: ProductCategoryApplication.AsObject,
  }
}

export class SaveProductCategoryApplicationResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductCategoryApplicationResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductCategoryApplicationResponse): SaveProductCategoryApplicationResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductCategoryApplicationResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductCategoryApplicationResponse;
  static deserializeBinaryFromReader(message: SaveProductCategoryApplicationResponse, reader: jspb.BinaryReader): SaveProductCategoryApplicationResponse;
}

export namespace SaveProductCategoryApplicationResponse {
  export type AsObject = {
    id: number,
  }
}

