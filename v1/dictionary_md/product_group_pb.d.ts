// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/product_group.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";

export class ProductGroup extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getProductType(): v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap];
  setProductType(value: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]): void;

  getProductProcess(): v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap];
  setProductProcess(value: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap]): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductGroup.AsObject;
  static toObject(includeInstance: boolean, msg: ProductGroup): ProductGroup.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductGroup, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductGroup;
  static deserializeBinaryFromReader(message: ProductGroup, reader: jspb.BinaryReader): ProductGroup;
}

export namespace ProductGroup {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    productType: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap],
    productProcess: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap],
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ProductGroupFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasProductProcess(): boolean;
  clearProductProcess(): void;
  getProductProcess(): v1_dictionary_common_enum_product_pb.ProductProcessValue | undefined;
  setProductProcess(value?: v1_dictionary_common_enum_product_pb.ProductProcessValue): void;

  hasProductType(): boolean;
  clearProductType(): void;
  getProductType(): v1_dictionary_common_enum_product_pb.ProductTypeValue | undefined;
  setProductType(value?: v1_dictionary_common_enum_product_pb.ProductTypeValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductGroupFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductGroupFilter): ProductGroupFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductGroupFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductGroupFilter;
  static deserializeBinaryFromReader(message: ProductGroupFilter, reader: jspb.BinaryReader): ProductGroupFilter;
}

export namespace ProductGroupFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    productProcess?: v1_dictionary_common_enum_product_pb.ProductProcessValue.AsObject,
    productType?: v1_dictionary_common_enum_product_pb.ProductTypeValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetProductGroupRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductGroupRequest): GetProductGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductGroupRequest;
  static deserializeBinaryFromReader(message: GetProductGroupRequest, reader: jspb.BinaryReader): GetProductGroupRequest;
}

export namespace GetProductGroupRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductGroupResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductGroup | undefined;
  setItem(value?: ProductGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductGroupResponse): GetProductGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductGroupResponse;
  static deserializeBinaryFromReader(message: GetProductGroupResponse, reader: jspb.BinaryReader): GetProductGroupResponse;
}

export namespace GetProductGroupResponse {
  export type AsObject = {
    item?: ProductGroup.AsObject,
  }
}

export class ListProductGroupRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductGroupFilter | undefined;
  setFilter(value?: ProductGroupFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductGroupRequest): ListProductGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductGroupRequest;
  static deserializeBinaryFromReader(message: ListProductGroupRequest, reader: jspb.BinaryReader): ListProductGroupRequest;
}

export namespace ListProductGroupRequest {
  export type AsObject = {
    filter?: ProductGroupFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductGroupResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductGroup>;
  setItemsList(value: Array<ProductGroup>): void;
  addItems(value?: ProductGroup, index?: number): ProductGroup;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductGroupResponse): ListProductGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductGroupResponse;
  static deserializeBinaryFromReader(message: ListProductGroupResponse, reader: jspb.BinaryReader): ListProductGroupResponse;
}

export namespace ListProductGroupResponse {
  export type AsObject = {
    itemsList: Array<ProductGroup.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveProductGroupRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductGroup | undefined;
  setItem(value?: ProductGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductGroupRequest): SaveProductGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductGroupRequest;
  static deserializeBinaryFromReader(message: SaveProductGroupRequest, reader: jspb.BinaryReader): SaveProductGroupRequest;
}

export namespace SaveProductGroupRequest {
  export type AsObject = {
    item?: ProductGroup.AsObject,
  }
}

export class SaveProductGroupResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductGroupResponse): SaveProductGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductGroupResponse;
  static deserializeBinaryFromReader(message: SaveProductGroupResponse, reader: jspb.BinaryReader): SaveProductGroupResponse;
}

export namespace SaveProductGroupResponse {
  export type AsObject = {
    id: number,
  }
}

