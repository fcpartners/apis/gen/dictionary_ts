// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/active_substance_unit.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ActiveSubstanceUnit extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getActiveSubstanceId(): number;
  setActiveSubstanceId(value: number): void;

  getActiveSubstanceName(): string;
  setActiveSubstanceName(value: string): void;

  getUnitId(): number;
  setUnitId(value: number): void;

  getUnitName(): string;
  setUnitName(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActiveSubstanceUnit.AsObject;
  static toObject(includeInstance: boolean, msg: ActiveSubstanceUnit): ActiveSubstanceUnit.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActiveSubstanceUnit, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActiveSubstanceUnit;
  static deserializeBinaryFromReader(message: ActiveSubstanceUnit, reader: jspb.BinaryReader): ActiveSubstanceUnit;
}

export namespace ActiveSubstanceUnit {
  export type AsObject = {
    id: number,
    activeSubstanceId: number,
    activeSubstanceName: string,
    unitId: number,
    unitName: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ActiveSubstanceUnitFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasActiveSubstanceId(): boolean;
  clearActiveSubstanceId(): void;
  getActiveSubstanceId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setActiveSubstanceId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasActiveSubstanceName(): boolean;
  clearActiveSubstanceName(): void;
  getActiveSubstanceName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setActiveSubstanceName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUnitId(): boolean;
  clearUnitId(): void;
  getUnitId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setUnitId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasUnitName(): boolean;
  clearUnitName(): void;
  getUnitName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUnitName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActiveSubstanceUnitFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ActiveSubstanceUnitFilter): ActiveSubstanceUnitFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActiveSubstanceUnitFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActiveSubstanceUnitFilter;
  static deserializeBinaryFromReader(message: ActiveSubstanceUnitFilter, reader: jspb.BinaryReader): ActiveSubstanceUnitFilter;
}

export namespace ActiveSubstanceUnitFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    activeSubstanceId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    activeSubstanceName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    unitId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    unitName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetActiveSubstanceUnitRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetActiveSubstanceUnitRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetActiveSubstanceUnitRequest): GetActiveSubstanceUnitRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetActiveSubstanceUnitRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetActiveSubstanceUnitRequest;
  static deserializeBinaryFromReader(message: GetActiveSubstanceUnitRequest, reader: jspb.BinaryReader): GetActiveSubstanceUnitRequest;
}

export namespace GetActiveSubstanceUnitRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetActiveSubstanceUnitResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ActiveSubstanceUnit | undefined;
  setItem(value?: ActiveSubstanceUnit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetActiveSubstanceUnitResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetActiveSubstanceUnitResponse): GetActiveSubstanceUnitResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetActiveSubstanceUnitResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetActiveSubstanceUnitResponse;
  static deserializeBinaryFromReader(message: GetActiveSubstanceUnitResponse, reader: jspb.BinaryReader): GetActiveSubstanceUnitResponse;
}

export namespace GetActiveSubstanceUnitResponse {
  export type AsObject = {
    item?: ActiveSubstanceUnit.AsObject,
  }
}

export class ListActiveSubstanceUnitRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ActiveSubstanceUnitFilter | undefined;
  setFilter(value?: ActiveSubstanceUnitFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListActiveSubstanceUnitRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListActiveSubstanceUnitRequest): ListActiveSubstanceUnitRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListActiveSubstanceUnitRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListActiveSubstanceUnitRequest;
  static deserializeBinaryFromReader(message: ListActiveSubstanceUnitRequest, reader: jspb.BinaryReader): ListActiveSubstanceUnitRequest;
}

export namespace ListActiveSubstanceUnitRequest {
  export type AsObject = {
    filter?: ActiveSubstanceUnitFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListActiveSubstanceUnitResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ActiveSubstanceUnit>;
  setItemsList(value: Array<ActiveSubstanceUnit>): void;
  addItems(value?: ActiveSubstanceUnit, index?: number): ActiveSubstanceUnit;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListActiveSubstanceUnitResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListActiveSubstanceUnitResponse): ListActiveSubstanceUnitResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListActiveSubstanceUnitResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListActiveSubstanceUnitResponse;
  static deserializeBinaryFromReader(message: ListActiveSubstanceUnitResponse, reader: jspb.BinaryReader): ListActiveSubstanceUnitResponse;
}

export namespace ListActiveSubstanceUnitResponse {
  export type AsObject = {
    itemsList: Array<ActiveSubstanceUnit.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveActiveSubstanceUnitRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ActiveSubstanceUnit | undefined;
  setItem(value?: ActiveSubstanceUnit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveActiveSubstanceUnitRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveActiveSubstanceUnitRequest): SaveActiveSubstanceUnitRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveActiveSubstanceUnitRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveActiveSubstanceUnitRequest;
  static deserializeBinaryFromReader(message: SaveActiveSubstanceUnitRequest, reader: jspb.BinaryReader): SaveActiveSubstanceUnitRequest;
}

export namespace SaveActiveSubstanceUnitRequest {
  export type AsObject = {
    item?: ActiveSubstanceUnit.AsObject,
  }
}

export class SaveActiveSubstanceUnitResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveActiveSubstanceUnitResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveActiveSubstanceUnitResponse): SaveActiveSubstanceUnitResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveActiveSubstanceUnitResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveActiveSubstanceUnitResponse;
  static deserializeBinaryFromReader(message: SaveActiveSubstanceUnitResponse, reader: jspb.BinaryReader): SaveActiveSubstanceUnitResponse;
}

export namespace SaveActiveSubstanceUnitResponse {
  export type AsObject = {
    id: number,
  }
}

