// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/file.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_md_product_package_pb from "../../v1/dictionary_md/product_package_pb";

export class File extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getUrl(): string;
  setUrl(value: string): void;

  getType(): v1_dictionary_common_enum_product_pb.FileTypeMap[keyof v1_dictionary_common_enum_product_pb.FileTypeMap];
  setType(value: v1_dictionary_common_enum_product_pb.FileTypeMap[keyof v1_dictionary_common_enum_product_pb.FileTypeMap]): void;

  getQuality(): v1_dictionary_common_enum_product_pb.ImageQualityMap[keyof v1_dictionary_common_enum_product_pb.ImageQualityMap];
  setQuality(value: v1_dictionary_common_enum_product_pb.ImageQualityMap[keyof v1_dictionary_common_enum_product_pb.ImageQualityMap]): void;

  getOrder(): number;
  setOrder(value: number): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): google_protobuf_timestamp_pb.Timestamp | undefined;
  setCreatedAt(value?: google_protobuf_timestamp_pb.Timestamp): void;

  getCreatedBy(): string;
  setCreatedBy(value: string): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): File.AsObject;
  static toObject(includeInstance: boolean, msg: File): File.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: File, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): File;
  static deserializeBinaryFromReader(message: File, reader: jspb.BinaryReader): File;
}

export namespace File {
  export type AsObject = {
    id: number,
    name: string,
    url: string,
    type: v1_dictionary_common_enum_product_pb.FileTypeMap[keyof v1_dictionary_common_enum_product_pb.FileTypeMap],
    quality: v1_dictionary_common_enum_product_pb.ImageQualityMap[keyof v1_dictionary_common_enum_product_pb.ImageQualityMap],
    order: number,
    createdAt?: google_protobuf_timestamp_pb.Timestamp.AsObject,
    createdBy: string,
  }
}

export class ProductFileFilter extends jspb.Message {
  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasQuality(): boolean;
  clearQuality(): void;
  getQuality(): v1_dictionary_common_enum_product_pb.ImageQualityValue | undefined;
  setQuality(value?: v1_dictionary_common_enum_product_pb.ImageQualityValue): void;

  clearProductPackageIdsList(): void;
  getProductPackageIdsList(): Array<number>;
  setProductPackageIdsList(value: Array<number>): void;
  addProductPackageIds(value: number, index?: number): number;

  clearFileTypeList(): void;
  getFileTypeList(): Array<v1_dictionary_common_enum_product_pb.FileTypeMap[keyof v1_dictionary_common_enum_product_pb.FileTypeMap]>;
  setFileTypeList(value: Array<v1_dictionary_common_enum_product_pb.FileTypeMap[keyof v1_dictionary_common_enum_product_pb.FileTypeMap]>): void;
  addFileType(value: v1_dictionary_common_enum_product_pb.FileTypeMap[keyof v1_dictionary_common_enum_product_pb.FileTypeMap], index?: number): v1_dictionary_common_enum_product_pb.FileTypeMap[keyof v1_dictionary_common_enum_product_pb.FileTypeMap];

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductFileFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductFileFilter): ProductFileFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductFileFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductFileFilter;
  static deserializeBinaryFromReader(message: ProductFileFilter, reader: jspb.BinaryReader): ProductFileFilter;
}

export namespace ProductFileFilter {
  export type AsObject = {
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    quality?: v1_dictionary_common_enum_product_pb.ImageQualityValue.AsObject,
    productPackageIdsList: Array<number>,
    fileTypeList: Array<v1_dictionary_common_enum_product_pb.FileTypeMap[keyof v1_dictionary_common_enum_product_pb.FileTypeMap]>,
  }
}

export class GetProductFileRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductFileRequest): GetProductFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductFileRequest;
  static deserializeBinaryFromReader(message: GetProductFileRequest, reader: jspb.BinaryReader): GetProductFileRequest;
}

export namespace GetProductFileRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductFileResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): File | undefined;
  setItem(value?: File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductFileResponse): GetProductFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductFileResponse;
  static deserializeBinaryFromReader(message: GetProductFileResponse, reader: jspb.BinaryReader): GetProductFileResponse;
}

export namespace GetProductFileResponse {
  export type AsObject = {
    item?: File.AsObject,
  }
}

export class ListProductFileRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductFileFilter | undefined;
  setFilter(value?: ProductFileFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductFileRequest): ListProductFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductFileRequest;
  static deserializeBinaryFromReader(message: ListProductFileRequest, reader: jspb.BinaryReader): ListProductFileRequest;
}

export namespace ListProductFileRequest {
  export type AsObject = {
    filter?: ProductFileFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductFileResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<File>;
  setItemsList(value: Array<File>): void;
  addItems(value?: File, index?: number): File;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductFileResponse): ListProductFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductFileResponse;
  static deserializeBinaryFromReader(message: ListProductFileResponse, reader: jspb.BinaryReader): ListProductFileResponse;
}

export namespace ListProductFileResponse {
  export type AsObject = {
    itemsList: Array<File.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveProductFileRequest extends jspb.Message {
  getProductId(): number;
  setProductId(value: number): void;

  hasFile(): boolean;
  clearFile(): void;
  getFile(): File | undefined;
  setFile(value?: File): void;

  getProductPackageId(): number;
  setProductPackageId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductFileRequest): SaveProductFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductFileRequest;
  static deserializeBinaryFromReader(message: SaveProductFileRequest, reader: jspb.BinaryReader): SaveProductFileRequest;
}

export namespace SaveProductFileRequest {
  export type AsObject = {
    productId: number,
    file?: File.AsObject,
    productPackageId: number,
  }
}

export class SaveProductFileResponse extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): File | undefined;
  setFile(value?: File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductFileResponse): SaveProductFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductFileResponse;
  static deserializeBinaryFromReader(message: SaveProductFileResponse, reader: jspb.BinaryReader): SaveProductFileResponse;
}

export namespace SaveProductFileResponse {
  export type AsObject = {
    file?: File.AsObject,
  }
}

export class DeleteProductFileRequest extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): File | undefined;
  setFile(value?: File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteProductFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteProductFileRequest): DeleteProductFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteProductFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteProductFileRequest;
  static deserializeBinaryFromReader(message: DeleteProductFileRequest, reader: jspb.BinaryReader): DeleteProductFileRequest;
}

export namespace DeleteProductFileRequest {
  export type AsObject = {
    file?: File.AsObject,
  }
}

export class DeleteProductFileResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DeleteProductFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DeleteProductFileResponse): DeleteProductFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DeleteProductFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DeleteProductFileResponse;
  static deserializeBinaryFromReader(message: DeleteProductFileResponse, reader: jspb.BinaryReader): DeleteProductFileResponse;
}

export namespace DeleteProductFileResponse {
  export type AsObject = {
  }
}

export class GroupedPackageFile extends jspb.Message {
  hasPackage(): boolean;
  clearPackage(): void;
  getPackage(): v1_dictionary_md_product_package_pb.ProductPackage | undefined;
  setPackage(value?: v1_dictionary_md_product_package_pb.ProductPackage): void;

  clearFilesList(): void;
  getFilesList(): Array<File>;
  setFilesList(value: Array<File>): void;
  addFiles(value?: File, index?: number): File;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GroupedPackageFile.AsObject;
  static toObject(includeInstance: boolean, msg: GroupedPackageFile): GroupedPackageFile.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GroupedPackageFile, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GroupedPackageFile;
  static deserializeBinaryFromReader(message: GroupedPackageFile, reader: jspb.BinaryReader): GroupedPackageFile;
}

export namespace GroupedPackageFile {
  export type AsObject = {
    pb_package?: v1_dictionary_md_product_package_pb.ProductPackage.AsObject,
    filesList: Array<File.AsObject>,
  }
}

export class ListGroupedPackageFileRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductFileFilter | undefined;
  setFilter(value?: ProductFileFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGroupedPackageFileRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListGroupedPackageFileRequest): ListGroupedPackageFileRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGroupedPackageFileRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGroupedPackageFileRequest;
  static deserializeBinaryFromReader(message: ListGroupedPackageFileRequest, reader: jspb.BinaryReader): ListGroupedPackageFileRequest;
}

export namespace ListGroupedPackageFileRequest {
  export type AsObject = {
    filter?: ProductFileFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
  }
}

export class ListGroupedPackageFileResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<GroupedPackageFile>;
  setItemsList(value: Array<GroupedPackageFile>): void;
  addItems(value?: GroupedPackageFile, index?: number): GroupedPackageFile;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListGroupedPackageFileResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListGroupedPackageFileResponse): ListGroupedPackageFileResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListGroupedPackageFileResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListGroupedPackageFileResponse;
  static deserializeBinaryFromReader(message: ListGroupedPackageFileResponse, reader: jspb.BinaryReader): ListGroupedPackageFileResponse;
}

export namespace ListGroupedPackageFileResponse {
  export type AsObject = {
    itemsList: Array<GroupedPackageFile.AsObject>,
  }
}

