// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/product_quantity_type.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ProductQuantityType extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getProductId(): number;
  setProductId(value: number): void;

  getProductName(): string;
  setProductName(value: string): void;

  getQuantityTypeId(): number;
  setQuantityTypeId(value: number): void;

  getQuantityTypeName(): string;
  setQuantityTypeName(value: string): void;

  getQuantityTypeShortName(): string;
  setQuantityTypeShortName(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductQuantityType.AsObject;
  static toObject(includeInstance: boolean, msg: ProductQuantityType): ProductQuantityType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductQuantityType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductQuantityType;
  static deserializeBinaryFromReader(message: ProductQuantityType, reader: jspb.BinaryReader): ProductQuantityType;
}

export namespace ProductQuantityType {
  export type AsObject = {
    id: number,
    productId: number,
    productName: string,
    quantityTypeId: number,
    quantityTypeName: string,
    quantityTypeShortName: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ProductQuantityTypeFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasQuantityTypeId(): boolean;
  clearQuantityTypeId(): void;
  getQuantityTypeId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setQuantityTypeId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductQuantityTypeFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductQuantityTypeFilter): ProductQuantityTypeFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductQuantityTypeFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductQuantityTypeFilter;
  static deserializeBinaryFromReader(message: ProductQuantityTypeFilter, reader: jspb.BinaryReader): ProductQuantityTypeFilter;
}

export namespace ProductQuantityTypeFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    quantityTypeId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetProductQuantityTypeRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductQuantityTypeRequest): GetProductQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductQuantityTypeRequest;
  static deserializeBinaryFromReader(message: GetProductQuantityTypeRequest, reader: jspb.BinaryReader): GetProductQuantityTypeRequest;
}

export namespace GetProductQuantityTypeRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductQuantityTypeResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductQuantityType | undefined;
  setItem(value?: ProductQuantityType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductQuantityTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductQuantityTypeResponse): GetProductQuantityTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductQuantityTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductQuantityTypeResponse;
  static deserializeBinaryFromReader(message: GetProductQuantityTypeResponse, reader: jspb.BinaryReader): GetProductQuantityTypeResponse;
}

export namespace GetProductQuantityTypeResponse {
  export type AsObject = {
    item?: ProductQuantityType.AsObject,
  }
}

export class ListProductQuantityTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductQuantityTypeFilter | undefined;
  setFilter(value?: ProductQuantityTypeFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductQuantityTypeRequest): ListProductQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductQuantityTypeRequest;
  static deserializeBinaryFromReader(message: ListProductQuantityTypeRequest, reader: jspb.BinaryReader): ListProductQuantityTypeRequest;
}

export namespace ListProductQuantityTypeRequest {
  export type AsObject = {
    filter?: ProductQuantityTypeFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductQuantityTypeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductQuantityType>;
  setItemsList(value: Array<ProductQuantityType>): void;
  addItems(value?: ProductQuantityType, index?: number): ProductQuantityType;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductQuantityTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductQuantityTypeResponse): ListProductQuantityTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductQuantityTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductQuantityTypeResponse;
  static deserializeBinaryFromReader(message: ListProductQuantityTypeResponse, reader: jspb.BinaryReader): ListProductQuantityTypeResponse;
}

export namespace ListProductQuantityTypeResponse {
  export type AsObject = {
    itemsList: Array<ProductQuantityType.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveProductQuantityTypeRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductQuantityType | undefined;
  setItem(value?: ProductQuantityType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductQuantityTypeRequest): SaveProductQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductQuantityTypeRequest;
  static deserializeBinaryFromReader(message: SaveProductQuantityTypeRequest, reader: jspb.BinaryReader): SaveProductQuantityTypeRequest;
}

export namespace SaveProductQuantityTypeRequest {
  export type AsObject = {
    item?: ProductQuantityType.AsObject,
  }
}

export class SaveProductQuantityTypeResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductQuantityTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductQuantityTypeResponse): SaveProductQuantityTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductQuantityTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductQuantityTypeResponse;
  static deserializeBinaryFromReader(message: SaveProductQuantityTypeResponse, reader: jspb.BinaryReader): SaveProductQuantityTypeResponse;
}

export namespace SaveProductQuantityTypeResponse {
  export type AsObject = {
    id: number,
  }
}

