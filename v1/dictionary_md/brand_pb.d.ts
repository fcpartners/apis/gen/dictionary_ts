// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/brand.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class Brand extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getAddress(): string;
  setAddress(value: string): void;

  getOrganization(): string;
  setOrganization(value: string): void;

  getPhone(): string;
  setPhone(value: string): void;

  getUrl(): string;
  setUrl(value: string): void;

  getLogo(): string;
  setLogo(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Brand.AsObject;
  static toObject(includeInstance: boolean, msg: Brand): Brand.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Brand, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Brand;
  static deserializeBinaryFromReader(message: Brand, reader: jspb.BinaryReader): Brand;
}

export namespace Brand {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    address: string,
    organization: string,
    phone: string,
    url: string,
    logo: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class BrandFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasAddress(): boolean;
  clearAddress(): void;
  getAddress(): google_protobuf_wrappers_pb.StringValue | undefined;
  setAddress(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasOrganization(): boolean;
  clearOrganization(): void;
  getOrganization(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOrganization(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasPhone(): boolean;
  clearPhone(): void;
  getPhone(): google_protobuf_wrappers_pb.StringValue | undefined;
  setPhone(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): BrandFilter.AsObject;
  static toObject(includeInstance: boolean, msg: BrandFilter): BrandFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: BrandFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): BrandFilter;
  static deserializeBinaryFromReader(message: BrandFilter, reader: jspb.BinaryReader): BrandFilter;
}

export namespace BrandFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    address?: google_protobuf_wrappers_pb.StringValue.AsObject,
    organization?: google_protobuf_wrappers_pb.StringValue.AsObject,
    phone?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetBrandRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetBrandRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetBrandRequest): GetBrandRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetBrandRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetBrandRequest;
  static deserializeBinaryFromReader(message: GetBrandRequest, reader: jspb.BinaryReader): GetBrandRequest;
}

export namespace GetBrandRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetBrandResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Brand | undefined;
  setItem(value?: Brand): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetBrandResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetBrandResponse): GetBrandResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetBrandResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetBrandResponse;
  static deserializeBinaryFromReader(message: GetBrandResponse, reader: jspb.BinaryReader): GetBrandResponse;
}

export namespace GetBrandResponse {
  export type AsObject = {
    item?: Brand.AsObject,
  }
}

export class ListBrandRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): BrandFilter | undefined;
  setFilter(value?: BrandFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListBrandRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListBrandRequest): ListBrandRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListBrandRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListBrandRequest;
  static deserializeBinaryFromReader(message: ListBrandRequest, reader: jspb.BinaryReader): ListBrandRequest;
}

export namespace ListBrandRequest {
  export type AsObject = {
    filter?: BrandFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListBrandResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Brand>;
  setItemsList(value: Array<Brand>): void;
  addItems(value?: Brand, index?: number): Brand;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListBrandResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListBrandResponse): ListBrandResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListBrandResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListBrandResponse;
  static deserializeBinaryFromReader(message: ListBrandResponse, reader: jspb.BinaryReader): ListBrandResponse;
}

export namespace ListBrandResponse {
  export type AsObject = {
    itemsList: Array<Brand.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveBrandRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Brand | undefined;
  setItem(value?: Brand): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveBrandRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveBrandRequest): SaveBrandRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveBrandRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveBrandRequest;
  static deserializeBinaryFromReader(message: SaveBrandRequest, reader: jspb.BinaryReader): SaveBrandRequest;
}

export namespace SaveBrandRequest {
  export type AsObject = {
    item?: Brand.AsObject,
  }
}

export class SaveBrandResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveBrandResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveBrandResponse): SaveBrandResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveBrandResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveBrandResponse;
  static deserializeBinaryFromReader(message: SaveBrandResponse, reader: jspb.BinaryReader): SaveBrandResponse;
}

export namespace SaveBrandResponse {
  export type AsObject = {
    id: number,
  }
}

