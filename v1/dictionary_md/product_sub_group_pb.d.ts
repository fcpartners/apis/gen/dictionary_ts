// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/product_sub_group.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";
import * as v1_dictionary_md_product_group_pb from "../../v1/dictionary_md/product_group_pb";

export class ProductSubGroup extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getProductProcess(): v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap];
  setProductProcess(value: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap]): void;

  getProductType(): v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap];
  setProductType(value: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]): void;

  getProductGroupId(): number;
  setProductGroupId(value: number): void;

  getProductGroupName(): string;
  setProductGroupName(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductSubGroup.AsObject;
  static toObject(includeInstance: boolean, msg: ProductSubGroup): ProductSubGroup.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductSubGroup, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductSubGroup;
  static deserializeBinaryFromReader(message: ProductSubGroup, reader: jspb.BinaryReader): ProductSubGroup;
}

export namespace ProductSubGroup {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    productProcess: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap],
    productType: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap],
    productGroupId: number,
    productGroupName: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ProductSubGroupFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasProductProcess(): boolean;
  clearProductProcess(): void;
  getProductProcess(): v1_dictionary_common_enum_product_pb.ProductProcessValue | undefined;
  setProductProcess(value?: v1_dictionary_common_enum_product_pb.ProductProcessValue): void;

  hasProductType(): boolean;
  clearProductType(): void;
  getProductType(): v1_dictionary_common_enum_product_pb.ProductTypeValue | undefined;
  setProductType(value?: v1_dictionary_common_enum_product_pb.ProductTypeValue): void;

  hasProductGroupId(): boolean;
  clearProductGroupId(): void;
  getProductGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductGroupName(): boolean;
  clearProductGroupName(): void;
  getProductGroupName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setProductGroupName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductSubGroupFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductSubGroupFilter): ProductSubGroupFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductSubGroupFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductSubGroupFilter;
  static deserializeBinaryFromReader(message: ProductSubGroupFilter, reader: jspb.BinaryReader): ProductSubGroupFilter;
}

export namespace ProductSubGroupFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    productProcess?: v1_dictionary_common_enum_product_pb.ProductProcessValue.AsObject,
    productType?: v1_dictionary_common_enum_product_pb.ProductTypeValue.AsObject,
    productGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productGroupName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetProductSubGroupRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductSubGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductSubGroupRequest): GetProductSubGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductSubGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductSubGroupRequest;
  static deserializeBinaryFromReader(message: GetProductSubGroupRequest, reader: jspb.BinaryReader): GetProductSubGroupRequest;
}

export namespace GetProductSubGroupRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductSubGroupResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductSubGroup | undefined;
  setItem(value?: ProductSubGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductSubGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductSubGroupResponse): GetProductSubGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductSubGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductSubGroupResponse;
  static deserializeBinaryFromReader(message: GetProductSubGroupResponse, reader: jspb.BinaryReader): GetProductSubGroupResponse;
}

export namespace GetProductSubGroupResponse {
  export type AsObject = {
    item?: ProductSubGroup.AsObject,
  }
}

export class ListProductSubGroupRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductSubGroupFilter | undefined;
  setFilter(value?: ProductSubGroupFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductSubGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductSubGroupRequest): ListProductSubGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductSubGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductSubGroupRequest;
  static deserializeBinaryFromReader(message: ListProductSubGroupRequest, reader: jspb.BinaryReader): ListProductSubGroupRequest;
}

export namespace ListProductSubGroupRequest {
  export type AsObject = {
    filter?: ProductSubGroupFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductSubGroupResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductSubGroup>;
  setItemsList(value: Array<ProductSubGroup>): void;
  addItems(value?: ProductSubGroup, index?: number): ProductSubGroup;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductSubGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductSubGroupResponse): ListProductSubGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductSubGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductSubGroupResponse;
  static deserializeBinaryFromReader(message: ListProductSubGroupResponse, reader: jspb.BinaryReader): ListProductSubGroupResponse;
}

export namespace ListProductSubGroupResponse {
  export type AsObject = {
    itemsList: Array<ProductSubGroup.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveProductSubGroupRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductSubGroup | undefined;
  setItem(value?: ProductSubGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductSubGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductSubGroupRequest): SaveProductSubGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductSubGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductSubGroupRequest;
  static deserializeBinaryFromReader(message: SaveProductSubGroupRequest, reader: jspb.BinaryReader): SaveProductSubGroupRequest;
}

export namespace SaveProductSubGroupRequest {
  export type AsObject = {
    item?: ProductSubGroup.AsObject,
  }
}

export class SaveProductSubGroupResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductSubGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductSubGroupResponse): SaveProductSubGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductSubGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductSubGroupResponse;
  static deserializeBinaryFromReader(message: SaveProductSubGroupResponse, reader: jspb.BinaryReader): SaveProductSubGroupResponse;
}

export namespace SaveProductSubGroupResponse {
  export type AsObject = {
    id: number,
  }
}

