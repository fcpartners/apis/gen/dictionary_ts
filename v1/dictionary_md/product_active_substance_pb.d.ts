// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/product_active_substance.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ProductActiveSubstance extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getProductId(): number;
  setProductId(value: number): void;

  getProductName(): string;
  setProductName(value: string): void;

  getActiveSubstanceId(): number;
  setActiveSubstanceId(value: number): void;

  getActiveSubstanceName(): string;
  setActiveSubstanceName(value: string): void;

  getConcentration(): number;
  setConcentration(value: number): void;

  getUnitId(): number;
  setUnitId(value: number): void;

  getUnitName(): string;
  setUnitName(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductActiveSubstance.AsObject;
  static toObject(includeInstance: boolean, msg: ProductActiveSubstance): ProductActiveSubstance.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductActiveSubstance, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductActiveSubstance;
  static deserializeBinaryFromReader(message: ProductActiveSubstance, reader: jspb.BinaryReader): ProductActiveSubstance;
}

export namespace ProductActiveSubstance {
  export type AsObject = {
    id: number,
    productId: number,
    productName: string,
    activeSubstanceId: number,
    activeSubstanceName: string,
    concentration: number,
    unitId: number,
    unitName: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ProductActiveSubstanceFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasActiveSubstanceId(): boolean;
  clearActiveSubstanceId(): void;
  getActiveSubstanceId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setActiveSubstanceId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasUnitId(): boolean;
  clearUnitId(): void;
  getUnitId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setUnitId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasConcentration(): boolean;
  clearConcentration(): void;
  getConcentration(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setConcentration(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductActiveSubstanceFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductActiveSubstanceFilter): ProductActiveSubstanceFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductActiveSubstanceFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductActiveSubstanceFilter;
  static deserializeBinaryFromReader(message: ProductActiveSubstanceFilter, reader: jspb.BinaryReader): ProductActiveSubstanceFilter;
}

export namespace ProductActiveSubstanceFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    activeSubstanceId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    unitId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    concentration?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetProductActiveSubstanceRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductActiveSubstanceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductActiveSubstanceRequest): GetProductActiveSubstanceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductActiveSubstanceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductActiveSubstanceRequest;
  static deserializeBinaryFromReader(message: GetProductActiveSubstanceRequest, reader: jspb.BinaryReader): GetProductActiveSubstanceRequest;
}

export namespace GetProductActiveSubstanceRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductActiveSubstanceResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductActiveSubstance | undefined;
  setItem(value?: ProductActiveSubstance): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductActiveSubstanceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductActiveSubstanceResponse): GetProductActiveSubstanceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductActiveSubstanceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductActiveSubstanceResponse;
  static deserializeBinaryFromReader(message: GetProductActiveSubstanceResponse, reader: jspb.BinaryReader): GetProductActiveSubstanceResponse;
}

export namespace GetProductActiveSubstanceResponse {
  export type AsObject = {
    item?: ProductActiveSubstance.AsObject,
  }
}

export class ListProductActiveSubstanceRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductActiveSubstanceFilter | undefined;
  setFilter(value?: ProductActiveSubstanceFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductActiveSubstanceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductActiveSubstanceRequest): ListProductActiveSubstanceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductActiveSubstanceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductActiveSubstanceRequest;
  static deserializeBinaryFromReader(message: ListProductActiveSubstanceRequest, reader: jspb.BinaryReader): ListProductActiveSubstanceRequest;
}

export namespace ListProductActiveSubstanceRequest {
  export type AsObject = {
    filter?: ProductActiveSubstanceFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductActiveSubstanceResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductActiveSubstance>;
  setItemsList(value: Array<ProductActiveSubstance>): void;
  addItems(value?: ProductActiveSubstance, index?: number): ProductActiveSubstance;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductActiveSubstanceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductActiveSubstanceResponse): ListProductActiveSubstanceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductActiveSubstanceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductActiveSubstanceResponse;
  static deserializeBinaryFromReader(message: ListProductActiveSubstanceResponse, reader: jspb.BinaryReader): ListProductActiveSubstanceResponse;
}

export namespace ListProductActiveSubstanceResponse {
  export type AsObject = {
    itemsList: Array<ProductActiveSubstance.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveProductActiveSubstanceRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductActiveSubstance | undefined;
  setItem(value?: ProductActiveSubstance): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductActiveSubstanceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductActiveSubstanceRequest): SaveProductActiveSubstanceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductActiveSubstanceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductActiveSubstanceRequest;
  static deserializeBinaryFromReader(message: SaveProductActiveSubstanceRequest, reader: jspb.BinaryReader): SaveProductActiveSubstanceRequest;
}

export namespace SaveProductActiveSubstanceRequest {
  export type AsObject = {
    item?: ProductActiveSubstance.AsObject,
  }
}

export class SaveProductActiveSubstanceResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductActiveSubstanceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductActiveSubstanceResponse): SaveProductActiveSubstanceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductActiveSubstanceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductActiveSubstanceResponse;
  static deserializeBinaryFromReader(message: SaveProductActiveSubstanceResponse, reader: jspb.BinaryReader): SaveProductActiveSubstanceResponse;
}

export namespace SaveProductActiveSubstanceResponse {
  export type AsObject = {
    id: number,
  }
}

