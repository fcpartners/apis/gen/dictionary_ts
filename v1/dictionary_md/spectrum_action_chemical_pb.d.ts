// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/spectrum_action_chemical.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";

export class SpectrumActionChemical extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getType(): v1_dictionary_common_enum_product_pb.SpectrumActionTypeMap[keyof v1_dictionary_common_enum_product_pb.SpectrumActionTypeMap];
  setType(value: v1_dictionary_common_enum_product_pb.SpectrumActionTypeMap[keyof v1_dictionary_common_enum_product_pb.SpectrumActionTypeMap]): void;

  clearPhotoUrlsList(): void;
  getPhotoUrlsList(): Array<string>;
  setPhotoUrlsList(value: Array<string>): void;
  addPhotoUrls(value: string, index?: number): string;

  clearVideoUrlsList(): void;
  getVideoUrlsList(): Array<string>;
  setVideoUrlsList(value: Array<string>): void;
  addVideoUrls(value: string, index?: number): string;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SpectrumActionChemical.AsObject;
  static toObject(includeInstance: boolean, msg: SpectrumActionChemical): SpectrumActionChemical.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SpectrumActionChemical, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SpectrumActionChemical;
  static deserializeBinaryFromReader(message: SpectrumActionChemical, reader: jspb.BinaryReader): SpectrumActionChemical;
}

export namespace SpectrumActionChemical {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    type: v1_dictionary_common_enum_product_pb.SpectrumActionTypeMap[keyof v1_dictionary_common_enum_product_pb.SpectrumActionTypeMap],
    photoUrlsList: Array<string>,
    videoUrlsList: Array<string>,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class SpectrumActionChemicalFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasType(): boolean;
  clearType(): void;
  getType(): v1_dictionary_common_enum_product_pb.SpectrumActionTypeValue | undefined;
  setType(value?: v1_dictionary_common_enum_product_pb.SpectrumActionTypeValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SpectrumActionChemicalFilter.AsObject;
  static toObject(includeInstance: boolean, msg: SpectrumActionChemicalFilter): SpectrumActionChemicalFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SpectrumActionChemicalFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SpectrumActionChemicalFilter;
  static deserializeBinaryFromReader(message: SpectrumActionChemicalFilter, reader: jspb.BinaryReader): SpectrumActionChemicalFilter;
}

export namespace SpectrumActionChemicalFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    type?: v1_dictionary_common_enum_product_pb.SpectrumActionTypeValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetSpectrumActionChemicalRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSpectrumActionChemicalRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetSpectrumActionChemicalRequest): GetSpectrumActionChemicalRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSpectrumActionChemicalRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSpectrumActionChemicalRequest;
  static deserializeBinaryFromReader(message: GetSpectrumActionChemicalRequest, reader: jspb.BinaryReader): GetSpectrumActionChemicalRequest;
}

export namespace GetSpectrumActionChemicalRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetSpectrumActionChemicalResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): SpectrumActionChemical | undefined;
  setItem(value?: SpectrumActionChemical): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetSpectrumActionChemicalResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetSpectrumActionChemicalResponse): GetSpectrumActionChemicalResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetSpectrumActionChemicalResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetSpectrumActionChemicalResponse;
  static deserializeBinaryFromReader(message: GetSpectrumActionChemicalResponse, reader: jspb.BinaryReader): GetSpectrumActionChemicalResponse;
}

export namespace GetSpectrumActionChemicalResponse {
  export type AsObject = {
    item?: SpectrumActionChemical.AsObject,
  }
}

export class ListSpectrumActionChemicalRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): SpectrumActionChemicalFilter | undefined;
  setFilter(value?: SpectrumActionChemicalFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSpectrumActionChemicalRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListSpectrumActionChemicalRequest): ListSpectrumActionChemicalRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSpectrumActionChemicalRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSpectrumActionChemicalRequest;
  static deserializeBinaryFromReader(message: ListSpectrumActionChemicalRequest, reader: jspb.BinaryReader): ListSpectrumActionChemicalRequest;
}

export namespace ListSpectrumActionChemicalRequest {
  export type AsObject = {
    filter?: SpectrumActionChemicalFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListSpectrumActionChemicalResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<SpectrumActionChemical>;
  setItemsList(value: Array<SpectrumActionChemical>): void;
  addItems(value?: SpectrumActionChemical, index?: number): SpectrumActionChemical;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListSpectrumActionChemicalResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListSpectrumActionChemicalResponse): ListSpectrumActionChemicalResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListSpectrumActionChemicalResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListSpectrumActionChemicalResponse;
  static deserializeBinaryFromReader(message: ListSpectrumActionChemicalResponse, reader: jspb.BinaryReader): ListSpectrumActionChemicalResponse;
}

export namespace ListSpectrumActionChemicalResponse {
  export type AsObject = {
    itemsList: Array<SpectrumActionChemical.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveSpectrumActionChemicalRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): SpectrumActionChemical | undefined;
  setItem(value?: SpectrumActionChemical): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSpectrumActionChemicalRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSpectrumActionChemicalRequest): SaveSpectrumActionChemicalRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSpectrumActionChemicalRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSpectrumActionChemicalRequest;
  static deserializeBinaryFromReader(message: SaveSpectrumActionChemicalRequest, reader: jspb.BinaryReader): SaveSpectrumActionChemicalRequest;
}

export namespace SaveSpectrumActionChemicalRequest {
  export type AsObject = {
    item?: SpectrumActionChemical.AsObject,
  }
}

export class SaveSpectrumActionChemicalResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveSpectrumActionChemicalResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveSpectrumActionChemicalResponse): SaveSpectrumActionChemicalResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveSpectrumActionChemicalResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveSpectrumActionChemicalResponse;
  static deserializeBinaryFromReader(message: SaveSpectrumActionChemicalResponse, reader: jspb.BinaryReader): SaveSpectrumActionChemicalResponse;
}

export namespace SaveSpectrumActionChemicalResponse {
  export type AsObject = {
    id: number,
  }
}

