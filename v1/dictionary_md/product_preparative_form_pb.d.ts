// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/product_preparative_form.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ProductPreparativeForm extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getProductId(): number;
  setProductId(value: number): void;

  getProductName(): string;
  setProductName(value: string): void;

  getPreparativeFormId(): number;
  setPreparativeFormId(value: number): void;

  getPreparativeFormName(): string;
  setPreparativeFormName(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductPreparativeForm.AsObject;
  static toObject(includeInstance: boolean, msg: ProductPreparativeForm): ProductPreparativeForm.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductPreparativeForm, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductPreparativeForm;
  static deserializeBinaryFromReader(message: ProductPreparativeForm, reader: jspb.BinaryReader): ProductPreparativeForm;
}

export namespace ProductPreparativeForm {
  export type AsObject = {
    id: number,
    productId: number,
    productName: string,
    preparativeFormId: number,
    preparativeFormName: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ProductPreparativeFormFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasPreparativeFormId(): boolean;
  clearPreparativeFormId(): void;
  getPreparativeFormId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setPreparativeFormId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductPreparativeFormFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductPreparativeFormFilter): ProductPreparativeFormFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductPreparativeFormFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductPreparativeFormFilter;
  static deserializeBinaryFromReader(message: ProductPreparativeFormFilter, reader: jspb.BinaryReader): ProductPreparativeFormFilter;
}

export namespace ProductPreparativeFormFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    preparativeFormId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetProductPreparativeFormRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductPreparativeFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductPreparativeFormRequest): GetProductPreparativeFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductPreparativeFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductPreparativeFormRequest;
  static deserializeBinaryFromReader(message: GetProductPreparativeFormRequest, reader: jspb.BinaryReader): GetProductPreparativeFormRequest;
}

export namespace GetProductPreparativeFormRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductPreparativeFormResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductPreparativeForm | undefined;
  setItem(value?: ProductPreparativeForm): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductPreparativeFormResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductPreparativeFormResponse): GetProductPreparativeFormResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductPreparativeFormResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductPreparativeFormResponse;
  static deserializeBinaryFromReader(message: GetProductPreparativeFormResponse, reader: jspb.BinaryReader): GetProductPreparativeFormResponse;
}

export namespace GetProductPreparativeFormResponse {
  export type AsObject = {
    item?: ProductPreparativeForm.AsObject,
  }
}

export class ListProductPreparativeFormRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductPreparativeFormFilter | undefined;
  setFilter(value?: ProductPreparativeFormFilter): void;

  clearSortingsList(): void;
  getSortingsList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingsList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSortings(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductPreparativeFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductPreparativeFormRequest): ListProductPreparativeFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductPreparativeFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductPreparativeFormRequest;
  static deserializeBinaryFromReader(message: ListProductPreparativeFormRequest, reader: jspb.BinaryReader): ListProductPreparativeFormRequest;
}

export namespace ListProductPreparativeFormRequest {
  export type AsObject = {
    filter?: ProductPreparativeFormFilter.AsObject,
    sortingsList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductPreparativeFormResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductPreparativeForm>;
  setItemsList(value: Array<ProductPreparativeForm>): void;
  addItems(value?: ProductPreparativeForm, index?: number): ProductPreparativeForm;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductPreparativeFormResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductPreparativeFormResponse): ListProductPreparativeFormResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductPreparativeFormResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductPreparativeFormResponse;
  static deserializeBinaryFromReader(message: ListProductPreparativeFormResponse, reader: jspb.BinaryReader): ListProductPreparativeFormResponse;
}

export namespace ListProductPreparativeFormResponse {
  export type AsObject = {
    itemsList: Array<ProductPreparativeForm.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveProductPreparativeFormRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductPreparativeForm | undefined;
  setItem(value?: ProductPreparativeForm): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductPreparativeFormRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductPreparativeFormRequest): SaveProductPreparativeFormRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductPreparativeFormRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductPreparativeFormRequest;
  static deserializeBinaryFromReader(message: SaveProductPreparativeFormRequest, reader: jspb.BinaryReader): SaveProductPreparativeFormRequest;
}

export namespace SaveProductPreparativeFormRequest {
  export type AsObject = {
    item?: ProductPreparativeForm.AsObject,
  }
}

export class SaveProductPreparativeFormResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductPreparativeFormResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductPreparativeFormResponse): SaveProductPreparativeFormResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductPreparativeFormResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductPreparativeFormResponse;
  static deserializeBinaryFromReader(message: SaveProductPreparativeFormResponse, reader: jspb.BinaryReader): SaveProductPreparativeFormResponse;
}

export namespace SaveProductPreparativeFormResponse {
  export type AsObject = {
    id: number,
  }
}

