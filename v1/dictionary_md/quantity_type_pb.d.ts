// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/quantity_type.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class QuantityType extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getShortName(): string;
  setShortName(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): QuantityType.AsObject;
  static toObject(includeInstance: boolean, msg: QuantityType): QuantityType.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: QuantityType, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): QuantityType;
  static deserializeBinaryFromReader(message: QuantityType, reader: jspb.BinaryReader): QuantityType;
}

export namespace QuantityType {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    shortName: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class QuantityTypeFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasShortName(): boolean;
  clearShortName(): void;
  getShortName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setShortName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): QuantityTypeFilter.AsObject;
  static toObject(includeInstance: boolean, msg: QuantityTypeFilter): QuantityTypeFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: QuantityTypeFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): QuantityTypeFilter;
  static deserializeBinaryFromReader(message: QuantityTypeFilter, reader: jspb.BinaryReader): QuantityTypeFilter;
}

export namespace QuantityTypeFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    shortName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetQuantityTypeRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetQuantityTypeRequest): GetQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetQuantityTypeRequest;
  static deserializeBinaryFromReader(message: GetQuantityTypeRequest, reader: jspb.BinaryReader): GetQuantityTypeRequest;
}

export namespace GetQuantityTypeRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetQuantityTypeResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): QuantityType | undefined;
  setItem(value?: QuantityType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetQuantityTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetQuantityTypeResponse): GetQuantityTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetQuantityTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetQuantityTypeResponse;
  static deserializeBinaryFromReader(message: GetQuantityTypeResponse, reader: jspb.BinaryReader): GetQuantityTypeResponse;
}

export namespace GetQuantityTypeResponse {
  export type AsObject = {
    item?: QuantityType.AsObject,
  }
}

export class ListQuantityTypeRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): QuantityTypeFilter | undefined;
  setFilter(value?: QuantityTypeFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListQuantityTypeRequest): ListQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListQuantityTypeRequest;
  static deserializeBinaryFromReader(message: ListQuantityTypeRequest, reader: jspb.BinaryReader): ListQuantityTypeRequest;
}

export namespace ListQuantityTypeRequest {
  export type AsObject = {
    filter?: QuantityTypeFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListQuantityTypeResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<QuantityType>;
  setItemsList(value: Array<QuantityType>): void;
  addItems(value?: QuantityType, index?: number): QuantityType;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListQuantityTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListQuantityTypeResponse): ListQuantityTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListQuantityTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListQuantityTypeResponse;
  static deserializeBinaryFromReader(message: ListQuantityTypeResponse, reader: jspb.BinaryReader): ListQuantityTypeResponse;
}

export namespace ListQuantityTypeResponse {
  export type AsObject = {
    itemsList: Array<QuantityType.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveQuantityTypeRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): QuantityType | undefined;
  setItem(value?: QuantityType): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveQuantityTypeRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveQuantityTypeRequest): SaveQuantityTypeRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveQuantityTypeRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveQuantityTypeRequest;
  static deserializeBinaryFromReader(message: SaveQuantityTypeRequest, reader: jspb.BinaryReader): SaveQuantityTypeRequest;
}

export namespace SaveQuantityTypeRequest {
  export type AsObject = {
    item?: QuantityType.AsObject,
  }
}

export class SaveQuantityTypeResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveQuantityTypeResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveQuantityTypeResponse): SaveQuantityTypeResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveQuantityTypeResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveQuantityTypeResponse;
  static deserializeBinaryFromReader(message: SaveQuantityTypeResponse, reader: jspb.BinaryReader): SaveQuantityTypeResponse;
}

export namespace SaveQuantityTypeResponse {
  export type AsObject = {
    id: number,
  }
}

