// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/color.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class Color extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Color.AsObject;
  static toObject(includeInstance: boolean, msg: Color): Color.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Color, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Color;
  static deserializeBinaryFromReader(message: Color, reader: jspb.BinaryReader): Color;
}

export namespace Color {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ColorFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ColorFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ColorFilter): ColorFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ColorFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ColorFilter;
  static deserializeBinaryFromReader(message: ColorFilter, reader: jspb.BinaryReader): ColorFilter;
}

export namespace ColorFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetColorRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetColorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetColorRequest): GetColorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetColorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetColorRequest;
  static deserializeBinaryFromReader(message: GetColorRequest, reader: jspb.BinaryReader): GetColorRequest;
}

export namespace GetColorRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetColorResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Color | undefined;
  setItem(value?: Color): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetColorResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetColorResponse): GetColorResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetColorResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetColorResponse;
  static deserializeBinaryFromReader(message: GetColorResponse, reader: jspb.BinaryReader): GetColorResponse;
}

export namespace GetColorResponse {
  export type AsObject = {
    item?: Color.AsObject,
  }
}

export class ListColorRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ColorFilter | undefined;
  setFilter(value?: ColorFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListColorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListColorRequest): ListColorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListColorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListColorRequest;
  static deserializeBinaryFromReader(message: ListColorRequest, reader: jspb.BinaryReader): ListColorRequest;
}

export namespace ListColorRequest {
  export type AsObject = {
    filter?: ColorFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListColorResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Color>;
  setItemsList(value: Array<Color>): void;
  addItems(value?: Color, index?: number): Color;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListColorResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListColorResponse): ListColorResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListColorResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListColorResponse;
  static deserializeBinaryFromReader(message: ListColorResponse, reader: jspb.BinaryReader): ListColorResponse;
}

export namespace ListColorResponse {
  export type AsObject = {
    itemsList: Array<Color.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveColorRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Color | undefined;
  setItem(value?: Color): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveColorRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveColorRequest): SaveColorRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveColorRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveColorRequest;
  static deserializeBinaryFromReader(message: SaveColorRequest, reader: jspb.BinaryReader): SaveColorRequest;
}

export namespace SaveColorRequest {
  export type AsObject = {
    item?: Color.AsObject,
  }
}

export class SaveColorResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveColorResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveColorResponse): SaveColorResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveColorResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveColorResponse;
  static deserializeBinaryFromReader(message: SaveColorResponse, reader: jspb.BinaryReader): SaveColorResponse;
}

export namespace SaveColorResponse {
  export type AsObject = {
    id: number,
  }
}

