// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/product.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_md_application_method_pb from "../../v1/dictionary_md/application_method_pb";
import * as v1_dictionary_md_application_rate_unit_chemical_pb from "../../v1/dictionary_md/application_rate_unit_chemical_pb";
import * as v1_dictionary_md_category_pb from "../../v1/dictionary_md/category_pb";
import * as v1_dictionary_md_chemical_class_group_pb from "../../v1/dictionary_md/chemical_class_group_pb";
import * as v1_dictionary_md_color_pb from "../../v1/dictionary_md/color_pb";
import * as v1_dictionary_md_distribution_type_chemical_pb from "../../v1/dictionary_md/distribution_type_chemical_pb";
import * as v1_dictionary_md_file_pb from "../../v1/dictionary_md/file_pb";
import * as v1_dictionary_md_product_category_application_pb from "../../v1/dictionary_md/product_category_application_pb";
import * as v1_dictionary_md_product_package_pb from "../../v1/dictionary_md/product_package_pb";
import * as v1_dictionary_md_preparative_form_pb from "../../v1/dictionary_md/preparative_form_pb";
import * as v1_dictionary_md_product_active_substance_pb from "../../v1/dictionary_md/product_active_substance_pb";
import * as v1_dictionary_md_product_condition_pb from "../../v1/dictionary_md/product_condition_pb";
import * as v1_dictionary_md_spectrum_action_chemical_pb from "../../v1/dictionary_md/spectrum_action_chemical_pb";
import * as v1_dictionary_md_quantity_type_pb from "../../v1/dictionary_md/quantity_type_pb";
import * as v1_dictionary_md_seed_sort_type_pb from "../../v1/dictionary_md_seed/sort_type_pb";
import * as v1_dictionary_md_seed_adapt_type_pb from "../../v1/dictionary_md_seed/adapt_type_pb";
import * as v1_dictionary_md_seed_fruit_average_weight_pb from "../../v1/dictionary_md_seed/fruit_average_weight_pb";
import * as v1_dictionary_md_seed_fruit_form_pb from "../../v1/dictionary_md_seed/fruit_form_pb";
import * as v1_dictionary_md_seed_grow_type_pb from "../../v1/dictionary_md_seed/grow_type_pb";
import * as v1_dictionary_md_seed_grow_season_pb from "../../v1/dictionary_md_seed/grow_season_pb";
import * as v1_dictionary_md_seed_maturity_group_pb from "../../v1/dictionary_md_seed/maturity_group_pb";
import * as v1_dictionary_md_seed_origin_country_pb from "../../v1/dictionary_md_seed/origin_country_pb";
import * as v1_dictionary_md_seed_plant_type_pb from "../../v1/dictionary_md_seed/plant_type_pb";
import * as v1_dictionary_md_seed_pollination_type_pb from "../../v1/dictionary_md_seed/pollination_type_pb";
import * as v1_dictionary_md_seed_purpose_pb from "../../v1/dictionary_md_seed/purpose_pb";
import * as v1_dictionary_md_seed_seed_type_pb from "../../v1/dictionary_md_seed/seed_type_pb";
import * as v1_dictionary_md_seed_species_pb from "../../v1/dictionary_md_seed/species_pb";
import * as v1_dictionary_md_seed_technology_type_pb from "../../v1/dictionary_md_seed/technology_type_pb";
import * as v1_dictionary_md_seed_reproduction_pb from "../../v1/dictionary_md_seed/reproduction_pb";

export class Product extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getType(): v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap];
  setType(value: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]): void;

  getProcess(): v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap];
  setProcess(value: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap]): void;

  getBrandId(): number;
  setBrandId(value: number): void;

  getBrandName(): string;
  setBrandName(value: string): void;

  getProductGroupId(): number;
  setProductGroupId(value: number): void;

  getProductGroupName(): string;
  setProductGroupName(value: string): void;

  getProductSubGroupId(): number;
  setProductSubGroupId(value: number): void;

  getProductSubGroupName(): string;
  setProductSubGroupName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  getToxicityClassId(): number;
  setToxicityClassId(value: number): void;

  getToxicityClassName(): string;
  setToxicityClassName(value: string): void;

  hasProductCondition(): boolean;
  clearProductCondition(): void;
  getProductCondition(): v1_dictionary_md_product_condition_pb.ProductCondition | undefined;
  setProductCondition(value?: v1_dictionary_md_product_condition_pb.ProductCondition): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Product.AsObject;
  static toObject(includeInstance: boolean, msg: Product): Product.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Product, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Product;
  static deserializeBinaryFromReader(message: Product, reader: jspb.BinaryReader): Product;
}

export namespace Product {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    type: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap],
    process: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap],
    brandId: number,
    brandName: string,
    productGroupId: number,
    productGroupName: string,
    productSubGroupId: number,
    productSubGroupName: string,
    description: string,
    toxicityClassId: number,
    toxicityClassName: string,
    productCondition?: v1_dictionary_md_product_condition_pb.ProductCondition.AsObject,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ProductFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasProcess(): boolean;
  clearProcess(): void;
  getProcess(): v1_dictionary_common_enum_product_pb.ProductProcessValue | undefined;
  setProcess(value?: v1_dictionary_common_enum_product_pb.ProductProcessValue): void;

  hasType(): boolean;
  clearType(): void;
  getType(): v1_dictionary_common_enum_product_pb.ProductTypeValue | undefined;
  setType(value?: v1_dictionary_common_enum_product_pb.ProductTypeValue): void;

  hasBrandId(): boolean;
  clearBrandId(): void;
  getBrandId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setBrandId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasBrandName(): boolean;
  clearBrandName(): void;
  getBrandName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setBrandName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasProductGroupId(): boolean;
  clearProductGroupId(): void;
  getProductGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductGroupName(): boolean;
  clearProductGroupName(): void;
  getProductGroupName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setProductGroupName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasCategoryId(): boolean;
  clearCategoryId(): void;
  getCategoryId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCategoryId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasQuantityTypeId(): boolean;
  clearQuantityTypeId(): void;
  getQuantityTypeId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setQuantityTypeId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasQuantityTypeName(): boolean;
  clearQuantityTypeName(): void;
  getQuantityTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setQuantityTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasProductSubGroupId(): boolean;
  clearProductSubGroupId(): void;
  getProductSubGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductSubGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductSubGroupName(): boolean;
  clearProductSubGroupName(): void;
  getProductSubGroupName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setProductSubGroupName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasApplicationMethodId(): boolean;
  clearApplicationMethodId(): void;
  getApplicationMethodId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setApplicationMethodId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasToxicityClassId(): boolean;
  clearToxicityClassId(): void;
  getToxicityClassId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setToxicityClassId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasToxicityClassName(): boolean;
  clearToxicityClassName(): void;
  getToxicityClassName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setToxicityClassName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasChemicalClassGroupId(): boolean;
  clearChemicalClassGroupId(): void;
  getChemicalClassGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setChemicalClassGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasColorId(): boolean;
  clearColorId(): void;
  getColorId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setColorId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasColorName(): boolean;
  clearColorName(): void;
  getColorName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setColorName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductFilter): ProductFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductFilter;
  static deserializeBinaryFromReader(message: ProductFilter, reader: jspb.BinaryReader): ProductFilter;
}

export namespace ProductFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    process?: v1_dictionary_common_enum_product_pb.ProductProcessValue.AsObject,
    type?: v1_dictionary_common_enum_product_pb.ProductTypeValue.AsObject,
    brandId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    brandName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    productGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productGroupName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    categoryId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    quantityTypeId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    quantityTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    productSubGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productSubGroupName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    applicationMethodId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    toxicityClassId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    toxicityClassName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    chemicalClassGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    colorId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    colorName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class ProductFilterMd extends jspb.Message {
  clearIdList(): void;
  getIdList(): Array<number>;
  setIdList(value: Array<number>): void;
  addId(value: number, index?: number): number;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  clearProcessList(): void;
  getProcessList(): Array<v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap]>;
  setProcessList(value: Array<v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap]>): void;
  addProcess(value: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap], index?: number): v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap];

  clearTypeList(): void;
  getTypeList(): Array<v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]>;
  setTypeList(value: Array<v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]>): void;
  addType(value: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap], index?: number): v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap];

  clearBrandIdList(): void;
  getBrandIdList(): Array<number>;
  setBrandIdList(value: Array<number>): void;
  addBrandId(value: number, index?: number): number;

  clearProductGroupIdList(): void;
  getProductGroupIdList(): Array<number>;
  setProductGroupIdList(value: Array<number>): void;
  addProductGroupId(value: number, index?: number): number;

  clearProductSubGroupIdList(): void;
  getProductSubGroupIdList(): Array<number>;
  setProductSubGroupIdList(value: Array<number>): void;
  addProductSubGroupId(value: number, index?: number): number;

  clearCategoryGroupIdList(): void;
  getCategoryGroupIdList(): Array<number>;
  setCategoryGroupIdList(value: Array<number>): void;
  addCategoryGroupId(value: number, index?: number): number;

  clearCategoryIdList(): void;
  getCategoryIdList(): Array<number>;
  setCategoryIdList(value: Array<number>): void;
  addCategoryId(value: number, index?: number): number;

  clearQuantityTypeIdList(): void;
  getQuantityTypeIdList(): Array<number>;
  setQuantityTypeIdList(value: Array<number>): void;
  addQuantityTypeId(value: number, index?: number): number;

  clearProductPackageIdList(): void;
  getProductPackageIdList(): Array<number>;
  setProductPackageIdList(value: Array<number>): void;
  addProductPackageId(value: number, index?: number): number;

  clearColorIdList(): void;
  getColorIdList(): Array<number>;
  setColorIdList(value: Array<number>): void;
  addColorId(value: number, index?: number): number;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductFilterMd.AsObject;
  static toObject(includeInstance: boolean, msg: ProductFilterMd): ProductFilterMd.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductFilterMd, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductFilterMd;
  static deserializeBinaryFromReader(message: ProductFilterMd, reader: jspb.BinaryReader): ProductFilterMd;
}

export namespace ProductFilterMd {
  export type AsObject = {
    idList: Array<number>,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    processList: Array<v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap]>,
    typeList: Array<v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]>,
    brandIdList: Array<number>,
    productGroupIdList: Array<number>,
    productSubGroupIdList: Array<number>,
    categoryGroupIdList: Array<number>,
    categoryIdList: Array<number>,
    quantityTypeIdList: Array<number>,
    productPackageIdList: Array<number>,
    colorIdList: Array<number>,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
  }
}

export class ProductDetailed extends jspb.Message {
  hasProduct(): boolean;
  clearProduct(): void;
  getProduct(): Product | undefined;
  setProduct(value?: Product): void;

  hasProductConditionSeeds(): boolean;
  clearProductConditionSeeds(): void;
  getProductConditionSeeds(): ProductConditionSeeds | undefined;
  setProductConditionSeeds(value?: ProductConditionSeeds): void;

  hasProductConditionChemicals(): boolean;
  clearProductConditionChemicals(): void;
  getProductConditionChemicals(): v1_dictionary_md_product_condition_pb.ProductConditionChemicals | undefined;
  setProductConditionChemicals(value?: v1_dictionary_md_product_condition_pb.ProductConditionChemicals): void;

  clearCategoriesList(): void;
  getCategoriesList(): Array<v1_dictionary_md_category_pb.Category>;
  setCategoriesList(value: Array<v1_dictionary_md_category_pb.Category>): void;
  addCategories(value?: v1_dictionary_md_category_pb.Category, index?: number): v1_dictionary_md_category_pb.Category;

  clearQuantityTypesList(): void;
  getQuantityTypesList(): Array<v1_dictionary_md_quantity_type_pb.QuantityType>;
  setQuantityTypesList(value: Array<v1_dictionary_md_quantity_type_pb.QuantityType>): void;
  addQuantityTypes(value?: v1_dictionary_md_quantity_type_pb.QuantityType, index?: number): v1_dictionary_md_quantity_type_pb.QuantityType;

  clearPackagesList(): void;
  getPackagesList(): Array<v1_dictionary_md_product_package_pb.ProductPackage>;
  setPackagesList(value: Array<v1_dictionary_md_product_package_pb.ProductPackage>): void;
  addPackages(value?: v1_dictionary_md_product_package_pb.ProductPackage, index?: number): v1_dictionary_md_product_package_pb.ProductPackage;

  clearProductActiveSubstancesList(): void;
  getProductActiveSubstancesList(): Array<v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance>;
  setProductActiveSubstancesList(value: Array<v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance>): void;
  addProductActiveSubstances(value?: v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance, index?: number): v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance;

  clearPreparativeFormsList(): void;
  getPreparativeFormsList(): Array<v1_dictionary_md_preparative_form_pb.PreparativeForm>;
  setPreparativeFormsList(value: Array<v1_dictionary_md_preparative_form_pb.PreparativeForm>): void;
  addPreparativeForms(value?: v1_dictionary_md_preparative_form_pb.PreparativeForm, index?: number): v1_dictionary_md_preparative_form_pb.PreparativeForm;

  clearApplicationMethodsList(): void;
  getApplicationMethodsList(): Array<v1_dictionary_md_application_method_pb.ApplicationMethod>;
  setApplicationMethodsList(value: Array<v1_dictionary_md_application_method_pb.ApplicationMethod>): void;
  addApplicationMethods(value?: v1_dictionary_md_application_method_pb.ApplicationMethod, index?: number): v1_dictionary_md_application_method_pb.ApplicationMethod;

  clearChemicalClassGroupsList(): void;
  getChemicalClassGroupsList(): Array<v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup>;
  setChemicalClassGroupsList(value: Array<v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup>): void;
  addChemicalClassGroups(value?: v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup, index?: number): v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup;

  clearDistributionTypeChemicalsList(): void;
  getDistributionTypeChemicalsList(): Array<v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical>;
  setDistributionTypeChemicalsList(value: Array<v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical>): void;
  addDistributionTypeChemicals(value?: v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical, index?: number): v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductDetailed.AsObject;
  static toObject(includeInstance: boolean, msg: ProductDetailed): ProductDetailed.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductDetailed, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductDetailed;
  static deserializeBinaryFromReader(message: ProductDetailed, reader: jspb.BinaryReader): ProductDetailed;
}

export namespace ProductDetailed {
  export type AsObject = {
    product?: Product.AsObject,
    productConditionSeeds?: ProductConditionSeeds.AsObject,
    productConditionChemicals?: v1_dictionary_md_product_condition_pb.ProductConditionChemicals.AsObject,
    categoriesList: Array<v1_dictionary_md_category_pb.Category.AsObject>,
    quantityTypesList: Array<v1_dictionary_md_quantity_type_pb.QuantityType.AsObject>,
    packagesList: Array<v1_dictionary_md_product_package_pb.ProductPackage.AsObject>,
    productActiveSubstancesList: Array<v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance.AsObject>,
    preparativeFormsList: Array<v1_dictionary_md_preparative_form_pb.PreparativeForm.AsObject>,
    applicationMethodsList: Array<v1_dictionary_md_application_method_pb.ApplicationMethod.AsObject>,
    chemicalClassGroupsList: Array<v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup.AsObject>,
    distributionTypeChemicalsList: Array<v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical.AsObject>,
  }
}

export class GetProductRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductRequest): GetProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductRequest;
  static deserializeBinaryFromReader(message: GetProductRequest, reader: jspb.BinaryReader): GetProductRequest;
}

export namespace GetProductRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductDetailed | undefined;
  setItem(value?: ProductDetailed): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductResponse): GetProductResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductResponse;
  static deserializeBinaryFromReader(message: GetProductResponse, reader: jspb.BinaryReader): GetProductResponse;
}

export namespace GetProductResponse {
  export type AsObject = {
    item?: ProductDetailed.AsObject,
  }
}

export class ListProductIdsRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductFilterMd | undefined;
  setFilter(value?: ProductFilterMd): void;

  hasSeedsConditionFilter(): boolean;
  clearSeedsConditionFilter(): void;
  getSeedsConditionFilter(): ProductConditionSeedsFilterMd | undefined;
  setSeedsConditionFilter(value?: ProductConditionSeedsFilterMd): void;

  hasChemicalsConditionFilter(): boolean;
  clearChemicalsConditionFilter(): void;
  getChemicalsConditionFilter(): ProductConditionChemicalsFilterMd | undefined;
  setChemicalsConditionFilter(value?: ProductConditionChemicalsFilterMd): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductIdsRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductIdsRequest): ListProductIdsRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductIdsRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductIdsRequest;
  static deserializeBinaryFromReader(message: ListProductIdsRequest, reader: jspb.BinaryReader): ListProductIdsRequest;
}

export namespace ListProductIdsRequest {
  export type AsObject = {
    filter?: ProductFilterMd.AsObject,
    seedsConditionFilter?: ProductConditionSeedsFilterMd.AsObject,
    chemicalsConditionFilter?: ProductConditionChemicalsFilterMd.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductIdsResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<number>;
  setItemsList(value: Array<number>): void;
  addItems(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductIdsResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductIdsResponse): ListProductIdsResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductIdsResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductIdsResponse;
  static deserializeBinaryFromReader(message: ListProductIdsResponse, reader: jspb.BinaryReader): ListProductIdsResponse;
}

export namespace ListProductIdsResponse {
  export type AsObject = {
    itemsList: Array<number>,
  }
}

export class ListProductRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductFilter | undefined;
  setFilter(value?: ProductFilter): void;

  hasConditionSeedsFilter(): boolean;
  clearConditionSeedsFilter(): void;
  getConditionSeedsFilter(): ProductConditionSeedsFilter | undefined;
  setConditionSeedsFilter(value?: ProductConditionSeedsFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductRequest): ListProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductRequest;
  static deserializeBinaryFromReader(message: ListProductRequest, reader: jspb.BinaryReader): ListProductRequest;
}

export namespace ListProductRequest {
  export type AsObject = {
    filter?: ProductFilter.AsObject,
    conditionSeedsFilter?: ProductConditionSeedsFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductDetailed>;
  setItemsList(value: Array<ProductDetailed>): void;
  addItems(value?: ProductDetailed, index?: number): ProductDetailed;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductResponse): ListProductResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductResponse;
  static deserializeBinaryFromReader(message: ListProductResponse, reader: jspb.BinaryReader): ListProductResponse;
}

export namespace ListProductResponse {
  export type AsObject = {
    itemsList: Array<ProductDetailed.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveProductRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductDetailed | undefined;
  setItem(value?: ProductDetailed): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductRequest): SaveProductRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductRequest;
  static deserializeBinaryFromReader(message: SaveProductRequest, reader: jspb.BinaryReader): SaveProductRequest;
}

export namespace SaveProductRequest {
  export type AsObject = {
    item?: ProductDetailed.AsObject,
  }
}

export class SaveProductResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductResponse): SaveProductResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductResponse;
  static deserializeBinaryFromReader(message: SaveProductResponse, reader: jspb.BinaryReader): SaveProductResponse;
}

export namespace SaveProductResponse {
  export type AsObject = {
    id: number,
  }
}

export class ProductConditionChemicalsFilterMd extends jspb.Message {
  clearToxicityClassIdList(): void;
  getToxicityClassIdList(): Array<number>;
  setToxicityClassIdList(value: Array<number>): void;
  addToxicityClassId(value: number, index?: number): number;

  clearPreparativeFormIdList(): void;
  getPreparativeFormIdList(): Array<number>;
  setPreparativeFormIdList(value: Array<number>): void;
  addPreparativeFormId(value: number, index?: number): number;

  clearProductActiveSubstanceIdList(): void;
  getProductActiveSubstanceIdList(): Array<number>;
  setProductActiveSubstanceIdList(value: Array<number>): void;
  addProductActiveSubstanceId(value: number, index?: number): number;

  clearChemicalClassGroupIdList(): void;
  getChemicalClassGroupIdList(): Array<number>;
  setChemicalClassGroupIdList(value: Array<number>): void;
  addChemicalClassGroupId(value: number, index?: number): number;

  clearDistributionTypeChemicalIdList(): void;
  getDistributionTypeChemicalIdList(): Array<number>;
  setDistributionTypeChemicalIdList(value: Array<number>): void;
  addDistributionTypeChemicalId(value: number, index?: number): number;

  clearApplicationMethodIdList(): void;
  getApplicationMethodIdList(): Array<number>;
  setApplicationMethodIdList(value: Array<number>): void;
  addApplicationMethodId(value: number, index?: number): number;

  clearSpectrumActionChemicalIdList(): void;
  getSpectrumActionChemicalIdList(): Array<number>;
  setSpectrumActionChemicalIdList(value: Array<number>): void;
  addSpectrumActionChemicalId(value: number, index?: number): number;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductConditionChemicalsFilterMd.AsObject;
  static toObject(includeInstance: boolean, msg: ProductConditionChemicalsFilterMd): ProductConditionChemicalsFilterMd.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductConditionChemicalsFilterMd, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductConditionChemicalsFilterMd;
  static deserializeBinaryFromReader(message: ProductConditionChemicalsFilterMd, reader: jspb.BinaryReader): ProductConditionChemicalsFilterMd;
}

export namespace ProductConditionChemicalsFilterMd {
  export type AsObject = {
    toxicityClassIdList: Array<number>,
    preparativeFormIdList: Array<number>,
    productActiveSubstanceIdList: Array<number>,
    chemicalClassGroupIdList: Array<number>,
    distributionTypeChemicalIdList: Array<number>,
    applicationMethodIdList: Array<number>,
    spectrumActionChemicalIdList: Array<number>,
  }
}

export class ProductConditionSeeds extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getOrganic(): boolean;
  setOrganic(value: boolean): void;

  getSowingRate(): string;
  setSowingRate(value: string): void;

  getSowingDensity(): string;
  setSowingDensity(value: string): void;

  getSowingDistance(): string;
  setSowingDistance(value: string): void;

  getSowingTerm(): string;
  setSowingTerm(value: string): void;

  getHarvestTerm(): string;
  setHarvestTerm(value: string): void;

  getOneThousandSeedsWeight(): string;
  setOneThousandSeedsWeight(value: string): void;

  getGermination(): string;
  setGermination(value: string): void;

  getGerminationEnergy(): string;
  setGerminationEnergy(value: string): void;

  getMaturityPeriod(): string;
  setMaturityPeriod(value: string): void;

  getPlantHeight(): string;
  setPlantHeight(value: string): void;

  getFruitWeight(): string;
  setFruitWeight(value: string): void;

  getWallThickness(): string;
  setWallThickness(value: string): void;

  getFruitTaste(): string;
  setFruitTaste(value: string): void;

  getSugarConcentration(): string;
  setSugarConcentration(value: string): void;

  getDryMaterialConcentration(): string;
  setDryMaterialConcentration(value: string): void;

  getFrostresist(): string;
  setFrostresist(value: string): void;

  getPollinator(): boolean;
  setPollinator(value: boolean): void;

  getHarvestYear(): number;
  setHarvestYear(value: number): void;

  getApplicationRecommendations(): string;
  setApplicationRecommendations(value: string): void;

  getFlowerType(): string;
  setFlowerType(value: string): void;

  getFlowerDiameter(): string;
  setFlowerDiameter(value: string): void;

  getSeedForm(): string;
  setSeedForm(value: string): void;

  hasFruitForm(): boolean;
  clearFruitForm(): void;
  getFruitForm(): v1_dictionary_md_seed_fruit_form_pb.FruitForm | undefined;
  setFruitForm(value?: v1_dictionary_md_seed_fruit_form_pb.FruitForm): void;

  hasReproduction(): boolean;
  clearReproduction(): void;
  getReproduction(): v1_dictionary_md_seed_reproduction_pb.Reproduction | undefined;
  setReproduction(value?: v1_dictionary_md_seed_reproduction_pb.Reproduction): void;

  hasSpecies(): boolean;
  clearSpecies(): void;
  getSpecies(): v1_dictionary_md_seed_species_pb.Species | undefined;
  setSpecies(value?: v1_dictionary_md_seed_species_pb.Species): void;

  hasTechnologyType(): boolean;
  clearTechnologyType(): void;
  getTechnologyType(): v1_dictionary_md_seed_technology_type_pb.TechnologyType | undefined;
  setTechnologyType(value?: v1_dictionary_md_seed_technology_type_pb.TechnologyType): void;

  hasMaturityGroup(): boolean;
  clearMaturityGroup(): void;
  getMaturityGroup(): v1_dictionary_md_seed_maturity_group_pb.MaturityGroup | undefined;
  setMaturityGroup(value?: v1_dictionary_md_seed_maturity_group_pb.MaturityGroup): void;

  hasFruitAverageWeight(): boolean;
  clearFruitAverageWeight(): void;
  getFruitAverageWeight(): v1_dictionary_md_seed_fruit_average_weight_pb.FruitAverageWeight | undefined;
  setFruitAverageWeight(value?: v1_dictionary_md_seed_fruit_average_weight_pb.FruitAverageWeight): void;

  hasSeedType(): boolean;
  clearSeedType(): void;
  getSeedType(): v1_dictionary_md_seed_seed_type_pb.SeedType | undefined;
  setSeedType(value?: v1_dictionary_md_seed_seed_type_pb.SeedType): void;

  hasPollinationType(): boolean;
  clearPollinationType(): void;
  getPollinationType(): v1_dictionary_md_seed_pollination_type_pb.PollinationType | undefined;
  setPollinationType(value?: v1_dictionary_md_seed_pollination_type_pb.PollinationType): void;

  hasAdaptType(): boolean;
  clearAdaptType(): void;
  getAdaptType(): v1_dictionary_md_seed_adapt_type_pb.AdaptType | undefined;
  setAdaptType(value?: v1_dictionary_md_seed_adapt_type_pb.AdaptType): void;

  hasOriginCountry(): boolean;
  clearOriginCountry(): void;
  getOriginCountry(): v1_dictionary_md_seed_origin_country_pb.OriginCountry | undefined;
  setOriginCountry(value?: v1_dictionary_md_seed_origin_country_pb.OriginCountry): void;

  hasPlantType(): boolean;
  clearPlantType(): void;
  getPlantType(): v1_dictionary_md_seed_plant_type_pb.PlantType | undefined;
  setPlantType(value?: v1_dictionary_md_seed_plant_type_pb.PlantType): void;

  hasSortType(): boolean;
  clearSortType(): void;
  getSortType(): v1_dictionary_md_seed_sort_type_pb.SortType | undefined;
  setSortType(value?: v1_dictionary_md_seed_sort_type_pb.SortType): void;

  hasFruitTechColor(): boolean;
  clearFruitTechColor(): void;
  getFruitTechColor(): v1_dictionary_md_color_pb.Color | undefined;
  setFruitTechColor(value?: v1_dictionary_md_color_pb.Color): void;

  hasFruitBioColor(): boolean;
  clearFruitBioColor(): void;
  getFruitBioColor(): v1_dictionary_md_color_pb.Color | undefined;
  setFruitBioColor(value?: v1_dictionary_md_color_pb.Color): void;

  hasPulpColor(): boolean;
  clearPulpColor(): void;
  getPulpColor(): v1_dictionary_md_color_pb.Color | undefined;
  setPulpColor(value?: v1_dictionary_md_color_pb.Color): void;

  clearPurposesList(): void;
  getPurposesList(): Array<v1_dictionary_md_seed_purpose_pb.Purpose>;
  setPurposesList(value: Array<v1_dictionary_md_seed_purpose_pb.Purpose>): void;
  addPurposes(value?: v1_dictionary_md_seed_purpose_pb.Purpose, index?: number): v1_dictionary_md_seed_purpose_pb.Purpose;

  clearGrowTypesList(): void;
  getGrowTypesList(): Array<v1_dictionary_md_seed_grow_type_pb.GrowType>;
  setGrowTypesList(value: Array<v1_dictionary_md_seed_grow_type_pb.GrowType>): void;
  addGrowTypes(value?: v1_dictionary_md_seed_grow_type_pb.GrowType, index?: number): v1_dictionary_md_seed_grow_type_pb.GrowType;

  clearGrowSeasonsList(): void;
  getGrowSeasonsList(): Array<v1_dictionary_md_seed_grow_season_pb.GrowSeason>;
  setGrowSeasonsList(value: Array<v1_dictionary_md_seed_grow_season_pb.GrowSeason>): void;
  addGrowSeasons(value?: v1_dictionary_md_seed_grow_season_pb.GrowSeason, index?: number): v1_dictionary_md_seed_grow_season_pb.GrowSeason;

  clearTreatmentChemicalsList(): void;
  getTreatmentChemicalsList(): Array<Product>;
  setTreatmentChemicalsList(value: Array<Product>): void;
  addTreatmentChemicals(value?: Product, index?: number): Product;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductConditionSeeds.AsObject;
  static toObject(includeInstance: boolean, msg: ProductConditionSeeds): ProductConditionSeeds.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductConditionSeeds, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductConditionSeeds;
  static deserializeBinaryFromReader(message: ProductConditionSeeds, reader: jspb.BinaryReader): ProductConditionSeeds;
}

export namespace ProductConditionSeeds {
  export type AsObject = {
    id: number,
    organic: boolean,
    sowingRate: string,
    sowingDensity: string,
    sowingDistance: string,
    sowingTerm: string,
    harvestTerm: string,
    oneThousandSeedsWeight: string,
    germination: string,
    germinationEnergy: string,
    maturityPeriod: string,
    plantHeight: string,
    fruitWeight: string,
    wallThickness: string,
    fruitTaste: string,
    sugarConcentration: string,
    dryMaterialConcentration: string,
    frostresist: string,
    pollinator: boolean,
    harvestYear: number,
    applicationRecommendations: string,
    flowerType: string,
    flowerDiameter: string,
    seedForm: string,
    fruitForm?: v1_dictionary_md_seed_fruit_form_pb.FruitForm.AsObject,
    reproduction?: v1_dictionary_md_seed_reproduction_pb.Reproduction.AsObject,
    species?: v1_dictionary_md_seed_species_pb.Species.AsObject,
    technologyType?: v1_dictionary_md_seed_technology_type_pb.TechnologyType.AsObject,
    maturityGroup?: v1_dictionary_md_seed_maturity_group_pb.MaturityGroup.AsObject,
    fruitAverageWeight?: v1_dictionary_md_seed_fruit_average_weight_pb.FruitAverageWeight.AsObject,
    seedType?: v1_dictionary_md_seed_seed_type_pb.SeedType.AsObject,
    pollinationType?: v1_dictionary_md_seed_pollination_type_pb.PollinationType.AsObject,
    adaptType?: v1_dictionary_md_seed_adapt_type_pb.AdaptType.AsObject,
    originCountry?: v1_dictionary_md_seed_origin_country_pb.OriginCountry.AsObject,
    plantType?: v1_dictionary_md_seed_plant_type_pb.PlantType.AsObject,
    sortType?: v1_dictionary_md_seed_sort_type_pb.SortType.AsObject,
    fruitTechColor?: v1_dictionary_md_color_pb.Color.AsObject,
    fruitBioColor?: v1_dictionary_md_color_pb.Color.AsObject,
    pulpColor?: v1_dictionary_md_color_pb.Color.AsObject,
    purposesList: Array<v1_dictionary_md_seed_purpose_pb.Purpose.AsObject>,
    growTypesList: Array<v1_dictionary_md_seed_grow_type_pb.GrowType.AsObject>,
    growSeasonsList: Array<v1_dictionary_md_seed_grow_season_pb.GrowSeason.AsObject>,
    treatmentChemicalsList: Array<Product.AsObject>,
  }
}

export class ProductConditionSeedsFilterMd extends jspb.Message {
  hasFlowerType(): boolean;
  clearFlowerType(): void;
  getFlowerType(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFlowerType(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasIsOrganic(): boolean;
  clearIsOrganic(): void;
  getIsOrganic(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setIsOrganic(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasIsPollinator(): boolean;
  clearIsPollinator(): void;
  getIsPollinator(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setIsPollinator(value?: google_protobuf_wrappers_pb.BoolValue): void;

  clearHarvestYearList(): void;
  getHarvestYearList(): Array<number>;
  setHarvestYearList(value: Array<number>): void;
  addHarvestYear(value: number, index?: number): number;

  clearSpeciesIdList(): void;
  getSpeciesIdList(): Array<number>;
  setSpeciesIdList(value: Array<number>): void;
  addSpeciesId(value: number, index?: number): number;

  clearMaturityGroupIdList(): void;
  getMaturityGroupIdList(): Array<number>;
  setMaturityGroupIdList(value: Array<number>): void;
  addMaturityGroupId(value: number, index?: number): number;

  clearPollinationTypeIdList(): void;
  getPollinationTypeIdList(): Array<number>;
  setPollinationTypeIdList(value: Array<number>): void;
  addPollinationTypeId(value: number, index?: number): number;

  clearPlantTypeIdList(): void;
  getPlantTypeIdList(): Array<number>;
  setPlantTypeIdList(value: Array<number>): void;
  addPlantTypeId(value: number, index?: number): number;

  clearFruitFormIdList(): void;
  getFruitFormIdList(): Array<number>;
  setFruitFormIdList(value: Array<number>): void;
  addFruitFormId(value: number, index?: number): number;

  clearSortTypeIdList(): void;
  getSortTypeIdList(): Array<number>;
  setSortTypeIdList(value: Array<number>): void;
  addSortTypeId(value: number, index?: number): number;

  clearFruitAverageWeightIdList(): void;
  getFruitAverageWeightIdList(): Array<number>;
  setFruitAverageWeightIdList(value: Array<number>): void;
  addFruitAverageWeightId(value: number, index?: number): number;

  clearOriginCountryIdList(): void;
  getOriginCountryIdList(): Array<number>;
  setOriginCountryIdList(value: Array<number>): void;
  addOriginCountryId(value: number, index?: number): number;

  clearTreatmentChemicalsIdList(): void;
  getTreatmentChemicalsIdList(): Array<number>;
  setTreatmentChemicalsIdList(value: Array<number>): void;
  addTreatmentChemicalsId(value: number, index?: number): number;

  clearTechnologyTypeList(): void;
  getTechnologyTypeList(): Array<number>;
  setTechnologyTypeList(value: Array<number>): void;
  addTechnologyType(value: number, index?: number): number;

  clearGrowTypesIdList(): void;
  getGrowTypesIdList(): Array<number>;
  setGrowTypesIdList(value: Array<number>): void;
  addGrowTypesId(value: number, index?: number): number;

  clearGrowSeasonsIdList(): void;
  getGrowSeasonsIdList(): Array<number>;
  setGrowSeasonsIdList(value: Array<number>): void;
  addGrowSeasonsId(value: number, index?: number): number;

  clearPurposeIdList(): void;
  getPurposeIdList(): Array<number>;
  setPurposeIdList(value: Array<number>): void;
  addPurposeId(value: number, index?: number): number;

  clearReproductionIdList(): void;
  getReproductionIdList(): Array<number>;
  setReproductionIdList(value: Array<number>): void;
  addReproductionId(value: number, index?: number): number;

  clearFruitTechColorList(): void;
  getFruitTechColorList(): Array<string>;
  setFruitTechColorList(value: Array<string>): void;
  addFruitTechColor(value: string, index?: number): string;

  clearFruitBioColorList(): void;
  getFruitBioColorList(): Array<string>;
  setFruitBioColorList(value: Array<string>): void;
  addFruitBioColor(value: string, index?: number): string;

  clearPulpColorList(): void;
  getPulpColorList(): Array<string>;
  setPulpColorList(value: Array<string>): void;
  addPulpColor(value: string, index?: number): string;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductConditionSeedsFilterMd.AsObject;
  static toObject(includeInstance: boolean, msg: ProductConditionSeedsFilterMd): ProductConditionSeedsFilterMd.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductConditionSeedsFilterMd, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductConditionSeedsFilterMd;
  static deserializeBinaryFromReader(message: ProductConditionSeedsFilterMd, reader: jspb.BinaryReader): ProductConditionSeedsFilterMd;
}

export namespace ProductConditionSeedsFilterMd {
  export type AsObject = {
    flowerType?: google_protobuf_wrappers_pb.StringValue.AsObject,
    isOrganic?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    isPollinator?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    harvestYearList: Array<number>,
    speciesIdList: Array<number>,
    maturityGroupIdList: Array<number>,
    pollinationTypeIdList: Array<number>,
    plantTypeIdList: Array<number>,
    fruitFormIdList: Array<number>,
    sortTypeIdList: Array<number>,
    fruitAverageWeightIdList: Array<number>,
    originCountryIdList: Array<number>,
    treatmentChemicalsIdList: Array<number>,
    technologyTypeList: Array<number>,
    growTypesIdList: Array<number>,
    growSeasonsIdList: Array<number>,
    purposeIdList: Array<number>,
    reproductionIdList: Array<number>,
    fruitTechColorList: Array<string>,
    fruitBioColorList: Array<string>,
    pulpColorList: Array<string>,
  }
}

export class ProductConditionSeedsFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasFlowerType(): boolean;
  clearFlowerType(): void;
  getFlowerType(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFlowerType(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasIsOrganic(): boolean;
  clearIsOrganic(): void;
  getIsOrganic(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setIsOrganic(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasIsPollinator(): boolean;
  clearIsPollinator(): void;
  getIsPollinator(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setIsPollinator(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasHarvestYear(): boolean;
  clearHarvestYear(): void;
  getHarvestYear(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setHarvestYear(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasFruitFormId(): boolean;
  clearFruitFormId(): void;
  getFruitFormId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFruitFormId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasFruitFormName(): boolean;
  clearFruitFormName(): void;
  getFruitFormName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFruitFormName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasReproductionId(): boolean;
  clearReproductionId(): void;
  getReproductionId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setReproductionId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasReproductionName(): boolean;
  clearReproductionName(): void;
  getReproductionName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setReproductionName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasSpeciesId(): boolean;
  clearSpeciesId(): void;
  getSpeciesId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setSpeciesId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasSpeciesName(): boolean;
  clearSpeciesName(): void;
  getSpeciesName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSpeciesName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasTechnologyTypeId(): boolean;
  clearTechnologyTypeId(): void;
  getTechnologyTypeId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setTechnologyTypeId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasTechnologyTypeName(): boolean;
  clearTechnologyTypeName(): void;
  getTechnologyTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setTechnologyTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasMaturityGroupId(): boolean;
  clearMaturityGroupId(): void;
  getMaturityGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setMaturityGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasMaturityGroupName(): boolean;
  clearMaturityGroupName(): void;
  getMaturityGroupName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setMaturityGroupName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasFruitAverageWeightId(): boolean;
  clearFruitAverageWeightId(): void;
  getFruitAverageWeightId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFruitAverageWeightId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasFruitAverageWeightName(): boolean;
  clearFruitAverageWeightName(): void;
  getFruitAverageWeightName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFruitAverageWeightName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasSeedTypeId(): boolean;
  clearSeedTypeId(): void;
  getSeedTypeId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setSeedTypeId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasSeedTypeName(): boolean;
  clearSeedTypeName(): void;
  getSeedTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSeedTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasPollinationTypeId(): boolean;
  clearPollinationTypeId(): void;
  getPollinationTypeId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setPollinationTypeId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasPollinationTypeName(): boolean;
  clearPollinationTypeName(): void;
  getPollinationTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setPollinationTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasAdaptTypeId(): boolean;
  clearAdaptTypeId(): void;
  getAdaptTypeId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setAdaptTypeId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasAdaptTypeName(): boolean;
  clearAdaptTypeName(): void;
  getAdaptTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setAdaptTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasOriginCountryId(): boolean;
  clearOriginCountryId(): void;
  getOriginCountryId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setOriginCountryId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasOriginCountryName(): boolean;
  clearOriginCountryName(): void;
  getOriginCountryName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setOriginCountryName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasPlantTypeId(): boolean;
  clearPlantTypeId(): void;
  getPlantTypeId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setPlantTypeId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasPlantTypeName(): boolean;
  clearPlantTypeName(): void;
  getPlantTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setPlantTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasFruitTechColorId(): boolean;
  clearFruitTechColorId(): void;
  getFruitTechColorId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFruitTechColorId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasFruitTechColorName(): boolean;
  clearFruitTechColorName(): void;
  getFruitTechColorName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFruitTechColorName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasFruitBioColorId(): boolean;
  clearFruitBioColorId(): void;
  getFruitBioColorId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFruitBioColorId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasFruitBioColorName(): boolean;
  clearFruitBioColorName(): void;
  getFruitBioColorName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFruitBioColorName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasPulpColorId(): boolean;
  clearPulpColorId(): void;
  getPulpColorId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setPulpColorId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasPulpColorName(): boolean;
  clearPulpColorName(): void;
  getPulpColorName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setPulpColorName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasPurposesId(): boolean;
  clearPurposesId(): void;
  getPurposesId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setPurposesId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasPurposesName(): boolean;
  clearPurposesName(): void;
  getPurposesName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setPurposesName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasGrowTypesId(): boolean;
  clearGrowTypesId(): void;
  getGrowTypesId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setGrowTypesId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasGrowTypesName(): boolean;
  clearGrowTypesName(): void;
  getGrowTypesName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setGrowTypesName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasGrowSeasonsId(): boolean;
  clearGrowSeasonsId(): void;
  getGrowSeasonsId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setGrowSeasonsId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasGrowSeasonsName(): boolean;
  clearGrowSeasonsName(): void;
  getGrowSeasonsName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setGrowSeasonsName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasTreatmentChemicalsId(): boolean;
  clearTreatmentChemicalsId(): void;
  getTreatmentChemicalsId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setTreatmentChemicalsId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasTreatmentChemicalsName(): boolean;
  clearTreatmentChemicalsName(): void;
  getTreatmentChemicalsName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setTreatmentChemicalsName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasSortTypeId(): boolean;
  clearSortTypeId(): void;
  getSortTypeId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setSortTypeId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasSortTypeName(): boolean;
  clearSortTypeName(): void;
  getSortTypeName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setSortTypeName(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductConditionSeedsFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductConditionSeedsFilter): ProductConditionSeedsFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductConditionSeedsFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductConditionSeedsFilter;
  static deserializeBinaryFromReader(message: ProductConditionSeedsFilter, reader: jspb.BinaryReader): ProductConditionSeedsFilter;
}

export namespace ProductConditionSeedsFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    flowerType?: google_protobuf_wrappers_pb.StringValue.AsObject,
    isOrganic?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    isPollinator?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    harvestYear?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    fruitFormId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    fruitFormName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    reproductionId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    reproductionName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    speciesId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    speciesName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    technologyTypeId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    technologyTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    maturityGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    maturityGroupName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    fruitAverageWeightId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    fruitAverageWeightName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    seedTypeId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    seedTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    pollinationTypeId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    pollinationTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    adaptTypeId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    adaptTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    originCountryId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    originCountryName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    plantTypeId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    plantTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    fruitTechColorId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    fruitTechColorName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    fruitBioColorId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    fruitBioColorName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    pulpColorId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    pulpColorName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    purposesId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    purposesName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    growTypesId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    growTypesName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    growSeasonsId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    growSeasonsName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    treatmentChemicalsId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    treatmentChemicalsName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    sortTypeId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    sortTypeName?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

