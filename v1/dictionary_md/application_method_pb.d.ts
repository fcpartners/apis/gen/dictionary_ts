// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/application_method.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ApplicationMethod extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ApplicationMethod.AsObject;
  static toObject(includeInstance: boolean, msg: ApplicationMethod): ApplicationMethod.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ApplicationMethod, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ApplicationMethod;
  static deserializeBinaryFromReader(message: ApplicationMethod, reader: jspb.BinaryReader): ApplicationMethod;
}

export namespace ApplicationMethod {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ApplicationMethodFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ApplicationMethodFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ApplicationMethodFilter): ApplicationMethodFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ApplicationMethodFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ApplicationMethodFilter;
  static deserializeBinaryFromReader(message: ApplicationMethodFilter, reader: jspb.BinaryReader): ApplicationMethodFilter;
}

export namespace ApplicationMethodFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetApplicationMethodRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetApplicationMethodRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetApplicationMethodRequest): GetApplicationMethodRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetApplicationMethodRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetApplicationMethodRequest;
  static deserializeBinaryFromReader(message: GetApplicationMethodRequest, reader: jspb.BinaryReader): GetApplicationMethodRequest;
}

export namespace GetApplicationMethodRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetApplicationMethodResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ApplicationMethod | undefined;
  setItem(value?: ApplicationMethod): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetApplicationMethodResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetApplicationMethodResponse): GetApplicationMethodResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetApplicationMethodResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetApplicationMethodResponse;
  static deserializeBinaryFromReader(message: GetApplicationMethodResponse, reader: jspb.BinaryReader): GetApplicationMethodResponse;
}

export namespace GetApplicationMethodResponse {
  export type AsObject = {
    item?: ApplicationMethod.AsObject,
  }
}

export class ListApplicationMethodRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ApplicationMethodFilter | undefined;
  setFilter(value?: ApplicationMethodFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListApplicationMethodRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListApplicationMethodRequest): ListApplicationMethodRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListApplicationMethodRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListApplicationMethodRequest;
  static deserializeBinaryFromReader(message: ListApplicationMethodRequest, reader: jspb.BinaryReader): ListApplicationMethodRequest;
}

export namespace ListApplicationMethodRequest {
  export type AsObject = {
    filter?: ApplicationMethodFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListApplicationMethodResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ApplicationMethod>;
  setItemsList(value: Array<ApplicationMethod>): void;
  addItems(value?: ApplicationMethod, index?: number): ApplicationMethod;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListApplicationMethodResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListApplicationMethodResponse): ListApplicationMethodResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListApplicationMethodResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListApplicationMethodResponse;
  static deserializeBinaryFromReader(message: ListApplicationMethodResponse, reader: jspb.BinaryReader): ListApplicationMethodResponse;
}

export namespace ListApplicationMethodResponse {
  export type AsObject = {
    itemsList: Array<ApplicationMethod.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveApplicationMethodRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ApplicationMethod | undefined;
  setItem(value?: ApplicationMethod): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveApplicationMethodRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveApplicationMethodRequest): SaveApplicationMethodRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveApplicationMethodRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveApplicationMethodRequest;
  static deserializeBinaryFromReader(message: SaveApplicationMethodRequest, reader: jspb.BinaryReader): SaveApplicationMethodRequest;
}

export namespace SaveApplicationMethodRequest {
  export type AsObject = {
    item?: ApplicationMethod.AsObject,
  }
}

export class SaveApplicationMethodResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveApplicationMethodResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveApplicationMethodResponse): SaveApplicationMethodResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveApplicationMethodResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveApplicationMethodResponse;
  static deserializeBinaryFromReader(message: SaveApplicationMethodResponse, reader: jspb.BinaryReader): SaveApplicationMethodResponse;
}

export namespace SaveApplicationMethodResponse {
  export type AsObject = {
    id: number,
  }
}

