// source: v1/dictionary_md/product.proto
/**
 * @fileoverview
 * @enhanceable
 * @suppress {messageConventions} JS Compiler reports an error if a variable or
 *     field starts with 'MSG_' and isn't a translatable message.
 * @public
 */
// GENERATED CODE -- DO NOT EDIT!

var jspb = require('google-protobuf');
var goog = jspb;
var global = Function('return this')();

var google_protobuf_wrappers_pb = require('google-protobuf/google/protobuf/wrappers_pb.js');
goog.object.extend(proto, google_protobuf_wrappers_pb);
var google_protobuf_timestamp_pb = require('google-protobuf/google/protobuf/timestamp_pb.js');
goog.object.extend(proto, google_protobuf_timestamp_pb);
var v1_dictionary_common_enum_product_pb = require('../../v1/dictionary_common/enum_product_pb.js');
goog.object.extend(proto, v1_dictionary_common_enum_product_pb);
var v1_dictionary_common_dictionary_common_pb = require('../../v1/dictionary_common/dictionary_common_pb.js');
goog.object.extend(proto, v1_dictionary_common_dictionary_common_pb);
var v1_dictionary_md_application_method_pb = require('../../v1/dictionary_md/application_method_pb.js');
goog.object.extend(proto, v1_dictionary_md_application_method_pb);
var v1_dictionary_md_application_rate_unit_chemical_pb = require('../../v1/dictionary_md/application_rate_unit_chemical_pb.js');
goog.object.extend(proto, v1_dictionary_md_application_rate_unit_chemical_pb);
var v1_dictionary_md_category_pb = require('../../v1/dictionary_md/category_pb.js');
goog.object.extend(proto, v1_dictionary_md_category_pb);
var v1_dictionary_md_chemical_class_group_pb = require('../../v1/dictionary_md/chemical_class_group_pb.js');
goog.object.extend(proto, v1_dictionary_md_chemical_class_group_pb);
var v1_dictionary_md_color_pb = require('../../v1/dictionary_md/color_pb.js');
goog.object.extend(proto, v1_dictionary_md_color_pb);
var v1_dictionary_md_distribution_type_chemical_pb = require('../../v1/dictionary_md/distribution_type_chemical_pb.js');
goog.object.extend(proto, v1_dictionary_md_distribution_type_chemical_pb);
var v1_dictionary_md_file_pb = require('../../v1/dictionary_md/file_pb.js');
goog.object.extend(proto, v1_dictionary_md_file_pb);
var v1_dictionary_md_product_category_application_pb = require('../../v1/dictionary_md/product_category_application_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_category_application_pb);
var v1_dictionary_md_product_package_pb = require('../../v1/dictionary_md/product_package_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_package_pb);
var v1_dictionary_md_preparative_form_pb = require('../../v1/dictionary_md/preparative_form_pb.js');
goog.object.extend(proto, v1_dictionary_md_preparative_form_pb);
var v1_dictionary_md_product_active_substance_pb = require('../../v1/dictionary_md/product_active_substance_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_active_substance_pb);
var v1_dictionary_md_product_condition_pb = require('../../v1/dictionary_md/product_condition_pb.js');
goog.object.extend(proto, v1_dictionary_md_product_condition_pb);
var v1_dictionary_md_spectrum_action_chemical_pb = require('../../v1/dictionary_md/spectrum_action_chemical_pb.js');
goog.object.extend(proto, v1_dictionary_md_spectrum_action_chemical_pb);
var v1_dictionary_md_quantity_type_pb = require('../../v1/dictionary_md/quantity_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_quantity_type_pb);
var v1_dictionary_md_seed_sort_type_pb = require('../../v1/dictionary_md_seed/sort_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_sort_type_pb);
var v1_dictionary_md_seed_adapt_type_pb = require('../../v1/dictionary_md_seed/adapt_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_adapt_type_pb);
var v1_dictionary_md_seed_fruit_average_weight_pb = require('../../v1/dictionary_md_seed/fruit_average_weight_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_fruit_average_weight_pb);
var v1_dictionary_md_seed_fruit_form_pb = require('../../v1/dictionary_md_seed/fruit_form_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_fruit_form_pb);
var v1_dictionary_md_seed_grow_type_pb = require('../../v1/dictionary_md_seed/grow_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_grow_type_pb);
var v1_dictionary_md_seed_grow_season_pb = require('../../v1/dictionary_md_seed/grow_season_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_grow_season_pb);
var v1_dictionary_md_seed_maturity_group_pb = require('../../v1/dictionary_md_seed/maturity_group_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_maturity_group_pb);
var v1_dictionary_md_seed_origin_country_pb = require('../../v1/dictionary_md_seed/origin_country_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_origin_country_pb);
var v1_dictionary_md_seed_plant_type_pb = require('../../v1/dictionary_md_seed/plant_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_plant_type_pb);
var v1_dictionary_md_seed_pollination_type_pb = require('../../v1/dictionary_md_seed/pollination_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_pollination_type_pb);
var v1_dictionary_md_seed_purpose_pb = require('../../v1/dictionary_md_seed/purpose_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_purpose_pb);
var v1_dictionary_md_seed_seed_type_pb = require('../../v1/dictionary_md_seed/seed_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_seed_type_pb);
var v1_dictionary_md_seed_species_pb = require('../../v1/dictionary_md_seed/species_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_species_pb);
var v1_dictionary_md_seed_technology_type_pb = require('../../v1/dictionary_md_seed/technology_type_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_technology_type_pb);
var v1_dictionary_md_seed_reproduction_pb = require('../../v1/dictionary_md_seed/reproduction_pb.js');
goog.object.extend(proto, v1_dictionary_md_seed_reproduction_pb);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.GetProductRequest', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.GetProductResponse', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.ListProductRequest', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.ListProductResponse', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.Product', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.ProductDetailed', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.ProductFilter', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest', null, global);
goog.exportSymbol('proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse', null, global);
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.Product = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.Product, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.Product.displayName = 'proto.fcp.dictionary.v1.dictionary_md.Product';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.ProductFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.ProductFilter.displayName = 'proto.fcp.dictionary.v1.dictionary_md.ProductFilter';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.repeatedFields_, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.displayName = 'proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.repeatedFields_, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.ProductDetailed, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.displayName = 'proto.fcp.dictionary.v1.dictionary_md.ProductDetailed';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.GetProductRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.displayName = 'proto.fcp.dictionary.v1.dictionary_md.GetProductRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.GetProductResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.displayName = 'proto.fcp.dictionary.v1.dictionary_md.GetProductResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.repeatedFields_, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.displayName = 'proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.displayName = 'proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.repeatedFields_, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.ListProductRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.displayName = 'proto.fcp.dictionary.v1.dictionary_md.ListProductRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.repeatedFields_, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.ListProductResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.displayName = 'proto.fcp.dictionary.v1.dictionary_md.ListProductResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.displayName = 'proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.displayName = 'proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.repeatedFields_, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.displayName = 'proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.repeatedFields_, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.displayName = 'proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.repeatedFields_, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.displayName = 'proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd';
}
/**
 * Generated by JsPbCodeGenerator.
 * @param {Array=} opt_data Optional initial data array, typically from a
 * server response, or constructed directly in Javascript. The array is used
 * in place and becomes part of the constructed object. It is not cloned.
 * If no data is provided, the constructed object will be empty, but still
 * valid.
 * @extends {jspb.Message}
 * @constructor
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter = function(opt_data) {
  jspb.Message.initialize(this, opt_data, 0, -1, null, null);
};
goog.inherits(proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter, jspb.Message);
if (goog.DEBUG && !COMPILED) {
  /**
   * @public
   * @override
   */
  proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.displayName = 'proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter';
}



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.Product.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.Product} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.Product.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0),
    externalId: jspb.Message.getFieldWithDefault(msg, 2, 0),
    name: jspb.Message.getFieldWithDefault(msg, 3, ""),
    type: jspb.Message.getFieldWithDefault(msg, 9, 0),
    process: jspb.Message.getFieldWithDefault(msg, 10, 0),
    brandId: jspb.Message.getFieldWithDefault(msg, 4, 0),
    brandName: jspb.Message.getFieldWithDefault(msg, 5, ""),
    productGroupId: jspb.Message.getFieldWithDefault(msg, 6, 0),
    productGroupName: jspb.Message.getFieldWithDefault(msg, 7, ""),
    productSubGroupId: jspb.Message.getFieldWithDefault(msg, 11, 0),
    productSubGroupName: jspb.Message.getFieldWithDefault(msg, 12, ""),
    description: jspb.Message.getFieldWithDefault(msg, 8, ""),
    toxicityClassId: jspb.Message.getFieldWithDefault(msg, 14, 0),
    toxicityClassName: jspb.Message.getFieldWithDefault(msg, 15, ""),
    productCondition: (f = msg.getProductCondition()) && v1_dictionary_md_product_condition_pb.ProductCondition.toObject(includeInstance, f),
    audit: (f = msg.getAudit()) && v1_dictionary_common_dictionary_common_pb.Audit.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.Product;
  return proto.fcp.dictionary.v1.dictionary_md.Product.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.Product} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    case 2:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setExternalId(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setName(value);
      break;
    case 9:
      var value = /** @type {!proto.fcp.dictionary.v1.dictionary_common.ProductType} */ (reader.readEnum());
      msg.setType(value);
      break;
    case 10:
      var value = /** @type {!proto.fcp.dictionary.v1.dictionary_common.ProductProcess} */ (reader.readEnum());
      msg.setProcess(value);
      break;
    case 4:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setBrandId(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setBrandName(value);
      break;
    case 6:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setProductGroupId(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setProductGroupName(value);
      break;
    case 11:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setProductSubGroupId(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setProductSubGroupName(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setDescription(value);
      break;
    case 14:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setToxicityClassId(value);
      break;
    case 15:
      var value = /** @type {string} */ (reader.readString());
      msg.setToxicityClassName(value);
      break;
    case 16:
      var value = new v1_dictionary_md_product_condition_pb.ProductCondition;
      reader.readMessage(value,v1_dictionary_md_product_condition_pb.ProductCondition.deserializeBinaryFromReader);
      msg.setProductCondition(value);
      break;
    case 21:
      var value = new v1_dictionary_common_dictionary_common_pb.Audit;
      reader.readMessage(value,v1_dictionary_common_dictionary_common_pb.Audit.deserializeBinaryFromReader);
      msg.setAudit(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.Product.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.Product} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.Product.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
  f = message.getExternalId();
  if (f !== 0) {
    writer.writeInt64(
      2,
      f
    );
  }
  f = message.getName();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getType();
  if (f !== 0.0) {
    writer.writeEnum(
      9,
      f
    );
  }
  f = message.getProcess();
  if (f !== 0.0) {
    writer.writeEnum(
      10,
      f
    );
  }
  f = message.getBrandId();
  if (f !== 0) {
    writer.writeInt64(
      4,
      f
    );
  }
  f = message.getBrandName();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getProductGroupId();
  if (f !== 0) {
    writer.writeInt64(
      6,
      f
    );
  }
  f = message.getProductGroupName();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getProductSubGroupId();
  if (f !== 0) {
    writer.writeInt64(
      11,
      f
    );
  }
  f = message.getProductSubGroupName();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getDescription();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getToxicityClassId();
  if (f !== 0) {
    writer.writeInt64(
      14,
      f
    );
  }
  f = message.getToxicityClassName();
  if (f.length > 0) {
    writer.writeString(
      15,
      f
    );
  }
  f = message.getProductCondition();
  if (f != null) {
    writer.writeMessage(
      16,
      f,
      v1_dictionary_md_product_condition_pb.ProductCondition.serializeBinaryToWriter
    );
  }
  f = message.getAudit();
  if (f != null) {
    writer.writeMessage(
      21,
      f,
      v1_dictionary_common_dictionary_common_pb.Audit.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};


/**
 * optional int64 external_id = 2;
 * @return {number}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getExternalId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 2, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setExternalId = function(value) {
  return jspb.Message.setProto3IntField(this, 2, value);
};


/**
 * optional string name = 3;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setName = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional fcp.dictionary.v1.dictionary_common.ProductType type = 9;
 * @return {!proto.fcp.dictionary.v1.dictionary_common.ProductType}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getType = function() {
  return /** @type {!proto.fcp.dictionary.v1.dictionary_common.ProductType} */ (jspb.Message.getFieldWithDefault(this, 9, 0));
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_common.ProductType} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setType = function(value) {
  return jspb.Message.setProto3EnumField(this, 9, value);
};


/**
 * optional fcp.dictionary.v1.dictionary_common.ProductProcess process = 10;
 * @return {!proto.fcp.dictionary.v1.dictionary_common.ProductProcess}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getProcess = function() {
  return /** @type {!proto.fcp.dictionary.v1.dictionary_common.ProductProcess} */ (jspb.Message.getFieldWithDefault(this, 10, 0));
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_common.ProductProcess} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setProcess = function(value) {
  return jspb.Message.setProto3EnumField(this, 10, value);
};


/**
 * optional int64 brand_id = 4;
 * @return {number}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getBrandId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 4, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setBrandId = function(value) {
  return jspb.Message.setProto3IntField(this, 4, value);
};


/**
 * optional string brand_name = 5;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getBrandName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setBrandName = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional int64 product_group_id = 6;
 * @return {number}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getProductGroupId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 6, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setProductGroupId = function(value) {
  return jspb.Message.setProto3IntField(this, 6, value);
};


/**
 * optional string product_group_name = 7;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getProductGroupName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setProductGroupName = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional int64 product_sub_group_id = 11;
 * @return {number}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getProductSubGroupId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 11, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setProductSubGroupId = function(value) {
  return jspb.Message.setProto3IntField(this, 11, value);
};


/**
 * optional string product_sub_group_name = 12;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getProductSubGroupName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setProductSubGroupName = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional string description = 8;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getDescription = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setDescription = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional int64 toxicity_class_id = 14;
 * @return {number}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getToxicityClassId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 14, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setToxicityClassId = function(value) {
  return jspb.Message.setProto3IntField(this, 14, value);
};


/**
 * optional string toxicity_class_name = 15;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getToxicityClassName = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 15, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setToxicityClassName = function(value) {
  return jspb.Message.setProto3StringField(this, 15, value);
};


/**
 * optional ProductCondition product_condition = 16;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.ProductCondition}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getProductCondition = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.ProductCondition} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_product_condition_pb.ProductCondition, 16));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.ProductCondition|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setProductCondition = function(value) {
  return jspb.Message.setWrapperField(this, 16, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.clearProductCondition = function() {
  return this.setProductCondition(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.hasProductCondition = function() {
  return jspb.Message.getField(this, 16) != null;
};


/**
 * optional fcp.dictionary.v1.dictionary_common.Audit audit = 21;
 * @return {?proto.fcp.dictionary.v1.dictionary_common.Audit}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.getAudit = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_common.Audit} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_common_dictionary_common_pb.Audit, 21));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_common.Audit|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.setAudit = function(value) {
  return jspb.Message.setWrapperField(this, 21, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.clearAudit = function() {
  return this.setAudit(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.Product.prototype.hasAudit = function() {
  return jspb.Message.getField(this, 21) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.ProductFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: (f = msg.getId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    externalId: (f = msg.getExternalId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    name: (f = msg.getName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    process: (f = msg.getProcess()) && v1_dictionary_common_enum_product_pb.ProductProcessValue.toObject(includeInstance, f),
    type: (f = msg.getType()) && v1_dictionary_common_enum_product_pb.ProductTypeValue.toObject(includeInstance, f),
    brandId: (f = msg.getBrandId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    brandName: (f = msg.getBrandName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    productGroupId: (f = msg.getProductGroupId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    productGroupName: (f = msg.getProductGroupName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    categoryId: (f = msg.getCategoryId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    quantityTypeId: (f = msg.getQuantityTypeId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    quantityTypeName: (f = msg.getQuantityTypeName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    productSubGroupId: (f = msg.getProductSubGroupId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    productSubGroupName: (f = msg.getProductSubGroupName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    applicationMethodId: (f = msg.getApplicationMethodId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    toxicityClassId: (f = msg.getToxicityClassId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    toxicityClassName: (f = msg.getToxicityClassName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    chemicalClassGroupId: (f = msg.getChemicalClassGroupId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    colorId: (f = msg.getColorId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    colorName: (f = msg.getColorName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    state: (f = msg.getState()) && v1_dictionary_common_dictionary_common_pb.StateValue.toObject(includeInstance, f),
    createdAt: (f = msg.getCreatedAt()) && v1_dictionary_common_dictionary_common_pb.TimeRange.toObject(includeInstance, f),
    createdBy: (f = msg.getCreatedBy()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    updatedAt: (f = msg.getUpdatedAt()) && v1_dictionary_common_dictionary_common_pb.TimeRange.toObject(includeInstance, f),
    updatedBy: (f = msg.getUpdatedBy()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.ProductFilter;
  return proto.fcp.dictionary.v1.dictionary_md.ProductFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setId(value);
      break;
    case 2:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setExternalId(value);
      break;
    case 3:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setName(value);
      break;
    case 8:
      var value = new v1_dictionary_common_enum_product_pb.ProductProcessValue;
      reader.readMessage(value,v1_dictionary_common_enum_product_pb.ProductProcessValue.deserializeBinaryFromReader);
      msg.setProcess(value);
      break;
    case 9:
      var value = new v1_dictionary_common_enum_product_pb.ProductTypeValue;
      reader.readMessage(value,v1_dictionary_common_enum_product_pb.ProductTypeValue.deserializeBinaryFromReader);
      msg.setType(value);
      break;
    case 4:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setBrandId(value);
      break;
    case 5:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setBrandName(value);
      break;
    case 6:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setProductGroupId(value);
      break;
    case 7:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setProductGroupName(value);
      break;
    case 10:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setCategoryId(value);
      break;
    case 11:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setQuantityTypeId(value);
      break;
    case 12:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setQuantityTypeName(value);
      break;
    case 13:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setProductSubGroupId(value);
      break;
    case 14:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setProductSubGroupName(value);
      break;
    case 15:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setApplicationMethodId(value);
      break;
    case 18:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setToxicityClassId(value);
      break;
    case 19:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setToxicityClassName(value);
      break;
    case 17:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setChemicalClassGroupId(value);
      break;
    case 20:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setColorId(value);
      break;
    case 31:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setColorName(value);
      break;
    case 21:
      var value = new v1_dictionary_common_dictionary_common_pb.StateValue;
      reader.readMessage(value,v1_dictionary_common_dictionary_common_pb.StateValue.deserializeBinaryFromReader);
      msg.setState(value);
      break;
    case 23:
      var value = new v1_dictionary_common_dictionary_common_pb.TimeRange;
      reader.readMessage(value,v1_dictionary_common_dictionary_common_pb.TimeRange.deserializeBinaryFromReader);
      msg.setCreatedAt(value);
      break;
    case 24:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setCreatedBy(value);
      break;
    case 25:
      var value = new v1_dictionary_common_dictionary_common_pb.TimeRange;
      reader.readMessage(value,v1_dictionary_common_dictionary_common_pb.TimeRange.deserializeBinaryFromReader);
      msg.setUpdatedAt(value);
      break;
    case 26:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setUpdatedBy(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.ProductFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getExternalId();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getName();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getProcess();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      v1_dictionary_common_enum_product_pb.ProductProcessValue.serializeBinaryToWriter
    );
  }
  f = message.getType();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      v1_dictionary_common_enum_product_pb.ProductTypeValue.serializeBinaryToWriter
    );
  }
  f = message.getBrandId();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getBrandName();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getProductGroupId();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getProductGroupName();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getCategoryId();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getQuantityTypeId();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getQuantityTypeName();
  if (f != null) {
    writer.writeMessage(
      12,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getProductSubGroupId();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getProductSubGroupName();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getApplicationMethodId();
  if (f != null) {
    writer.writeMessage(
      15,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getToxicityClassId();
  if (f != null) {
    writer.writeMessage(
      18,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getToxicityClassName();
  if (f != null) {
    writer.writeMessage(
      19,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getChemicalClassGroupId();
  if (f != null) {
    writer.writeMessage(
      17,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getColorId();
  if (f != null) {
    writer.writeMessage(
      20,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getColorName();
  if (f != null) {
    writer.writeMessage(
      31,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getState();
  if (f != null) {
    writer.writeMessage(
      21,
      f,
      v1_dictionary_common_dictionary_common_pb.StateValue.serializeBinaryToWriter
    );
  }
  f = message.getCreatedAt();
  if (f != null) {
    writer.writeMessage(
      23,
      f,
      v1_dictionary_common_dictionary_common_pb.TimeRange.serializeBinaryToWriter
    );
  }
  f = message.getCreatedBy();
  if (f != null) {
    writer.writeMessage(
      24,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getUpdatedAt();
  if (f != null) {
    writer.writeMessage(
      25,
      f,
      v1_dictionary_common_dictionary_common_pb.TimeRange.serializeBinaryToWriter
    );
  }
  f = message.getUpdatedBy();
  if (f != null) {
    writer.writeMessage(
      26,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.Int64Value id = 1;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 1));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setId = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearId = function() {
  return this.setId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasId = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.Int64Value external_id = 2;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getExternalId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 2));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setExternalId = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearExternalId = function() {
  return this.setExternalId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasExternalId = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional google.protobuf.StringValue name = 3;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 3));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setName = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearName = function() {
  return this.setName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasName = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional fcp.dictionary.v1.dictionary_common.ProductProcessValue process = 8;
 * @return {?proto.fcp.dictionary.v1.dictionary_common.ProductProcessValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getProcess = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_common.ProductProcessValue} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_common_enum_product_pb.ProductProcessValue, 8));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_common.ProductProcessValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setProcess = function(value) {
  return jspb.Message.setWrapperField(this, 8, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearProcess = function() {
  return this.setProcess(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasProcess = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional fcp.dictionary.v1.dictionary_common.ProductTypeValue type = 9;
 * @return {?proto.fcp.dictionary.v1.dictionary_common.ProductTypeValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getType = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_common.ProductTypeValue} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_common_enum_product_pb.ProductTypeValue, 9));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_common.ProductTypeValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setType = function(value) {
  return jspb.Message.setWrapperField(this, 9, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearType = function() {
  return this.setType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasType = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional google.protobuf.Int64Value brand_id = 4;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getBrandId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 4));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setBrandId = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearBrandId = function() {
  return this.setBrandId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasBrandId = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional google.protobuf.StringValue brand_name = 5;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getBrandName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 5));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setBrandName = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearBrandName = function() {
  return this.setBrandName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasBrandName = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional google.protobuf.Int64Value product_group_id = 6;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getProductGroupId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 6));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setProductGroupId = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearProductGroupId = function() {
  return this.setProductGroupId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasProductGroupId = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional google.protobuf.StringValue product_group_name = 7;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getProductGroupName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 7));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setProductGroupName = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearProductGroupName = function() {
  return this.setProductGroupName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasProductGroupName = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional google.protobuf.Int64Value category_id = 10;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getCategoryId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 10));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setCategoryId = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearCategoryId = function() {
  return this.setCategoryId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasCategoryId = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional google.protobuf.Int64Value quantity_type_id = 11;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getQuantityTypeId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 11));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setQuantityTypeId = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearQuantityTypeId = function() {
  return this.setQuantityTypeId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasQuantityTypeId = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * optional google.protobuf.StringValue quantity_type_name = 12;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getQuantityTypeName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 12));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setQuantityTypeName = function(value) {
  return jspb.Message.setWrapperField(this, 12, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearQuantityTypeName = function() {
  return this.setQuantityTypeName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasQuantityTypeName = function() {
  return jspb.Message.getField(this, 12) != null;
};


/**
 * optional google.protobuf.Int64Value product_sub_group_id = 13;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getProductSubGroupId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 13));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setProductSubGroupId = function(value) {
  return jspb.Message.setWrapperField(this, 13, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearProductSubGroupId = function() {
  return this.setProductSubGroupId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasProductSubGroupId = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional google.protobuf.StringValue product_sub_group_name = 14;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getProductSubGroupName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 14));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setProductSubGroupName = function(value) {
  return jspb.Message.setWrapperField(this, 14, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearProductSubGroupName = function() {
  return this.setProductSubGroupName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasProductSubGroupName = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional google.protobuf.Int64Value application_method_id = 15;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getApplicationMethodId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 15));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setApplicationMethodId = function(value) {
  return jspb.Message.setWrapperField(this, 15, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearApplicationMethodId = function() {
  return this.setApplicationMethodId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasApplicationMethodId = function() {
  return jspb.Message.getField(this, 15) != null;
};


/**
 * optional google.protobuf.Int64Value toxicity_class_id = 18;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getToxicityClassId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 18));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setToxicityClassId = function(value) {
  return jspb.Message.setWrapperField(this, 18, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearToxicityClassId = function() {
  return this.setToxicityClassId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasToxicityClassId = function() {
  return jspb.Message.getField(this, 18) != null;
};


/**
 * optional google.protobuf.StringValue toxicity_class_name = 19;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getToxicityClassName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 19));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setToxicityClassName = function(value) {
  return jspb.Message.setWrapperField(this, 19, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearToxicityClassName = function() {
  return this.setToxicityClassName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasToxicityClassName = function() {
  return jspb.Message.getField(this, 19) != null;
};


/**
 * optional google.protobuf.Int64Value chemical_class_group_id = 17;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getChemicalClassGroupId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 17));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setChemicalClassGroupId = function(value) {
  return jspb.Message.setWrapperField(this, 17, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearChemicalClassGroupId = function() {
  return this.setChemicalClassGroupId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasChemicalClassGroupId = function() {
  return jspb.Message.getField(this, 17) != null;
};


/**
 * optional google.protobuf.Int64Value color_id = 20;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getColorId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 20));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setColorId = function(value) {
  return jspb.Message.setWrapperField(this, 20, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearColorId = function() {
  return this.setColorId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasColorId = function() {
  return jspb.Message.getField(this, 20) != null;
};


/**
 * optional google.protobuf.StringValue color_name = 31;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getColorName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 31));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setColorName = function(value) {
  return jspb.Message.setWrapperField(this, 31, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearColorName = function() {
  return this.setColorName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasColorName = function() {
  return jspb.Message.getField(this, 31) != null;
};


/**
 * optional fcp.dictionary.v1.dictionary_common.StateValue state = 21;
 * @return {?proto.fcp.dictionary.v1.dictionary_common.StateValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getState = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_common.StateValue} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_common_dictionary_common_pb.StateValue, 21));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_common.StateValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setState = function(value) {
  return jspb.Message.setWrapperField(this, 21, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearState = function() {
  return this.setState(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasState = function() {
  return jspb.Message.getField(this, 21) != null;
};


/**
 * optional fcp.dictionary.v1.dictionary_common.TimeRange created_at = 23;
 * @return {?proto.fcp.dictionary.v1.dictionary_common.TimeRange}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getCreatedAt = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_common.TimeRange} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_common_dictionary_common_pb.TimeRange, 23));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_common.TimeRange|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setCreatedAt = function(value) {
  return jspb.Message.setWrapperField(this, 23, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearCreatedAt = function() {
  return this.setCreatedAt(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasCreatedAt = function() {
  return jspb.Message.getField(this, 23) != null;
};


/**
 * optional google.protobuf.StringValue created_by = 24;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getCreatedBy = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 24));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setCreatedBy = function(value) {
  return jspb.Message.setWrapperField(this, 24, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearCreatedBy = function() {
  return this.setCreatedBy(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasCreatedBy = function() {
  return jspb.Message.getField(this, 24) != null;
};


/**
 * optional fcp.dictionary.v1.dictionary_common.TimeRange updated_at = 25;
 * @return {?proto.fcp.dictionary.v1.dictionary_common.TimeRange}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getUpdatedAt = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_common.TimeRange} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_common_dictionary_common_pb.TimeRange, 25));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_common.TimeRange|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setUpdatedAt = function(value) {
  return jspb.Message.setWrapperField(this, 25, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearUpdatedAt = function() {
  return this.setUpdatedAt(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasUpdatedAt = function() {
  return jspb.Message.getField(this, 25) != null;
};


/**
 * optional google.protobuf.StringValue updated_by = 26;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.getUpdatedBy = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 26));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.setUpdatedBy = function(value) {
  return jspb.Message.setWrapperField(this, 26, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.clearUpdatedBy = function() {
  return this.setUpdatedBy(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilter.prototype.hasUpdatedBy = function() {
  return jspb.Message.getField(this, 26) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.repeatedFields_ = [1,2,3,4,5,6,12,7,8,9,10];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.toObject = function(includeInstance, msg) {
  var f, obj = {
    idList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f,
    name: (f = msg.getName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    processList: (f = jspb.Message.getRepeatedField(msg, 2)) == null ? undefined : f,
    typeList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f,
    brandIdList: (f = jspb.Message.getRepeatedField(msg, 4)) == null ? undefined : f,
    productGroupIdList: (f = jspb.Message.getRepeatedField(msg, 5)) == null ? undefined : f,
    productSubGroupIdList: (f = jspb.Message.getRepeatedField(msg, 6)) == null ? undefined : f,
    categoryGroupIdList: (f = jspb.Message.getRepeatedField(msg, 12)) == null ? undefined : f,
    categoryIdList: (f = jspb.Message.getRepeatedField(msg, 7)) == null ? undefined : f,
    quantityTypeIdList: (f = jspb.Message.getRepeatedField(msg, 8)) == null ? undefined : f,
    productPackageIdList: (f = jspb.Message.getRepeatedField(msg, 9)) == null ? undefined : f,
    colorIdList: (f = jspb.Message.getRepeatedField(msg, 10)) == null ? undefined : f,
    state: (f = msg.getState()) && v1_dictionary_common_dictionary_common_pb.StateValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd;
  return proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setIdList(value);
      break;
    case 14:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setName(value);
      break;
    case 2:
      var value = /** @type {!Array<!proto.fcp.dictionary.v1.dictionary_common.ProductProcess>} */ (reader.readPackedEnum());
      msg.setProcessList(value);
      break;
    case 3:
      var value = /** @type {!Array<!proto.fcp.dictionary.v1.dictionary_common.ProductType>} */ (reader.readPackedEnum());
      msg.setTypeList(value);
      break;
    case 4:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setBrandIdList(value);
      break;
    case 5:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setProductGroupIdList(value);
      break;
    case 6:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setProductSubGroupIdList(value);
      break;
    case 12:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setCategoryGroupIdList(value);
      break;
    case 7:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setCategoryIdList(value);
      break;
    case 8:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setQuantityTypeIdList(value);
      break;
    case 9:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setProductPackageIdList(value);
      break;
    case 10:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setColorIdList(value);
      break;
    case 21:
      var value = new v1_dictionary_common_dictionary_common_pb.StateValue;
      reader.readMessage(value,v1_dictionary_common_dictionary_common_pb.StateValue.deserializeBinaryFromReader);
      msg.setState(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      1,
      f
    );
  }
  f = message.getName();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getProcessList();
  if (f.length > 0) {
    writer.writePackedEnum(
      2,
      f
    );
  }
  f = message.getTypeList();
  if (f.length > 0) {
    writer.writePackedEnum(
      3,
      f
    );
  }
  f = message.getBrandIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      4,
      f
    );
  }
  f = message.getProductGroupIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      5,
      f
    );
  }
  f = message.getProductSubGroupIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      6,
      f
    );
  }
  f = message.getCategoryGroupIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      12,
      f
    );
  }
  f = message.getCategoryIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      7,
      f
    );
  }
  f = message.getQuantityTypeIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      8,
      f
    );
  }
  f = message.getProductPackageIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      9,
      f
    );
  }
  f = message.getColorIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      10,
      f
    );
  }
  f = message.getState();
  if (f != null) {
    writer.writeMessage(
      21,
      f,
      v1_dictionary_common_dictionary_common_pb.StateValue.serializeBinaryToWriter
    );
  }
};


/**
 * repeated int64 id = 1;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setIdList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.addId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearIdList = function() {
  return this.setIdList([]);
};


/**
 * optional google.protobuf.StringValue name = 14;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 14));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setName = function(value) {
  return jspb.Message.setWrapperField(this, 14, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearName = function() {
  return this.setName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.hasName = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * repeated fcp.dictionary.v1.dictionary_common.ProductProcess process = 2;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_common.ProductProcess>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getProcessList = function() {
  return /** @type {!Array<!proto.fcp.dictionary.v1.dictionary_common.ProductProcess>} */ (jspb.Message.getRepeatedField(this, 2));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_common.ProductProcess>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setProcessList = function(value) {
  return jspb.Message.setField(this, 2, value || []);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_common.ProductProcess} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.addProcess = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 2, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearProcessList = function() {
  return this.setProcessList([]);
};


/**
 * repeated fcp.dictionary.v1.dictionary_common.ProductType type = 3;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_common.ProductType>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getTypeList = function() {
  return /** @type {!Array<!proto.fcp.dictionary.v1.dictionary_common.ProductType>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_common.ProductType>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setTypeList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_common.ProductType} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.addType = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearTypeList = function() {
  return this.setTypeList([]);
};


/**
 * repeated int64 brand_id = 4;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getBrandIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 4));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setBrandIdList = function(value) {
  return jspb.Message.setField(this, 4, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.addBrandId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 4, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearBrandIdList = function() {
  return this.setBrandIdList([]);
};


/**
 * repeated int64 product_group_id = 5;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getProductGroupIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 5));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setProductGroupIdList = function(value) {
  return jspb.Message.setField(this, 5, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.addProductGroupId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 5, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearProductGroupIdList = function() {
  return this.setProductGroupIdList([]);
};


/**
 * repeated int64 product_sub_group_id = 6;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getProductSubGroupIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 6));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setProductSubGroupIdList = function(value) {
  return jspb.Message.setField(this, 6, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.addProductSubGroupId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 6, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearProductSubGroupIdList = function() {
  return this.setProductSubGroupIdList([]);
};


/**
 * repeated int64 category_group_id = 12;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getCategoryGroupIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 12));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setCategoryGroupIdList = function(value) {
  return jspb.Message.setField(this, 12, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.addCategoryGroupId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 12, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearCategoryGroupIdList = function() {
  return this.setCategoryGroupIdList([]);
};


/**
 * repeated int64 category_id = 7;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getCategoryIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 7));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setCategoryIdList = function(value) {
  return jspb.Message.setField(this, 7, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.addCategoryId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 7, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearCategoryIdList = function() {
  return this.setCategoryIdList([]);
};


/**
 * repeated int64 quantity_type_id = 8;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getQuantityTypeIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 8));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setQuantityTypeIdList = function(value) {
  return jspb.Message.setField(this, 8, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.addQuantityTypeId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 8, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearQuantityTypeIdList = function() {
  return this.setQuantityTypeIdList([]);
};


/**
 * repeated int64 product_package_id = 9;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getProductPackageIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 9));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setProductPackageIdList = function(value) {
  return jspb.Message.setField(this, 9, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.addProductPackageId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 9, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearProductPackageIdList = function() {
  return this.setProductPackageIdList([]);
};


/**
 * repeated int64 color_id = 10;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getColorIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 10));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setColorIdList = function(value) {
  return jspb.Message.setField(this, 10, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.addColorId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 10, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearColorIdList = function() {
  return this.setColorIdList([]);
};


/**
 * optional fcp.dictionary.v1.dictionary_common.StateValue state = 21;
 * @return {?proto.fcp.dictionary.v1.dictionary_common.StateValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.getState = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_common.StateValue} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_common_dictionary_common_pb.StateValue, 21));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_common.StateValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.setState = function(value) {
  return jspb.Message.setWrapperField(this, 21, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.clearState = function() {
  return this.setState(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.prototype.hasState = function() {
  return jspb.Message.getField(this, 21) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.repeatedFields_ = [3,5,6,2,4,7,8,9];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.toObject = function(includeInstance, msg) {
  var f, obj = {
    product: (f = msg.getProduct()) && proto.fcp.dictionary.v1.dictionary_md.Product.toObject(includeInstance, f),
    productConditionSeeds: (f = msg.getProductConditionSeeds()) && proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.toObject(includeInstance, f),
    productConditionChemicals: (f = msg.getProductConditionChemicals()) && v1_dictionary_md_product_condition_pb.ProductConditionChemicals.toObject(includeInstance, f),
    categoriesList: jspb.Message.toObjectList(msg.getCategoriesList(),
    v1_dictionary_md_category_pb.Category.toObject, includeInstance),
    quantityTypesList: jspb.Message.toObjectList(msg.getQuantityTypesList(),
    v1_dictionary_md_quantity_type_pb.QuantityType.toObject, includeInstance),
    packagesList: jspb.Message.toObjectList(msg.getPackagesList(),
    v1_dictionary_md_product_package_pb.ProductPackage.toObject, includeInstance),
    productActiveSubstancesList: jspb.Message.toObjectList(msg.getProductActiveSubstancesList(),
    v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance.toObject, includeInstance),
    preparativeFormsList: jspb.Message.toObjectList(msg.getPreparativeFormsList(),
    v1_dictionary_md_preparative_form_pb.PreparativeForm.toObject, includeInstance),
    applicationMethodsList: jspb.Message.toObjectList(msg.getApplicationMethodsList(),
    v1_dictionary_md_application_method_pb.ApplicationMethod.toObject, includeInstance),
    chemicalClassGroupsList: jspb.Message.toObjectList(msg.getChemicalClassGroupsList(),
    v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup.toObject, includeInstance),
    distributionTypeChemicalsList: jspb.Message.toObjectList(msg.getDistributionTypeChemicalsList(),
    v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.ProductDetailed;
  return proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.dictionary.v1.dictionary_md.Product;
      reader.readMessage(value,proto.fcp.dictionary.v1.dictionary_md.Product.deserializeBinaryFromReader);
      msg.setProduct(value);
      break;
    case 15:
      var value = new proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds;
      reader.readMessage(value,proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.deserializeBinaryFromReader);
      msg.setProductConditionSeeds(value);
      break;
    case 16:
      var value = new v1_dictionary_md_product_condition_pb.ProductConditionChemicals;
      reader.readMessage(value,v1_dictionary_md_product_condition_pb.ProductConditionChemicals.deserializeBinaryFromReader);
      msg.setProductConditionChemicals(value);
      break;
    case 3:
      var value = new v1_dictionary_md_category_pb.Category;
      reader.readMessage(value,v1_dictionary_md_category_pb.Category.deserializeBinaryFromReader);
      msg.addCategories(value);
      break;
    case 5:
      var value = new v1_dictionary_md_quantity_type_pb.QuantityType;
      reader.readMessage(value,v1_dictionary_md_quantity_type_pb.QuantityType.deserializeBinaryFromReader);
      msg.addQuantityTypes(value);
      break;
    case 6:
      var value = new v1_dictionary_md_product_package_pb.ProductPackage;
      reader.readMessage(value,v1_dictionary_md_product_package_pb.ProductPackage.deserializeBinaryFromReader);
      msg.addPackages(value);
      break;
    case 2:
      var value = new v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance;
      reader.readMessage(value,v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance.deserializeBinaryFromReader);
      msg.addProductActiveSubstances(value);
      break;
    case 4:
      var value = new v1_dictionary_md_preparative_form_pb.PreparativeForm;
      reader.readMessage(value,v1_dictionary_md_preparative_form_pb.PreparativeForm.deserializeBinaryFromReader);
      msg.addPreparativeForms(value);
      break;
    case 7:
      var value = new v1_dictionary_md_application_method_pb.ApplicationMethod;
      reader.readMessage(value,v1_dictionary_md_application_method_pb.ApplicationMethod.deserializeBinaryFromReader);
      msg.addApplicationMethods(value);
      break;
    case 8:
      var value = new v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup;
      reader.readMessage(value,v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup.deserializeBinaryFromReader);
      msg.addChemicalClassGroups(value);
      break;
    case 9:
      var value = new v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical;
      reader.readMessage(value,v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical.deserializeBinaryFromReader);
      msg.addDistributionTypeChemicals(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getProduct();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.dictionary.v1.dictionary_md.Product.serializeBinaryToWriter
    );
  }
  f = message.getProductConditionSeeds();
  if (f != null) {
    writer.writeMessage(
      15,
      f,
      proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.serializeBinaryToWriter
    );
  }
  f = message.getProductConditionChemicals();
  if (f != null) {
    writer.writeMessage(
      16,
      f,
      v1_dictionary_md_product_condition_pb.ProductConditionChemicals.serializeBinaryToWriter
    );
  }
  f = message.getCategoriesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      v1_dictionary_md_category_pb.Category.serializeBinaryToWriter
    );
  }
  f = message.getQuantityTypesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      5,
      f,
      v1_dictionary_md_quantity_type_pb.QuantityType.serializeBinaryToWriter
    );
  }
  f = message.getPackagesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      6,
      f,
      v1_dictionary_md_product_package_pb.ProductPackage.serializeBinaryToWriter
    );
  }
  f = message.getProductActiveSubstancesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance.serializeBinaryToWriter
    );
  }
  f = message.getPreparativeFormsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      4,
      f,
      v1_dictionary_md_preparative_form_pb.PreparativeForm.serializeBinaryToWriter
    );
  }
  f = message.getApplicationMethodsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      7,
      f,
      v1_dictionary_md_application_method_pb.ApplicationMethod.serializeBinaryToWriter
    );
  }
  f = message.getChemicalClassGroupsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      8,
      f,
      v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup.serializeBinaryToWriter
    );
  }
  f = message.getDistributionTypeChemicalsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      9,
      f,
      v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical.serializeBinaryToWriter
    );
  }
};


/**
 * optional Product product = 1;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.Product}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.getProduct = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.Product} */ (
    jspb.Message.getWrapperField(this, proto.fcp.dictionary.v1.dictionary_md.Product, 1));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.Product|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.setProduct = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.clearProduct = function() {
  return this.setProduct(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.hasProduct = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional ProductConditionSeeds product_condition_seeds = 15;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.getProductConditionSeeds = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} */ (
    jspb.Message.getWrapperField(this, proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds, 15));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.setProductConditionSeeds = function(value) {
  return jspb.Message.setWrapperField(this, 15, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.clearProductConditionSeeds = function() {
  return this.setProductConditionSeeds(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.hasProductConditionSeeds = function() {
  return jspb.Message.getField(this, 15) != null;
};


/**
 * optional ProductConditionChemicals product_condition_chemicals = 16;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicals}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.getProductConditionChemicals = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicals} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_product_condition_pb.ProductConditionChemicals, 16));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicals|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.setProductConditionChemicals = function(value) {
  return jspb.Message.setWrapperField(this, 16, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.clearProductConditionChemicals = function() {
  return this.setProductConditionChemicals(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.hasProductConditionChemicals = function() {
  return jspb.Message.getField(this, 16) != null;
};


/**
 * repeated Category categories = 3;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.Category>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.getCategoriesList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.Category>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_md_category_pb.Category, 3));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.Category>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.setCategoriesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.Category=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Category}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.addCategories = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.fcp.dictionary.v1.dictionary_md.Category, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.clearCategoriesList = function() {
  return this.setCategoriesList([]);
};


/**
 * repeated QuantityType quantity_types = 5;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.QuantityType>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.getQuantityTypesList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.QuantityType>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_md_quantity_type_pb.QuantityType, 5));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.QuantityType>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.setQuantityTypesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 5, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.QuantityType=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.QuantityType}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.addQuantityTypes = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 5, opt_value, proto.fcp.dictionary.v1.dictionary_md.QuantityType, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.clearQuantityTypesList = function() {
  return this.setQuantityTypesList([]);
};


/**
 * repeated ProductPackage packages = 6;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.ProductPackage>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.getPackagesList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.ProductPackage>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_md_product_package_pb.ProductPackage, 6));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.ProductPackage>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.setPackagesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 6, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductPackage=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductPackage}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.addPackages = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 6, opt_value, proto.fcp.dictionary.v1.dictionary_md.ProductPackage, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.clearPackagesList = function() {
  return this.setPackagesList([]);
};


/**
 * repeated ProductActiveSubstance product_active_substances = 2;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.ProductActiveSubstance>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.getProductActiveSubstancesList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.ProductActiveSubstance>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance, 2));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.ProductActiveSubstance>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.setProductActiveSubstancesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductActiveSubstance=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductActiveSubstance}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.addProductActiveSubstances = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.fcp.dictionary.v1.dictionary_md.ProductActiveSubstance, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.clearProductActiveSubstancesList = function() {
  return this.setProductActiveSubstancesList([]);
};


/**
 * repeated PreparativeForm preparative_forms = 4;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.PreparativeForm>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.getPreparativeFormsList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.PreparativeForm>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_md_preparative_form_pb.PreparativeForm, 4));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.PreparativeForm>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.setPreparativeFormsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 4, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.PreparativeForm=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.PreparativeForm}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.addPreparativeForms = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 4, opt_value, proto.fcp.dictionary.v1.dictionary_md.PreparativeForm, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.clearPreparativeFormsList = function() {
  return this.setPreparativeFormsList([]);
};


/**
 * repeated ApplicationMethod application_methods = 7;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.ApplicationMethod>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.getApplicationMethodsList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.ApplicationMethod>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_md_application_method_pb.ApplicationMethod, 7));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.ApplicationMethod>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.setApplicationMethodsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 7, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ApplicationMethod=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ApplicationMethod}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.addApplicationMethods = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 7, opt_value, proto.fcp.dictionary.v1.dictionary_md.ApplicationMethod, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.clearApplicationMethodsList = function() {
  return this.setApplicationMethodsList([]);
};


/**
 * repeated ChemicalClassGroup chemical_class_groups = 8;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.ChemicalClassGroup>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.getChemicalClassGroupsList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.ChemicalClassGroup>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup, 8));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.ChemicalClassGroup>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.setChemicalClassGroupsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 8, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ChemicalClassGroup=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ChemicalClassGroup}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.addChemicalClassGroups = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 8, opt_value, proto.fcp.dictionary.v1.dictionary_md.ChemicalClassGroup, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.clearChemicalClassGroupsList = function() {
  return this.setChemicalClassGroupsList([]);
};


/**
 * repeated DistributionTypeChemical distribution_type_chemicals = 9;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.DistributionTypeChemical>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.getDistributionTypeChemicalsList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.DistributionTypeChemical>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical, 9));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.DistributionTypeChemical>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.setDistributionTypeChemicalsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 9, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.DistributionTypeChemical=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.DistributionTypeChemical}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.addDistributionTypeChemicals = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 9, opt_value, proto.fcp.dictionary.v1.dictionary_md.DistributionTypeChemical, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.prototype.clearDistributionTypeChemicalsList = function() {
  return this.setDistributionTypeChemicalsList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.GetProductRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.GetProductRequest}
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.GetProductRequest;
  return proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.GetProductRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.GetProductRequest}
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.GetProductRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.GetProductRequest} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductRequest.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.GetProductResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.GetProductResponse}
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.GetProductResponse;
  return proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.GetProductResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.GetProductResponse}
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.dictionary.v1.dictionary_md.ProductDetailed;
      reader.readMessage(value,proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.GetProductResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.serializeBinaryToWriter
    );
  }
};


/**
 * optional ProductDetailed item = 1;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.ProductDetailed}
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.prototype.getItem = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} */ (
    jspb.Message.getWrapperField(this, proto.fcp.dictionary.v1.dictionary_md.ProductDetailed, 1));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.ProductDetailed|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.GetProductResponse} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.GetProductResponse} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.GetProductResponse.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.repeatedFields_ = [3];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.toObject(includeInstance, f),
    seedsConditionFilter: (f = msg.getSeedsConditionFilter()) && proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.toObject(includeInstance, f),
    chemicalsConditionFilter: (f = msg.getChemicalsConditionFilter()) && proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.toObject(includeInstance, f),
    sortingList: jspb.Message.toObjectList(msg.getSortingList(),
    v1_dictionary_common_dictionary_common_pb.Sorting.toObject, includeInstance),
    pagination: (f = msg.getPagination()) && v1_dictionary_common_dictionary_common_pb.PaginationRequest.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest;
  return proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd;
      reader.readMessage(value,proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    case 2:
      var value = new proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd;
      reader.readMessage(value,proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.deserializeBinaryFromReader);
      msg.setSeedsConditionFilter(value);
      break;
    case 5:
      var value = new proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd;
      reader.readMessage(value,proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.deserializeBinaryFromReader);
      msg.setChemicalsConditionFilter(value);
      break;
    case 3:
      var value = new v1_dictionary_common_dictionary_common_pb.Sorting;
      reader.readMessage(value,v1_dictionary_common_dictionary_common_pb.Sorting.deserializeBinaryFromReader);
      msg.addSorting(value);
      break;
    case 4:
      var value = new v1_dictionary_common_dictionary_common_pb.PaginationRequest;
      reader.readMessage(value,v1_dictionary_common_dictionary_common_pb.PaginationRequest.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd.serializeBinaryToWriter
    );
  }
  f = message.getSeedsConditionFilter();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.serializeBinaryToWriter
    );
  }
  f = message.getChemicalsConditionFilter();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.serializeBinaryToWriter
    );
  }
  f = message.getSortingList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      3,
      f,
      v1_dictionary_common_dictionary_common_pb.Sorting.serializeBinaryToWriter
    );
  }
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      v1_dictionary_common_dictionary_common_pb.PaginationRequest.serializeBinaryToWriter
    );
  }
};


/**
 * optional ProductFilterMd filter = 1;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd} */ (
    jspb.Message.getWrapperField(this, proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd, 1));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.ProductFilterMd|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional ProductConditionSeedsFilterMd seeds_condition_filter = 2;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.getSeedsConditionFilter = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} */ (
    jspb.Message.getWrapperField(this, proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd, 2));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.setSeedsConditionFilter = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.clearSeedsConditionFilter = function() {
  return this.setSeedsConditionFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.hasSeedsConditionFilter = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional ProductConditionChemicalsFilterMd chemicals_condition_filter = 5;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.getChemicalsConditionFilter = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} */ (
    jspb.Message.getWrapperField(this, proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd, 5));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.setChemicalsConditionFilter = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.clearChemicalsConditionFilter = function() {
  return this.setChemicalsConditionFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.hasChemicalsConditionFilter = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * repeated fcp.dictionary.v1.dictionary_common.Sorting sorting = 3;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_common.Sorting>}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.getSortingList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_common.Sorting>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_common_dictionary_common_pb.Sorting, 3));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_common.Sorting>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.setSortingList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 3, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_common.Sorting=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_common.Sorting}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.addSorting = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 3, opt_value, proto.fcp.dictionary.v1.dictionary_common.Sorting, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.clearSortingList = function() {
  return this.setSortingList([]);
};


/**
 * optional fcp.dictionary.v1.dictionary_common.PaginationRequest pagination = 4;
 * @return {?proto.fcp.dictionary.v1.dictionary_common.PaginationRequest}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.getPagination = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_common.PaginationRequest} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_common_dictionary_common_pb.PaginationRequest, 4));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_common.PaginationRequest|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsRequest.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 4) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse;
  return proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setItemsList(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writePackedInt64(
      1,
      f
    );
  }
};


/**
 * repeated int64 items = 1;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.prototype.getItemsList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.prototype.setItemsList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.prototype.addItems = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductIdsResponse.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.repeatedFields_ = [2];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    filter: (f = msg.getFilter()) && proto.fcp.dictionary.v1.dictionary_md.ProductFilter.toObject(includeInstance, f),
    conditionSeedsFilter: (f = msg.getConditionSeedsFilter()) && proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.toObject(includeInstance, f),
    sortingList: jspb.Message.toObjectList(msg.getSortingList(),
    v1_dictionary_common_dictionary_common_pb.Sorting.toObject, includeInstance),
    pagination: (f = msg.getPagination()) && v1_dictionary_common_dictionary_common_pb.PaginationRequest.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.ListProductRequest;
  return proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.dictionary.v1.dictionary_md.ProductFilter;
      reader.readMessage(value,proto.fcp.dictionary.v1.dictionary_md.ProductFilter.deserializeBinaryFromReader);
      msg.setFilter(value);
      break;
    case 4:
      var value = new proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter;
      reader.readMessage(value,proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.deserializeBinaryFromReader);
      msg.setConditionSeedsFilter(value);
      break;
    case 2:
      var value = new v1_dictionary_common_dictionary_common_pb.Sorting;
      reader.readMessage(value,v1_dictionary_common_dictionary_common_pb.Sorting.deserializeBinaryFromReader);
      msg.addSorting(value);
      break;
    case 3:
      var value = new v1_dictionary_common_dictionary_common_pb.PaginationRequest;
      reader.readMessage(value,v1_dictionary_common_dictionary_common_pb.PaginationRequest.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFilter();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.dictionary.v1.dictionary_md.ProductFilter.serializeBinaryToWriter
    );
  }
  f = message.getConditionSeedsFilter();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.serializeBinaryToWriter
    );
  }
  f = message.getSortingList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      2,
      f,
      v1_dictionary_common_dictionary_common_pb.Sorting.serializeBinaryToWriter
    );
  }
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      v1_dictionary_common_dictionary_common_pb.PaginationRequest.serializeBinaryToWriter
    );
  }
};


/**
 * optional ProductFilter filter = 1;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.ProductFilter}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.getFilter = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.ProductFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.dictionary.v1.dictionary_md.ProductFilter, 1));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.ProductFilter|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.setFilter = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.clearFilter = function() {
  return this.setFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.hasFilter = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional ProductConditionSeedsFilter condition_seeds_filter = 4;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.getConditionSeedsFilter = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} */ (
    jspb.Message.getWrapperField(this, proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter, 4));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.setConditionSeedsFilter = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.clearConditionSeedsFilter = function() {
  return this.setConditionSeedsFilter(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.hasConditionSeedsFilter = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * repeated fcp.dictionary.v1.dictionary_common.Sorting sorting = 2;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_common.Sorting>}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.getSortingList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_common.Sorting>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_common_dictionary_common_pb.Sorting, 2));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_common.Sorting>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.setSortingList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 2, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_common.Sorting=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_common.Sorting}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.addSorting = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 2, opt_value, proto.fcp.dictionary.v1.dictionary_common.Sorting, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.clearSortingList = function() {
  return this.setSortingList([]);
};


/**
 * optional fcp.dictionary.v1.dictionary_common.PaginationRequest pagination = 3;
 * @return {?proto.fcp.dictionary.v1.dictionary_common.PaginationRequest}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.getPagination = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_common.PaginationRequest} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_common_dictionary_common_pb.PaginationRequest, 3));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_common.PaginationRequest|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductRequest} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductRequest.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 3) != null;
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.repeatedFields_ = [1];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    itemsList: jspb.Message.toObjectList(msg.getItemsList(),
    proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.toObject, includeInstance),
    pagination: (f = msg.getPagination()) && v1_dictionary_common_dictionary_common_pb.PaginationResponse.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductResponse}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.ListProductResponse;
  return proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductResponse}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.dictionary.v1.dictionary_md.ProductDetailed;
      reader.readMessage(value,proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.deserializeBinaryFromReader);
      msg.addItems(value);
      break;
    case 2:
      var value = new v1_dictionary_common_dictionary_common_pb.PaginationResponse;
      reader.readMessage(value,v1_dictionary_common_dictionary_common_pb.PaginationResponse.deserializeBinaryFromReader);
      msg.setPagination(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ListProductResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItemsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      1,
      f,
      proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.serializeBinaryToWriter
    );
  }
  f = message.getPagination();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      v1_dictionary_common_dictionary_common_pb.PaginationResponse.serializeBinaryToWriter
    );
  }
};


/**
 * repeated ProductDetailed items = 1;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed>}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.prototype.getItemsList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.dictionary.v1.dictionary_md.ProductDetailed, 1));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductResponse} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.prototype.setItemsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 1, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductDetailed}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.prototype.addItems = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 1, opt_value, proto.fcp.dictionary.v1.dictionary_md.ProductDetailed, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductResponse} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.prototype.clearItemsList = function() {
  return this.setItemsList([]);
};


/**
 * optional fcp.dictionary.v1.dictionary_common.PaginationResponse pagination = 2;
 * @return {?proto.fcp.dictionary.v1.dictionary_common.PaginationResponse}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.prototype.getPagination = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_common.PaginationResponse} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_common_dictionary_common_pb.PaginationResponse, 2));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_common.PaginationResponse|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductResponse} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.prototype.setPagination = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ListProductResponse} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.prototype.clearPagination = function() {
  return this.setPagination(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ListProductResponse.prototype.hasPagination = function() {
  return jspb.Message.getField(this, 2) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.toObject = function(includeInstance, msg) {
  var f, obj = {
    item: (f = msg.getItem()) && proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest}
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest;
  return proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest}
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new proto.fcp.dictionary.v1.dictionary_md.ProductDetailed;
      reader.readMessage(value,proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.deserializeBinaryFromReader);
      msg.setItem(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getItem();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      proto.fcp.dictionary.v1.dictionary_md.ProductDetailed.serializeBinaryToWriter
    );
  }
};


/**
 * optional ProductDetailed item = 1;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.ProductDetailed}
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.prototype.getItem = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.ProductDetailed} */ (
    jspb.Message.getWrapperField(this, proto.fcp.dictionary.v1.dictionary_md.ProductDetailed, 1));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.ProductDetailed|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.prototype.setItem = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.prototype.clearItem = function() {
  return this.setItem(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductRequest.prototype.hasItem = function() {
  return jspb.Message.getField(this, 1) != null;
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 1, 0)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse}
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse;
  return proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse}
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      1,
      f
    );
  }
};


/**
 * optional int64 id = 1;
 * @return {number}
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 1, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.SaveProductResponse.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 1, value);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.repeatedFields_ = [1,2,3,4,5,6,7];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.toObject = function(includeInstance, msg) {
  var f, obj = {
    toxicityClassIdList: (f = jspb.Message.getRepeatedField(msg, 1)) == null ? undefined : f,
    preparativeFormIdList: (f = jspb.Message.getRepeatedField(msg, 2)) == null ? undefined : f,
    productActiveSubstanceIdList: (f = jspb.Message.getRepeatedField(msg, 3)) == null ? undefined : f,
    chemicalClassGroupIdList: (f = jspb.Message.getRepeatedField(msg, 4)) == null ? undefined : f,
    distributionTypeChemicalIdList: (f = jspb.Message.getRepeatedField(msg, 5)) == null ? undefined : f,
    applicationMethodIdList: (f = jspb.Message.getRepeatedField(msg, 6)) == null ? undefined : f,
    spectrumActionChemicalIdList: (f = jspb.Message.getRepeatedField(msg, 7)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd;
  return proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setToxicityClassIdList(value);
      break;
    case 2:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setPreparativeFormIdList(value);
      break;
    case 3:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setProductActiveSubstanceIdList(value);
      break;
    case 4:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setChemicalClassGroupIdList(value);
      break;
    case 5:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setDistributionTypeChemicalIdList(value);
      break;
    case 6:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setApplicationMethodIdList(value);
      break;
    case 7:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setSpectrumActionChemicalIdList(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getToxicityClassIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      1,
      f
    );
  }
  f = message.getPreparativeFormIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      2,
      f
    );
  }
  f = message.getProductActiveSubstanceIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      3,
      f
    );
  }
  f = message.getChemicalClassGroupIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      4,
      f
    );
  }
  f = message.getDistributionTypeChemicalIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      5,
      f
    );
  }
  f = message.getApplicationMethodIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      6,
      f
    );
  }
  f = message.getSpectrumActionChemicalIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      7,
      f
    );
  }
};


/**
 * repeated int64 toxicity_class_id = 1;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.getToxicityClassIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 1));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.setToxicityClassIdList = function(value) {
  return jspb.Message.setField(this, 1, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.addToxicityClassId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 1, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.clearToxicityClassIdList = function() {
  return this.setToxicityClassIdList([]);
};


/**
 * repeated int64 preparative_form_id = 2;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.getPreparativeFormIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 2));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.setPreparativeFormIdList = function(value) {
  return jspb.Message.setField(this, 2, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.addPreparativeFormId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 2, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.clearPreparativeFormIdList = function() {
  return this.setPreparativeFormIdList([]);
};


/**
 * repeated int64 product_active_substance_id = 3;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.getProductActiveSubstanceIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 3));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.setProductActiveSubstanceIdList = function(value) {
  return jspb.Message.setField(this, 3, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.addProductActiveSubstanceId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 3, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.clearProductActiveSubstanceIdList = function() {
  return this.setProductActiveSubstanceIdList([]);
};


/**
 * repeated int64 chemical_class_group_id = 4;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.getChemicalClassGroupIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 4));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.setChemicalClassGroupIdList = function(value) {
  return jspb.Message.setField(this, 4, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.addChemicalClassGroupId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 4, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.clearChemicalClassGroupIdList = function() {
  return this.setChemicalClassGroupIdList([]);
};


/**
 * repeated int64 distribution_type_chemical_id = 5;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.getDistributionTypeChemicalIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 5));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.setDistributionTypeChemicalIdList = function(value) {
  return jspb.Message.setField(this, 5, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.addDistributionTypeChemicalId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 5, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.clearDistributionTypeChemicalIdList = function() {
  return this.setDistributionTypeChemicalIdList([]);
};


/**
 * repeated int64 application_method_id = 6;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.getApplicationMethodIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 6));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.setApplicationMethodIdList = function(value) {
  return jspb.Message.setField(this, 6, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.addApplicationMethodId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 6, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.clearApplicationMethodIdList = function() {
  return this.setApplicationMethodIdList([]);
};


/**
 * repeated int64 spectrum_action_chemical_id = 7;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.getSpectrumActionChemicalIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 7));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.setSpectrumActionChemicalIdList = function(value) {
  return jspb.Message.setField(this, 7, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.addSpectrumActionChemicalId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 7, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionChemicalsFilterMd.prototype.clearSpectrumActionChemicalIdList = function() {
  return this.setSpectrumActionChemicalIdList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.repeatedFields_ = [51,52,54,55];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: jspb.Message.getFieldWithDefault(msg, 36, 0),
    organic: jspb.Message.getBooleanFieldWithDefault(msg, 1, false),
    sowingRate: jspb.Message.getFieldWithDefault(msg, 3, ""),
    sowingDensity: jspb.Message.getFieldWithDefault(msg, 4, ""),
    sowingDistance: jspb.Message.getFieldWithDefault(msg, 5, ""),
    sowingTerm: jspb.Message.getFieldWithDefault(msg, 6, ""),
    harvestTerm: jspb.Message.getFieldWithDefault(msg, 7, ""),
    oneThousandSeedsWeight: jspb.Message.getFieldWithDefault(msg, 8, ""),
    germination: jspb.Message.getFieldWithDefault(msg, 9, ""),
    germinationEnergy: jspb.Message.getFieldWithDefault(msg, 10, ""),
    maturityPeriod: jspb.Message.getFieldWithDefault(msg, 11, ""),
    plantHeight: jspb.Message.getFieldWithDefault(msg, 12, ""),
    fruitWeight: jspb.Message.getFieldWithDefault(msg, 13, ""),
    wallThickness: jspb.Message.getFieldWithDefault(msg, 14, ""),
    fruitTaste: jspb.Message.getFieldWithDefault(msg, 16, ""),
    sugarConcentration: jspb.Message.getFieldWithDefault(msg, 17, ""),
    dryMaterialConcentration: jspb.Message.getFieldWithDefault(msg, 18, ""),
    frostresist: jspb.Message.getFieldWithDefault(msg, 19, ""),
    pollinator: jspb.Message.getBooleanFieldWithDefault(msg, 20, false),
    harvestYear: jspb.Message.getFieldWithDefault(msg, 21, 0),
    applicationRecommendations: jspb.Message.getFieldWithDefault(msg, 22, ""),
    flowerType: jspb.Message.getFieldWithDefault(msg, 23, ""),
    flowerDiameter: jspb.Message.getFieldWithDefault(msg, 15, ""),
    seedForm: jspb.Message.getFieldWithDefault(msg, 24, ""),
    fruitForm: (f = msg.getFruitForm()) && v1_dictionary_md_seed_fruit_form_pb.FruitForm.toObject(includeInstance, f),
    reproduction: (f = msg.getReproduction()) && v1_dictionary_md_seed_reproduction_pb.Reproduction.toObject(includeInstance, f),
    species: (f = msg.getSpecies()) && v1_dictionary_md_seed_species_pb.Species.toObject(includeInstance, f),
    technologyType: (f = msg.getTechnologyType()) && v1_dictionary_md_seed_technology_type_pb.TechnologyType.toObject(includeInstance, f),
    maturityGroup: (f = msg.getMaturityGroup()) && v1_dictionary_md_seed_maturity_group_pb.MaturityGroup.toObject(includeInstance, f),
    fruitAverageWeight: (f = msg.getFruitAverageWeight()) && v1_dictionary_md_seed_fruit_average_weight_pb.FruitAverageWeight.toObject(includeInstance, f),
    seedType: (f = msg.getSeedType()) && v1_dictionary_md_seed_seed_type_pb.SeedType.toObject(includeInstance, f),
    pollinationType: (f = msg.getPollinationType()) && v1_dictionary_md_seed_pollination_type_pb.PollinationType.toObject(includeInstance, f),
    adaptType: (f = msg.getAdaptType()) && v1_dictionary_md_seed_adapt_type_pb.AdaptType.toObject(includeInstance, f),
    originCountry: (f = msg.getOriginCountry()) && v1_dictionary_md_seed_origin_country_pb.OriginCountry.toObject(includeInstance, f),
    plantType: (f = msg.getPlantType()) && v1_dictionary_md_seed_plant_type_pb.PlantType.toObject(includeInstance, f),
    sortType: (f = msg.getSortType()) && v1_dictionary_md_seed_sort_type_pb.SortType.toObject(includeInstance, f),
    fruitTechColor: (f = msg.getFruitTechColor()) && v1_dictionary_md_color_pb.Color.toObject(includeInstance, f),
    fruitBioColor: (f = msg.getFruitBioColor()) && v1_dictionary_md_color_pb.Color.toObject(includeInstance, f),
    pulpColor: (f = msg.getPulpColor()) && v1_dictionary_md_color_pb.Color.toObject(includeInstance, f),
    purposesList: jspb.Message.toObjectList(msg.getPurposesList(),
    v1_dictionary_md_seed_purpose_pb.Purpose.toObject, includeInstance),
    growTypesList: jspb.Message.toObjectList(msg.getGrowTypesList(),
    v1_dictionary_md_seed_grow_type_pb.GrowType.toObject, includeInstance),
    growSeasonsList: jspb.Message.toObjectList(msg.getGrowSeasonsList(),
    v1_dictionary_md_seed_grow_season_pb.GrowSeason.toObject, includeInstance),
    treatmentChemicalsList: jspb.Message.toObjectList(msg.getTreatmentChemicalsList(),
    proto.fcp.dictionary.v1.dictionary_md.Product.toObject, includeInstance)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds;
  return proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 36:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setId(value);
      break;
    case 1:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setOrganic(value);
      break;
    case 3:
      var value = /** @type {string} */ (reader.readString());
      msg.setSowingRate(value);
      break;
    case 4:
      var value = /** @type {string} */ (reader.readString());
      msg.setSowingDensity(value);
      break;
    case 5:
      var value = /** @type {string} */ (reader.readString());
      msg.setSowingDistance(value);
      break;
    case 6:
      var value = /** @type {string} */ (reader.readString());
      msg.setSowingTerm(value);
      break;
    case 7:
      var value = /** @type {string} */ (reader.readString());
      msg.setHarvestTerm(value);
      break;
    case 8:
      var value = /** @type {string} */ (reader.readString());
      msg.setOneThousandSeedsWeight(value);
      break;
    case 9:
      var value = /** @type {string} */ (reader.readString());
      msg.setGermination(value);
      break;
    case 10:
      var value = /** @type {string} */ (reader.readString());
      msg.setGerminationEnergy(value);
      break;
    case 11:
      var value = /** @type {string} */ (reader.readString());
      msg.setMaturityPeriod(value);
      break;
    case 12:
      var value = /** @type {string} */ (reader.readString());
      msg.setPlantHeight(value);
      break;
    case 13:
      var value = /** @type {string} */ (reader.readString());
      msg.setFruitWeight(value);
      break;
    case 14:
      var value = /** @type {string} */ (reader.readString());
      msg.setWallThickness(value);
      break;
    case 16:
      var value = /** @type {string} */ (reader.readString());
      msg.setFruitTaste(value);
      break;
    case 17:
      var value = /** @type {string} */ (reader.readString());
      msg.setSugarConcentration(value);
      break;
    case 18:
      var value = /** @type {string} */ (reader.readString());
      msg.setDryMaterialConcentration(value);
      break;
    case 19:
      var value = /** @type {string} */ (reader.readString());
      msg.setFrostresist(value);
      break;
    case 20:
      var value = /** @type {boolean} */ (reader.readBool());
      msg.setPollinator(value);
      break;
    case 21:
      var value = /** @type {number} */ (reader.readInt64());
      msg.setHarvestYear(value);
      break;
    case 22:
      var value = /** @type {string} */ (reader.readString());
      msg.setApplicationRecommendations(value);
      break;
    case 23:
      var value = /** @type {string} */ (reader.readString());
      msg.setFlowerType(value);
      break;
    case 15:
      var value = /** @type {string} */ (reader.readString());
      msg.setFlowerDiameter(value);
      break;
    case 24:
      var value = /** @type {string} */ (reader.readString());
      msg.setSeedForm(value);
      break;
    case 25:
      var value = new v1_dictionary_md_seed_fruit_form_pb.FruitForm;
      reader.readMessage(value,v1_dictionary_md_seed_fruit_form_pb.FruitForm.deserializeBinaryFromReader);
      msg.setFruitForm(value);
      break;
    case 26:
      var value = new v1_dictionary_md_seed_reproduction_pb.Reproduction;
      reader.readMessage(value,v1_dictionary_md_seed_reproduction_pb.Reproduction.deserializeBinaryFromReader);
      msg.setReproduction(value);
      break;
    case 27:
      var value = new v1_dictionary_md_seed_species_pb.Species;
      reader.readMessage(value,v1_dictionary_md_seed_species_pb.Species.deserializeBinaryFromReader);
      msg.setSpecies(value);
      break;
    case 28:
      var value = new v1_dictionary_md_seed_technology_type_pb.TechnologyType;
      reader.readMessage(value,v1_dictionary_md_seed_technology_type_pb.TechnologyType.deserializeBinaryFromReader);
      msg.setTechnologyType(value);
      break;
    case 29:
      var value = new v1_dictionary_md_seed_maturity_group_pb.MaturityGroup;
      reader.readMessage(value,v1_dictionary_md_seed_maturity_group_pb.MaturityGroup.deserializeBinaryFromReader);
      msg.setMaturityGroup(value);
      break;
    case 30:
      var value = new v1_dictionary_md_seed_fruit_average_weight_pb.FruitAverageWeight;
      reader.readMessage(value,v1_dictionary_md_seed_fruit_average_weight_pb.FruitAverageWeight.deserializeBinaryFromReader);
      msg.setFruitAverageWeight(value);
      break;
    case 31:
      var value = new v1_dictionary_md_seed_seed_type_pb.SeedType;
      reader.readMessage(value,v1_dictionary_md_seed_seed_type_pb.SeedType.deserializeBinaryFromReader);
      msg.setSeedType(value);
      break;
    case 32:
      var value = new v1_dictionary_md_seed_pollination_type_pb.PollinationType;
      reader.readMessage(value,v1_dictionary_md_seed_pollination_type_pb.PollinationType.deserializeBinaryFromReader);
      msg.setPollinationType(value);
      break;
    case 33:
      var value = new v1_dictionary_md_seed_adapt_type_pb.AdaptType;
      reader.readMessage(value,v1_dictionary_md_seed_adapt_type_pb.AdaptType.deserializeBinaryFromReader);
      msg.setAdaptType(value);
      break;
    case 34:
      var value = new v1_dictionary_md_seed_origin_country_pb.OriginCountry;
      reader.readMessage(value,v1_dictionary_md_seed_origin_country_pb.OriginCountry.deserializeBinaryFromReader);
      msg.setOriginCountry(value);
      break;
    case 35:
      var value = new v1_dictionary_md_seed_plant_type_pb.PlantType;
      reader.readMessage(value,v1_dictionary_md_seed_plant_type_pb.PlantType.deserializeBinaryFromReader);
      msg.setPlantType(value);
      break;
    case 37:
      var value = new v1_dictionary_md_seed_sort_type_pb.SortType;
      reader.readMessage(value,v1_dictionary_md_seed_sort_type_pb.SortType.deserializeBinaryFromReader);
      msg.setSortType(value);
      break;
    case 48:
      var value = new v1_dictionary_md_color_pb.Color;
      reader.readMessage(value,v1_dictionary_md_color_pb.Color.deserializeBinaryFromReader);
      msg.setFruitTechColor(value);
      break;
    case 49:
      var value = new v1_dictionary_md_color_pb.Color;
      reader.readMessage(value,v1_dictionary_md_color_pb.Color.deserializeBinaryFromReader);
      msg.setFruitBioColor(value);
      break;
    case 50:
      var value = new v1_dictionary_md_color_pb.Color;
      reader.readMessage(value,v1_dictionary_md_color_pb.Color.deserializeBinaryFromReader);
      msg.setPulpColor(value);
      break;
    case 51:
      var value = new v1_dictionary_md_seed_purpose_pb.Purpose;
      reader.readMessage(value,v1_dictionary_md_seed_purpose_pb.Purpose.deserializeBinaryFromReader);
      msg.addPurposes(value);
      break;
    case 52:
      var value = new v1_dictionary_md_seed_grow_type_pb.GrowType;
      reader.readMessage(value,v1_dictionary_md_seed_grow_type_pb.GrowType.deserializeBinaryFromReader);
      msg.addGrowTypes(value);
      break;
    case 54:
      var value = new v1_dictionary_md_seed_grow_season_pb.GrowSeason;
      reader.readMessage(value,v1_dictionary_md_seed_grow_season_pb.GrowSeason.deserializeBinaryFromReader);
      msg.addGrowSeasons(value);
      break;
    case 55:
      var value = new proto.fcp.dictionary.v1.dictionary_md.Product;
      reader.readMessage(value,proto.fcp.dictionary.v1.dictionary_md.Product.deserializeBinaryFromReader);
      msg.addTreatmentChemicals(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f !== 0) {
    writer.writeInt64(
      36,
      f
    );
  }
  f = message.getOrganic();
  if (f) {
    writer.writeBool(
      1,
      f
    );
  }
  f = message.getSowingRate();
  if (f.length > 0) {
    writer.writeString(
      3,
      f
    );
  }
  f = message.getSowingDensity();
  if (f.length > 0) {
    writer.writeString(
      4,
      f
    );
  }
  f = message.getSowingDistance();
  if (f.length > 0) {
    writer.writeString(
      5,
      f
    );
  }
  f = message.getSowingTerm();
  if (f.length > 0) {
    writer.writeString(
      6,
      f
    );
  }
  f = message.getHarvestTerm();
  if (f.length > 0) {
    writer.writeString(
      7,
      f
    );
  }
  f = message.getOneThousandSeedsWeight();
  if (f.length > 0) {
    writer.writeString(
      8,
      f
    );
  }
  f = message.getGermination();
  if (f.length > 0) {
    writer.writeString(
      9,
      f
    );
  }
  f = message.getGerminationEnergy();
  if (f.length > 0) {
    writer.writeString(
      10,
      f
    );
  }
  f = message.getMaturityPeriod();
  if (f.length > 0) {
    writer.writeString(
      11,
      f
    );
  }
  f = message.getPlantHeight();
  if (f.length > 0) {
    writer.writeString(
      12,
      f
    );
  }
  f = message.getFruitWeight();
  if (f.length > 0) {
    writer.writeString(
      13,
      f
    );
  }
  f = message.getWallThickness();
  if (f.length > 0) {
    writer.writeString(
      14,
      f
    );
  }
  f = message.getFruitTaste();
  if (f.length > 0) {
    writer.writeString(
      16,
      f
    );
  }
  f = message.getSugarConcentration();
  if (f.length > 0) {
    writer.writeString(
      17,
      f
    );
  }
  f = message.getDryMaterialConcentration();
  if (f.length > 0) {
    writer.writeString(
      18,
      f
    );
  }
  f = message.getFrostresist();
  if (f.length > 0) {
    writer.writeString(
      19,
      f
    );
  }
  f = message.getPollinator();
  if (f) {
    writer.writeBool(
      20,
      f
    );
  }
  f = message.getHarvestYear();
  if (f !== 0) {
    writer.writeInt64(
      21,
      f
    );
  }
  f = message.getApplicationRecommendations();
  if (f.length > 0) {
    writer.writeString(
      22,
      f
    );
  }
  f = message.getFlowerType();
  if (f.length > 0) {
    writer.writeString(
      23,
      f
    );
  }
  f = message.getFlowerDiameter();
  if (f.length > 0) {
    writer.writeString(
      15,
      f
    );
  }
  f = message.getSeedForm();
  if (f.length > 0) {
    writer.writeString(
      24,
      f
    );
  }
  f = message.getFruitForm();
  if (f != null) {
    writer.writeMessage(
      25,
      f,
      v1_dictionary_md_seed_fruit_form_pb.FruitForm.serializeBinaryToWriter
    );
  }
  f = message.getReproduction();
  if (f != null) {
    writer.writeMessage(
      26,
      f,
      v1_dictionary_md_seed_reproduction_pb.Reproduction.serializeBinaryToWriter
    );
  }
  f = message.getSpecies();
  if (f != null) {
    writer.writeMessage(
      27,
      f,
      v1_dictionary_md_seed_species_pb.Species.serializeBinaryToWriter
    );
  }
  f = message.getTechnologyType();
  if (f != null) {
    writer.writeMessage(
      28,
      f,
      v1_dictionary_md_seed_technology_type_pb.TechnologyType.serializeBinaryToWriter
    );
  }
  f = message.getMaturityGroup();
  if (f != null) {
    writer.writeMessage(
      29,
      f,
      v1_dictionary_md_seed_maturity_group_pb.MaturityGroup.serializeBinaryToWriter
    );
  }
  f = message.getFruitAverageWeight();
  if (f != null) {
    writer.writeMessage(
      30,
      f,
      v1_dictionary_md_seed_fruit_average_weight_pb.FruitAverageWeight.serializeBinaryToWriter
    );
  }
  f = message.getSeedType();
  if (f != null) {
    writer.writeMessage(
      31,
      f,
      v1_dictionary_md_seed_seed_type_pb.SeedType.serializeBinaryToWriter
    );
  }
  f = message.getPollinationType();
  if (f != null) {
    writer.writeMessage(
      32,
      f,
      v1_dictionary_md_seed_pollination_type_pb.PollinationType.serializeBinaryToWriter
    );
  }
  f = message.getAdaptType();
  if (f != null) {
    writer.writeMessage(
      33,
      f,
      v1_dictionary_md_seed_adapt_type_pb.AdaptType.serializeBinaryToWriter
    );
  }
  f = message.getOriginCountry();
  if (f != null) {
    writer.writeMessage(
      34,
      f,
      v1_dictionary_md_seed_origin_country_pb.OriginCountry.serializeBinaryToWriter
    );
  }
  f = message.getPlantType();
  if (f != null) {
    writer.writeMessage(
      35,
      f,
      v1_dictionary_md_seed_plant_type_pb.PlantType.serializeBinaryToWriter
    );
  }
  f = message.getSortType();
  if (f != null) {
    writer.writeMessage(
      37,
      f,
      v1_dictionary_md_seed_sort_type_pb.SortType.serializeBinaryToWriter
    );
  }
  f = message.getFruitTechColor();
  if (f != null) {
    writer.writeMessage(
      48,
      f,
      v1_dictionary_md_color_pb.Color.serializeBinaryToWriter
    );
  }
  f = message.getFruitBioColor();
  if (f != null) {
    writer.writeMessage(
      49,
      f,
      v1_dictionary_md_color_pb.Color.serializeBinaryToWriter
    );
  }
  f = message.getPulpColor();
  if (f != null) {
    writer.writeMessage(
      50,
      f,
      v1_dictionary_md_color_pb.Color.serializeBinaryToWriter
    );
  }
  f = message.getPurposesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      51,
      f,
      v1_dictionary_md_seed_purpose_pb.Purpose.serializeBinaryToWriter
    );
  }
  f = message.getGrowTypesList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      52,
      f,
      v1_dictionary_md_seed_grow_type_pb.GrowType.serializeBinaryToWriter
    );
  }
  f = message.getGrowSeasonsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      54,
      f,
      v1_dictionary_md_seed_grow_season_pb.GrowSeason.serializeBinaryToWriter
    );
  }
  f = message.getTreatmentChemicalsList();
  if (f.length > 0) {
    writer.writeRepeatedMessage(
      55,
      f,
      proto.fcp.dictionary.v1.dictionary_md.Product.serializeBinaryToWriter
    );
  }
};


/**
 * optional int64 id = 36;
 * @return {number}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getId = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 36, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setId = function(value) {
  return jspb.Message.setProto3IntField(this, 36, value);
};


/**
 * optional bool organic = 1;
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getOrganic = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 1, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setOrganic = function(value) {
  return jspb.Message.setProto3BooleanField(this, 1, value);
};


/**
 * optional string sowing_rate = 3;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getSowingRate = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 3, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setSowingRate = function(value) {
  return jspb.Message.setProto3StringField(this, 3, value);
};


/**
 * optional string sowing_density = 4;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getSowingDensity = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 4, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setSowingDensity = function(value) {
  return jspb.Message.setProto3StringField(this, 4, value);
};


/**
 * optional string sowing_distance = 5;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getSowingDistance = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 5, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setSowingDistance = function(value) {
  return jspb.Message.setProto3StringField(this, 5, value);
};


/**
 * optional string sowing_term = 6;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getSowingTerm = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 6, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setSowingTerm = function(value) {
  return jspb.Message.setProto3StringField(this, 6, value);
};


/**
 * optional string harvest_term = 7;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getHarvestTerm = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 7, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setHarvestTerm = function(value) {
  return jspb.Message.setProto3StringField(this, 7, value);
};


/**
 * optional string one_thousand_seeds_weight = 8;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getOneThousandSeedsWeight = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 8, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setOneThousandSeedsWeight = function(value) {
  return jspb.Message.setProto3StringField(this, 8, value);
};


/**
 * optional string germination = 9;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getGermination = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 9, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setGermination = function(value) {
  return jspb.Message.setProto3StringField(this, 9, value);
};


/**
 * optional string germination_energy = 10;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getGerminationEnergy = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 10, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setGerminationEnergy = function(value) {
  return jspb.Message.setProto3StringField(this, 10, value);
};


/**
 * optional string maturity_period = 11;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getMaturityPeriod = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 11, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setMaturityPeriod = function(value) {
  return jspb.Message.setProto3StringField(this, 11, value);
};


/**
 * optional string plant_height = 12;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getPlantHeight = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 12, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setPlantHeight = function(value) {
  return jspb.Message.setProto3StringField(this, 12, value);
};


/**
 * optional string fruit_weight = 13;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getFruitWeight = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 13, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setFruitWeight = function(value) {
  return jspb.Message.setProto3StringField(this, 13, value);
};


/**
 * optional string wall_thickness = 14;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getWallThickness = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 14, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setWallThickness = function(value) {
  return jspb.Message.setProto3StringField(this, 14, value);
};


/**
 * optional string fruit_taste = 16;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getFruitTaste = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 16, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setFruitTaste = function(value) {
  return jspb.Message.setProto3StringField(this, 16, value);
};


/**
 * optional string sugar_concentration = 17;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getSugarConcentration = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 17, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setSugarConcentration = function(value) {
  return jspb.Message.setProto3StringField(this, 17, value);
};


/**
 * optional string dry_material_concentration = 18;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getDryMaterialConcentration = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 18, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setDryMaterialConcentration = function(value) {
  return jspb.Message.setProto3StringField(this, 18, value);
};


/**
 * optional string frostResist = 19;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getFrostresist = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 19, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setFrostresist = function(value) {
  return jspb.Message.setProto3StringField(this, 19, value);
};


/**
 * optional bool pollinator = 20;
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getPollinator = function() {
  return /** @type {boolean} */ (jspb.Message.getBooleanFieldWithDefault(this, 20, false));
};


/**
 * @param {boolean} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setPollinator = function(value) {
  return jspb.Message.setProto3BooleanField(this, 20, value);
};


/**
 * optional int64 harvest_year = 21;
 * @return {number}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getHarvestYear = function() {
  return /** @type {number} */ (jspb.Message.getFieldWithDefault(this, 21, 0));
};


/**
 * @param {number} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setHarvestYear = function(value) {
  return jspb.Message.setProto3IntField(this, 21, value);
};


/**
 * optional string application_recommendations = 22;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getApplicationRecommendations = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 22, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setApplicationRecommendations = function(value) {
  return jspb.Message.setProto3StringField(this, 22, value);
};


/**
 * optional string flower_type = 23;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getFlowerType = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 23, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setFlowerType = function(value) {
  return jspb.Message.setProto3StringField(this, 23, value);
};


/**
 * optional string flower_diameter = 15;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getFlowerDiameter = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 15, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setFlowerDiameter = function(value) {
  return jspb.Message.setProto3StringField(this, 15, value);
};


/**
 * optional string seed_form = 24;
 * @return {string}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getSeedForm = function() {
  return /** @type {string} */ (jspb.Message.getFieldWithDefault(this, 24, ""));
};


/**
 * @param {string} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setSeedForm = function(value) {
  return jspb.Message.setProto3StringField(this, 24, value);
};


/**
 * optional FruitForm fruit_form = 25;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.FruitForm}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getFruitForm = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.FruitForm} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_fruit_form_pb.FruitForm, 25));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.FruitForm|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setFruitForm = function(value) {
  return jspb.Message.setWrapperField(this, 25, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearFruitForm = function() {
  return this.setFruitForm(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasFruitForm = function() {
  return jspb.Message.getField(this, 25) != null;
};


/**
 * optional Reproduction reproduction = 26;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.Reproduction}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getReproduction = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.Reproduction} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_reproduction_pb.Reproduction, 26));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.Reproduction|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setReproduction = function(value) {
  return jspb.Message.setWrapperField(this, 26, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearReproduction = function() {
  return this.setReproduction(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasReproduction = function() {
  return jspb.Message.getField(this, 26) != null;
};


/**
 * optional Species species = 27;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.Species}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getSpecies = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.Species} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_species_pb.Species, 27));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.Species|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setSpecies = function(value) {
  return jspb.Message.setWrapperField(this, 27, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearSpecies = function() {
  return this.setSpecies(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasSpecies = function() {
  return jspb.Message.getField(this, 27) != null;
};


/**
 * optional TechnologyType technology_type = 28;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.TechnologyType}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getTechnologyType = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.TechnologyType} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_technology_type_pb.TechnologyType, 28));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.TechnologyType|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setTechnologyType = function(value) {
  return jspb.Message.setWrapperField(this, 28, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearTechnologyType = function() {
  return this.setTechnologyType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasTechnologyType = function() {
  return jspb.Message.getField(this, 28) != null;
};


/**
 * optional MaturityGroup maturity_group = 29;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.MaturityGroup}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getMaturityGroup = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.MaturityGroup} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_maturity_group_pb.MaturityGroup, 29));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.MaturityGroup|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setMaturityGroup = function(value) {
  return jspb.Message.setWrapperField(this, 29, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearMaturityGroup = function() {
  return this.setMaturityGroup(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasMaturityGroup = function() {
  return jspb.Message.getField(this, 29) != null;
};


/**
 * optional FruitAverageWeight fruit_average_weight = 30;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.FruitAverageWeight}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getFruitAverageWeight = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.FruitAverageWeight} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_fruit_average_weight_pb.FruitAverageWeight, 30));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.FruitAverageWeight|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setFruitAverageWeight = function(value) {
  return jspb.Message.setWrapperField(this, 30, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearFruitAverageWeight = function() {
  return this.setFruitAverageWeight(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasFruitAverageWeight = function() {
  return jspb.Message.getField(this, 30) != null;
};


/**
 * optional SeedType seed_type = 31;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.SeedType}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getSeedType = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.SeedType} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_seed_type_pb.SeedType, 31));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.SeedType|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setSeedType = function(value) {
  return jspb.Message.setWrapperField(this, 31, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearSeedType = function() {
  return this.setSeedType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasSeedType = function() {
  return jspb.Message.getField(this, 31) != null;
};


/**
 * optional PollinationType pollination_type = 32;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.PollinationType}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getPollinationType = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.PollinationType} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_pollination_type_pb.PollinationType, 32));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.PollinationType|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setPollinationType = function(value) {
  return jspb.Message.setWrapperField(this, 32, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearPollinationType = function() {
  return this.setPollinationType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasPollinationType = function() {
  return jspb.Message.getField(this, 32) != null;
};


/**
 * optional AdaptType adapt_type = 33;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.AdaptType}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getAdaptType = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.AdaptType} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_adapt_type_pb.AdaptType, 33));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.AdaptType|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setAdaptType = function(value) {
  return jspb.Message.setWrapperField(this, 33, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearAdaptType = function() {
  return this.setAdaptType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasAdaptType = function() {
  return jspb.Message.getField(this, 33) != null;
};


/**
 * optional OriginCountry origin_country = 34;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.OriginCountry}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getOriginCountry = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.OriginCountry} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_origin_country_pb.OriginCountry, 34));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.OriginCountry|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setOriginCountry = function(value) {
  return jspb.Message.setWrapperField(this, 34, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearOriginCountry = function() {
  return this.setOriginCountry(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasOriginCountry = function() {
  return jspb.Message.getField(this, 34) != null;
};


/**
 * optional PlantType plant_type = 35;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.PlantType}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getPlantType = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.PlantType} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_plant_type_pb.PlantType, 35));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.PlantType|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setPlantType = function(value) {
  return jspb.Message.setWrapperField(this, 35, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearPlantType = function() {
  return this.setPlantType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasPlantType = function() {
  return jspb.Message.getField(this, 35) != null;
};


/**
 * optional SortType sort_type = 37;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.SortType}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getSortType = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.SortType} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_seed_sort_type_pb.SortType, 37));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.SortType|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setSortType = function(value) {
  return jspb.Message.setWrapperField(this, 37, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearSortType = function() {
  return this.setSortType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasSortType = function() {
  return jspb.Message.getField(this, 37) != null;
};


/**
 * optional Color fruit_tech_color = 48;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.Color}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getFruitTechColor = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.Color} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_color_pb.Color, 48));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.Color|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setFruitTechColor = function(value) {
  return jspb.Message.setWrapperField(this, 48, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearFruitTechColor = function() {
  return this.setFruitTechColor(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasFruitTechColor = function() {
  return jspb.Message.getField(this, 48) != null;
};


/**
 * optional Color fruit_bio_color = 49;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.Color}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getFruitBioColor = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.Color} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_color_pb.Color, 49));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.Color|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setFruitBioColor = function(value) {
  return jspb.Message.setWrapperField(this, 49, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearFruitBioColor = function() {
  return this.setFruitBioColor(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasFruitBioColor = function() {
  return jspb.Message.getField(this, 49) != null;
};


/**
 * optional Color pulp_color = 50;
 * @return {?proto.fcp.dictionary.v1.dictionary_md.Color}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getPulpColor = function() {
  return /** @type{?proto.fcp.dictionary.v1.dictionary_md.Color} */ (
    jspb.Message.getWrapperField(this, v1_dictionary_md_color_pb.Color, 50));
};


/**
 * @param {?proto.fcp.dictionary.v1.dictionary_md.Color|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setPulpColor = function(value) {
  return jspb.Message.setWrapperField(this, 50, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearPulpColor = function() {
  return this.setPulpColor(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.hasPulpColor = function() {
  return jspb.Message.getField(this, 50) != null;
};


/**
 * repeated Purpose purposes = 51;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.Purpose>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getPurposesList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.Purpose>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_md_seed_purpose_pb.Purpose, 51));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.Purpose>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setPurposesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 51, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.Purpose=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Purpose}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.addPurposes = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 51, opt_value, proto.fcp.dictionary.v1.dictionary_md.Purpose, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearPurposesList = function() {
  return this.setPurposesList([]);
};


/**
 * repeated GrowType grow_types = 52;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.GrowType>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getGrowTypesList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.GrowType>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_md_seed_grow_type_pb.GrowType, 52));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.GrowType>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setGrowTypesList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 52, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.GrowType=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.GrowType}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.addGrowTypes = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 52, opt_value, proto.fcp.dictionary.v1.dictionary_md.GrowType, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearGrowTypesList = function() {
  return this.setGrowTypesList([]);
};


/**
 * repeated GrowSeason grow_seasons = 54;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.GrowSeason>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getGrowSeasonsList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.GrowSeason>} */ (
    jspb.Message.getRepeatedWrapperField(this, v1_dictionary_md_seed_grow_season_pb.GrowSeason, 54));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.GrowSeason>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setGrowSeasonsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 54, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.GrowSeason=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.GrowSeason}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.addGrowSeasons = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 54, opt_value, proto.fcp.dictionary.v1.dictionary_md.GrowSeason, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearGrowSeasonsList = function() {
  return this.setGrowSeasonsList([]);
};


/**
 * repeated Product treatment_chemicals = 55;
 * @return {!Array<!proto.fcp.dictionary.v1.dictionary_md.Product>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.getTreatmentChemicalsList = function() {
  return /** @type{!Array<!proto.fcp.dictionary.v1.dictionary_md.Product>} */ (
    jspb.Message.getRepeatedWrapperField(this, proto.fcp.dictionary.v1.dictionary_md.Product, 55));
};


/**
 * @param {!Array<!proto.fcp.dictionary.v1.dictionary_md.Product>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.setTreatmentChemicalsList = function(value) {
  return jspb.Message.setRepeatedWrapperField(this, 55, value);
};


/**
 * @param {!proto.fcp.dictionary.v1.dictionary_md.Product=} opt_value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.Product}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.addTreatmentChemicals = function(opt_value, opt_index) {
  return jspb.Message.addToRepeatedWrapperField(this, 55, opt_value, proto.fcp.dictionary.v1.dictionary_md.Product, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeeds.prototype.clearTreatmentChemicalsList = function() {
  return this.setTreatmentChemicalsList([]);
};



/**
 * List of repeated fields within this message type.
 * @private {!Array<number>}
 * @const
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.repeatedFields_ = [4,5,6,7,8,9,10,11,12,13,14,18,19,20,21,23,24,25];



if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.toObject = function(includeInstance, msg) {
  var f, obj = {
    flowerType: (f = msg.getFlowerType()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    isOrganic: (f = msg.getIsOrganic()) && google_protobuf_wrappers_pb.BoolValue.toObject(includeInstance, f),
    isPollinator: (f = msg.getIsPollinator()) && google_protobuf_wrappers_pb.BoolValue.toObject(includeInstance, f),
    harvestYearList: (f = jspb.Message.getRepeatedField(msg, 4)) == null ? undefined : f,
    speciesIdList: (f = jspb.Message.getRepeatedField(msg, 5)) == null ? undefined : f,
    maturityGroupIdList: (f = jspb.Message.getRepeatedField(msg, 6)) == null ? undefined : f,
    pollinationTypeIdList: (f = jspb.Message.getRepeatedField(msg, 7)) == null ? undefined : f,
    plantTypeIdList: (f = jspb.Message.getRepeatedField(msg, 8)) == null ? undefined : f,
    fruitFormIdList: (f = jspb.Message.getRepeatedField(msg, 9)) == null ? undefined : f,
    sortTypeIdList: (f = jspb.Message.getRepeatedField(msg, 10)) == null ? undefined : f,
    fruitAverageWeightIdList: (f = jspb.Message.getRepeatedField(msg, 11)) == null ? undefined : f,
    originCountryIdList: (f = jspb.Message.getRepeatedField(msg, 12)) == null ? undefined : f,
    treatmentChemicalsIdList: (f = jspb.Message.getRepeatedField(msg, 13)) == null ? undefined : f,
    technologyTypeList: (f = jspb.Message.getRepeatedField(msg, 14)) == null ? undefined : f,
    growTypesIdList: (f = jspb.Message.getRepeatedField(msg, 18)) == null ? undefined : f,
    growSeasonsIdList: (f = jspb.Message.getRepeatedField(msg, 19)) == null ? undefined : f,
    purposeIdList: (f = jspb.Message.getRepeatedField(msg, 20)) == null ? undefined : f,
    reproductionIdList: (f = jspb.Message.getRepeatedField(msg, 21)) == null ? undefined : f,
    fruitTechColorList: (f = jspb.Message.getRepeatedField(msg, 23)) == null ? undefined : f,
    fruitBioColorList: (f = jspb.Message.getRepeatedField(msg, 24)) == null ? undefined : f,
    pulpColorList: (f = jspb.Message.getRepeatedField(msg, 25)) == null ? undefined : f
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd;
  return proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 1:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setFlowerType(value);
      break;
    case 2:
      var value = new google_protobuf_wrappers_pb.BoolValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.BoolValue.deserializeBinaryFromReader);
      msg.setIsOrganic(value);
      break;
    case 3:
      var value = new google_protobuf_wrappers_pb.BoolValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.BoolValue.deserializeBinaryFromReader);
      msg.setIsPollinator(value);
      break;
    case 4:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setHarvestYearList(value);
      break;
    case 5:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setSpeciesIdList(value);
      break;
    case 6:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setMaturityGroupIdList(value);
      break;
    case 7:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setPollinationTypeIdList(value);
      break;
    case 8:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setPlantTypeIdList(value);
      break;
    case 9:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setFruitFormIdList(value);
      break;
    case 10:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setSortTypeIdList(value);
      break;
    case 11:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setFruitAverageWeightIdList(value);
      break;
    case 12:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setOriginCountryIdList(value);
      break;
    case 13:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setTreatmentChemicalsIdList(value);
      break;
    case 14:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setTechnologyTypeList(value);
      break;
    case 18:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setGrowTypesIdList(value);
      break;
    case 19:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setGrowSeasonsIdList(value);
      break;
    case 20:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setPurposeIdList(value);
      break;
    case 21:
      var value = /** @type {!Array<number>} */ (reader.readPackedInt64());
      msg.setReproductionIdList(value);
      break;
    case 23:
      var value = /** @type {string} */ (reader.readString());
      msg.addFruitTechColor(value);
      break;
    case 24:
      var value = /** @type {string} */ (reader.readString());
      msg.addFruitBioColor(value);
      break;
    case 25:
      var value = /** @type {string} */ (reader.readString());
      msg.addPulpColor(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getFlowerType();
  if (f != null) {
    writer.writeMessage(
      1,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getIsOrganic();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_wrappers_pb.BoolValue.serializeBinaryToWriter
    );
  }
  f = message.getIsPollinator();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      google_protobuf_wrappers_pb.BoolValue.serializeBinaryToWriter
    );
  }
  f = message.getHarvestYearList();
  if (f.length > 0) {
    writer.writePackedInt64(
      4,
      f
    );
  }
  f = message.getSpeciesIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      5,
      f
    );
  }
  f = message.getMaturityGroupIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      6,
      f
    );
  }
  f = message.getPollinationTypeIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      7,
      f
    );
  }
  f = message.getPlantTypeIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      8,
      f
    );
  }
  f = message.getFruitFormIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      9,
      f
    );
  }
  f = message.getSortTypeIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      10,
      f
    );
  }
  f = message.getFruitAverageWeightIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      11,
      f
    );
  }
  f = message.getOriginCountryIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      12,
      f
    );
  }
  f = message.getTreatmentChemicalsIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      13,
      f
    );
  }
  f = message.getTechnologyTypeList();
  if (f.length > 0) {
    writer.writePackedInt64(
      14,
      f
    );
  }
  f = message.getGrowTypesIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      18,
      f
    );
  }
  f = message.getGrowSeasonsIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      19,
      f
    );
  }
  f = message.getPurposeIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      20,
      f
    );
  }
  f = message.getReproductionIdList();
  if (f.length > 0) {
    writer.writePackedInt64(
      21,
      f
    );
  }
  f = message.getFruitTechColorList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      23,
      f
    );
  }
  f = message.getFruitBioColorList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      24,
      f
    );
  }
  f = message.getPulpColorList();
  if (f.length > 0) {
    writer.writeRepeatedString(
      25,
      f
    );
  }
};


/**
 * optional google.protobuf.StringValue flower_type = 1;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getFlowerType = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 1));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setFlowerType = function(value) {
  return jspb.Message.setWrapperField(this, 1, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearFlowerType = function() {
  return this.setFlowerType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.hasFlowerType = function() {
  return jspb.Message.getField(this, 1) != null;
};


/**
 * optional google.protobuf.BoolValue is_organic = 2;
 * @return {?proto.google.protobuf.BoolValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getIsOrganic = function() {
  return /** @type{?proto.google.protobuf.BoolValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.BoolValue, 2));
};


/**
 * @param {?proto.google.protobuf.BoolValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setIsOrganic = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearIsOrganic = function() {
  return this.setIsOrganic(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.hasIsOrganic = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional google.protobuf.BoolValue is_pollinator = 3;
 * @return {?proto.google.protobuf.BoolValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getIsPollinator = function() {
  return /** @type{?proto.google.protobuf.BoolValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.BoolValue, 3));
};


/**
 * @param {?proto.google.protobuf.BoolValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setIsPollinator = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearIsPollinator = function() {
  return this.setIsPollinator(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.hasIsPollinator = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * repeated int64 harvest_year = 4;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getHarvestYearList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 4));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setHarvestYearList = function(value) {
  return jspb.Message.setField(this, 4, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addHarvestYear = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 4, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearHarvestYearList = function() {
  return this.setHarvestYearList([]);
};


/**
 * repeated int64 species_id = 5;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getSpeciesIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 5));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setSpeciesIdList = function(value) {
  return jspb.Message.setField(this, 5, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addSpeciesId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 5, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearSpeciesIdList = function() {
  return this.setSpeciesIdList([]);
};


/**
 * repeated int64 maturity_group_id = 6;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getMaturityGroupIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 6));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setMaturityGroupIdList = function(value) {
  return jspb.Message.setField(this, 6, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addMaturityGroupId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 6, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearMaturityGroupIdList = function() {
  return this.setMaturityGroupIdList([]);
};


/**
 * repeated int64 pollination_type_id = 7;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getPollinationTypeIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 7));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setPollinationTypeIdList = function(value) {
  return jspb.Message.setField(this, 7, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addPollinationTypeId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 7, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearPollinationTypeIdList = function() {
  return this.setPollinationTypeIdList([]);
};


/**
 * repeated int64 plant_type_id = 8;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getPlantTypeIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 8));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setPlantTypeIdList = function(value) {
  return jspb.Message.setField(this, 8, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addPlantTypeId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 8, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearPlantTypeIdList = function() {
  return this.setPlantTypeIdList([]);
};


/**
 * repeated int64 fruit_form_id = 9;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getFruitFormIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 9));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setFruitFormIdList = function(value) {
  return jspb.Message.setField(this, 9, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addFruitFormId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 9, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearFruitFormIdList = function() {
  return this.setFruitFormIdList([]);
};


/**
 * repeated int64 sort_type_id = 10;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getSortTypeIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 10));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setSortTypeIdList = function(value) {
  return jspb.Message.setField(this, 10, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addSortTypeId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 10, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearSortTypeIdList = function() {
  return this.setSortTypeIdList([]);
};


/**
 * repeated int64 fruit_average_weight_id = 11;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getFruitAverageWeightIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 11));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setFruitAverageWeightIdList = function(value) {
  return jspb.Message.setField(this, 11, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addFruitAverageWeightId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 11, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearFruitAverageWeightIdList = function() {
  return this.setFruitAverageWeightIdList([]);
};


/**
 * repeated int64 origin_country_id = 12;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getOriginCountryIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 12));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setOriginCountryIdList = function(value) {
  return jspb.Message.setField(this, 12, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addOriginCountryId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 12, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearOriginCountryIdList = function() {
  return this.setOriginCountryIdList([]);
};


/**
 * repeated int64 treatment_chemicals_id = 13;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getTreatmentChemicalsIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 13));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setTreatmentChemicalsIdList = function(value) {
  return jspb.Message.setField(this, 13, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addTreatmentChemicalsId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 13, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearTreatmentChemicalsIdList = function() {
  return this.setTreatmentChemicalsIdList([]);
};


/**
 * repeated int64 technology_type = 14;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getTechnologyTypeList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 14));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setTechnologyTypeList = function(value) {
  return jspb.Message.setField(this, 14, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addTechnologyType = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 14, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearTechnologyTypeList = function() {
  return this.setTechnologyTypeList([]);
};


/**
 * repeated int64 grow_types_id = 18;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getGrowTypesIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 18));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setGrowTypesIdList = function(value) {
  return jspb.Message.setField(this, 18, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addGrowTypesId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 18, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearGrowTypesIdList = function() {
  return this.setGrowTypesIdList([]);
};


/**
 * repeated int64 grow_seasons_id = 19;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getGrowSeasonsIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 19));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setGrowSeasonsIdList = function(value) {
  return jspb.Message.setField(this, 19, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addGrowSeasonsId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 19, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearGrowSeasonsIdList = function() {
  return this.setGrowSeasonsIdList([]);
};


/**
 * repeated int64 purpose_id = 20;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getPurposeIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 20));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setPurposeIdList = function(value) {
  return jspb.Message.setField(this, 20, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addPurposeId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 20, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearPurposeIdList = function() {
  return this.setPurposeIdList([]);
};


/**
 * repeated int64 reproduction_id = 21;
 * @return {!Array<number>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getReproductionIdList = function() {
  return /** @type {!Array<number>} */ (jspb.Message.getRepeatedField(this, 21));
};


/**
 * @param {!Array<number>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setReproductionIdList = function(value) {
  return jspb.Message.setField(this, 21, value || []);
};


/**
 * @param {number} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addReproductionId = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 21, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearReproductionIdList = function() {
  return this.setReproductionIdList([]);
};


/**
 * repeated string fruit_tech_color = 23;
 * @return {!Array<string>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getFruitTechColorList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 23));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setFruitTechColorList = function(value) {
  return jspb.Message.setField(this, 23, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addFruitTechColor = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 23, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearFruitTechColorList = function() {
  return this.setFruitTechColorList([]);
};


/**
 * repeated string fruit_bio_color = 24;
 * @return {!Array<string>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getFruitBioColorList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 24));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setFruitBioColorList = function(value) {
  return jspb.Message.setField(this, 24, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addFruitBioColor = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 24, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearFruitBioColorList = function() {
  return this.setFruitBioColorList([]);
};


/**
 * repeated string pulp_color = 25;
 * @return {!Array<string>}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.getPulpColorList = function() {
  return /** @type {!Array<string>} */ (jspb.Message.getRepeatedField(this, 25));
};


/**
 * @param {!Array<string>} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.setPulpColorList = function(value) {
  return jspb.Message.setField(this, 25, value || []);
};


/**
 * @param {string} value
 * @param {number=} opt_index
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.addPulpColor = function(value, opt_index) {
  return jspb.Message.addToRepeatedField(this, 25, value, opt_index);
};


/**
 * Clears the list making it empty but non-null.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilterMd.prototype.clearPulpColorList = function() {
  return this.setPulpColorList([]);
};





if (jspb.Message.GENERATE_TO_OBJECT) {
/**
 * Creates an object representation of this proto.
 * Field names that are reserved in JavaScript and will be renamed to pb_name.
 * Optional fields that are not set will be set to undefined.
 * To access a reserved field use, foo.pb_<name>, eg, foo.pb_default.
 * For the list of reserved names please see:
 *     net/proto2/compiler/js/internal/generator.cc#kKeyword.
 * @param {boolean=} opt_includeInstance Deprecated. whether to include the
 *     JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @return {!Object}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.toObject = function(opt_includeInstance) {
  return proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.toObject(opt_includeInstance, this);
};


/**
 * Static version of the {@see toObject} method.
 * @param {boolean|undefined} includeInstance Deprecated. Whether to include
 *     the JSPB instance for transitional soy proto support:
 *     http://goto/soy-param-migration
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} msg The msg instance to transform.
 * @return {!Object}
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.toObject = function(includeInstance, msg) {
  var f, obj = {
    id: (f = msg.getId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    flowerType: (f = msg.getFlowerType()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    isOrganic: (f = msg.getIsOrganic()) && google_protobuf_wrappers_pb.BoolValue.toObject(includeInstance, f),
    isPollinator: (f = msg.getIsPollinator()) && google_protobuf_wrappers_pb.BoolValue.toObject(includeInstance, f),
    harvestYear: (f = msg.getHarvestYear()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    fruitFormId: (f = msg.getFruitFormId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    fruitFormName: (f = msg.getFruitFormName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    reproductionId: (f = msg.getReproductionId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    reproductionName: (f = msg.getReproductionName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    speciesId: (f = msg.getSpeciesId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    speciesName: (f = msg.getSpeciesName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    technologyTypeId: (f = msg.getTechnologyTypeId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    technologyTypeName: (f = msg.getTechnologyTypeName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    maturityGroupId: (f = msg.getMaturityGroupId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    maturityGroupName: (f = msg.getMaturityGroupName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    fruitAverageWeightId: (f = msg.getFruitAverageWeightId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    fruitAverageWeightName: (f = msg.getFruitAverageWeightName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    seedTypeId: (f = msg.getSeedTypeId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    seedTypeName: (f = msg.getSeedTypeName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    pollinationTypeId: (f = msg.getPollinationTypeId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    pollinationTypeName: (f = msg.getPollinationTypeName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    adaptTypeId: (f = msg.getAdaptTypeId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    adaptTypeName: (f = msg.getAdaptTypeName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    originCountryId: (f = msg.getOriginCountryId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    originCountryName: (f = msg.getOriginCountryName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    plantTypeId: (f = msg.getPlantTypeId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    plantTypeName: (f = msg.getPlantTypeName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    fruitTechColorId: (f = msg.getFruitTechColorId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    fruitTechColorName: (f = msg.getFruitTechColorName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    fruitBioColorId: (f = msg.getFruitBioColorId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    fruitBioColorName: (f = msg.getFruitBioColorName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    pulpColorId: (f = msg.getPulpColorId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    pulpColorName: (f = msg.getPulpColorName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    purposesId: (f = msg.getPurposesId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    purposesName: (f = msg.getPurposesName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    growTypesId: (f = msg.getGrowTypesId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    growTypesName: (f = msg.getGrowTypesName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    growSeasonsId: (f = msg.getGrowSeasonsId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    growSeasonsName: (f = msg.getGrowSeasonsName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    treatmentChemicalsId: (f = msg.getTreatmentChemicalsId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    treatmentChemicalsName: (f = msg.getTreatmentChemicalsName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f),
    sortTypeId: (f = msg.getSortTypeId()) && google_protobuf_wrappers_pb.Int64Value.toObject(includeInstance, f),
    sortTypeName: (f = msg.getSortTypeName()) && google_protobuf_wrappers_pb.StringValue.toObject(includeInstance, f)
  };

  if (includeInstance) {
    obj.$jspbMessageInstance = msg;
  }
  return obj;
};
}


/**
 * Deserializes binary data (in protobuf wire format).
 * @param {jspb.ByteSource} bytes The bytes to deserialize.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.deserializeBinary = function(bytes) {
  var reader = new jspb.BinaryReader(bytes);
  var msg = new proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter;
  return proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.deserializeBinaryFromReader(msg, reader);
};


/**
 * Deserializes binary data (in protobuf wire format) from the
 * given reader into the given message object.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} msg The message object to deserialize into.
 * @param {!jspb.BinaryReader} reader The BinaryReader to use.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.deserializeBinaryFromReader = function(msg, reader) {
  while (reader.nextField()) {
    if (reader.isEndGroup()) {
      break;
    }
    var field = reader.getFieldNumber();
    switch (field) {
    case 42:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setId(value);
      break;
    case 2:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setFlowerType(value);
      break;
    case 3:
      var value = new google_protobuf_wrappers_pb.BoolValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.BoolValue.deserializeBinaryFromReader);
      msg.setIsOrganic(value);
      break;
    case 4:
      var value = new google_protobuf_wrappers_pb.BoolValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.BoolValue.deserializeBinaryFromReader);
      msg.setIsPollinator(value);
      break;
    case 5:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setHarvestYear(value);
      break;
    case 6:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setFruitFormId(value);
      break;
    case 7:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setFruitFormName(value);
      break;
    case 8:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setReproductionId(value);
      break;
    case 9:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setReproductionName(value);
      break;
    case 10:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setSpeciesId(value);
      break;
    case 11:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setSpeciesName(value);
      break;
    case 12:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setTechnologyTypeId(value);
      break;
    case 13:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setTechnologyTypeName(value);
      break;
    case 14:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setMaturityGroupId(value);
      break;
    case 15:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setMaturityGroupName(value);
      break;
    case 16:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setFruitAverageWeightId(value);
      break;
    case 17:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setFruitAverageWeightName(value);
      break;
    case 18:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setSeedTypeId(value);
      break;
    case 19:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setSeedTypeName(value);
      break;
    case 20:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setPollinationTypeId(value);
      break;
    case 21:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setPollinationTypeName(value);
      break;
    case 22:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setAdaptTypeId(value);
      break;
    case 23:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setAdaptTypeName(value);
      break;
    case 24:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setOriginCountryId(value);
      break;
    case 25:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setOriginCountryName(value);
      break;
    case 26:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setPlantTypeId(value);
      break;
    case 27:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setPlantTypeName(value);
      break;
    case 28:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setFruitTechColorId(value);
      break;
    case 29:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setFruitTechColorName(value);
      break;
    case 30:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setFruitBioColorId(value);
      break;
    case 31:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setFruitBioColorName(value);
      break;
    case 32:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setPulpColorId(value);
      break;
    case 33:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setPulpColorName(value);
      break;
    case 34:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setPurposesId(value);
      break;
    case 35:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setPurposesName(value);
      break;
    case 36:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setGrowTypesId(value);
      break;
    case 37:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setGrowTypesName(value);
      break;
    case 38:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setGrowSeasonsId(value);
      break;
    case 39:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setGrowSeasonsName(value);
      break;
    case 40:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setTreatmentChemicalsId(value);
      break;
    case 41:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setTreatmentChemicalsName(value);
      break;
    case 43:
      var value = new google_protobuf_wrappers_pb.Int64Value;
      reader.readMessage(value,google_protobuf_wrappers_pb.Int64Value.deserializeBinaryFromReader);
      msg.setSortTypeId(value);
      break;
    case 44:
      var value = new google_protobuf_wrappers_pb.StringValue;
      reader.readMessage(value,google_protobuf_wrappers_pb.StringValue.deserializeBinaryFromReader);
      msg.setSortTypeName(value);
      break;
    default:
      reader.skipField();
      break;
    }
  }
  return msg;
};


/**
 * Serializes the message to binary data (in protobuf wire format).
 * @return {!Uint8Array}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.serializeBinary = function() {
  var writer = new jspb.BinaryWriter();
  proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.serializeBinaryToWriter(this, writer);
  return writer.getResultBuffer();
};


/**
 * Serializes the given message to binary data (in protobuf wire
 * format), writing to the given BinaryWriter.
 * @param {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} message
 * @param {!jspb.BinaryWriter} writer
 * @suppress {unusedLocalVariables} f is only used for nested messages
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.serializeBinaryToWriter = function(message, writer) {
  var f = undefined;
  f = message.getId();
  if (f != null) {
    writer.writeMessage(
      42,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getFlowerType();
  if (f != null) {
    writer.writeMessage(
      2,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getIsOrganic();
  if (f != null) {
    writer.writeMessage(
      3,
      f,
      google_protobuf_wrappers_pb.BoolValue.serializeBinaryToWriter
    );
  }
  f = message.getIsPollinator();
  if (f != null) {
    writer.writeMessage(
      4,
      f,
      google_protobuf_wrappers_pb.BoolValue.serializeBinaryToWriter
    );
  }
  f = message.getHarvestYear();
  if (f != null) {
    writer.writeMessage(
      5,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getFruitFormId();
  if (f != null) {
    writer.writeMessage(
      6,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getFruitFormName();
  if (f != null) {
    writer.writeMessage(
      7,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getReproductionId();
  if (f != null) {
    writer.writeMessage(
      8,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getReproductionName();
  if (f != null) {
    writer.writeMessage(
      9,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getSpeciesId();
  if (f != null) {
    writer.writeMessage(
      10,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getSpeciesName();
  if (f != null) {
    writer.writeMessage(
      11,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getTechnologyTypeId();
  if (f != null) {
    writer.writeMessage(
      12,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getTechnologyTypeName();
  if (f != null) {
    writer.writeMessage(
      13,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getMaturityGroupId();
  if (f != null) {
    writer.writeMessage(
      14,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getMaturityGroupName();
  if (f != null) {
    writer.writeMessage(
      15,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getFruitAverageWeightId();
  if (f != null) {
    writer.writeMessage(
      16,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getFruitAverageWeightName();
  if (f != null) {
    writer.writeMessage(
      17,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getSeedTypeId();
  if (f != null) {
    writer.writeMessage(
      18,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getSeedTypeName();
  if (f != null) {
    writer.writeMessage(
      19,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getPollinationTypeId();
  if (f != null) {
    writer.writeMessage(
      20,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getPollinationTypeName();
  if (f != null) {
    writer.writeMessage(
      21,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getAdaptTypeId();
  if (f != null) {
    writer.writeMessage(
      22,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getAdaptTypeName();
  if (f != null) {
    writer.writeMessage(
      23,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getOriginCountryId();
  if (f != null) {
    writer.writeMessage(
      24,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getOriginCountryName();
  if (f != null) {
    writer.writeMessage(
      25,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getPlantTypeId();
  if (f != null) {
    writer.writeMessage(
      26,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getPlantTypeName();
  if (f != null) {
    writer.writeMessage(
      27,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getFruitTechColorId();
  if (f != null) {
    writer.writeMessage(
      28,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getFruitTechColorName();
  if (f != null) {
    writer.writeMessage(
      29,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getFruitBioColorId();
  if (f != null) {
    writer.writeMessage(
      30,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getFruitBioColorName();
  if (f != null) {
    writer.writeMessage(
      31,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getPulpColorId();
  if (f != null) {
    writer.writeMessage(
      32,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getPulpColorName();
  if (f != null) {
    writer.writeMessage(
      33,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getPurposesId();
  if (f != null) {
    writer.writeMessage(
      34,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getPurposesName();
  if (f != null) {
    writer.writeMessage(
      35,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getGrowTypesId();
  if (f != null) {
    writer.writeMessage(
      36,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getGrowTypesName();
  if (f != null) {
    writer.writeMessage(
      37,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getGrowSeasonsId();
  if (f != null) {
    writer.writeMessage(
      38,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getGrowSeasonsName();
  if (f != null) {
    writer.writeMessage(
      39,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getTreatmentChemicalsId();
  if (f != null) {
    writer.writeMessage(
      40,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getTreatmentChemicalsName();
  if (f != null) {
    writer.writeMessage(
      41,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
  f = message.getSortTypeId();
  if (f != null) {
    writer.writeMessage(
      43,
      f,
      google_protobuf_wrappers_pb.Int64Value.serializeBinaryToWriter
    );
  }
  f = message.getSortTypeName();
  if (f != null) {
    writer.writeMessage(
      44,
      f,
      google_protobuf_wrappers_pb.StringValue.serializeBinaryToWriter
    );
  }
};


/**
 * optional google.protobuf.Int64Value id = 42;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 42));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setId = function(value) {
  return jspb.Message.setWrapperField(this, 42, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearId = function() {
  return this.setId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasId = function() {
  return jspb.Message.getField(this, 42) != null;
};


/**
 * optional google.protobuf.StringValue flower_type = 2;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getFlowerType = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 2));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setFlowerType = function(value) {
  return jspb.Message.setWrapperField(this, 2, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearFlowerType = function() {
  return this.setFlowerType(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasFlowerType = function() {
  return jspb.Message.getField(this, 2) != null;
};


/**
 * optional google.protobuf.BoolValue is_organic = 3;
 * @return {?proto.google.protobuf.BoolValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getIsOrganic = function() {
  return /** @type{?proto.google.protobuf.BoolValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.BoolValue, 3));
};


/**
 * @param {?proto.google.protobuf.BoolValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setIsOrganic = function(value) {
  return jspb.Message.setWrapperField(this, 3, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearIsOrganic = function() {
  return this.setIsOrganic(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasIsOrganic = function() {
  return jspb.Message.getField(this, 3) != null;
};


/**
 * optional google.protobuf.BoolValue is_pollinator = 4;
 * @return {?proto.google.protobuf.BoolValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getIsPollinator = function() {
  return /** @type{?proto.google.protobuf.BoolValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.BoolValue, 4));
};


/**
 * @param {?proto.google.protobuf.BoolValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setIsPollinator = function(value) {
  return jspb.Message.setWrapperField(this, 4, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearIsPollinator = function() {
  return this.setIsPollinator(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasIsPollinator = function() {
  return jspb.Message.getField(this, 4) != null;
};


/**
 * optional google.protobuf.Int64Value harvest_year = 5;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getHarvestYear = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 5));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setHarvestYear = function(value) {
  return jspb.Message.setWrapperField(this, 5, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearHarvestYear = function() {
  return this.setHarvestYear(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasHarvestYear = function() {
  return jspb.Message.getField(this, 5) != null;
};


/**
 * optional google.protobuf.Int64Value fruit_form_id = 6;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getFruitFormId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 6));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setFruitFormId = function(value) {
  return jspb.Message.setWrapperField(this, 6, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearFruitFormId = function() {
  return this.setFruitFormId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasFruitFormId = function() {
  return jspb.Message.getField(this, 6) != null;
};


/**
 * optional google.protobuf.StringValue fruit_form_name = 7;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getFruitFormName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 7));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setFruitFormName = function(value) {
  return jspb.Message.setWrapperField(this, 7, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearFruitFormName = function() {
  return this.setFruitFormName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasFruitFormName = function() {
  return jspb.Message.getField(this, 7) != null;
};


/**
 * optional google.protobuf.Int64Value reproduction_id = 8;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getReproductionId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 8));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setReproductionId = function(value) {
  return jspb.Message.setWrapperField(this, 8, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearReproductionId = function() {
  return this.setReproductionId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasReproductionId = function() {
  return jspb.Message.getField(this, 8) != null;
};


/**
 * optional google.protobuf.StringValue reproduction_name = 9;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getReproductionName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 9));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setReproductionName = function(value) {
  return jspb.Message.setWrapperField(this, 9, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearReproductionName = function() {
  return this.setReproductionName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasReproductionName = function() {
  return jspb.Message.getField(this, 9) != null;
};


/**
 * optional google.protobuf.Int64Value species_id = 10;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getSpeciesId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 10));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setSpeciesId = function(value) {
  return jspb.Message.setWrapperField(this, 10, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearSpeciesId = function() {
  return this.setSpeciesId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasSpeciesId = function() {
  return jspb.Message.getField(this, 10) != null;
};


/**
 * optional google.protobuf.StringValue species_name = 11;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getSpeciesName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 11));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setSpeciesName = function(value) {
  return jspb.Message.setWrapperField(this, 11, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearSpeciesName = function() {
  return this.setSpeciesName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasSpeciesName = function() {
  return jspb.Message.getField(this, 11) != null;
};


/**
 * optional google.protobuf.Int64Value technology_type_id = 12;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getTechnologyTypeId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 12));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setTechnologyTypeId = function(value) {
  return jspb.Message.setWrapperField(this, 12, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearTechnologyTypeId = function() {
  return this.setTechnologyTypeId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasTechnologyTypeId = function() {
  return jspb.Message.getField(this, 12) != null;
};


/**
 * optional google.protobuf.StringValue technology_type_name = 13;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getTechnologyTypeName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 13));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setTechnologyTypeName = function(value) {
  return jspb.Message.setWrapperField(this, 13, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearTechnologyTypeName = function() {
  return this.setTechnologyTypeName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasTechnologyTypeName = function() {
  return jspb.Message.getField(this, 13) != null;
};


/**
 * optional google.protobuf.Int64Value maturity_group_id = 14;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getMaturityGroupId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 14));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setMaturityGroupId = function(value) {
  return jspb.Message.setWrapperField(this, 14, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearMaturityGroupId = function() {
  return this.setMaturityGroupId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasMaturityGroupId = function() {
  return jspb.Message.getField(this, 14) != null;
};


/**
 * optional google.protobuf.StringValue maturity_group_name = 15;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getMaturityGroupName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 15));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setMaturityGroupName = function(value) {
  return jspb.Message.setWrapperField(this, 15, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearMaturityGroupName = function() {
  return this.setMaturityGroupName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasMaturityGroupName = function() {
  return jspb.Message.getField(this, 15) != null;
};


/**
 * optional google.protobuf.Int64Value fruit_average_weight_id = 16;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getFruitAverageWeightId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 16));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setFruitAverageWeightId = function(value) {
  return jspb.Message.setWrapperField(this, 16, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearFruitAverageWeightId = function() {
  return this.setFruitAverageWeightId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasFruitAverageWeightId = function() {
  return jspb.Message.getField(this, 16) != null;
};


/**
 * optional google.protobuf.StringValue fruit_average_weight_name = 17;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getFruitAverageWeightName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 17));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setFruitAverageWeightName = function(value) {
  return jspb.Message.setWrapperField(this, 17, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearFruitAverageWeightName = function() {
  return this.setFruitAverageWeightName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasFruitAverageWeightName = function() {
  return jspb.Message.getField(this, 17) != null;
};


/**
 * optional google.protobuf.Int64Value seed_type_id = 18;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getSeedTypeId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 18));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setSeedTypeId = function(value) {
  return jspb.Message.setWrapperField(this, 18, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearSeedTypeId = function() {
  return this.setSeedTypeId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasSeedTypeId = function() {
  return jspb.Message.getField(this, 18) != null;
};


/**
 * optional google.protobuf.StringValue seed_type_name = 19;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getSeedTypeName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 19));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setSeedTypeName = function(value) {
  return jspb.Message.setWrapperField(this, 19, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearSeedTypeName = function() {
  return this.setSeedTypeName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasSeedTypeName = function() {
  return jspb.Message.getField(this, 19) != null;
};


/**
 * optional google.protobuf.Int64Value pollination_type_id = 20;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getPollinationTypeId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 20));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setPollinationTypeId = function(value) {
  return jspb.Message.setWrapperField(this, 20, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearPollinationTypeId = function() {
  return this.setPollinationTypeId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasPollinationTypeId = function() {
  return jspb.Message.getField(this, 20) != null;
};


/**
 * optional google.protobuf.StringValue pollination_type_name = 21;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getPollinationTypeName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 21));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setPollinationTypeName = function(value) {
  return jspb.Message.setWrapperField(this, 21, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearPollinationTypeName = function() {
  return this.setPollinationTypeName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasPollinationTypeName = function() {
  return jspb.Message.getField(this, 21) != null;
};


/**
 * optional google.protobuf.Int64Value adapt_type_id = 22;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getAdaptTypeId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 22));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setAdaptTypeId = function(value) {
  return jspb.Message.setWrapperField(this, 22, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearAdaptTypeId = function() {
  return this.setAdaptTypeId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasAdaptTypeId = function() {
  return jspb.Message.getField(this, 22) != null;
};


/**
 * optional google.protobuf.StringValue adapt_type_name = 23;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getAdaptTypeName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 23));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setAdaptTypeName = function(value) {
  return jspb.Message.setWrapperField(this, 23, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearAdaptTypeName = function() {
  return this.setAdaptTypeName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasAdaptTypeName = function() {
  return jspb.Message.getField(this, 23) != null;
};


/**
 * optional google.protobuf.Int64Value origin_country_id = 24;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getOriginCountryId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 24));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setOriginCountryId = function(value) {
  return jspb.Message.setWrapperField(this, 24, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearOriginCountryId = function() {
  return this.setOriginCountryId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasOriginCountryId = function() {
  return jspb.Message.getField(this, 24) != null;
};


/**
 * optional google.protobuf.StringValue origin_country_name = 25;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getOriginCountryName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 25));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setOriginCountryName = function(value) {
  return jspb.Message.setWrapperField(this, 25, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearOriginCountryName = function() {
  return this.setOriginCountryName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasOriginCountryName = function() {
  return jspb.Message.getField(this, 25) != null;
};


/**
 * optional google.protobuf.Int64Value plant_type_id = 26;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getPlantTypeId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 26));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setPlantTypeId = function(value) {
  return jspb.Message.setWrapperField(this, 26, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearPlantTypeId = function() {
  return this.setPlantTypeId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasPlantTypeId = function() {
  return jspb.Message.getField(this, 26) != null;
};


/**
 * optional google.protobuf.StringValue plant_type_name = 27;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getPlantTypeName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 27));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setPlantTypeName = function(value) {
  return jspb.Message.setWrapperField(this, 27, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearPlantTypeName = function() {
  return this.setPlantTypeName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasPlantTypeName = function() {
  return jspb.Message.getField(this, 27) != null;
};


/**
 * optional google.protobuf.Int64Value fruit_tech_color_id = 28;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getFruitTechColorId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 28));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setFruitTechColorId = function(value) {
  return jspb.Message.setWrapperField(this, 28, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearFruitTechColorId = function() {
  return this.setFruitTechColorId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasFruitTechColorId = function() {
  return jspb.Message.getField(this, 28) != null;
};


/**
 * optional google.protobuf.StringValue fruit_tech_color_name = 29;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getFruitTechColorName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 29));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setFruitTechColorName = function(value) {
  return jspb.Message.setWrapperField(this, 29, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearFruitTechColorName = function() {
  return this.setFruitTechColorName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasFruitTechColorName = function() {
  return jspb.Message.getField(this, 29) != null;
};


/**
 * optional google.protobuf.Int64Value fruit_bio_color_id = 30;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getFruitBioColorId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 30));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setFruitBioColorId = function(value) {
  return jspb.Message.setWrapperField(this, 30, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearFruitBioColorId = function() {
  return this.setFruitBioColorId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasFruitBioColorId = function() {
  return jspb.Message.getField(this, 30) != null;
};


/**
 * optional google.protobuf.StringValue fruit_bio_color_name = 31;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getFruitBioColorName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 31));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setFruitBioColorName = function(value) {
  return jspb.Message.setWrapperField(this, 31, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearFruitBioColorName = function() {
  return this.setFruitBioColorName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasFruitBioColorName = function() {
  return jspb.Message.getField(this, 31) != null;
};


/**
 * optional google.protobuf.Int64Value pulp_color_id = 32;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getPulpColorId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 32));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setPulpColorId = function(value) {
  return jspb.Message.setWrapperField(this, 32, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearPulpColorId = function() {
  return this.setPulpColorId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasPulpColorId = function() {
  return jspb.Message.getField(this, 32) != null;
};


/**
 * optional google.protobuf.StringValue pulp_color_name = 33;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getPulpColorName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 33));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setPulpColorName = function(value) {
  return jspb.Message.setWrapperField(this, 33, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearPulpColorName = function() {
  return this.setPulpColorName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasPulpColorName = function() {
  return jspb.Message.getField(this, 33) != null;
};


/**
 * optional google.protobuf.Int64Value purposes_id = 34;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getPurposesId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 34));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setPurposesId = function(value) {
  return jspb.Message.setWrapperField(this, 34, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearPurposesId = function() {
  return this.setPurposesId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasPurposesId = function() {
  return jspb.Message.getField(this, 34) != null;
};


/**
 * optional google.protobuf.StringValue purposes_name = 35;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getPurposesName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 35));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setPurposesName = function(value) {
  return jspb.Message.setWrapperField(this, 35, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearPurposesName = function() {
  return this.setPurposesName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasPurposesName = function() {
  return jspb.Message.getField(this, 35) != null;
};


/**
 * optional google.protobuf.Int64Value grow_types_id = 36;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getGrowTypesId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 36));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setGrowTypesId = function(value) {
  return jspb.Message.setWrapperField(this, 36, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearGrowTypesId = function() {
  return this.setGrowTypesId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasGrowTypesId = function() {
  return jspb.Message.getField(this, 36) != null;
};


/**
 * optional google.protobuf.StringValue grow_types_name = 37;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getGrowTypesName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 37));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setGrowTypesName = function(value) {
  return jspb.Message.setWrapperField(this, 37, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearGrowTypesName = function() {
  return this.setGrowTypesName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasGrowTypesName = function() {
  return jspb.Message.getField(this, 37) != null;
};


/**
 * optional google.protobuf.Int64Value grow_seasons_id = 38;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getGrowSeasonsId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 38));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setGrowSeasonsId = function(value) {
  return jspb.Message.setWrapperField(this, 38, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearGrowSeasonsId = function() {
  return this.setGrowSeasonsId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasGrowSeasonsId = function() {
  return jspb.Message.getField(this, 38) != null;
};


/**
 * optional google.protobuf.StringValue grow_seasons_name = 39;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getGrowSeasonsName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 39));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setGrowSeasonsName = function(value) {
  return jspb.Message.setWrapperField(this, 39, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearGrowSeasonsName = function() {
  return this.setGrowSeasonsName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasGrowSeasonsName = function() {
  return jspb.Message.getField(this, 39) != null;
};


/**
 * optional google.protobuf.Int64Value treatment_chemicals_id = 40;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getTreatmentChemicalsId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 40));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setTreatmentChemicalsId = function(value) {
  return jspb.Message.setWrapperField(this, 40, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearTreatmentChemicalsId = function() {
  return this.setTreatmentChemicalsId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasTreatmentChemicalsId = function() {
  return jspb.Message.getField(this, 40) != null;
};


/**
 * optional google.protobuf.StringValue treatment_chemicals_name = 41;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getTreatmentChemicalsName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 41));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setTreatmentChemicalsName = function(value) {
  return jspb.Message.setWrapperField(this, 41, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearTreatmentChemicalsName = function() {
  return this.setTreatmentChemicalsName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasTreatmentChemicalsName = function() {
  return jspb.Message.getField(this, 41) != null;
};


/**
 * optional google.protobuf.Int64Value sort_type_id = 43;
 * @return {?proto.google.protobuf.Int64Value}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getSortTypeId = function() {
  return /** @type{?proto.google.protobuf.Int64Value} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.Int64Value, 43));
};


/**
 * @param {?proto.google.protobuf.Int64Value|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setSortTypeId = function(value) {
  return jspb.Message.setWrapperField(this, 43, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearSortTypeId = function() {
  return this.setSortTypeId(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasSortTypeId = function() {
  return jspb.Message.getField(this, 43) != null;
};


/**
 * optional google.protobuf.StringValue sort_type_name = 44;
 * @return {?proto.google.protobuf.StringValue}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.getSortTypeName = function() {
  return /** @type{?proto.google.protobuf.StringValue} */ (
    jspb.Message.getWrapperField(this, google_protobuf_wrappers_pb.StringValue, 44));
};


/**
 * @param {?proto.google.protobuf.StringValue|undefined} value
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
*/
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.setSortTypeName = function(value) {
  return jspb.Message.setWrapperField(this, 44, value);
};


/**
 * Clears the message field making it undefined.
 * @return {!proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter} returns this
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.clearSortTypeName = function() {
  return this.setSortTypeName(undefined);
};


/**
 * Returns whether this field is set.
 * @return {boolean}
 */
proto.fcp.dictionary.v1.dictionary_md.ProductConditionSeedsFilter.prototype.hasSortTypeName = function() {
  return jspb.Message.getField(this, 44) != null;
};


goog.object.extend(exports, proto.fcp.dictionary.v1.dictionary_md);
