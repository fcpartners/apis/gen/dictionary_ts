// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/application_rate_unit_chemical.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ApplicationRateUnitChemical extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getFullName(): string;
  setFullName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ApplicationRateUnitChemical.AsObject;
  static toObject(includeInstance: boolean, msg: ApplicationRateUnitChemical): ApplicationRateUnitChemical.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ApplicationRateUnitChemical, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ApplicationRateUnitChemical;
  static deserializeBinaryFromReader(message: ApplicationRateUnitChemical, reader: jspb.BinaryReader): ApplicationRateUnitChemical;
}

export namespace ApplicationRateUnitChemical {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    fullName: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ApplicationRateUnitChemicalFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasFullName(): boolean;
  clearFullName(): void;
  getFullName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFullName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ApplicationRateUnitChemicalFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ApplicationRateUnitChemicalFilter): ApplicationRateUnitChemicalFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ApplicationRateUnitChemicalFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ApplicationRateUnitChemicalFilter;
  static deserializeBinaryFromReader(message: ApplicationRateUnitChemicalFilter, reader: jspb.BinaryReader): ApplicationRateUnitChemicalFilter;
}

export namespace ApplicationRateUnitChemicalFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    fullName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetApplicationRateUnitChemicalRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetApplicationRateUnitChemicalRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetApplicationRateUnitChemicalRequest): GetApplicationRateUnitChemicalRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetApplicationRateUnitChemicalRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetApplicationRateUnitChemicalRequest;
  static deserializeBinaryFromReader(message: GetApplicationRateUnitChemicalRequest, reader: jspb.BinaryReader): GetApplicationRateUnitChemicalRequest;
}

export namespace GetApplicationRateUnitChemicalRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetApplicationRateUnitChemicalResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ApplicationRateUnitChemical | undefined;
  setItem(value?: ApplicationRateUnitChemical): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetApplicationRateUnitChemicalResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetApplicationRateUnitChemicalResponse): GetApplicationRateUnitChemicalResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetApplicationRateUnitChemicalResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetApplicationRateUnitChemicalResponse;
  static deserializeBinaryFromReader(message: GetApplicationRateUnitChemicalResponse, reader: jspb.BinaryReader): GetApplicationRateUnitChemicalResponse;
}

export namespace GetApplicationRateUnitChemicalResponse {
  export type AsObject = {
    item?: ApplicationRateUnitChemical.AsObject,
  }
}

export class ListApplicationRateUnitChemicalRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ApplicationRateUnitChemicalFilter | undefined;
  setFilter(value?: ApplicationRateUnitChemicalFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListApplicationRateUnitChemicalRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListApplicationRateUnitChemicalRequest): ListApplicationRateUnitChemicalRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListApplicationRateUnitChemicalRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListApplicationRateUnitChemicalRequest;
  static deserializeBinaryFromReader(message: ListApplicationRateUnitChemicalRequest, reader: jspb.BinaryReader): ListApplicationRateUnitChemicalRequest;
}

export namespace ListApplicationRateUnitChemicalRequest {
  export type AsObject = {
    filter?: ApplicationRateUnitChemicalFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListApplicationRateUnitChemicalResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ApplicationRateUnitChemical>;
  setItemsList(value: Array<ApplicationRateUnitChemical>): void;
  addItems(value?: ApplicationRateUnitChemical, index?: number): ApplicationRateUnitChemical;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListApplicationRateUnitChemicalResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListApplicationRateUnitChemicalResponse): ListApplicationRateUnitChemicalResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListApplicationRateUnitChemicalResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListApplicationRateUnitChemicalResponse;
  static deserializeBinaryFromReader(message: ListApplicationRateUnitChemicalResponse, reader: jspb.BinaryReader): ListApplicationRateUnitChemicalResponse;
}

export namespace ListApplicationRateUnitChemicalResponse {
  export type AsObject = {
    itemsList: Array<ApplicationRateUnitChemical.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveApplicationRateUnitChemicalRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ApplicationRateUnitChemical | undefined;
  setItem(value?: ApplicationRateUnitChemical): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveApplicationRateUnitChemicalRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveApplicationRateUnitChemicalRequest): SaveApplicationRateUnitChemicalRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveApplicationRateUnitChemicalRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveApplicationRateUnitChemicalRequest;
  static deserializeBinaryFromReader(message: SaveApplicationRateUnitChemicalRequest, reader: jspb.BinaryReader): SaveApplicationRateUnitChemicalRequest;
}

export namespace SaveApplicationRateUnitChemicalRequest {
  export type AsObject = {
    item?: ApplicationRateUnitChemical.AsObject,
  }
}

export class SaveApplicationRateUnitChemicalResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveApplicationRateUnitChemicalResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveApplicationRateUnitChemicalResponse): SaveApplicationRateUnitChemicalResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveApplicationRateUnitChemicalResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveApplicationRateUnitChemicalResponse;
  static deserializeBinaryFromReader(message: SaveApplicationRateUnitChemicalResponse, reader: jspb.BinaryReader): SaveApplicationRateUnitChemicalResponse;
}

export namespace SaveApplicationRateUnitChemicalResponse {
  export type AsObject = {
    id: number,
  }
}

