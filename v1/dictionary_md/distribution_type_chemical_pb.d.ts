// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/distribution_type_chemical.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class DistributionTypeChemical extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getFullName(): string;
  setFullName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DistributionTypeChemical.AsObject;
  static toObject(includeInstance: boolean, msg: DistributionTypeChemical): DistributionTypeChemical.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DistributionTypeChemical, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DistributionTypeChemical;
  static deserializeBinaryFromReader(message: DistributionTypeChemical, reader: jspb.BinaryReader): DistributionTypeChemical;
}

export namespace DistributionTypeChemical {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    fullName: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class DistributionTypeChemicalFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasFullName(): boolean;
  clearFullName(): void;
  getFullName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setFullName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DistributionTypeChemicalFilter.AsObject;
  static toObject(includeInstance: boolean, msg: DistributionTypeChemicalFilter): DistributionTypeChemicalFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DistributionTypeChemicalFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DistributionTypeChemicalFilter;
  static deserializeBinaryFromReader(message: DistributionTypeChemicalFilter, reader: jspb.BinaryReader): DistributionTypeChemicalFilter;
}

export namespace DistributionTypeChemicalFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    fullName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetDistributionTypeChemicalRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDistributionTypeChemicalRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDistributionTypeChemicalRequest): GetDistributionTypeChemicalRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDistributionTypeChemicalRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDistributionTypeChemicalRequest;
  static deserializeBinaryFromReader(message: GetDistributionTypeChemicalRequest, reader: jspb.BinaryReader): GetDistributionTypeChemicalRequest;
}

export namespace GetDistributionTypeChemicalRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetDistributionTypeChemicalResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): DistributionTypeChemical | undefined;
  setItem(value?: DistributionTypeChemical): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDistributionTypeChemicalResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetDistributionTypeChemicalResponse): GetDistributionTypeChemicalResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDistributionTypeChemicalResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDistributionTypeChemicalResponse;
  static deserializeBinaryFromReader(message: GetDistributionTypeChemicalResponse, reader: jspb.BinaryReader): GetDistributionTypeChemicalResponse;
}

export namespace GetDistributionTypeChemicalResponse {
  export type AsObject = {
    item?: DistributionTypeChemical.AsObject,
  }
}

export class ListDistributionTypeChemicalRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): DistributionTypeChemicalFilter | undefined;
  setFilter(value?: DistributionTypeChemicalFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDistributionTypeChemicalRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDistributionTypeChemicalRequest): ListDistributionTypeChemicalRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDistributionTypeChemicalRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDistributionTypeChemicalRequest;
  static deserializeBinaryFromReader(message: ListDistributionTypeChemicalRequest, reader: jspb.BinaryReader): ListDistributionTypeChemicalRequest;
}

export namespace ListDistributionTypeChemicalRequest {
  export type AsObject = {
    filter?: DistributionTypeChemicalFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListDistributionTypeChemicalResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<DistributionTypeChemical>;
  setItemsList(value: Array<DistributionTypeChemical>): void;
  addItems(value?: DistributionTypeChemical, index?: number): DistributionTypeChemical;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDistributionTypeChemicalResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListDistributionTypeChemicalResponse): ListDistributionTypeChemicalResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDistributionTypeChemicalResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDistributionTypeChemicalResponse;
  static deserializeBinaryFromReader(message: ListDistributionTypeChemicalResponse, reader: jspb.BinaryReader): ListDistributionTypeChemicalResponse;
}

export namespace ListDistributionTypeChemicalResponse {
  export type AsObject = {
    itemsList: Array<DistributionTypeChemical.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveDistributionTypeChemicalRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): DistributionTypeChemical | undefined;
  setItem(value?: DistributionTypeChemical): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveDistributionTypeChemicalRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveDistributionTypeChemicalRequest): SaveDistributionTypeChemicalRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveDistributionTypeChemicalRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveDistributionTypeChemicalRequest;
  static deserializeBinaryFromReader(message: SaveDistributionTypeChemicalRequest, reader: jspb.BinaryReader): SaveDistributionTypeChemicalRequest;
}

export namespace SaveDistributionTypeChemicalRequest {
  export type AsObject = {
    item?: DistributionTypeChemical.AsObject,
  }
}

export class SaveDistributionTypeChemicalResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveDistributionTypeChemicalResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveDistributionTypeChemicalResponse): SaveDistributionTypeChemicalResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveDistributionTypeChemicalResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveDistributionTypeChemicalResponse;
  static deserializeBinaryFromReader(message: SaveDistributionTypeChemicalResponse, reader: jspb.BinaryReader): SaveDistributionTypeChemicalResponse;
}

export namespace SaveDistributionTypeChemicalResponse {
  export type AsObject = {
    id: number,
  }
}

