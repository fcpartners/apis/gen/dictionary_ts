// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/product_condition.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_md_color_pb from "../../v1/dictionary_md/color_pb";
import * as v1_dictionary_md_chemical_class_group_pb from "../../v1/dictionary_md/chemical_class_group_pb";
import * as v1_dictionary_md_distribution_type_chemical_pb from "../../v1/dictionary_md/distribution_type_chemical_pb";
import * as v1_dictionary_md_preparative_form_pb from "../../v1/dictionary_md/preparative_form_pb";
import * as v1_dictionary_md_product_active_substance_pb from "../../v1/dictionary_md/product_active_substance_pb";

export class ProductCondition extends jspb.Message {
  getGuarantyPeriod(): string;
  setGuarantyPeriod(value: string): void;

  getStorageTemperature(): string;
  setStorageTemperature(value: string): void;

  getGeneralInfo(): string;
  setGeneralInfo(value: string): void;

  getProductAdvantage(): string;
  setProductAdvantage(value: string): void;

  getFeatures(): string;
  setFeatures(value: string): void;

  getMechanismAction(): string;
  setMechanismAction(value: string): void;

  hasColor(): boolean;
  clearColor(): void;
  getColor(): v1_dictionary_md_color_pb.Color | undefined;
  setColor(value?: v1_dictionary_md_color_pb.Color): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductCondition.AsObject;
  static toObject(includeInstance: boolean, msg: ProductCondition): ProductCondition.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductCondition, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductCondition;
  static deserializeBinaryFromReader(message: ProductCondition, reader: jspb.BinaryReader): ProductCondition;
}

export namespace ProductCondition {
  export type AsObject = {
    guarantyPeriod: string,
    storageTemperature: string,
    generalInfo: string,
    productAdvantage: string,
    features: string,
    mechanismAction: string,
    color?: v1_dictionary_md_color_pb.Color.AsObject,
  }
}

export class ProductConditionChemicals extends jspb.Message {
  getToxicityClassId(): number;
  setToxicityClassId(value: number): void;

  getToxicityClassName(): string;
  setToxicityClassName(value: string): void;

  getWaitingPeriod(): string;
  setWaitingPeriod(value: string): void;

  clearProductActiveSubstancesList(): void;
  getProductActiveSubstancesList(): Array<v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance>;
  setProductActiveSubstancesList(value: Array<v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance>): void;
  addProductActiveSubstances(value?: v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance, index?: number): v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance;

  clearPreparativeFormsList(): void;
  getPreparativeFormsList(): Array<v1_dictionary_md_preparative_form_pb.PreparativeForm>;
  setPreparativeFormsList(value: Array<v1_dictionary_md_preparative_form_pb.PreparativeForm>): void;
  addPreparativeForms(value?: v1_dictionary_md_preparative_form_pb.PreparativeForm, index?: number): v1_dictionary_md_preparative_form_pb.PreparativeForm;

  clearChemicalClassGroupsList(): void;
  getChemicalClassGroupsList(): Array<v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup>;
  setChemicalClassGroupsList(value: Array<v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup>): void;
  addChemicalClassGroups(value?: v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup, index?: number): v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup;

  clearDistributionTypeChemicalsList(): void;
  getDistributionTypeChemicalsList(): Array<v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical>;
  setDistributionTypeChemicalsList(value: Array<v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical>): void;
  addDistributionTypeChemicals(value?: v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical, index?: number): v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductConditionChemicals.AsObject;
  static toObject(includeInstance: boolean, msg: ProductConditionChemicals): ProductConditionChemicals.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductConditionChemicals, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductConditionChemicals;
  static deserializeBinaryFromReader(message: ProductConditionChemicals, reader: jspb.BinaryReader): ProductConditionChemicals;
}

export namespace ProductConditionChemicals {
  export type AsObject = {
    toxicityClassId: number,
    toxicityClassName: string,
    waitingPeriod: string,
    productActiveSubstancesList: Array<v1_dictionary_md_product_active_substance_pb.ProductActiveSubstance.AsObject>,
    preparativeFormsList: Array<v1_dictionary_md_preparative_form_pb.PreparativeForm.AsObject>,
    chemicalClassGroupsList: Array<v1_dictionary_md_chemical_class_group_pb.ChemicalClassGroup.AsObject>,
    distributionTypeChemicalsList: Array<v1_dictionary_md_distribution_type_chemical_pb.DistributionTypeChemical.AsObject>,
  }
}

