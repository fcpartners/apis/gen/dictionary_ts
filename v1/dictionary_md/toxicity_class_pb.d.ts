// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/toxicity_class.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class ToxicityClass extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ToxicityClass.AsObject;
  static toObject(includeInstance: boolean, msg: ToxicityClass): ToxicityClass.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ToxicityClass, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ToxicityClass;
  static deserializeBinaryFromReader(message: ToxicityClass, reader: jspb.BinaryReader): ToxicityClass;
}

export namespace ToxicityClass {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ToxicityClassFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ToxicityClassFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ToxicityClassFilter): ToxicityClassFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ToxicityClassFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ToxicityClassFilter;
  static deserializeBinaryFromReader(message: ToxicityClassFilter, reader: jspb.BinaryReader): ToxicityClassFilter;
}

export namespace ToxicityClassFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetToxicityClassRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetToxicityClassRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetToxicityClassRequest): GetToxicityClassRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetToxicityClassRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetToxicityClassRequest;
  static deserializeBinaryFromReader(message: GetToxicityClassRequest, reader: jspb.BinaryReader): GetToxicityClassRequest;
}

export namespace GetToxicityClassRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetToxicityClassResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ToxicityClass | undefined;
  setItem(value?: ToxicityClass): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetToxicityClassResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetToxicityClassResponse): GetToxicityClassResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetToxicityClassResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetToxicityClassResponse;
  static deserializeBinaryFromReader(message: GetToxicityClassResponse, reader: jspb.BinaryReader): GetToxicityClassResponse;
}

export namespace GetToxicityClassResponse {
  export type AsObject = {
    item?: ToxicityClass.AsObject,
  }
}

export class ListToxicityClassRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ToxicityClassFilter | undefined;
  setFilter(value?: ToxicityClassFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListToxicityClassRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListToxicityClassRequest): ListToxicityClassRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListToxicityClassRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListToxicityClassRequest;
  static deserializeBinaryFromReader(message: ListToxicityClassRequest, reader: jspb.BinaryReader): ListToxicityClassRequest;
}

export namespace ListToxicityClassRequest {
  export type AsObject = {
    filter?: ToxicityClassFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListToxicityClassResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ToxicityClass>;
  setItemsList(value: Array<ToxicityClass>): void;
  addItems(value?: ToxicityClass, index?: number): ToxicityClass;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListToxicityClassResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListToxicityClassResponse): ListToxicityClassResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListToxicityClassResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListToxicityClassResponse;
  static deserializeBinaryFromReader(message: ListToxicityClassResponse, reader: jspb.BinaryReader): ListToxicityClassResponse;
}

export namespace ListToxicityClassResponse {
  export type AsObject = {
    itemsList: Array<ToxicityClass.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveToxicityClassRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ToxicityClass | undefined;
  setItem(value?: ToxicityClass): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveToxicityClassRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveToxicityClassRequest): SaveToxicityClassRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveToxicityClassRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveToxicityClassRequest;
  static deserializeBinaryFromReader(message: SaveToxicityClassRequest, reader: jspb.BinaryReader): SaveToxicityClassRequest;
}

export namespace SaveToxicityClassRequest {
  export type AsObject = {
    item?: ToxicityClass.AsObject,
  }
}

export class SaveToxicityClassResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveToxicityClassResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveToxicityClassResponse): SaveToxicityClassResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveToxicityClassResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveToxicityClassResponse;
  static deserializeBinaryFromReader(message: SaveToxicityClassResponse, reader: jspb.BinaryReader): SaveToxicityClassResponse;
}

export namespace SaveToxicityClassResponse {
  export type AsObject = {
    id: number,
  }
}

