// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/chemical_class_group.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";

export class ChemicalClassGroup extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getProductType(): v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap];
  setProductType(value: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]): void;

  getProductProcess(): v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap];
  setProductProcess(value: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap]): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ChemicalClassGroup.AsObject;
  static toObject(includeInstance: boolean, msg: ChemicalClassGroup): ChemicalClassGroup.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ChemicalClassGroup, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ChemicalClassGroup;
  static deserializeBinaryFromReader(message: ChemicalClassGroup, reader: jspb.BinaryReader): ChemicalClassGroup;
}

export namespace ChemicalClassGroup {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    productType: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap],
    productProcess: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap],
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ChemicalClassGroupFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasProductProcess(): boolean;
  clearProductProcess(): void;
  getProductProcess(): v1_dictionary_common_enum_product_pb.ProductProcessValue | undefined;
  setProductProcess(value?: v1_dictionary_common_enum_product_pb.ProductProcessValue): void;

  hasProductType(): boolean;
  clearProductType(): void;
  getProductType(): v1_dictionary_common_enum_product_pb.ProductTypeValue | undefined;
  setProductType(value?: v1_dictionary_common_enum_product_pb.ProductTypeValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ChemicalClassGroupFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ChemicalClassGroupFilter): ChemicalClassGroupFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ChemicalClassGroupFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ChemicalClassGroupFilter;
  static deserializeBinaryFromReader(message: ChemicalClassGroupFilter, reader: jspb.BinaryReader): ChemicalClassGroupFilter;
}

export namespace ChemicalClassGroupFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    productProcess?: v1_dictionary_common_enum_product_pb.ProductProcessValue.AsObject,
    productType?: v1_dictionary_common_enum_product_pb.ProductTypeValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetChemicalClassGroupRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetChemicalClassGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetChemicalClassGroupRequest): GetChemicalClassGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetChemicalClassGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetChemicalClassGroupRequest;
  static deserializeBinaryFromReader(message: GetChemicalClassGroupRequest, reader: jspb.BinaryReader): GetChemicalClassGroupRequest;
}

export namespace GetChemicalClassGroupRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetChemicalClassGroupResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ChemicalClassGroup | undefined;
  setItem(value?: ChemicalClassGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetChemicalClassGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetChemicalClassGroupResponse): GetChemicalClassGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetChemicalClassGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetChemicalClassGroupResponse;
  static deserializeBinaryFromReader(message: GetChemicalClassGroupResponse, reader: jspb.BinaryReader): GetChemicalClassGroupResponse;
}

export namespace GetChemicalClassGroupResponse {
  export type AsObject = {
    item?: ChemicalClassGroup.AsObject,
  }
}

export class ListChemicalClassGroupRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ChemicalClassGroupFilter | undefined;
  setFilter(value?: ChemicalClassGroupFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListChemicalClassGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListChemicalClassGroupRequest): ListChemicalClassGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListChemicalClassGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListChemicalClassGroupRequest;
  static deserializeBinaryFromReader(message: ListChemicalClassGroupRequest, reader: jspb.BinaryReader): ListChemicalClassGroupRequest;
}

export namespace ListChemicalClassGroupRequest {
  export type AsObject = {
    filter?: ChemicalClassGroupFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListChemicalClassGroupResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ChemicalClassGroup>;
  setItemsList(value: Array<ChemicalClassGroup>): void;
  addItems(value?: ChemicalClassGroup, index?: number): ChemicalClassGroup;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListChemicalClassGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListChemicalClassGroupResponse): ListChemicalClassGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListChemicalClassGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListChemicalClassGroupResponse;
  static deserializeBinaryFromReader(message: ListChemicalClassGroupResponse, reader: jspb.BinaryReader): ListChemicalClassGroupResponse;
}

export namespace ListChemicalClassGroupResponse {
  export type AsObject = {
    itemsList: Array<ChemicalClassGroup.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveChemicalClassGroupRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ChemicalClassGroup | undefined;
  setItem(value?: ChemicalClassGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveChemicalClassGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveChemicalClassGroupRequest): SaveChemicalClassGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveChemicalClassGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveChemicalClassGroupRequest;
  static deserializeBinaryFromReader(message: SaveChemicalClassGroupRequest, reader: jspb.BinaryReader): SaveChemicalClassGroupRequest;
}

export namespace SaveChemicalClassGroupRequest {
  export type AsObject = {
    item?: ChemicalClassGroup.AsObject,
  }
}

export class SaveChemicalClassGroupResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveChemicalClassGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveChemicalClassGroupResponse): SaveChemicalClassGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveChemicalClassGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveChemicalClassGroupResponse;
  static deserializeBinaryFromReader(message: SaveChemicalClassGroupResponse, reader: jspb.BinaryReader): SaveChemicalClassGroupResponse;
}

export namespace SaveChemicalClassGroupResponse {
  export type AsObject = {
    id: number,
  }
}

