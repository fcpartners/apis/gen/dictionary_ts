// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/active_substance.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_md_unit_pb from "../../v1/dictionary_md/unit_pb";

export class ActiveSubstance extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  clearUnitsList(): void;
  getUnitsList(): Array<v1_dictionary_md_unit_pb.Unit>;
  setUnitsList(value: Array<v1_dictionary_md_unit_pb.Unit>): void;
  addUnits(value?: v1_dictionary_md_unit_pb.Unit, index?: number): v1_dictionary_md_unit_pb.Unit;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActiveSubstance.AsObject;
  static toObject(includeInstance: boolean, msg: ActiveSubstance): ActiveSubstance.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActiveSubstance, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActiveSubstance;
  static deserializeBinaryFromReader(message: ActiveSubstance, reader: jspb.BinaryReader): ActiveSubstance;
}

export namespace ActiveSubstance {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    unitsList: Array<v1_dictionary_md_unit_pb.Unit.AsObject>,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ActiveSubstanceFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ActiveSubstanceFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ActiveSubstanceFilter): ActiveSubstanceFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ActiveSubstanceFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ActiveSubstanceFilter;
  static deserializeBinaryFromReader(message: ActiveSubstanceFilter, reader: jspb.BinaryReader): ActiveSubstanceFilter;
}

export namespace ActiveSubstanceFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetActiveSubstanceRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetActiveSubstanceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetActiveSubstanceRequest): GetActiveSubstanceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetActiveSubstanceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetActiveSubstanceRequest;
  static deserializeBinaryFromReader(message: GetActiveSubstanceRequest, reader: jspb.BinaryReader): GetActiveSubstanceRequest;
}

export namespace GetActiveSubstanceRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetActiveSubstanceResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ActiveSubstance | undefined;
  setItem(value?: ActiveSubstance): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetActiveSubstanceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetActiveSubstanceResponse): GetActiveSubstanceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetActiveSubstanceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetActiveSubstanceResponse;
  static deserializeBinaryFromReader(message: GetActiveSubstanceResponse, reader: jspb.BinaryReader): GetActiveSubstanceResponse;
}

export namespace GetActiveSubstanceResponse {
  export type AsObject = {
    item?: ActiveSubstance.AsObject,
  }
}

export class ListActiveSubstanceRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ActiveSubstanceFilter | undefined;
  setFilter(value?: ActiveSubstanceFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListActiveSubstanceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListActiveSubstanceRequest): ListActiveSubstanceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListActiveSubstanceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListActiveSubstanceRequest;
  static deserializeBinaryFromReader(message: ListActiveSubstanceRequest, reader: jspb.BinaryReader): ListActiveSubstanceRequest;
}

export namespace ListActiveSubstanceRequest {
  export type AsObject = {
    filter?: ActiveSubstanceFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListActiveSubstanceResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ActiveSubstance>;
  setItemsList(value: Array<ActiveSubstance>): void;
  addItems(value?: ActiveSubstance, index?: number): ActiveSubstance;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListActiveSubstanceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListActiveSubstanceResponse): ListActiveSubstanceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListActiveSubstanceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListActiveSubstanceResponse;
  static deserializeBinaryFromReader(message: ListActiveSubstanceResponse, reader: jspb.BinaryReader): ListActiveSubstanceResponse;
}

export namespace ListActiveSubstanceResponse {
  export type AsObject = {
    itemsList: Array<ActiveSubstance.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveActiveSubstanceRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ActiveSubstance | undefined;
  setItem(value?: ActiveSubstance): void;

  clearUnitsList(): void;
  getUnitsList(): Array<v1_dictionary_md_unit_pb.Unit>;
  setUnitsList(value: Array<v1_dictionary_md_unit_pb.Unit>): void;
  addUnits(value?: v1_dictionary_md_unit_pb.Unit, index?: number): v1_dictionary_md_unit_pb.Unit;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveActiveSubstanceRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveActiveSubstanceRequest): SaveActiveSubstanceRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveActiveSubstanceRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveActiveSubstanceRequest;
  static deserializeBinaryFromReader(message: SaveActiveSubstanceRequest, reader: jspb.BinaryReader): SaveActiveSubstanceRequest;
}

export namespace SaveActiveSubstanceRequest {
  export type AsObject = {
    item?: ActiveSubstance.AsObject,
    unitsList: Array<v1_dictionary_md_unit_pb.Unit.AsObject>,
  }
}

export class SaveActiveSubstanceResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveActiveSubstanceResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveActiveSubstanceResponse): SaveActiveSubstanceResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveActiveSubstanceResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveActiveSubstanceResponse;
  static deserializeBinaryFromReader(message: SaveActiveSubstanceResponse, reader: jspb.BinaryReader): SaveActiveSubstanceResponse;
}

export namespace SaveActiveSubstanceResponse {
  export type AsObject = {
    id: number,
  }
}

