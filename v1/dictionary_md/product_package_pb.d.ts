// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/product_package.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";

export class ProductPackage extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getProductId(): number;
  setProductId(value: number): void;

  getProductName(): string;
  setProductName(value: string): void;

  getAmount(): number;
  setAmount(value: number): void;

  getQuantityTypeId(): number;
  setQuantityTypeId(value: number): void;

  getQuantityTypeName(): string;
  setQuantityTypeName(value: string): void;

  getQuantityTypeShortName(): string;
  setQuantityTypeShortName(value: string): void;

  getType(): v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap];
  setType(value: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]): void;

  getBrandId(): number;
  setBrandId(value: number): void;

  getBrandName(): string;
  setBrandName(value: string): void;

  getLength(): number;
  setLength(value: number): void;

  getWidth(): number;
  setWidth(value: number): void;

  getHeight(): number;
  setHeight(value: number): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductPackage.AsObject;
  static toObject(includeInstance: boolean, msg: ProductPackage): ProductPackage.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductPackage, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductPackage;
  static deserializeBinaryFromReader(message: ProductPackage, reader: jspb.BinaryReader): ProductPackage;
}

export namespace ProductPackage {
  export type AsObject = {
    id: number,
    productId: number,
    productName: string,
    amount: number,
    quantityTypeId: number,
    quantityTypeName: string,
    quantityTypeShortName: string,
    type: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap],
    brandId: number,
    brandName: string,
    length: number,
    width: number,
    height: number,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class ProductPackageFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductId(): boolean;
  clearProductId(): void;
  getProductId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setProductId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasProductName(): boolean;
  clearProductName(): void;
  getProductName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setProductName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasQuantityTypeId(): boolean;
  clearQuantityTypeId(): void;
  getQuantityTypeId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setQuantityTypeId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasBrandId(): boolean;
  clearBrandId(): void;
  getBrandId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setBrandId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasBrandName(): boolean;
  clearBrandName(): void;
  getBrandName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setBrandName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasProductType(): boolean;
  clearProductType(): void;
  getProductType(): v1_dictionary_common_enum_product_pb.ProductTypeValue | undefined;
  setProductType(value?: v1_dictionary_common_enum_product_pb.ProductTypeValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ProductPackageFilter.AsObject;
  static toObject(includeInstance: boolean, msg: ProductPackageFilter): ProductPackageFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ProductPackageFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ProductPackageFilter;
  static deserializeBinaryFromReader(message: ProductPackageFilter, reader: jspb.BinaryReader): ProductPackageFilter;
}

export namespace ProductPackageFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    productName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    quantityTypeId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    brandId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    brandName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    productType?: v1_dictionary_common_enum_product_pb.ProductTypeValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetProductPackageRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductPackageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductPackageRequest): GetProductPackageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductPackageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductPackageRequest;
  static deserializeBinaryFromReader(message: GetProductPackageRequest, reader: jspb.BinaryReader): GetProductPackageRequest;
}

export namespace GetProductPackageRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetProductPackageResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductPackage | undefined;
  setItem(value?: ProductPackage): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetProductPackageResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetProductPackageResponse): GetProductPackageResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetProductPackageResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetProductPackageResponse;
  static deserializeBinaryFromReader(message: GetProductPackageResponse, reader: jspb.BinaryReader): GetProductPackageResponse;
}

export namespace GetProductPackageResponse {
  export type AsObject = {
    item?: ProductPackage.AsObject,
  }
}

export class ListProductPackageRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): ProductPackageFilter | undefined;
  setFilter(value?: ProductPackageFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductPackageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductPackageRequest): ListProductPackageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductPackageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductPackageRequest;
  static deserializeBinaryFromReader(message: ListProductPackageRequest, reader: jspb.BinaryReader): ListProductPackageRequest;
}

export namespace ListProductPackageRequest {
  export type AsObject = {
    filter?: ProductPackageFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListProductPackageResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<ProductPackage>;
  setItemsList(value: Array<ProductPackage>): void;
  addItems(value?: ProductPackage, index?: number): ProductPackage;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListProductPackageResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListProductPackageResponse): ListProductPackageResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListProductPackageResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListProductPackageResponse;
  static deserializeBinaryFromReader(message: ListProductPackageResponse, reader: jspb.BinaryReader): ListProductPackageResponse;
}

export namespace ListProductPackageResponse {
  export type AsObject = {
    itemsList: Array<ProductPackage.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveProductPackageRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): ProductPackage | undefined;
  setItem(value?: ProductPackage): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductPackageRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductPackageRequest): SaveProductPackageRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductPackageRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductPackageRequest;
  static deserializeBinaryFromReader(message: SaveProductPackageRequest, reader: jspb.BinaryReader): SaveProductPackageRequest;
}

export namespace SaveProductPackageRequest {
  export type AsObject = {
    item?: ProductPackage.AsObject,
  }
}

export class SaveProductPackageResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveProductPackageResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveProductPackageResponse): SaveProductPackageResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveProductPackageResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveProductPackageResponse;
  static deserializeBinaryFromReader(message: SaveProductPackageResponse, reader: jspb.BinaryReader): SaveProductPackageResponse;
}

export namespace SaveProductPackageResponse {
  export type AsObject = {
    id: number,
  }
}

