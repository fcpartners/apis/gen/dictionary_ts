// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/category.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";
import * as v1_dictionary_common_enum_product_pb from "../../v1/dictionary_common/enum_product_pb";

export class Category extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getProductProcess(): v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap];
  setProductProcess(value: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap]): void;

  getProductType(): v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap];
  setProductType(value: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap]): void;

  getCategoryGroupId(): number;
  setCategoryGroupId(value: number): void;

  getCategoryGroupName(): string;
  setCategoryGroupName(value: string): void;

  getIsGeneralCategory(): boolean;
  setIsGeneralCategory(value: boolean): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): Category.AsObject;
  static toObject(includeInstance: boolean, msg: Category): Category.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: Category, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): Category;
  static deserializeBinaryFromReader(message: Category, reader: jspb.BinaryReader): Category;
}

export namespace Category {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    productProcess: v1_dictionary_common_enum_product_pb.ProductProcessMap[keyof v1_dictionary_common_enum_product_pb.ProductProcessMap],
    productType: v1_dictionary_common_enum_product_pb.ProductTypeMap[keyof v1_dictionary_common_enum_product_pb.ProductTypeMap],
    categoryGroupId: number,
    categoryGroupName: string,
    isGeneralCategory: boolean,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class CategoryFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasProductProcess(): boolean;
  clearProductProcess(): void;
  getProductProcess(): v1_dictionary_common_enum_product_pb.ProductProcessValue | undefined;
  setProductProcess(value?: v1_dictionary_common_enum_product_pb.ProductProcessValue): void;

  hasProductType(): boolean;
  clearProductType(): void;
  getProductType(): v1_dictionary_common_enum_product_pb.ProductTypeValue | undefined;
  setProductType(value?: v1_dictionary_common_enum_product_pb.ProductTypeValue): void;

  hasCategoryGroupId(): boolean;
  clearCategoryGroupId(): void;
  getCategoryGroupId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setCategoryGroupId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasCategoryGroupName(): boolean;
  clearCategoryGroupName(): void;
  getCategoryGroupName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCategoryGroupName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasIsGeneralCategory(): boolean;
  clearIsGeneralCategory(): void;
  getIsGeneralCategory(): google_protobuf_wrappers_pb.BoolValue | undefined;
  setIsGeneralCategory(value?: google_protobuf_wrappers_pb.BoolValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CategoryFilter.AsObject;
  static toObject(includeInstance: boolean, msg: CategoryFilter): CategoryFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CategoryFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CategoryFilter;
  static deserializeBinaryFromReader(message: CategoryFilter, reader: jspb.BinaryReader): CategoryFilter;
}

export namespace CategoryFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    productProcess?: v1_dictionary_common_enum_product_pb.ProductProcessValue.AsObject,
    productType?: v1_dictionary_common_enum_product_pb.ProductTypeValue.AsObject,
    categoryGroupId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    categoryGroupName?: google_protobuf_wrappers_pb.StringValue.AsObject,
    isGeneralCategory?: google_protobuf_wrappers_pb.BoolValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetCategoryRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetCategoryRequest): GetCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCategoryRequest;
  static deserializeBinaryFromReader(message: GetCategoryRequest, reader: jspb.BinaryReader): GetCategoryRequest;
}

export namespace GetCategoryRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetCategoryResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Category | undefined;
  setItem(value?: Category): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCategoryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetCategoryResponse): GetCategoryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCategoryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCategoryResponse;
  static deserializeBinaryFromReader(message: GetCategoryResponse, reader: jspb.BinaryReader): GetCategoryResponse;
}

export namespace GetCategoryResponse {
  export type AsObject = {
    item?: Category.AsObject,
  }
}

export class ListCategoryRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): CategoryFilter | undefined;
  setFilter(value?: CategoryFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListCategoryRequest): ListCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCategoryRequest;
  static deserializeBinaryFromReader(message: ListCategoryRequest, reader: jspb.BinaryReader): ListCategoryRequest;
}

export namespace ListCategoryRequest {
  export type AsObject = {
    filter?: CategoryFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListCategoryResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<Category>;
  setItemsList(value: Array<Category>): void;
  addItems(value?: Category, index?: number): Category;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCategoryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListCategoryResponse): ListCategoryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCategoryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCategoryResponse;
  static deserializeBinaryFromReader(message: ListCategoryResponse, reader: jspb.BinaryReader): ListCategoryResponse;
}

export namespace ListCategoryResponse {
  export type AsObject = {
    itemsList: Array<Category.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveCategoryRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): Category | undefined;
  setItem(value?: Category): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCategoryRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCategoryRequest): SaveCategoryRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCategoryRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCategoryRequest;
  static deserializeBinaryFromReader(message: SaveCategoryRequest, reader: jspb.BinaryReader): SaveCategoryRequest;
}

export namespace SaveCategoryRequest {
  export type AsObject = {
    item?: Category.AsObject,
  }
}

export class SaveCategoryResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCategoryResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCategoryResponse): SaveCategoryResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCategoryResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCategoryResponse;
  static deserializeBinaryFromReader(message: SaveCategoryResponse, reader: jspb.BinaryReader): SaveCategoryResponse;
}

export namespace SaveCategoryResponse {
  export type AsObject = {
    id: number,
  }
}

