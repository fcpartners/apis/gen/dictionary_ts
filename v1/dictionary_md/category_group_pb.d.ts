// package: fcp.dictionary.v1.dictionary_md
// file: v1/dictionary_md/category_group.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_dictionary_common_pb from "../../v1/dictionary_common/dictionary_common_pb";

export class CategoryGroup extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  getExternalId(): number;
  setExternalId(value: number): void;

  getName(): string;
  setName(value: string): void;

  getDescription(): string;
  setDescription(value: string): void;

  hasAudit(): boolean;
  clearAudit(): void;
  getAudit(): v1_dictionary_common_dictionary_common_pb.Audit | undefined;
  setAudit(value?: v1_dictionary_common_dictionary_common_pb.Audit): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CategoryGroup.AsObject;
  static toObject(includeInstance: boolean, msg: CategoryGroup): CategoryGroup.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CategoryGroup, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CategoryGroup;
  static deserializeBinaryFromReader(message: CategoryGroup, reader: jspb.BinaryReader): CategoryGroup;
}

export namespace CategoryGroup {
  export type AsObject = {
    id: number,
    externalId: number,
    name: string,
    description: string,
    audit?: v1_dictionary_common_dictionary_common_pb.Audit.AsObject,
  }
}

export class CategoryGroupFilter extends jspb.Message {
  hasId(): boolean;
  clearId(): void;
  getId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasExternalId(): boolean;
  clearExternalId(): void;
  getExternalId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setExternalId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  hasName(): boolean;
  clearName(): void;
  getName(): google_protobuf_wrappers_pb.StringValue | undefined;
  setName(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasState(): boolean;
  clearState(): void;
  getState(): v1_dictionary_common_dictionary_common_pb.StateValue | undefined;
  setState(value?: v1_dictionary_common_dictionary_common_pb.StateValue): void;

  hasCreatedAt(): boolean;
  clearCreatedAt(): void;
  getCreatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setCreatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasCreatedBy(): boolean;
  clearCreatedBy(): void;
  getCreatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setCreatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  hasUpdatedAt(): boolean;
  clearUpdatedAt(): void;
  getUpdatedAt(): v1_dictionary_common_dictionary_common_pb.TimeRange | undefined;
  setUpdatedAt(value?: v1_dictionary_common_dictionary_common_pb.TimeRange): void;

  hasUpdatedBy(): boolean;
  clearUpdatedBy(): void;
  getUpdatedBy(): google_protobuf_wrappers_pb.StringValue | undefined;
  setUpdatedBy(value?: google_protobuf_wrappers_pb.StringValue): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): CategoryGroupFilter.AsObject;
  static toObject(includeInstance: boolean, msg: CategoryGroupFilter): CategoryGroupFilter.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: CategoryGroupFilter, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): CategoryGroupFilter;
  static deserializeBinaryFromReader(message: CategoryGroupFilter, reader: jspb.BinaryReader): CategoryGroupFilter;
}

export namespace CategoryGroupFilter {
  export type AsObject = {
    id?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    externalId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    name?: google_protobuf_wrappers_pb.StringValue.AsObject,
    state?: v1_dictionary_common_dictionary_common_pb.StateValue.AsObject,
    createdAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    createdBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
    updatedAt?: v1_dictionary_common_dictionary_common_pb.TimeRange.AsObject,
    updatedBy?: google_protobuf_wrappers_pb.StringValue.AsObject,
  }
}

export class GetCategoryGroupRequest extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCategoryGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetCategoryGroupRequest): GetCategoryGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCategoryGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCategoryGroupRequest;
  static deserializeBinaryFromReader(message: GetCategoryGroupRequest, reader: jspb.BinaryReader): GetCategoryGroupRequest;
}

export namespace GetCategoryGroupRequest {
  export type AsObject = {
    id: number,
  }
}

export class GetCategoryGroupResponse extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): CategoryGroup | undefined;
  setItem(value?: CategoryGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetCategoryGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetCategoryGroupResponse): GetCategoryGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetCategoryGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetCategoryGroupResponse;
  static deserializeBinaryFromReader(message: GetCategoryGroupResponse, reader: jspb.BinaryReader): GetCategoryGroupResponse;
}

export namespace GetCategoryGroupResponse {
  export type AsObject = {
    item?: CategoryGroup.AsObject,
  }
}

export class ListCategoryGroupRequest extends jspb.Message {
  hasFilter(): boolean;
  clearFilter(): void;
  getFilter(): CategoryGroupFilter | undefined;
  setFilter(value?: CategoryGroupFilter): void;

  clearSortingList(): void;
  getSortingList(): Array<v1_dictionary_common_dictionary_common_pb.Sorting>;
  setSortingList(value: Array<v1_dictionary_common_dictionary_common_pb.Sorting>): void;
  addSorting(value?: v1_dictionary_common_dictionary_common_pb.Sorting, index?: number): v1_dictionary_common_dictionary_common_pb.Sorting;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationRequest | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationRequest): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCategoryGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListCategoryGroupRequest): ListCategoryGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCategoryGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCategoryGroupRequest;
  static deserializeBinaryFromReader(message: ListCategoryGroupRequest, reader: jspb.BinaryReader): ListCategoryGroupRequest;
}

export namespace ListCategoryGroupRequest {
  export type AsObject = {
    filter?: CategoryGroupFilter.AsObject,
    sortingList: Array<v1_dictionary_common_dictionary_common_pb.Sorting.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationRequest.AsObject,
  }
}

export class ListCategoryGroupResponse extends jspb.Message {
  clearItemsList(): void;
  getItemsList(): Array<CategoryGroup>;
  setItemsList(value: Array<CategoryGroup>): void;
  addItems(value?: CategoryGroup, index?: number): CategoryGroup;

  hasPagination(): boolean;
  clearPagination(): void;
  getPagination(): v1_dictionary_common_dictionary_common_pb.PaginationResponse | undefined;
  setPagination(value?: v1_dictionary_common_dictionary_common_pb.PaginationResponse): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListCategoryGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListCategoryGroupResponse): ListCategoryGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListCategoryGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListCategoryGroupResponse;
  static deserializeBinaryFromReader(message: ListCategoryGroupResponse, reader: jspb.BinaryReader): ListCategoryGroupResponse;
}

export namespace ListCategoryGroupResponse {
  export type AsObject = {
    itemsList: Array<CategoryGroup.AsObject>,
    pagination?: v1_dictionary_common_dictionary_common_pb.PaginationResponse.AsObject,
  }
}

export class SaveCategoryGroupRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): CategoryGroup | undefined;
  setItem(value?: CategoryGroup): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCategoryGroupRequest.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCategoryGroupRequest): SaveCategoryGroupRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCategoryGroupRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCategoryGroupRequest;
  static deserializeBinaryFromReader(message: SaveCategoryGroupRequest, reader: jspb.BinaryReader): SaveCategoryGroupRequest;
}

export namespace SaveCategoryGroupRequest {
  export type AsObject = {
    item?: CategoryGroup.AsObject,
  }
}

export class SaveCategoryGroupResponse extends jspb.Message {
  getId(): number;
  setId(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): SaveCategoryGroupResponse.AsObject;
  static toObject(includeInstance: boolean, msg: SaveCategoryGroupResponse): SaveCategoryGroupResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: SaveCategoryGroupResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): SaveCategoryGroupResponse;
  static deserializeBinaryFromReader(message: SaveCategoryGroupResponse, reader: jspb.BinaryReader): SaveCategoryGroupResponse;
}

export namespace SaveCategoryGroupResponse {
  export type AsObject = {
    id: number,
  }
}

