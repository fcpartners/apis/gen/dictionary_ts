// package: fcp.dictionary.v1.dictionary_mobile
// file: v1/dictionary_mobile/database.proto

import * as jspb from "google-protobuf";
import * as google_protobuf_empty_pb from "google-protobuf/google/protobuf/empty_pb";
import * as google_protobuf_wrappers_pb from "google-protobuf/google/protobuf/wrappers_pb";
import * as google_protobuf_timestamp_pb from "google-protobuf/google/protobuf/timestamp_pb";
import * as v1_dictionary_common_file_pb from "../../v1/dictionary_common/file_pb";

export class GetDbVersionRequest extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDbVersionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: GetDbVersionRequest): GetDbVersionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDbVersionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDbVersionRequest;
  static deserializeBinaryFromReader(message: GetDbVersionRequest, reader: jspb.BinaryReader): GetDbVersionRequest;
}

export namespace GetDbVersionRequest {
  export type AsObject = {
  }
}

export class GetDbVersionResponse extends jspb.Message {
  getVersion(): string;
  setVersion(value: string): void;

  getId(): number;
  setId(value: number): void;

  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_dictionary_common_file_pb.DbSnapshot | undefined;
  setItem(value?: v1_dictionary_common_file_pb.DbSnapshot): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): GetDbVersionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: GetDbVersionResponse): GetDbVersionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: GetDbVersionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): GetDbVersionResponse;
  static deserializeBinaryFromReader(message: GetDbVersionResponse, reader: jspb.BinaryReader): GetDbVersionResponse;
}

export namespace GetDbVersionResponse {
  export type AsObject = {
    version: string,
    id: number,
    item?: v1_dictionary_common_file_pb.DbSnapshot.AsObject,
  }
}

export class ListDbVersionRequest extends jspb.Message {
  hasItem(): boolean;
  clearItem(): void;
  getItem(): v1_dictionary_common_file_pb.DbSnapshot | undefined;
  setItem(value?: v1_dictionary_common_file_pb.DbSnapshot): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDbVersionRequest.AsObject;
  static toObject(includeInstance: boolean, msg: ListDbVersionRequest): ListDbVersionRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDbVersionRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDbVersionRequest;
  static deserializeBinaryFromReader(message: ListDbVersionRequest, reader: jspb.BinaryReader): ListDbVersionRequest;
}

export namespace ListDbVersionRequest {
  export type AsObject = {
    item?: v1_dictionary_common_file_pb.DbSnapshot.AsObject,
  }
}

export class ListDbVersionResponse extends jspb.Message {
  clearItemList(): void;
  getItemList(): Array<v1_dictionary_common_file_pb.DbSnapshot>;
  setItemList(value: Array<v1_dictionary_common_file_pb.DbSnapshot>): void;
  addItem(value?: v1_dictionary_common_file_pb.DbSnapshot, index?: number): v1_dictionary_common_file_pb.DbSnapshot;

  getRecommendedDbUpdate(): ListDbVersionResponse.RecommendedDbUpdateMap[keyof ListDbVersionResponse.RecommendedDbUpdateMap];
  setRecommendedDbUpdate(value: ListDbVersionResponse.RecommendedDbUpdateMap[keyof ListDbVersionResponse.RecommendedDbUpdateMap]): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): ListDbVersionResponse.AsObject;
  static toObject(includeInstance: boolean, msg: ListDbVersionResponse): ListDbVersionResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: ListDbVersionResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): ListDbVersionResponse;
  static deserializeBinaryFromReader(message: ListDbVersionResponse, reader: jspb.BinaryReader): ListDbVersionResponse;
}

export namespace ListDbVersionResponse {
  export type AsObject = {
    itemList: Array<v1_dictionary_common_file_pb.DbSnapshot.AsObject>,
    recommendedDbUpdate: ListDbVersionResponse.RecommendedDbUpdateMap[keyof ListDbVersionResponse.RecommendedDbUpdateMap],
  }

  export interface RecommendedDbUpdateMap {
    PARTIAL: 0;
    FULL: 1;
  }

  export const RecommendedDbUpdate: RecommendedDbUpdateMap;
}

export class DownloadDbRequest extends jspb.Message {
  getFileVersion(): string;
  setFileVersion(value: string): void;

  hasFileId(): boolean;
  clearFileId(): void;
  getFileId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFileId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  getFilePart(): number;
  setFilePart(value: number): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DownloadDbRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DownloadDbRequest): DownloadDbRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DownloadDbRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DownloadDbRequest;
  static deserializeBinaryFromReader(message: DownloadDbRequest, reader: jspb.BinaryReader): DownloadDbRequest;
}

export namespace DownloadDbRequest {
  export type AsObject = {
    fileVersion: string,
    fileId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
    filePart: number,
  }
}

export class DownloadDbResponse extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_dictionary_common_file_pb.File | undefined;
  setFile(value?: v1_dictionary_common_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DownloadDbResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DownloadDbResponse): DownloadDbResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DownloadDbResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DownloadDbResponse;
  static deserializeBinaryFromReader(message: DownloadDbResponse, reader: jspb.BinaryReader): DownloadDbResponse;
}

export namespace DownloadDbResponse {
  export type AsObject = {
    file?: v1_dictionary_common_file_pb.File.AsObject,
  }
}

export class DownloadDbSnapshotRequest extends jspb.Message {
  getFileVersion(): string;
  setFileVersion(value: string): void;

  getFilePart(): number;
  setFilePart(value: number): void;

  hasFileId(): boolean;
  clearFileId(): void;
  getFileId(): google_protobuf_wrappers_pb.Int64Value | undefined;
  setFileId(value?: google_protobuf_wrappers_pb.Int64Value): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DownloadDbSnapshotRequest.AsObject;
  static toObject(includeInstance: boolean, msg: DownloadDbSnapshotRequest): DownloadDbSnapshotRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DownloadDbSnapshotRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DownloadDbSnapshotRequest;
  static deserializeBinaryFromReader(message: DownloadDbSnapshotRequest, reader: jspb.BinaryReader): DownloadDbSnapshotRequest;
}

export namespace DownloadDbSnapshotRequest {
  export type AsObject = {
    fileVersion: string,
    filePart: number,
    fileId?: google_protobuf_wrappers_pb.Int64Value.AsObject,
  }
}

export class DownloadDbSnapshotResponse extends jspb.Message {
  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_dictionary_common_file_pb.File | undefined;
  setFile(value?: v1_dictionary_common_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): DownloadDbSnapshotResponse.AsObject;
  static toObject(includeInstance: boolean, msg: DownloadDbSnapshotResponse): DownloadDbSnapshotResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: DownloadDbSnapshotResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): DownloadDbSnapshotResponse;
  static deserializeBinaryFromReader(message: DownloadDbSnapshotResponse, reader: jspb.BinaryReader): DownloadDbSnapshotResponse;
}

export namespace DownloadDbSnapshotResponse {
  export type AsObject = {
    file?: v1_dictionary_common_file_pb.File.AsObject,
  }
}

export class UploadDbRequest extends jspb.Message {
  getVersion(): string;
  setVersion(value: string): void;

  hasFile(): boolean;
  clearFile(): void;
  getFile(): v1_dictionary_common_file_pb.File | undefined;
  setFile(value?: v1_dictionary_common_file_pb.File): void;

  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UploadDbRequest.AsObject;
  static toObject(includeInstance: boolean, msg: UploadDbRequest): UploadDbRequest.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UploadDbRequest, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UploadDbRequest;
  static deserializeBinaryFromReader(message: UploadDbRequest, reader: jspb.BinaryReader): UploadDbRequest;
}

export namespace UploadDbRequest {
  export type AsObject = {
    version: string,
    file?: v1_dictionary_common_file_pb.File.AsObject,
  }
}

export class UploadDbResponse extends jspb.Message {
  serializeBinary(): Uint8Array;
  toObject(includeInstance?: boolean): UploadDbResponse.AsObject;
  static toObject(includeInstance: boolean, msg: UploadDbResponse): UploadDbResponse.AsObject;
  static extensions: {[key: number]: jspb.ExtensionFieldInfo<jspb.Message>};
  static extensionsBinary: {[key: number]: jspb.ExtensionFieldBinaryInfo<jspb.Message>};
  static serializeBinaryToWriter(message: UploadDbResponse, writer: jspb.BinaryWriter): void;
  static deserializeBinary(bytes: Uint8Array): UploadDbResponse;
  static deserializeBinaryFromReader(message: UploadDbResponse, reader: jspb.BinaryReader): UploadDbResponse;
}

export namespace UploadDbResponse {
  export type AsObject = {
  }
}

